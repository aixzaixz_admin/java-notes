## 基础知识

### 	IO

Java 中 IO 流分为几种?

- 按照流的流向分，可以分为输入流和输出流；
- 按照操作单元划分，可以划分为字节流和字符流；
- 按照流的角色划分为节点流和处理流。

Java IO 流共涉及 40 多个类，这些类看上去很杂乱，但实际上很有规则，而且彼此之间存在非常紧密的联系， Java IO 流的 40 多个类都是从如下 4 个抽象类基类中派生出来的。

- InputStream/Reader: 所有的输入流的基类，前者是字节输入流，后者是字符输入流。
- OutputStream/Writer: 所有输出流的基类，前者是字节输出流，后者是字符输出流。

按操作方式分类结构图：

![IO-操作方式分类](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141500820.png)

按操作对象分类结构图：

![IO-操作对象分类](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141500760.png)



### 正则表达式

正则表达式定义了[字符串](https://so.csdn.net/so/search?q=字符串&spm=1001.2101.3001.7020)的模式。

正则表达式可以用来搜索、编辑或处理文本。

正则表达式并不仅限于某一种语言，但是在每种语言中有细微的差别。

一个字符串其实就是一个简单的正则表达式，例如 Hello World [正则表达式匹配](https://so.csdn.net/so/search?q=正则表达式匹配&spm=1001.2101.3001.7020) “Hello World” 字符串。
.（点号）也是一个正则表达式，它匹配任何一个字符如：“a” 或 “1”。

Java 正则表达式和 Perl 的是最为相似的。

java.util.regex 包主要包括以下三个类：

- Pattern 类：
  pattern 对象是一个正则表达式的编译表示。Pattern 类没有公共构造方法。要创建一个 Pattern 对象，你必须首先调用其公共静态编译方法，它返回一个 Pattern 对象。该方法接受一个正则表达式作为它的第一个参数。
- Matcher 类：
  Matcher 对象是对输入字符串进行解释和匹配操作的引擎。与Pattern 类一样，Matcher 也没有公共构造方法。你需要调用 Pattern 对象的 matcher 方法来获得一个 Matcher 对象。



#### java Pattern和Matcher 常用使用示例

浅谈Java Matcher对象中find()与matches()的区别
参考URL: https://zhuanlan.zhihu.com/p/142846161

java正则表达式通过java.util.regex包下的Pattern类与Matcher类实现。

Pattern类用于创建一个正则表达式,也可以说创建一个匹配模式,它的构造方法是私有的,不可以直接创建,但可以通过Pattern.complie(String regex)简单工厂方法创建一个正则表达式,

```java
Pattern p=Pattern.compile("\\w+"); 
p.pattern();//返回 \w+ 
12
```

pattern() 返回正则表达式的字符串形式,其实就是返回Pattern.complile(String regex)的regex参数

```java
Pattern.matches("\\d+","2223");//返回true 
Pattern.matches("\\d+","2223aa");//返回false,需要匹配到所有字符串才能返回true,这里aa不能匹配到 
Pattern.matches("\\d+","22bb23");//返回false,需要匹配到所有字符串才能返回true,这里bb不能匹配到 
123
```

> Pattern类用于创建一个正则表达式,也可以说创建一个匹配模式,它的构造方法是私有的,不可以直接创建,但可以通过Pattern.complie(String regex)简单工厂方法创建一个正则表达式。

Pattern.matcher(CharSequence input)返回一个Matcher对象.
Matcher类的构造方法也是私有的,不能随意创建,只能通过Pattern.matcher(CharSequence input)方法得到该类的实例。

```java
Pattern p=Pattern.compile("\d+");
Matcher m=p.matcher("22bb23");
```

m.pattern();//返回p 也就是返回该Matcher对象是由哪个Pattern对象的创建的。

Matcher 实例有一些常用方法。

```java
 Pattern p1 = Pattern.compile("[A-Z]");
 p1.matcher(str).find()
```

find()：是否存在与该模式匹配的下一个子序列。简单来说就是在字符某部分匹配上模式就会返回true，同时匹配位置会记录到当前位置，再次调用时从该处匹配下一个。

matches()：整个字符串是否匹配上模式，匹配上则返回true，否则false。

```java
@Test
public void patternTest() {
	String str = "hellohellohello";
	String regex = "hello";
	Pattern pattern = Pattern.compile(regex);
	Matcher matcher = pattern.matcher(str);
	System.out.println(matcher.find());
	System.out.println(matcher.matches());
}

输出结果为：
find() -> true
matches() -> false
12345678910111213
```

Matcher中的start()和end()。start(),点进方法可以看到返回的是上一个匹配项的起始索引，如果没有匹配项将抛出IllegalStateException异常。同理，end()则为结束的索引。

```java
@Test
public void patternTest() {
    String str = "hellohellohello";
    String regex = "hello";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(str);
    while (matcher.find()) {
      System.out.println(matcher.start() + "->" + matcher.end());
    }
}
输出：
0->5
5->10
10->15
```



#### java替换字符串（添加前后缀）

```java
    /**
     * 正则表达式定义
     */;
    String regxOne="(?!(where).{1,999})[a-z]{1,20}(.)(owner_name|id_no|id_no2)(?=(\\s{0,10}\\=))";

    /**
     * 编译正则式
     */
    Pattern patternOne = Pattern.compile(regxOne,Pattern.CASE_INSENSITIVE|Pattern.MULTILINE|Pattern.DOTALL);

    /**
     * 包裹字符
     */
    String start = "RERS_YZB_DECRYPT(";
    String end = ")";

//字符串缓冲区
StringBuffer buffer = new StringBuffer(sql.length());

Matcher matcher = patternOne.matcher(sql);
while (matcher.find()) {
    //当前匹配到的字符串
    String group = matcher.group();
    //替换匹配到的字符串
    matcher.appendReplacement(buffer,start+group+end);
}
//附加没替换的字符串
matcher.appendTail(buffer);

newSql = buffer.toString();		
```



#### 匹配中文

```js
[\u4e00-\u9fa5]
 [一-龥]
 [一-龟]
```

## 常用代码

### **判断集合是否为空/不为空**

**org.apache.commons.collections.CollectionUtils**

**例1: 判断集合是否为空:**

```java
　　CollectionUtils.isEmpty(null): true
　　CollectionUtils.isEmpty(new ArrayList()): true　　
　　CollectionUtils.isEmpty({a,b}): false
```

**例2: 判断集合是否不为空:**

```java
　　CollectionUtils.isNotEmpty(null): false
　　CollectionUtils.isNotEmpty(new ArrayList()): false
　　CollectionUtils.isNotEmpty({a,b}): true
```

### 获取Oracle 返回的 BigDecimal类型

```java
 //设备总数
            BigDecimal warningNum = new BigDecimal(item.get("warningNum") == null ? "0" : item.get("warningNum").toString());
            //正常设备数
            BigDecimal normalNum = new BigDecimal(item.get("normalNum") == null ? "0" : item.get("normalNum").toString());
           
```

### Bigdecimal类型相除并保留两位小数返回百分比

```java
//计算设备正常率如果不能整除保留6位小数并四舍五入
            BigDecimal rate = normalNum.divide(warningNum,6,RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            //计算结果四舍五入保留两位小数拼接%
            String normalRate = rate.setScale(2, RoundingMode.HALF_UP).toPlainString() + "%";
```



### 判断Bigdecimal类型是否等于0的方法

**判断相等**

```java
compareTo
```

Bigdecimal的equals方法不仅仅比较值的大小是否相等，首先比较的是scale（scale是bigdecimal的保留小数点位数，比如 new Bigdecimal("1.001"),scale为3），也就是说，不但值得大小要相等，保留位数也要相等，equals才能返回true。

Bigdecimal b = new Bigdecimal("0") 和 Bigdecimal c = new Bigdecimal("0.0"),用equals比较，返回就是false。

Bigdecimal.ZERO的scale为0。

所以，用equals方法要注意这一点。

```java
b.compareTo(BigDecimal.ZERO)==0
```

可以比较是否等于0，返回true则等于0，返回false，则不等于0

### Lambda forEach 关于 return 的使用

> JDK8 中新增的 Lambda [表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)对于 for 循环的操作变得非常简洁
> 但其中的 [forEach](https://so.csdn.net/so/search?q=forEach&spm=1001.2101.3001.7020) 和 for 之间存在一定差异
> 比如 forEach 无法使用 break 和 continue

```java

```

#### forEach 实现和 contiune 一样的效果

1. 参见以下代码可知，在 **forEach** 中 `return` 可实现和 `contiune` 一样的效果

```java
int[] arrs = new int[]{1, 3, 9, 2};

arrs.forEach(arr -> {
    if (arr > 4) {
        return;
    }

    // 输出 1 3 2
    System.out.println(arr);
})
```



###  list使用[stream](https://so.csdn.net/so/search?q=stream&spm=1001.2101.3001.7020)排序(多字段)

```java
List<类> list; 代表某集合
 
//返回 对象集合以类属性一升序排序
 
list.stream().sorted(Comparator.comparing(类::属性一));
 
//返回 对象集合以类属性一降序排序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed());//先以属性一升序,结果进行属性一降序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()));//以属性一降序
 
//返回 对象集合以类属性一升序 属性二升序
 
list.stream().sorted(Comparator.comparing(类::属性一).thenComparing(类::属性二));
 
//返回 对象集合以类属性一降序 属性二升序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二));//先以属性一升序,升序结果进行属性一降序,再进行属性二升序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()).thenComparing(类::属性二));//先以属性一降序,再进行属性二升序
 
//返回 对象集合以类属性一降序 属性二降序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一升序,升序结果进行属性一降序,再进行属性二降序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()).thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一降序,再进行属性二降序
 
//返回 对象集合以类属性一升序 属性二降序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二).reversed());//先以属性一升序,升序结果进行属性一降序,再进行属性二升序,结果进行属性一降序属性二降序
 
list.stream().sorted(Comparator.comparing(类::属性一).thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一升序,再进行属性二降序<br><br><br>
```

通过以上例子我们可以发现

> 1. Comparator.comparing(类::属性一).reversed();
>
> 2. Comparator.comparing(类::属性一,Comparator.reverseOrder());
>
>    两种排序是完全不一样的,一定要区分开来 1 是得到排序结果后再排序,2是直接进行排序,很多人会混淆导致理解出错,2更好理解,建议使用2

**实际例子:
现有一个类test 有两个属性:state 状态 time 时间,需要状态顺序且时间倒序**

```java
class test {
    //状态
    private int state;
    //时间
    private Date time;
 
    public test(int state, Date time) {
        this.state = state;
        this.time = time;
    }
 
    public int getState() {
        return state;
    }
 
    public void setState(int state) {
        this.state = state;
    }
 
    public Date getTime() {
        return time;
    }
 
    public void setTime(Date time) {
        this.time = time;
    }
 
    @Override
    public String toString() {
        return "test{" +
                "state=" + state +
                ", time=" + DateUtils.formatDateYMD(time) +
                '}';
    }
}
class testRun {
    public static void main(String[] args) {
        List<test> testList = new ArrayList<>();
        Date d = DateUtils.now();
        for (int i = 1; i <= 3; i++) {
            test t = new test(i, DateUtils.addDays(d, i));
            testList.add(t);
        }
        for (int i = 1; i <= 3; i++) {
            test t = new test(i, DateUtils.addMonths(d, i));
            testList.add(t);
        }
 
        testList.forEach(o -> {
            System.out.println(o.toString());
        });
        List<test> sort = testList.stream().sorted(Comparator.comparing(test::getState).thenComparing(test::getTime,Comparator.reverseOrder())).collect(toList());
        System.out.println("------------------------------------");
        sort.forEach(o -> {
            System.out.println(o.toString());
        });
 
 
    }
}
```

运行结果:

```java
排序前:
test{state=1, time=2019-07-24}
test{state=2, time=2019-07-25}
test{state=3, time=2019-07-26}
test{state=1, time=2019-08-23}
test{state=2, time=2019-09-23}
test{state=3, time=2019-10-23}
------------------------------------
排序后:
test{state=1, time=2019-08-23}
test{state=1, time=2019-07-24}
test{state=2, time=2019-09-23}
test{state=2, time=2019-07-25}
test{state=3, time=2019-10-23}
test{state=3, time=2019-07-26}
 
Process finished with exit code 0
```



### 数组去重

```java
 public  String[] dealContains(String[] arrays) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < arrays.length; i++) {
            String array = arrays[i];
            if (!arrayList.contains(array)) {
                arrayList.add(array);
            }
        }
        String[] b = (String[]) arrayList.toArray(new String[arrayList.size()]);
        return b;
    }
```



### Base64编码和解码

**在Java 中， 使用Base64进行编码和解码的方式常见的有三种：**

- [推荐] 从Java 8 开始， Base64 就成为Java类库的标准，位于java.util 的包下。对应类是Base64。
- Java 7及之前的版本，JDK默认没有支持Base64, 需要导入JRE 目录的lib 下的文件 rt.jar， 通过sun.misc.BASE64Encoder 和sun.misc.BASE64Decoder 进行加解密。（完整的路径%JAVA_HOME%\jre\lib\rt.jar）。 在JDK８中依旧可以使用这个方法。
- Apache Commons Codec， 这是Apache 提供的常见编码器和解码器的实现，包括： Base64、Hex、Phonetic 和 URL。

#### Java 8 内置Base 64 编码器、解码器

Java 8 中通过java.util.Base64 提供的方法进行Base 64的编码和解码。

- 这里以“123” 的编码为例， 编码后的字符串是 “MTIz”。

```java
	@Test
	public void jdk8() {
		// 编码
		String encodedStr = java.util.Base64.getEncoder().encodeToString("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");

		// 解码
		byte[] decodeBytes = java.util.Base64.getDecoder().decode("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");
	}
```

#### Java 7使用rt.jar　中的相关类进行编码和解码

rt.jar　中用于处理的类是 sun.misc.BASE64Encoder 和sun.misc.BASE64Decoder 。通过创建这两个类的实例后，调用encode() 和 decodeBuffer() 方法。 示例代码如下：

```java
	@Test
	public void jdk7() throws IOException {
		String encodedStr =  new sun.misc.BASE64Encoder().encode("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");
		
		byte[] decodeBytes =  new sun.misc.BASE64Decoder().decodeBuffer("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");

	}
```

#### [Apache](https://so.csdn.net/so/search?q=Apache&spm=1001.2101.3001.7020) Commons Codec 实现Base64 的编码和解码

Apache Commons Codec 的官方地址是：
https://commons.apache.org/proper/commons-codec/。
Apache Commons Codec 用来处理Base64的类是 org.apache.commons.codec.binary.Base64。

```java
	@Test
	public void apacheCodec() {
		String encodedStr = org.apache.commons.codec.binary.Base64.encodeBase64String("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");
		
		byte[] decodeBytes =   org.apache.commons.codec.binary.Base64.decodeBase64("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");	
	}
```





### 生成文件的md5校验值

```java
import java.io.File; 
import java.io.FileInputStream; 
import java.io.IOException; 
import java.io.InputStream; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 
import lombok.extern.slf4j.Slf4j; 
/** 
 * author yvioo 
 */ 
@Slf4j 
public class MD5Util { 
     /** 
      * 生成文件的md5校验值 
      *  
      * @param path 文件路径 
      * @return 文件md5校验值 
      * @throws IOException 
     * @throws NoSuchAlgorithmException  
      */ 
    public static String getFileMD5String(String path){ 
        InputStream fis = null; 
        MessageDigest messagedigest = null; 
        try { 
            messagedigest = MessageDigest.getInstance("md5"); 
            File file = new File(path); 
            if(!file.exists() || !file.isFile()) { 
                log.error("不存在或不是一个文件"); 
                return ""; 
            } 
             
            fis = new FileInputStream(file); 
            byte[] buffer = new byte[1024]; 
            int numRead = 0; 
            while ((numRead = fis.read(buffer)) > 0) { 
               messagedigest.update(buffer, 0, numRead); 
            } 
        }catch (IOException e) { 
            e.printStackTrace(); 
        }catch (NoSuchAlgorithmException e){ 
            e.printStackTrace(); 
        }finally { 
            try { 
                if(fis != null) { 
                    fis.close(); 
                } 
            } catch (IOException e) { 
                e.printStackTrace(); 
            } 
        } 
        return bufferToHex(messagedigest.digest()); 
    } 
    private static String bufferToHex(byte bytes[]) { 
        return bufferToHex(bytes, 0, bytes.length); 
    } 
    private static String bufferToHex(byte bytes[], int m, int n) { 
        StringBuffer stringbuffer = new StringBuffer(2 * n); 
        int k = m + n; 
        for (int l = m; l < k; l++) { 
         appendHexPair(bytes[l], stringbuffer); 
        } 
        return stringbuffer.toString(); 
    } 
    /** 
     * 默认的密码字符串组合，用来将字节转换成 16 进制表示的字符,apache校验下载的文件的正确性用的就是默认的这个组合 
     */ 
    protected static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }; 
    private static void appendHexPair(byte bt, StringBuffer stringbuffer) { 
     char c0 = hexDigits[(bt & 0xf0) >> 4];// 取字节中高 4 位的数字转换, >>> 为逻辑右移，将符号位一起右移,此处未发现两种符号有何不同  
     char c1 = hexDigits[bt & 0xf];// 取字节中低 4 位的数字转换  
     stringbuffer.append(c0); 
     stringbuffer.append(c1); 
    } 
}
```



### 文件和Base64、二进制（byte[]）互转

#### 导入包

```java
//导入spring工具包，如果使用spring框架（我想大概率会使用）直接使用FileCopyUtils.java
import org.springframework.util.FileCopyUtils;
//导入IO包
import java.io.*;
//导入base64包
import java.util.Base64;
```

#### 方法干货

在IO操作中关于关流这块，尽量使用JDK7的特性，将涉及到关流的对象放到try(...)里面，就可以不用写finally块了。

```java
/**
     * 文件转二进制
     * @param filePath
     * @return
     */
    public static byte[] file2Bytes(String filePath){
        File file = new File(filePath);
        if (!file.exists())return new byte[0];
        try(InputStream inputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ) {
            byte[] buf = new byte[1024];
            int len;
            if ((len=bufferedInputStream.read(buf))>=0){
                outputStream.write(buf,0,len);
            }
            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }


	/**
	 * 将文件转换成二进制数组
	 * @param filePath 文件路基
	 * @return 二进制数组
	 */
	public static  byte[] fileToByte(String filePath){
		File file = new File(filePath);
		try (InputStream in = new FileInputStream(file)){
			if (!file.exists()){
				return new byte[0];
			}
			return FileCopyUtils.copyToByteArray(file);
		}catch (Exception ignored){
			throw new RuntimeException("文件转换二进制数组出错!",ignored);
		}
	}

 
    /**
     * 将二进制流转换为文件
     * @param bytes
     * @return
     */
    public static String bytes2File(byte[] bytes,String fileName){
        File file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (FileOutputStream outputStream = new FileOutputStream(fileName);){
            outputStream.write(bytes);
            return fileName;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
 
    /**
     * 将二进制流转换为文件
     * @param bytes
     * @param fileName
     */
    public static void bytes2FileSpring(byte[] bytes,String fileName){
        try {
            FileCopyUtils.copy(bytes,new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    /**
     * 将文件转base64
     * @param filePath 文件路径
     */
    public static String fileToBase64(String filePath){
        try( InputStream fileInputStream = new FileInputStream(new File(filePath));
             BufferedInputStream buf = new BufferedInputStream(fileInputStream);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();){
            int len;
            byte [] bytes = new byte[1024];
            while ((len=buf.read(bytes))!=-1){
                byteArrayOutputStream.write(bytes,0,len);
            }
            return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 将base64字符串转文件
     * @param base64Str base64字符串
     */
    public static void base64ToFile(String base64Str,String fileName){
        //解码
        byte [] bytes = Base64.getDecoder().decode(base64Str);
        try (FileOutputStream outputStream = new FileOutputStream(fileName);){
            outputStream.write(bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
```

#### base64码表

base64码表，我觉得作为一个程序猿，尤其是多年经验的程序猿，建议背会，像背诵当年的化学元素周期表一样，况且只有64个：

| Base64   |              |          |              |          |              |          |              |
| -------- | ------------ | -------- | ------------ | -------- | ------------ | -------- | ------------ |
| **索引** | **对应字符** | **索引** | **对应字符** | **索引** | **对应字符** | **索引** | **对应字符** |
| 0        | **A**        | 17       | **R**        | 34       | **i**        | 51       | **z**        |
| 1        | **B**        | 18       | **S**        | 35       | **j**        | 52       | **0**        |
| 2        | **C**        | 19       | **T**        | 36       | **k**        | 53       | **1**        |
| 3        | **D**        | 20       | **U**        | 37       | **l**        | 54       | **2**        |
| 4        | **E**        | 21       | **V**        | 38       | **m**        | 55       | **3**        |
| 5        | **F**        | 22       | **W**        | 39       | **n**        | 56       | **4**        |
| 6        | **G**        | 23       | **X**        | 40       | **o**        | 57       | **5**        |
| 7        | **H**        | 24       | **Y**        | 41       | **p**        | 58       | **6**        |
| 8        | **I**        | 25       | **Z**        | 42       | **q**        | 59       | **7**        |
| 9        | **J**        | 26       | **a**        | 43       | **r**        | 60       | **8**        |
| 10       | **K**        | 27       | **b**        | 44       | **s**        | 61       | **9**        |
| 11       | **L**        | 28       | **c**        | 45       | **t**        | 62       | **+**        |
| 12       | **M**        | 29       | **d**        | 46       | **u**        | 63       | **/**        |
| 13       | **N**        | 30       | **e**        | 47       | **v**        |          |              |
| 14       | **O**        | 31       | **f**        | 48       | **w**        |          |              |
| 15       | **P**        | 32       | **g**        | 49       | **x**        |          |              |
| 16       | **Q**        | 33       | **h**        | 50       | **y**        |          |              |



### FastJson 解析指定泛型

```java
# 注意这里新建了一个内部类,区别在于最后有一对大括号
Map map = JSON.parseObject(str, new TypeReference<Map<String,String>>(){});
```



### Java中删除文件或文件夹的几种方法

#### 删除文件或文件夹的四种基础方法

下面的四个方法都可以删除文件或文件夹。
它们的共同点是：
**当文件夹中包含子文件的时候都会删除失败，也就是说这四个方法只能删除空文件夹。**

```java
//delete是立即执行删除，而deleteOnExit是程序退出虚拟机时才会删除。
File类的delete()
File类的deleteOnExit()：当虚拟机终止时，删除File对象表示的文件或目录，如果表示的是目录，需要保证目录是空的，否则无法删除，无返回值。
Files.delete(Path path)：删除位于作为参数传递的路径上的文件。对于其他文件系统操作，此方法可能不是原子的。如果文件是符号链接，则将删除符号链接本身而不是链接的最终目标。如果文件是目录，则此方法仅在目录为空时才删除该文件。
Files.deleteIfExists(Path path)
```

需要注意的是：

> 传统IO中的File类和NIO中的Path类既可以代表文件，也可以代表文件夹。

##### 上面的四个方法简单对比

| -                               | 说明                       | 成功的返回值 | 是否能判别文件夹不存在导致失败 | 是否能判别文件夹不为空导致失败 |
| ------------------------------- | -------------------------- | ------------ | ------------------------------ | ------------------------------ |
| File类的delete()                | 传统IO                     | true         | 不能(返回false)                | 不能(返回false)                |
| File类的deleteOnExit()          | 传统IO，这是个坑，避免使用 | void         | 不能，但不存在就不会去执行删除 | 不能(返回void)                 |
| Files.delete(Path path)         | NIO，推荐使用              | void         | NoSuchFileException            | DirectoryNotEmptyException     |
| Files.deleteIfExists(Path path) | NIO                        | true         | false                          | DirectoryNotEmptyException     |

###### File.delete()和Files.delete(Path path)对比

```java
//删除暂存的pdf
File file =new File(pdfFilename);
file.delete();

Path path2 = Paths.get(pdfFilename);
Files.delete(path2);
```

区别：

| -                    | -File.delete()             | Files.delete(Path path)                             |
| -------------------- | -------------------------- | --------------------------------------------------- |
| JDK                  | JDK1.0                     | JDK1.7                                              |
| 来源                 | java.io.File对象的实例方法 | java.nio.file.Files类的静态方法                     |
| 参数                 | 无参                       | java.nio.file.Path                                  |
| 返回值               | boolean                    | void                                                |
| 异常声明             | 无声明                     | 声明抛出java.io.IOException                         |
| 文件不存在           | 不抛异常,返回false         | 抛java.nio.file.NoSuchFileException                 |
| 删除非空目录         | 无法删除,返回false         | 无法删除,抛java.nio.file.DirectoryNotEmptyException |
| 删除被占用文件       | 无法删除,返回false         | 无法删除,抛java.nio.file.FileSystemException        |
| 其他原因文件无法删除 | 不抛异常,返回false         | 抛java.io.IOException的具体子类                     |

##### 如何删除整个目录或者目录中的部分文件

先造数据

```java
private  void createMoreFiles() throws IOException {
   Files.createDirectories(Paths.get("D:\data\test1\test2\test3\test4\test5\"));
   Files.write(Paths.get("D:\data\test1\test2\test2.log"), "hello".getBytes());
   Files.write(Paths.get("D:\data\test1\test2\test3\test3.log"), "hello".getBytes());
}
```

###### walkFileTree与FileVisitor

使用walkFileTree方法遍历整个文件目录树，使用FileVisitor处理遍历出来的每一项文件或文件夹
FileVisitor的visitFile方法用来处理遍历结果中的“文件”，所以我们可以在这个方法里面删除文件
FileVisitor的postVisitDirectory方法，注意方法中的“post”表示“后去做……”的意思，所以用来文件都处理完成之后再去处理文件夹，所以使用这个方法删除文件夹就可以有效避免文件夹内容不为空的异常，因为在去删除文件夹之前，该文件夹里面的文件已经被删除了。

```java
@Test
void testDeleteFileDir5() throws IOException {
   createMoreFiles();
   Path path = Paths.get("D:\data\test1\test2");

   Files.walkFileTree(path,
      new SimpleFileVisitor<Path>() {
         // 先去遍历删除文件
         @Override
         public FileVisitResult visitFile(Path file,
                                  BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            System.out.printf("文件被删除 : %s%n", file);
            return FileVisitResult.CONTINUE;
         }
         // 再去遍历删除目录
         @Override
         public FileVisitResult postVisitDirectory(Path dir,
                                         IOException exc) throws IOException {
            Files.delete(dir);
            System.out.printf("文件夹被删除: %s%n", dir);
            return FileVisitResult.CONTINUE;
         }

      }
   );

}
```

下面的输出体现了文件的删除顺序

```bash
文件被删除 : D:\data\test1\test2\test2.log
文件被删除 : D:\data\test1\test2\test3\test3.log
文件夹被删除 : D:\data\test1\test2\test3\test4\test5
文件夹被删除 : D:\data\test1\test2\test3\test4
文件夹被删除 : D:\data\test1\test2\test3
文件夹被删除 : D:\data\test1\test2
```

我们既然可以遍历出文件夹或者文件，我们就可以在处理的过程中进行过滤。比如：

按文件名删除文件或文件夹，参数Path里面含有文件或文件夹名称
按文件创建时间、修改时间、文件大小等信息去删除文件，参数BasicFileAttributes 里面包含了这些文件信息。

##### Files.walk

如果你对Stream流语法不太熟悉的话，这种方法稍微难理解一点，但是说实话也非常简单。

使用Files.walk遍历文件夹（包含子文件夹及子其文件），遍历结果是一个Stream
对每一个遍历出来的结果进行处理，调用Files.delete就可以了。

```java
@Test
void testDeleteFileDir6() throws IOException {
   createMoreFiles();
   Path path = Paths.get("D:\data\test1\test2");

   try (Stream<Path> walk = Files.walk(path)) {
      walk.sorted(Comparator.reverseOrder())
         .forEach(DeleteFileDir::deleteDirectoryStream);
   }

}

private static void deleteDirectoryStream(Path path) {
   try {
      Files.delete(path);
      System.out.printf("删除文件成功：%s%n",path.toString());
   } catch (IOException e) {
      System.err.printf("无法删除的路径 %s%n%s", path, e);
   }
}
```

问题：怎么能做到先去删除文件，再去删除文件夹？ 。 利用的是字符串的排序规则，从字符串排序规则上讲，“D:\data\test1\test2”一定排在“D:\data\test1\test2\test2.log”的前面。所以我们使用“sorted(Comparator.reverseOrder())”把Stream顺序颠倒一下，就达到了先删除文件，再删除文件夹的目的。

下面的输出，是最终执行结果的删除顺序。

```bash
删除文件成功：D:\data\test1\test2\test3\test4\test5
删除文件成功：D:\data\test1\test2\test3\test4
删除文件成功：D:\data\test1\test2\test3\test3.log
删除文件成功：D:\data\test1\test2\test3
删除文件成功：D:\data\test1\test2\test2.log
删除文件成功：D:\data\test1\test2
```

##### 传统IO-递归遍历删除文件夹

传统的通过递归去删除文件或文件夹的方法就比较经典了

```java
//传统IO递归删除
@Test
void testDeleteFileDir7() throws IOException {
   createMoreFiles();
   File file = new File("D:\data\test1\test2");
   deleteDirectoryLegacyIO(file);

}

private void deleteDirectoryLegacyIO(File file) {

   File[] list = file.listFiles();  //无法做到list多层文件夹数据
   if (list != null) {
      for (File temp : list) {     //先去递归删除子文件夹及子文件
         deleteDirectoryLegacyIO(temp);   //注意这里是递归调用
      }
   }

   if (file.delete()) {     //再删除自己本身的文件夹
      System.out.printf("删除成功 : %s%n", file);
   } else {
      System.err.printf("删除失败 : %s%n", file);
   }
}
```

需要注意的是：

listFiles()方法只能列出文件夹下面的一层文件或文件夹，不能列出子文件夹及其子文件。
先去递归删除子文件夹，再去删除文件夹自己本身



### 获取请求端的真实IP

```java
    /**
     * 获取请求端的真实IP
     * @param request 请求体对象 
     * @return IP
     */
    public static String getRemoteHost(HttpServletRequest request){
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddr();
        }
        return ip.equals("0:0:0:0:0:0:0:1")?"127.0.0.1":ip;
    }
```



### 设置文件的编码格式

```java
//输入
FileInputStream is = new FileInputStream(viewFile);
InputStreamReader reader = new InputStreamReader(is,"gbk");
//输出
FileOutputStream fos = new FileOutputStream(fileName);
OutputStreamWriter osw = new OutputStreamWriter(fos,"gbk");
```



### 发送http请求

#### xml报文

```java
public static void sendHttp() {
		log.info("第一部分：处理发送的数据========================================");

		String requestMethod = "POST";
		String requestContentType = "application/xml";
		String requestUrl = "http://127.0.0.1:8080/local/local";
		String requestData = "9999999999<head>这是一条xml测试报文</head>";
		String requestCharset = "UTF-8";

		log.info("发送给 {}的请求报文为 {}", requestUrl, requestData);
		try {
			URL url = new URL(requestUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestMethod(requestMethod);
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", requestContentType);
            connection.setConnectTimeout(60 * 1000);
            connection.setReadTimeout(60 * 1000);
			connection.connect();

			// 将发送的数据进行写入
			try (OutputStream os = connection.getOutputStream()) {
				os.write(requestData.getBytes(requestCharset));
			}

			log.info("第二部分：处理返回的数据========================================");
			// 获取服务端处理后的返回报文：
			// 1.获取输入流
			// 2.封装为一个BufferedReader
			// 3.读取数据写入Java缓存
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(connection.getInputStream()))) {
				String lines;
				StringBuffer sbf = new StringBuffer();
				while ((lines = reader.readLine()) != null) {
					lines = new String(lines.getBytes(), requestCharset);
					sbf.append(lines);
				}
				log.info("接收到服务端返回的数据为：{}", sbf.toString());
			}
			connection.disconnect();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
```



### 解析excel中的图片

#### 前置条件

```xml
<dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>easyexcel</artifactId>
            <version>4.0.3</version>
</dependency>

<dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>4.1.2</version>
        </dependency>

        <!-- Apache POI - OOXML (for .docx, .pptx, .xlsx) -->
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>4.1.2</version>
        </dependency>
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml-schemas</artifactId>
            <version>4.1.2</version>
        </dependency>
```

#### 注解

```java
@Inherited
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelImageProperty {

    String[] value() default {""};

    /**
     * 图片在第几列 1开始
     */
    int index() default 1;
}
```

#### 实体类

```java
@ApiModelProperty("隐患照片")
@ExcelImageProperty(index = 10)
private byte[] hiddenPhoto;
```

#### 工具类

```java
/**
     * 获取工作簿指定sheet中图片列表
     *
     * @param file       文件
     * @param list       需要映射的集合对象
     * @param sheetIndex 读取的sheet页
     * @param begIndex   开始读取的行号
     */
    public static <T> void readImage(MultipartFile file, List<T> list, int sheetIndex, int begIndex) throws IOException, IllegalAccessException {
        try (InputStream inputStream = file.getInputStream();
             Workbook workbook = WorkbookFactory.create(inputStream)) {
            if (sheetIndex < 0) {
                sheetIndex = 0;
            }
            // 默认读取第一页
            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(sheetIndex);
            List<POIXMLDocumentPart> documentPartList = sheet.getRelations();
            int size = list.size();
            for (POIXMLDocumentPart part : documentPartList) {
                if (part instanceof XSSFDrawing) {
                    XSSFDrawing drawing = (XSSFDrawing) part;
                    List<XSSFShape> shapes = drawing.getShapes();
                    for (XSSFShape shape : shapes) {
                        XSSFPicture picture = (XSSFPicture) shape;
                        // 使用 getAnchor() 获取锚点信息
                        XSSFAnchor anchor = picture.getAnchor();
                        if (anchor instanceof XSSFClientAnchor) {
                            XSSFClientAnchor clientAnchor = (XSSFClientAnchor) anchor;

                            int row = clientAnchor.getRow1();
                            int col = clientAnchor.getCol1();

                            // 仅在图片行大于开始索引，并在列表范围内
                            if (row >= begIndex && row - begIndex < size) {
                                PictureData pictureData = picture.getPictureData();
                                byte[] bytes = pictureData.getData();

                                // 查找对应的对象
                                T item = list.get(row - begIndex);
                                Class<?> clazz = item.getClass();
                                Field[] fields = clazz.getDeclaredFields();
                                for (Field field : fields) {
                                    // 通过注解找字段
                                    if (field.isAnnotationPresent(ExcelImageProperty.class)) {
                                        ExcelImageProperty excelImageProperty = field.getAnnotation(ExcelImageProperty.class);
                                        int index = excelImageProperty.index();
                                        //是否是对应列
                                        if (index == col) {
                                            field.setAccessible(true);
                                            // 直接设置图片数据（byte[]）
                                            field.set(item, bytes);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
```

#### 使用

```java
@Override
    public Map<String, Object> importLedger(MultipartFile file) throws IOException, IllegalAccessException {
        // 读取 Excel 并将数据映射到 GwjcRepairLedger 类中
        List<GwjcRepairLedger> list = EasyExcel.read(file.getInputStream(), GwjcRepairLedger.class, new PageReadListener<GwjcRepairLedger>(dataList -> {
                }))
                .sheet()
                .headRowNumber(2) // 假设从第3行开始读取数据（索引从0开始）
                .doReadSync();
        ExcelUtils.readImage(file,list,0,2);
        log.info(list.toString());
        return null;
    }
```

### 生成数据库设计文档

```xml
<!--            文档生成-->
            <plugin>
            <groupId>cn.smallbun.screw</groupId>
            <artifactId>screw-maven-plugin</artifactId>
            <version>1.0.5</version>
            <dependencies>
                <!-- HikariCP -->
                <dependency>
                    <groupId>com.zaxxer</groupId>
                    <artifactId>HikariCP</artifactId>
                    <version>3.4.5</version>
                </dependency>
                <!--postgresql-->
                <dependency>
                    <groupId>org.postgresql</groupId>
                    <artifactId>postgresql</artifactId>
                    <version>42.2.5</version><!--$NO-MVN-MAN-VER$ -->
                 </dependency>
            </dependencies>
            <configuration>
                <!--username-->
                <username>dwpipe</username>
                <!--password-->
                <password>Dwpipe#321</password>
                <!--driver-->
                <driverClassName>org.postgresql.Driver</driverClassName>
                <!--jdbc url-->
                <jdbcUrl>jdbc:postgresql://192.168.2.158:5452/dwpipedb</jdbcUrl>
                <!--生成文件类型-->
                <fileType>HTML</fileType>
                <!--打开文件输出目录-->
                <openOutputDir>false</openOutputDir>
                <!--生成模板-->
                <produceType>freemarker</produceType>
                <!--文档名称 为空时:将采用[数据库名称-描述-版本号]作为文档名称-->
                <fileName>水务数据库文档</fileName>
                <!--描述-->
                <description>数据库文档生成</description>
                <!--版本-->
                <version>${project.version}</version>
                <!--标题-->
                <title>数据库文档</title>
            </configuration>
            <executions>
                <execution>
                    <phase>compile</phase>
                    <goals>
                        <goal>run</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
```





## 问题

### List<Map> 集合 使用stream流 Comparator.reversed()颠倒排序时，使用lambda报错



#### 目的：

> 对list中元素的属性做操作之后进行颠倒排序，使用[lambda](https://so.csdn.net/so/search?q=lambda&spm=1001.2101.3001.7020)表达式报错



#### 提示：

> Cannot resolve method 'getContributionProportion' in 'Object' 



List<Employee> list = employeeService.listByIds(idList);

```java
list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed());
不使用lambda表达式时，直接使用方法引用，不会报错，但是没有达到需求
list.sort(Comparator.comparing(Employee::getContributionProportion).reversed());
```

使用lambda[表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)，不进行颠倒排序，也不会报错，但是也没有达到需求

```javascript
list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())));
```

正确的写法：

```coffeescript
list.sort(Comparator.comparing((Employee o) -> new BigDecimal(o.getContributionProportion())).reversed());
```

   

> 如果不使用方法引用，要传递一些其他的参数，使用lambda表达式时，这个时候，可以在lambda中显示提供参数类型。
>
> ​    当然，如果不是这样的写法，分开来处理，也是可以多种方式实现业务需求的，现在只是针对这种方式讲一下是为什么会报错。



**先简单了解一下lambda表达式：**

>  Lambda 表达式中的参数类型都是由编译器推断得出的。 Lambda 表达式中无需指定类型，程序依然可以编译，这是因为 javac 根据程序的上下文，在后台推断出了参数的类型。 Lambda 表达式的类型依赖于上下文环境，是由编译器推断出来的。这就是所谓的 “类型推断”。Lambda表达式会有类型检查和类型推断这两个过程，保证了Lambda使用的正确性。
>
>  **类型检查：**主要就是检查Lambda表达式是否能正确匹配函数式接口的方法。
>
>  **类型推断：**当Lambda表达式没有显式声明参数类型，JVM编译器会通过目标类型推断入参类型。
>
>  Lambda的上下文：使用Lambda表达式来传递的方法的参数，或接受Lambda表达式的值的局部变量    
>
>  目标类型：是指Lambda表达式所在上下文环境的类型。比如，将 Lambda 表达式赋值给一个局部变量，或传递给一个方法作为参数，局部变量或方法参数的类型就是 Lambda 表达式的目标类型。

**第一种分析：**

>  sort()的参数是Comparator<? super E> c，这里在对list进行排序时，此时的E是根据前面list推断出是Employee类型，那参数必须也是这个类型，list.sort()就需要传的参数是Comparator<? super Employee> c，也就是说Comparator.comparing()需要返回的类型为Comparator<Employee>，这意味着Comparator.comparing()需要Function采用一个Employee传入参数。
>
>  ​    2、4指定了类型，也就不需要作类型推断了，肯定能编译通过。
>
>  ​    3去掉了reversed()为什么可以编译通过？因为Comparator.comparing()没有指定具体的泛型参数，在方法调用前面使用<>指定，或给方法传入具体类型的入参，编译器也可以推断出来，否则会默认从sort()入参类型里推断，这里就默认从sort()的入参类型推断为Employee。
>
>  ​    1为什么编译失败？reversed()的返回类型依赖前一个方法的返回类型推断，Comparator.comparing()又依赖它的入参Function<? super T, ? extends U> keyExtractor推断，但是它的入参没有明确的类型，那么它只能直接推断成最顶级的类型，即Object类型。这里为什么没有默认从sort()入参类型里推断呢？注意，这里的比较器是多层的，comparing()和reversed()，最终我们需要的比较器是reversed()返回的，因为编译器的类型推断机制不够强大，无法同时采取两个步骤，推断不出来类型，也就是无法从上下文中推断出类型。个人的理解，lambda表达式要推断类型的时候，如果是一层操作，可以根据上下文推断出来，如果是多层操作则推断不出来，变成多个上下文，而reversed()是后操作的，是离lambda表达式最近的上下文，两层上下文，参数是未知的，就变成object，lambda推断的就是object。

  

**第二种分析：**

> ist.sort(Comparator.comparing(o -> new [BigDecimal](https://so.csdn.net/so/search?q=BigDecimal&spm=1001.2101.3001.7020)(o.getContributionProportion())));
>
> list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed());
>
> ​    再来对比一下这两种写法，o -> new BigDecimal(o.getContributionProportion())看成一个整体。
>
> 第一种写法：推断类型的时候，根据目标类型推断，也就是根据Comparator.comparing()的参数类型推断，因为comparing()的参数类型和返回类型是一个类型，这里只有一个comparing()操作，且sort()需要Employee类型，即comparing()需要返回Employee类型，综合，推断o类型是Employee；
>
> 第二种写法：整体看，sort()需要Employee类型，也就是原本需要Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed()返回一个Employee类型，由于这里有两个操作，最终返回Employee类型，所以编译器推断的时候并不确定comparing()的参数传的就是Employee类型，所以推断成它的父类Object，因此就推断了o类型是Object类型，而sort()不需要Object类型，所以报错。



