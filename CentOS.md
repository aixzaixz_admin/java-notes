## 一、环境安装



### CentOS 7

#### 简介

##### VMware

   VMware是一款虚拟机软件就是通过软件模拟的具有完整硬件系统功能的、运行在一个完全隔离环境中的完整计算机系统。

  与物理机一样，虚拟机是运行操作系统和应用程序的软件计算机。虚拟机包含一组规范和配置文件，并由主机的物理资源提供支持。每个虚拟机都具有一些虚拟设备，这些设备可提供与物理硬件相同的功能，但是可移植性更强、更安全且更易于管理。

官网：https://www.vmware.com/cn/products/workstation-pro/workstation-pro-evaluation.html

##### CentOS

  CentOS是免费的、开源的、可以重新分发的开源操作系统。全名为“社区企业操作系统（Community Enterprise Operating System）”，提供长期免费升级和更新服务，自由使用。国内最大的服务器操作系统，现在基本所有的互联网公司后台服务器都采用CentOS

官网：[Download![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507136.png)https://www.centos.org/download/](https://www.centos.org/download/)

------

#### 安装步骤



##### 一、安装前的准备

1.VMware的安装本文不再介绍，正常安装即可

2.下载CentOS 7 的镜像文件

##### 二、下载镜像文件

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507489.png

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507345.png)

##### 三、开始安装

1、创建新的虚拟机
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507317.png)
2、选择自定义
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507446.png)
3、硬盘兼容性-- **默认**
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507144.png)
4、稍后安装操作系统（需要在虚拟机安装完成之后，删除不需要的硬件，所以稍后安装操作系统)
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507467.png)
5、选择客户端操作系统：

1. 客户机操作系统–Linux
2. 版本–centos 64位

（注意：版本一定要对应镜像文件版本，其中centos是32位，centos 64位则就是64位，windows系统应安装64位版本）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507738.png)
6、**命名虚拟机**（简略表示出该虚拟机的类型、版本。例如：centos7 ）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507913.png)
7、处理器配置（CPU）–总处理器核心数一般为 4![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507058.png)

虚拟机总核心数不能超过主机核心数。若超出则会警告提醒。
![90e6a28e55dc402dae3be53775615c7e](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507060.png)

8、此虚拟机内存 => **一般2G** 

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507564.png)



9、网络类型–桥接网络（可以使虚拟机与主机使用同一网络）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507667.png)
注释：

- VMnet1网口对应的是仅主机模式
- VMnet8网口对应的是NAT模式
- VMnet0网口对应的是桥接模式

查看以上对应是在VMware workstation中的编辑-虚拟网络编辑器

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507107.png)

10、选择I/O控制器类型（相对于硬盘）-- **默认**
从硬盘到内存是I（input），从内存在硬盘是O（output）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507190.png)
11、选择磁盘类型-- **默认** （硬盘接口，家庭个人常用SATA类型，服务器常用SCSI类型）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507309.png)
12、选择磁盘–创建新的虚拟磁盘（其他两个不常用）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507211.png)
13、指定磁盘容量–100G（是假的虚拟的不占主机内存）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507862.png)
14、指定磁盘文件（.vmdk）文件
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507345.png)
15、完成
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507651.png)
**删除不需要的硬件** – 编辑虚拟机设置–删-USB控制器、声卡、打印机（可以使虚拟器启动的快一点）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507623.png)
也可以手动添加硬件，比如，一个网口不够，再添加一个。**（网络连接仍然选择桥接模式)**
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507628.png)
此时虚拟机中的硬件已经搭建完成

16、继续添加映像文件，选择设备中的CD/DVD（IDE），在连接处选择–使用ISO映像文件–确定
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507882.png)

17、开启虚拟机，选择第一项 Install CentOS 7，等待一段时间；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507111.png)

18、WELCOME TO CENTOS 7.设置语言–推荐使用English–点击Continue

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507633.png)

19、INSTALLATION SUMMARY 安装总览（这里可以完成centos 7 版本Linux的全部设置）

1. 首先，设置时区–DATE & TIME，找到Asia–Shanghai并点击–Done
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507984.png)
2. KEYBOARD 键盘就默认是English（US）
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507651.png)
3. LANGUAGE SUPPORT语言支持
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507760.png)
   可以是默认的English 也可以自行添加Chinese简体中文的支持
4. INSTALLATION SOURCE 安装资源，默认选择–Local media 本地媒体文件
5. SOFTWARE SELECTION软件安装选择，字符界面安装–Minimal install 或者 Basic Web Server
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507006.png)
   **图形界面安装–Server with GUI 或者 GNOME Desktop**
   字符界面与图形界面安装过程相同，只在这一步有区分。点击–Done进入下一步

20、INSTALLATION DESTINATION 安装位置—即进行系统分区，选中我们在创建虚拟机时候的100G虚拟硬盘
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507291.png)

21、这是我们已完成所有设置，-- Begin Installation
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507823.png)
22、这时需要设置管理员Root Password（务必记住密码！）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507604.png)
接下来可以创建用户（此处可以不进行创建，安装完成后进入root也可以重新创建）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507828.png)
23、等待一段时间，centos 7安装完成 – 点击reboot重启使用
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507197.png)
安装完成后如图所示：

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507140.png)

##### **四、Centos7网络设置**



###### 1、以系统管理员打开VMWare

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507247.png)

 

 

###### 2、选择虚拟网络编辑器菜单

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507824.png)

 

 

###### 3、选择VMnet8这一行

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507156.png)

 

 

###### 4、还原VMnet8的默认设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507646.png)

 

 

###### 5、修改VMnet8的参数

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507817.png)

 

 

**虚拟机子网IP地址段和子网掩码由您自己来定，如果你不熟练，就按上图中的内容来设置也没有问题。**

###### 6、NAT设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507552.png)

 

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507084.png)

 

 

 

###### 7、保存设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507294.png)

 

 

###### 8、确认虚拟机为NAT模式

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507605.png)

 

 

###### 9、启动虚拟机CentOS7

###### 10、设置CentOS7的不静态IP地址

修改虚拟机网卡配置文件，如/etc/sysconfig/network-scripts/ifcfg-ens33，注意，文件名不一定是ifcfg-ens33，根据您的实际情况决定。

1）修改BOOTPROTO参数，把地址协议改为静态IP方式。

```cpp
BOOTPROTO=static  # dhcp-动态分配，static-静态分配（重要）。
```

2）修改ONBOOT参数，把开机启动选项ONBOOT设置为yes。

```cpp
ONBOOT=yes  # 是否开机引导。
```

3）设置DSN服务器的IP，添加以下内容。

```cpp
DNS1=114.114.114.114  # 第1个DSN服务器的IP地址。
DNS2=1.2.4.8  # 第2个DSN服务器的IP地址。
```

4）设置CentOS7的IP地址、子网掩码和网关参数，添加以下内容。

```cpp
IPADDR=192.168.226.128  # IP地址（重要）。
NETMARSK=255.255.255.0  # 子网掩码（重要）。
GATEWAY=192.168.226.2   # 网关（重要）。
```

###### 11、重启CentOS7的网络服务

```cpp
systemctl restart network
```

###### 12、测试效果

ping一下百度。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507213.png)

ok。

###### 13、注意事项

如果您对网络知识不熟悉，或对虚拟机不熟悉，建议按本文章依葫芦画瓢，照抄参数。

 

二、动态IP上网（所有操作不涉及到IP地址，单纯能连外网）

1、以系统管理员打开VMWare

2、选择虚拟网络编辑器菜单

3、选择VMnet8这一行，勾选三处vi

*![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507569.png)*

 

在VMnet8中设置子网号192.168.xx.0,子网掩码为255.255.255.0，然后点开DHCP配置，按照默认的即可

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507489.png)

 

 注意一定要在下面“使用本地DHCP服务器将IP地址分配给虚拟机”打钩，否则就是静态NAT了。

4、编辑网卡

输入cd /etc/sysconfig/network-scripts ,然后用打开其中的ifcfg-ens33(我是这个，有些人是32，自己打开这个文件夹ls一下)

执行：vim ifcfg-ens33

然后把其中ONBOOT改为yes，再Esc  :wq退出即可（如果不用root来修改这个文件，保存的时候会出现问题![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507822.png)



5、虚拟机设置，选择网络适配器为NAT(在虚拟机关闭的时候可以在虚拟机设置中打开)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507686.png)

 

 

6、重启网卡生效

```sh
sudo systemctl restart network #重启网卡

sudo systemctl enable network #开机启动网卡
```



##### 五、关闭防火墙的命令!

###### 1:查看防火状态

```java
systemctl status firewalld

service  iptables status
```

###### 2:暂时关闭防火墙

```java
systemctl stop firewalld

service  iptables stop
```

###### 3:永久关闭防火墙

```java
systemctl disable firewalld

chkconfig iptables off
```

###### 4:重启防火墙

```java
systemctl enable firewalld

service iptables restart  
```

###### 5:永久关闭后重启

```java
chkconfig iptables on
```

#### 问题

##### 		一、无图形界面

​						（在安装CentOS7时，如果选择 “最小化” 安装那么系统就只有命令行界面，但是没有图形化界面）
​							![image-20210926100317797](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507107.png)

###### 解决措施：

一、先装X windows，-y表示参数同意所有软件安装操，当出现 Complete！说明这里安装成功了。

```
 yum groupinstall "X Window System" -y
```

二、yum grouplist 检查一下我们已经安装的软件以及可以安装的软件，下面安装的名字要和下面的对应起来，否则会出现No packages in any requested group available to install or update 的错误。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507966.png)

 

 

安装GNOME桌面环境，提示complete!，说明成功。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507965.png)

```
 yum groupinstall "GNOME Desktop" "Graphical Administration Tools" -y
```

 **安装报错 transaction check error**

file /boot/efi/EFI/centos from install of fwupdate-efi-12-5.el7.centos.x86_64 conflicts with file from package grub2-common-1:2.02-0.65.el7.centos.2.noarch

用  只升级所有包，不升级软件和系统内核。

```
yum upgrade -y
```

再进行安装即可

三、通过命令 startx 进入图形界面，第一次进入会比较慢，耐心等待就可以了。

四、更新系统的默认运行级别

经过上面的操作，系统启动默认还是命令行页面的，需要我们使用**Ctrl+Alt+F2**进行切换。如果想要使系统启动即为图形化窗口，需要执行下面的命令

```
systemctl set-default graphical.target  #设置成图形模式
或者
ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target #默认运行级别5
```

如果要换回默认开机命令模式使用 systemctl set-default multi-user.target #设置成命令模式

五、重启

##### 二、system restart network 报错

```
Job for network.service failed because the control process exited with error code
```

在CentOS系统上，目前有NetworkManager和network两种网络管理工具。如果两种都配置会引起冲突。由于一般我们都是使用 network 配置静态ip，可能是关机（某种缘故）导致NetWorkManager自动配置，发生了冲突，所以把它禁用掉就好了。

> 临时关闭
> systemctl stop NetworkManager
> 永久关闭
> systemctl disable NetworkManager
> 重启
> systemctl restart network

### CentOS 8



#### 一、下载ContOS 8

ContOS官网：https://www.centos.org/download/
![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507589.png)
官网显示只有版本7，但是需要下载CentOS 8，所以在历史版本中下载：http://mirror.nsc.liu.se/centos-store/8.5.2111/isos/x86_64/
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507700.png)

#### 二、创建虚拟机

##### 1.打开VMWare，选择创建新的虚拟机

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507088.png)

##### 2.选择类型配置

选择典型配置即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507369.png)

##### 3.选择稍后安装操作系统

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507318.png)

##### 4.选择操作系统

选择Linux系统，CentOS 8 64位
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507610.png)

##### 5.命名虚拟机与选择安装位置

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507312.png)

##### 6.指定磁盘容量

使用建议的容量大小，这里设置将虚拟磁盘存储为单个文件
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507504.png)

##### 7.确认创建虚拟机的信息

可以不用自定义硬件，使用默认的即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507567.png)

#### 三、安装设置CentOS 8

##### 1.安装CentOS 8

选择刚刚创建的虚拟机，点击编辑虚拟机设置
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507710.png)

##### 2.选择iso文件

选择CD/DVD(IDE)→使用ISO映像文件，选择下载的iso文件→确定
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507685.png)

##### 3、设置虚拟机

###### 1.选择并打开虚拟机

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507386.png)

###### 2.安装系统

上键选择Install CentOS Linux 8，如果按上键没有反应，使用鼠标左键进入虚拟机中，将鼠标退出虚拟机Ctrl+Alt
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507553.png)

###### 3.配置虚拟机

###### 3.1、选择语言

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507676.png)

###### 3.2、进入配置界面，红色的是必须设置

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507885.png)

###### 3.3、配置网络和主机名

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507948.png)
将以太网按钮打开，可以看到连接成功的信息，并设置一个主机名
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507366.png)

###### 3.4、设置时间

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507548.png)
地区选择亚洲，城市选择上海，打开网络时间即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507592.png)

###### 3.5、设置安装目的地

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507925.png)
点击自定义→完成进入手动分区界面
点击左下角"+“号，选择挂载点：/boot，期望容量400MB
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507164.png)
再次点击”+“号，选择挂载点：swap，期望容量：2GB
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507303.png)
再次点击”+"号，选择挂载点：/，剩余容量自动分配
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507562.png)
配置后→完成→接受更改
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507821.png)

###### 3.6、设置安装源与软件选择

选择自动检测到的安装介质，就是本地介质即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507975.png)
软件选择：选择最小安装即可，我这里选择带GUI的服务器（自己视情况而定）
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507423.png)

###### 3.7、设置根密码与创建用户

设置根密码
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507684.png)
创建用户
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507633.png)
设置完成后，点击安装，等待安装完成即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507379.png)

#### 四、登录虚拟机



因为安装的是带GUI的服务器，就像WINDOWS系统一样有图形化的操作界面，登录进去后，也有几个简单的设置，视自己情况而定即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507620.png)
登录之后的图形化界面
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507064.png)
至此安装虚拟机全部完成

#### 五、网络配置

##### 1. 物理机下的配置

1. 查看主机IP地址，win+R打开运行，输入[cmd](https://so.csdn.net/so/search?q=cmd&spm=1001.2101.3001.7020)并按下回车，在打开的窗口中输入`ipconfig`，查看所有连接。
   注意这一步不要找错IP地址！！！找的是**以太网适配器 VMware Network Adapter VMnet8:下的IPv4地址！！！**
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507011.png)
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507871.png)
2. 右击WiFi图标，点击 **打开“网络和Internet”设置**，在打开的窗口中点击**更改适配器选项**

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507067.png)

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507366.png)

1. 右键点击**VMware Network Adapter VMnet8**，选择**属性**
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507595.png)
2. 双击**Internet协议版本4（TCP/IPv4）**

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507623.png)

1. 都选择自动获得，点击确定，再点击确定

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507683.png)

##### 2. 虚拟机下的配置

1. 单击左侧**我的计算机**，打开界面
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507672.png)
2. 右键点击设置
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141507835.png)
3. 选择左侧网络适配器，右侧NAT模式，点击确定
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508658.png)
4. **编辑**中打开**虚拟网络编辑器**

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508638.png)

1. 点击右下角更改设置，如果有弹出来一个窗口，点击“是”即可

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508600.png)

1. 看图看图看图，注意在此处的步骤！！！下面细细说。

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508573.png)

① 选择**VMnet8 NAT模式**

②两个方框**都勾上**

③子网IP和子网掩码对应在物理机中获得的IPv4地址，你要根据你的物理机中IPv4地址和子网掩码得到相应的子网，这样我们就可以保证物理机和虚拟机在同一个网段上了

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508985.png)

④ 点击DHCP设置，在其中设置起始IP地址和结束IP地址，即是第③步得到的网段中的全部可用IP地址，从1~254。之后点击确定

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508885.png)

⑤点击NAT设置，设置网关，即你的**IP子网地址.2**，这里我的子网是192.168.208.0，即网关为192.168.208.2，点击确定。

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508994.png)

**这五个小步骤全部完成后点击“应用”，再点击“确定”**

1. 查看虚拟机的IP地址，右键点击**Open in Terminal**，打开终端，输入ifconfig -a
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508375.png)

可以看到，我的虚拟机的IP地址为192.168.208.3

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508758.png)

到此我们整个网络配置即完成了，下面让我们来测试一下

##### 物理机、虚拟机、[外网](https://so.csdn.net/so/search?q=外网&spm=1001.2101.3001.7020)相互ping测试

根据上面物理机下的配置中的第1步可以得到我的**物理机的IP地址为192.168.208.1**
根据上面虚拟机下的配置中的第7步可以得到我的**虚拟机的IP地址为192.168.208.3**

大家也可以根据我的步骤得到自己电脑的物理机和虚拟机的IP地址

###### 1. 物理机ping虚拟机

win+R输入cmd并按下回车，输入`ping 192.168.208.3`，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508854.png)

###### 2. 物理机ping 百度

还在刚才那个窗口输入`ping www.baidu.com`,可以看到已ping通
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508816.png)

###### 3. 虚拟机ping物理机

在虚拟机的终端（上面已讲怎么打开终端）中输入**ping 192.168.208.1**，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508963.png)

###### 4. 虚拟机ping百度

在终端输入**ping www.baidu.com**，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508105.png)



#### 六、防火墙命令

> 一、防火墙服务
> 1、启动、关闭、重启防火墙服务。
> systemctl start  firewalld.service
> systemctl stop  firewalld.service
> systemctl restart  firewalld.service
> 2、显示防火墙的状态。
> systemctl status firewalld.service
> 3、开机启动防火墙。
> systemctl enable firewalld.service
> 4、开机时禁用防火墙。
> systemctl disable firewalld.service
> 5、查看防火墙是否开机启动。
> systemctl  is-enabled  firewalld.service
> 6、查看防火墙是否开机启动。
> systemctl  is-enabled  firewalld.service
> 7、查看已启动的服务列表。
> systemctl list-unit-files|grep enabled
> 8、查看启动失败的服务列表。
> systemctl  --failed
> 9、启动、停止、重启httpd服务。
> systemctl  start   httpd
> systemctl  stop   httpd
> systemctl  restart  httpd
>
> 二、防火墙配置
> 1、查看版本。
> firewall-cmd --version
> 2、查看帮助。
> firewall-cmd  --help
> 3、显示防火墙状态。
> firewall-cmd --state
> 4、查看所有打开的端口。
> firewall-cmd --zone=public --list-ports
> 5、查看区域信息
> firewall-cmd --get-active-zones
> 6、查看指定接口所属区域。
> firewall-cmd --get-zone-of-interface=eth0
> 7、拒绝所有包、取消拒绝状态、查看是否拒绝
> firewall-cmd --panic-on
> firewall-cmd --panic-off
> firewall-cmd --query-panic
> 8、开启3306端口，–permanent永久生效，没有此参数重启后失效。
> firewall-cmd --zone=public --add-port=3306/tcp --permanent  
> 9、重新载入，更新防火墙规则。
> firewall-cmd --reload
> 10、查看3306端口是否开放。
> firewall-cmd --zone=public --query-port=3306/tcp 
> 11、删除3306端口配置。
> firewall-cmd --zone=public --remove-port=3306/tcp --permanent 

#### 问题

##### 配置页面显示不全

> 在安装的时候选在第一项按 Tab 键，输入 vga=791，注意前面先有一个空格

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508577.png)

在附上一张分辨率的参数表

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202209141508038.png)



##### 设置基础软件仓库时出错

> https://mirrors.aliyun.com/centos/8-stream/BaseOS/x86_64/os/

## 二、软件安装



### 使用以下命令来设置稳定的仓库。



#### 使用官方源地址（比较慢）

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**download.docker.com**/**linux**/**centos**/**docker-ce.repo
```

可以选择国内的一些源地址：

#### 阿里云

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**download.docker.com**/**linux**/**centos**/**docker-ce.repo
```

#### 清华大学源	

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**mirrors.tuna.tsinghua.edu.cn**/**docke
```

### node.js

#### 		1)安装步骤：

##### 		centos 7 yum

```
yum install -y epel-release  
/usr/bin/yum install -y nodejs
```

##### 		centos 6安装

- 由于yum源版本过低，cnpm安装失败，使用nvm管理node包的时候

```
yum remove nodejs -y  
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash  
```

- 安装成功后:一定要重新启动shell

```
command -v nvm  
```

- 查看nvm可安装版本

```
nvm ls-remote  
```

- 安装nodejs

```
nvm install v6.12.3  
```

##### 		国内安装源

- cnpm

```
/usr/bin/npm install -g cnpm --registry=https://registry.npm.taobao.org
```

- 配置文件 ~/.npmrc 文件中写入源地址

```
registry =https://registry.npm.taobao.org  
```

#### 		2）问题：

##### 				nvm command not found

​						nvm是node的包版本管理工具，github地址如下：[nvm](https://github.com/creationix/nvm)

​       **安装命令**

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
1
nvm //检查nvm是否安装成功
-bash: nvm: command not found  //boom，失败了
12
```

​		解决nvm command not found问题

​		进入.nvm文件夹，新建.bash_profile：

```bash
touch .bash_profile //新建文件
open .bash_profile //打开文件
12
```

​		在里面copy如下内容：

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
12
```

​		关闭文件，然后执行这个文件：

```bash
source .bash_profile
```

​		执行完毕，我们再看看是否安装成功：

```bash
nvm --version
```

​		输出：

```bash
0.33.8
```

​		安装成功。

​		卸载：

```bash
$ nvm use system
$ npm uninstall -g a_module
```

