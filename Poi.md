# Poi

## 模板

![image-20220818180556329](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202208181806654.png)



## 示例代码

```java
public void exportExcel(Map<String, String> queryMap, HttpServletResponse response) {
        log.info("-----------------导出值班记录数据开始" + LocalDateUtil.now(LocalDateUtil.DATE_COMPREHENSIVE) + "-----------------");
        log.info("--------------开始查询值班记录------------");
        List<CsdzWatch> csdzWatch = csdzWatchMapper.findAll(queryMap);
        log.info("--------------查询值班记录结束,共有" + csdzWatch.size() + "条数据------------");
        //如果没有查询到数据则不处理
        if (!CollectionUtils.isEmpty(csdzWatch)) {
            String now = LocalDateUtil.now(LocalDateUtil.DATE_DETAIL_CHINA);
            String fileName = "值班记录表" + now + ".xlsx";
            ClassPathResource classPathResource = new ClassPathResource("template/exportDutyRecord.xlsx");
            try (InputStream in = classPathResource.getInputStream();
                 ServletOutputStream outputStream = response.getOutputStream();
            ) {
                //读取excel模板
                XSSFWorkbook wb = new XSSFWorkbook(in);
                //读取了模板内所有sheet内容
                XSSFSheet sheet = wb.getSheetAt(0);
                //如果这行没有了，整个公式都不会有自动计算的效果的
                sheet.setForceFormulaRecalculation(true);
                //定义开始填充数据的行
                AtomicInteger startRow = new AtomicInteger(3);
                //定义初始数据序号
                AtomicInteger serialNumber = new AtomicInteger(1);
                csdzWatch.forEach(watch -> {
                    XSSFRow row = sheet.getRow(startRow.get());
                    //定义开始填充数据的列
                    int startCell = 0;
                    //数据序号
                    row.getCell(startCell++).setCellValue(serialNumber.toString());
                    //值班时间
                    row.getCell(startCell++).setCellValue(watch.getDutyTime());
                    //值班地点
                    row.getCell(startCell++).setCellValue(watch.getDutyAddr());
                    //值班人员
                    row.getCell(startCell++).setCellValue(watch.getDutyUserId());
                    //气象信息简介
                    row.getCell(startCell++).setCellValue(watch.getWettherIntroduct());
                    //气象信息时段1
                    row.getCell(startCell++).setCellValue(watch.getWettherTime1());
                    //气象信息时段2
                    row.getCell(startCell++).setCellValue(watch.getWettherTime2());
                    //气象信息时段3
                    row.getCell(startCell++).setCellValue(watch.getWettherTime3());
                    //气象信息时段4
                    row.getCell(startCell++).setCellValue(watch.getWettherTime4());
                    //获取预警内容
                    List<CsdzWaringInfo> waring = watch.getWaring();
                    if (!CollectionUtils.isEmpty(waring)) {
                        //记录预警数据初始列
                        AtomicInteger finalWaringCell = new AtomicInteger(startCell);
                        //定义预警列编号
                        AtomicInteger waringCell = new AtomicInteger(startCell);
                        //定义预警行编号
                        AtomicInteger waringStartRow = new AtomicInteger(startRow.get());
                        //遍历写入预警内容
                        waring.forEach(waringItem -> {
                            XSSFRow waringRow = sheet.getRow(waringStartRow.get());
                            //预警等级
                            waringRow.getCell(waringCell.getAndIncrement()).setCellValue(waringItem.getGerde());
                            //预警内容
                            waringRow.getCell(waringCell.getAndIncrement()).setCellValue(waringItem.getContent());
                            //初始化序号
                            waringCell.set(finalWaringCell.get());
                            //预警行编号增加
                            waringStartRow.getAndIncrement();
                        });
                        //增加预警列
                        startCell += 2;
                    }
                    //获取险情内容
                    List<CsdzDanger> danger = watch.getDanger();
                    if (!CollectionUtils.isEmpty(danger)) {
                        //定义险情数据编号
                        AtomicInteger dangerNumber = new AtomicInteger(0);
                        //记录险情数据初始列
                        AtomicInteger finalDangerCell = new AtomicInteger(startCell);
                        //定义险情数据编号
                        AtomicInteger dangerCell = new AtomicInteger(startCell);
                        //定义预警行编号
                        AtomicInteger dangerStartRow = new AtomicInteger(startRow.get());
                        //遍历写入险情内容
                        danger.forEach(dangerItem -> {
                            XSSFRow dangerRow = sheet.getRow(dangerStartRow.get());
                            //序号
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerNumber.get() + 1);
                            //发灾时间
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerTime());
                            //发灾内容
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerContent());
                            //发灾雨量
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerRainfall());
                            //灾险情简述
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerAddress());
                            //初始化序号
                            dangerCell.set(finalDangerCell.get());
                            //序号增加
                            dangerNumber.getAndIncrement();
                            //行编号增加
                            dangerStartRow.getAndIncrement();
                        });
                    }
                    //获取当前数据最底层列
                    int maxRow = Math.max(waring.size(), danger.size());
                    //记录行号
                    int begRow = startRow.get();
                    //设置下一条数据的列索引
                    startRow.addAndGet(Math.max(maxRow, 1));
                    //序号增加
                    serialNumber.getAndIncrement();
                    //如果数据列需要合并
                    if (maxRow > 1) {
                        ExcelUtil.addMergeAddress(sheet, begRow, startRow.get() - 1, 0, 9);
                    }
                });
                //写入响应头信息
                response.setHeader("content-Type", "application/octet-stream;charset=utf-8");
                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
                response.setHeader("filename", URLEncoder.encode(fileName, "UTF-8"));
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.setCharacterEncoding("UTF-8");
                log.info("--------------开始写入文档------------");
                wb.write(outputStream);
                log.info("--------------写入文档结束------------");
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(204);
            }
        }
        log.info("-----------------导出值班记录数据结束" + LocalDateUtil.now(LocalDateUtil.DATE_COMPREHENSIVE) + "-----------------");
    }
```



## 合并单元格

````java
1. 添加依赖
 
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-ooxml</artifactId>
    <version>4.1.2</version>
</dependency>
2.
``` 
  @PostMapping(value = "/mergeCell",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public HashMap<String,String>  mergeCell(@RequestParam("file") MultipartFile file){
        String fileName = file.getOriginalFilename();
        System.out.println(fileName);
        String filePath = "D:\\WorkPlace";
        File dir = new File(filePath);
        if(!dir.exists()){
            dir.mkdir();
        }
        String originFilePath = filePath+File.separator+fileName;
        String newFilePath = filePath+File.separator+"合并后"+fileName;
        System.out.println(filePath);
        try {
            MergeCellUtil.mergeCellAndSave(originFilePath,newFilePath);
            return new HashMap<String, String>(){{put("单元格合并", "合并成功");}};
        } catch (Exception e) {
            return new HashMap<String,String>(){{put("单元格合并","合并失败");}};
        }
    }
```
 
3. 
 
 
/**
 * 检测 多次给付重疾险智核问卷0810.xlsx
 */
@Slf4j
public class MergeCellUtil {
    /**
     * 列方向合并相同数据的单元格. 空白单元格不合并. 将文件保存到硬盘
     *
     * @param originFilePath xlsx 原文件
     * @param newFilePath    合并后生成的 xlsx 文件保存路径
     */
    public static void mergeCellAndSave(String originFilePath, String newFilePath) {
 
        FileInputStream in = null;
        FileOutputStream outputStream = null;
        try {
            //Workbook newBook = MergeCellUtil.getMemoryWorkbook(originFilePath);
            //将文件读入
            in = new FileInputStream(new File(originFilePath));
            //追加写入----避免相同文件合并后生成相同文件名无法再写入
            outputStream = new FileOutputStream(newFilePath, true);
            //创建工作薄
            Workbook newBook = new XSSFWorkbook(in);
            MergeCellUtil.mergeCell(newBook);
            newBook.write(outputStream);
        } catch (Exception e) {
            throw new RuntimeException();
        } finally {
            try {
                outputStream.flush();
                outputStream.close();
                in.close();
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
    }
 
    /**
     * 将硬盘中的excel sheet0 的数据复制到纯内存excel中.
     * 原因: 从硬盘读入的excel操作之后, 没有写出成功
     *
     * @param originFilePath xlsx 文件路径
     * @return 内存中生成的workbook
     */
    private static Workbook getMemoryWorkbook(String originFilePath) throws Exception {
//        String originFilePath = "D:\\operate-data\\多次给付重疾险智核问卷0809.xlsx";
 
        Workbook originBook = new XSSFWorkbook(originFilePath);
        originBook.close();
        Sheet originSheet = originBook.getSheetAt(0);
 
        int lastRowIndex = originSheet.getLastRowNum();
        short columnSize = originSheet.getRow(0).getLastCellNum();
 
        Workbook newBook = new XSSFWorkbook();
        Sheet newSheet = newBook.createSheet();
 
        for (int i = 0; i <= lastRowIndex; i++) {
            Row originRowI = originSheet.getRow(i);
            Row newRowI = newSheet.createRow(i);
            for (int j = 0; j < columnSize; j++) {
                Cell newCell = newRowI.createCell(j);
 
                Cell originCell = originRowI.getCell(j);
                if (originCell != null) {
                    CellType originCellType = originCell.getCellType();
                    if (originCellType == CellType.STRING) {
                        newCell.setCellValue(originCell.getStringCellValue());
                    } else if (originCellType == CellType.NUMERIC) {
                        newCell.setCellValue(originCell.getNumericCellValue());
                    }
                }
 
            }
        }
 
        return newBook;
    }
 
    /**
     * {@code sheet} 添加合并单元格的区域. 合并方式是按列方向合并
     *
     * @param sheet    需要添加合并区域的 sheet 页
     * @param rowStart 合并区域起始行 index
     * @param rowEnd   合并区域终止行 index
     * @param column   合并区域列
     */
    public static void addMergeAddress(Sheet sheet, int rowStart, int rowEnd, int column) {
        if (rowStart < rowEnd) {
            CellRangeAddress mergeAddress = new CellRangeAddress(rowStart, rowEnd, column, column);
            sheet.addMergedRegion(mergeAddress);
        }
    }
    
    /**
     * 添加合并单元格的区域. 合并方式是按列方向合并
     *
     * @param sheet    需要添加合并区域的 sheet 页
     * @param rowStart 合并区域起始行 index
     * @param rowEnd   合并区域终止行 index
     * @param begCol   合并区域起始列
     * @param enfCol   合并区域结束列
     */
    public static void addMergeAddress(Sheet sheet, int rowStart, int rowEnd, int begCol, int enfCol) {
        for (int column = begCol; column < enfCol; column++) {
            if (rowStart < rowEnd) {
                CellRangeAddress mergeAddress = new CellRangeAddress(rowStart, rowEnd, column, column);
                sheet.addMergedRegion(mergeAddress);
            }
        }
    }
 
    /**
     * 获取sheet {@code rowIndex} 行, {@code columnIndex} 列的 String 值
     *
     * @param newSheet    sheet 页
     * @param rowIndex    行索引
     * @param columnIndex 列索引
     * @return 指定单元格的值, 如果单元格为 blank, return null
     */
    public static String getCellStringValue(Sheet newSheet, int rowIndex, int columnIndex) {
        Cell cell = newSheet.getRow(rowIndex).getCell(columnIndex);
        CellType cellType = cell.getCellType();
        if (cellType == CellType.BLANK) {
            return null;
        }
        return cell.getStringCellValue();
    }
 
    /**
     * 合并excel 单元格.
     * 合并方式: 列方向合并相同数据的单元格, 空白单元格不合并.
     *
     * @param newBook 要合并的excel
     */
    private static void mergeCell(Workbook newBook) {
        Sheet newSheet = newBook.getSheetAt(0);
 
        //合并区域最大行索引
        int mergeMaxRowIndex = newSheet.getLastRowNum();
        //合并区域最大列索引
        int mergeMaxColumnIndex = newSheet.getRow(0).getLastCellNum() - 1;
 
        //记录单元格的值
        String value = null;
        //合并区域起始行索引
        int rowStart;
 
        //开始合并
        for (int j = 0; j <= mergeMaxColumnIndex; j++) {
            //找出某列, 第一个不空的单元格的行索引, 并将单元格的值赋给 value
            int i0;
            for (i0 = 1; i0 < mergeMaxRowIndex; i0++) {
                value = MergeCellUtil.getCellStringValue(newSheet, i0, j);
                if (StringUtils.isNotBlank(value)) {
                    break;
                }
            }
 
            if (i0 == mergeMaxRowIndex + 1) {
                //整列为空, 不需要合并操作
 
                continue;
            }
 
            rowStart = i0;
            for (int i = i0 + 1; i <= mergeMaxRowIndex; i++) {
 
                //循环到当前的单元格的值
                String cellValue = MergeCellUtil.getCellStringValue(newSheet, i, j);
 
                if (StringUtils.isBlank(cellValue)) {
                    if (StringUtils.isNotBlank(value)) {
                        //当前单元格为一组连续空白区域的第一个空白格
 
                        MergeCellUtil.addMergeAddress(newSheet, rowStart, i - 1, j);
 
                        value = null;
                        rowStart = i;
                    }
                } else {
                    if (StringUtils.isBlank(value)) {
                        //当前单元格为空白区域下第一个不为空的单元格
 
                        value = cellValue;
                        rowStart = i;
                    } else if (!value.equals(cellValue)) {
                        //当前单元格和记录的值不同
 
                        MergeCellUtil.addMergeAddress(newSheet, rowStart, i - 1, j);
 
                        rowStart = i;
                        value = cellValue;
                    }
                }
 
                if (i == mergeMaxRowIndex) {
                    //最后一行
                    MergeCellUtil.addMergeAddress(newSheet, rowStart, i, j);
                }
            }
        }
 
    }
 
}

````







# EasyPoi 

[API文档](https://apidoc.gitee.com/lemur/easypoi/)

## 概况

今天做Excel导出时，发现了一款非常好用的POI框架EasyPoi，其 使用起来简洁明了。现在我们就来介绍下EasyPoi,首先感谢EasyPoi 的开发者  Lemur开源

### easypoi 简介

easypoi 是为了让开发者快速的实现excel，word,pdf的导入导出，基于Apache poi基础上的一个工具包。

## 特性

> 基于注解的导入导出，修改注解就可以修改Excel
> 支持常用的样式自定义
> 基于map可以灵活定义的表头字段
> 支持一对多的导出，导入
> 支持模板的导出，一些常见的标签，自定义标签
> 支持HTML/Excel转换
> 支持word的导出，支持图片，Excel

## 常用注解

### @Excel注解

@Excel 注解是作用到Filed 上面，是对Excel一列的一个描述，这个注解是必须要的注解，其部分属性如下：

![EasyPoi_01](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129214815689.png)


其使用如下，其中orderNum是指定该字段在Excel中的位置，name与Excel中对应的表头单元格的名称

    @Excel(name = "主讲老", orderNum = "1")
    private String name;

### @ExcelCollection 注解

@ExcelCollection 注解表示一个集合，主要针对一对多的导出
比如一个老师对应多个科目，科目就可以用集合表示，作用在一个类型是List的属性上面，属性如下：

![EasyPoi_02](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215026238.png)

其使用如下所示。

```java
@ExcelCollection(name = "学生", orderNum = "4")
    private List<StudentEntity> students;

```



### @ExcelEntity注解

@ExcelEntity注解表示一个继续深入导出的实体，是作用一个类型为实体的属性上面，其属性如下：

![EasyPoi_03](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215051213.png)

其使用如下所示。

```java
   @ExcelEntity(id = "major")
    private TeacherEntity chineseTeacher;

```

​	

### @ExcelIgnore 注解

@ExcelIgnore 注解修饰的字段，表示在导出的时候补导出，被忽略。

### @ExcelTarget 注解

@ExcelTarget注解作用于最外层的对象，描述这个对象的id,以便支持一个对象，可以针对不同导出做出不同处理，其作用在实体类的上，属性如下：

![EasyPoi_04](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215115779.png)

其使用如下：

```java
@ExcelTarget("scoreIssueReqPOJO")
public class ScoreIssueReqPOJO implements java.io.Serializable{}
```



## EasyPOI的使用

### 引入依赖

SSM 项目，引入依赖
如果spring的版本是4.x的话引入的easypoi的版本是3.0.1，如果spring是5.x的话引入easypoi的版本是4.0.0

```xml
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-base</artifactId>
    <version>4.0.0</version>
</dependency>
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-web</artifactId>
    <version>4.0.0</version>
</dependency>
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-annotation</artifactId>
    <version>4.0.0</version>
</dependency>
```



Spring Boot 项目（2.x以上的版本，我demo的版本是2.1.3.RELEASE），引入依赖

```xml
	<dependency>
        <groupId>cn.afterturn</groupId>
        <artifactId>easypoi-spring-boot-starter</artifactId>
        <version>4.0.0</version>
	</dependency>
```



需要注意的是由于easypoi的依赖内部依赖原生的poi，所以，引入了easypoi的依赖之后，需要把原生的poi的依赖删掉

### 注解方式导出Excel

#### 导出测试的demo

​    

```java
@Test
public void testExportExcel() throws Exception {
    List<CourseEntity> courseEntityList = new ArrayList<>();
    CourseEntity courseEntity = new CourseEntity();
    courseEntity.setId("1");
    courseEntity.setName("测试课程");
    TeacherEntity teacherEntity = new TeacherEntity();
    teacherEntity.setName("张老师");
    teacherEntity.setSex(1);
    courseEntity.setMathTeacher(teacherEntity);

    List<StudentEntity> studentEntities = new ArrayList<>();
    for (int i = 1; i <= 2; i++) {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setName("学生" + i);
        studentEntity.setSex(i);
        studentEntity.setBirthday(new Date());
        studentEntities.add(studentEntity);
    }
    courseEntity.setStudents(studentEntities);
    courseEntityList.add(courseEntity);
    Date start = new Date();
    Workbook workbook = ExcelExportUtil.exportExcel( new ExportParams("导出测试", null, "测试"),
            CourseEntity.class, courseEntityList);
    System.out.println(new Date().getTime() - start.getTime());
    File savefile = new File("D:/excel/");
    if (!savefile.exists()) {
        savefile.mkdirs();
    }
    FileOutputStream fos = new FileOutputStream("D:/excel/教师课程学生导出测试.xls");
    workbook.write(fos);
    fos.close();
}
```

#### 导出对应的Bean

##### CourseEntity 类

```java
@ExcelTarget("courseEntity")
public class CourseEntity implements java.io.Serializable {
    /** 主键 */
    private String        id;
    /** 课程名称 */
    @Excel(name = "课程名称", orderNum = "1", width = 25,needMerge = true)
    private String        name;
    /** 老师主键 */
    //@ExcelEntity(id = "major")
    private TeacherEntity chineseTeacher;
    /** 老师主键 */
    @ExcelEntity(id = "absent")
    private TeacherEntity mathTeacher;
    @ExcelCollection(name = "学生", orderNum = "4")
    private List<StudentEntity> students;
```

##### TeacherEntity 类

```java
@Data
public class TeacherEntity {
    /**

   * 学生姓名
     /
         @Excel(name = "教师姓名", height = 20, width = 30, isImportField = "true_st")
         private String name;
         /**
        * 学生性别
          /
              @Excel(name = "教师性别", replace = {"男_1", "女_2"}, suffix = "生", isImportField = "true_st")
              private int sex;
}
```



##### StudentEntity 类



    public class StudentEntity implements java.io.Serializable {
        /**
         * id
         */
        private String        id;
        /**
         * 学生姓名
         */
        @Excel(name = "学生姓名", height = 20, width = 30, isImportField = "true_st")
        private String        name;
        /**
         * 学生性别
         */
        @Excel(name = "学生性别", replace = { "男_1", "女_2" }, suffix = "生", isImportField = "true_st")
        private int           sex;
        
            @Excel(name = "出生日期", exportFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd", isImportField = "true_st", width = 20)
            private Date          birthday;
    
        @Excel(name = "进校日期", exportFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd")
        private Date registrationDate;

#### 导出结果

![EasyPoi_05](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319134714898.png)

##### 关于日期格式化的说明

如果是导出的实体类(就是说这个实体类是对应导出的Excel的)，那么用 @Excel注解的exportFormat属性来格式化日期。如下所示：

```java
@Excel(name = "出生日期", exportFormat = "yyyy-MM-dd HH:mm:ss", width = 20)
```

如果是导入的实体类（就是说这个实体类是对应导入的Excel的），那么用@Excel注解的importFormat属性来格式化日期。如下所示：

```java
   @Excel(name = "添加时间",importFormat =  "yyyy-MM-dd HH:mm:ss",orderNum = "14")
    private Date createTime;
```

@Excel注解的databaseFormat 属性是用于数据库的格式不是日期类型，如datetime时用。



### 注解方式导入Excel

基于注解的导入导出,配置配置上是一样的，只是方式反过来而已。首先让我们来看看 ImportParams类，这个类主要是设置导入参数，例如表格行数，表头行数等等。

#### ImportParams参数介绍

![EasyPoi_06](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129221846393.png)

需要说明的是
  1、titleRows表示的是表格标题行数，如果没有就是0，如果有一个标题就是1，如果是两个标题就2

2. headRows表示的是表头行数，默认是1，如果有两个表头则需要设置2。

#### 导入情形一：有标题有表头

我们有如下格式的Excel需要导入：

![EasyPoi_07](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319134942331.png)

这个Excel有一个标题行，有一个表头行，所以我们有了如下设置：

```java
  ImportParams params = new ImportParams();
		//设置标题的行数，有标题时一定要有
        params.setTitleRows(1);
		//设置表头的行数
        params.setHeadRows(1);
```

##### 导入的demo

```java
@Test
    public void haveTitleTest() {
        ImportParams params = new ImportParams();
		//设置标题的行数，有标题时一定要有
        params.setTitleRows(1);
		//设置表头的行数
        params.setHeadRows(1);
        String file = Thread.currentThread().getContextClassLoader().getResource("haveTitle.xlsx").getFile();
        List<ScoreIssueReqPOJO> list = ExcelImportUtil.importExcel(
                new File(file),
                ScoreIssueReqPOJO.class, params);
        System.out.println("解析到的数据长度是：" + list.size());
        for (ScoreIssueReqPOJO scoreIssueReqPOJO : list) {
                       System.out.println("***********有标题有表头导入的数据是=" + scoreIssueReqPOJO.toString());
        }
    }
```

导入测试的结果是：

![EasyPoi_08](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319135225371.png)



导入情形二：有表头没有标题

只有一个表头没有标题的话，我们ImportParams可以用默认的值。

导入的demo

```java
@Test
    public void notTitleTest() {
        ImportParams params = new ImportParams();
        String file = Thread.currentThread().getContextClassLoader().getResource("notTitle.xlsx").getFile();
        List<ScoreIssueReqPOJO> list = ExcelImportUtil.importExcel(
                new File(file),
                ScoreIssueReqPOJO.class, params);
        System.out.println("解析到的数据长度是：" + list.size());
        for (ScoreIssueReqPOJO scoreIssueReqPOJO : list) {
           System.out.println("***********有表头没有标题导入的数据是=" + scoreIssueReqPOJO.toString());
        }
    }
```


导入结果如下：

![EasyPoi_10](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319135341203.png)

导入实体Bean配置

```java
public class ScoreIssueReqPOJO implements java.io.Serializable{
    /**
     * 用户手机号
     */
    @Excel(name = "个人用户手机号*",width = 14)
    private String mobile;
    /**
     * 企业用户税号
     */
    @Excel(name = "企业用户税号*",width = 20)
    private String taxNum;
    /**
     * 公司名称或者用户名
     */
    @Excel(name = "名称",width = 20)
    @Length(max =50 ,message = "名称过长")
    private String realname;
    /**
     * 积分数量
     * isStatistics 自动统计数据
     */
    @Excel(name = "积分数量*")
    @NotNull(message = "积分数量不能为空")
    private String scoreNum;
    /**
     * 平台类型
     */
    @Excel(name = "业务代码")
    @Length(max =2 ,message = "业务代码过长，最长2个字符（必须由诺诺网分配，请勿乱填）")
    private String platform;
    /**
     * 备注
     */
    @Excel(name = "备注")
    @Length(max =120 ,message = "备注过长,最长120个字符")
    private String typeContent;
}
```



### Excel导入校验

EasyPoi的校验使用也很简单,在导入对象上加上通用的校验规则或者这定义的这个看你用的哪个实现
然后params.setNeedVerfiy(true);配置下需要校验就可以了

看下具体的代码

```java
/**
     * Email校验
     */
    @Excel(name = "Email", width = 25)
    private String email;
    /**
     * 最大
     */
    @Excel(name = "Max")
    @Max(value = 15,message = "max 最大值不能超过15" ,groups = {ViliGroupOne.class})
    private int    max;
    /**
     * 最小
     */
    @Excel(name = "Min")
    @Min(value = 3, groups = {ViliGroupTwo.class})
    private int    min;
    /**
     * 非空校验
     */
    @Excel(name = "NotNull")
    @NotNull
    private String notNull;
    /**
     * 正则校验
     */
    @Excel(name = "Regex")
    @Pattern(regexp = "[\u4E00-\u9FA5]*", message = "不是中文")
    private String regex;
```



使用方式就是在导入时设置needVerfiy属性为true。导入的demo如下所示：

```java
@Test
    public void basetest() {
        try {
            ImportParams params = new ImportParams();
            params.setNeedVerfiy(true);
            params.setVerfiyGroup(new Class[]{ViliGroupOne.class});
            ExcelImportResult<ExcelVerifyEntity> result = ExcelImportUtil.importExcelMore(
                new File(PoiPublicUtil.getWebRootPath("import/verfiy.xlsx")),
                ExcelVerifyEntity.class, params);
            FileOutputStream fos = new FileOutputStream("D:/excel/ExcelVerifyTest.basetest.xlsx");
            result.getWorkbook().write(fos);
            fos.close();
            for (int i = 0; i < result.getList().size(); i++) {
                System.out.println(ReflectionToStringBuilder.toString(result.getList().get(i)));
            }
            Assert.assertTrue(result.getList().size() == 1);
            Assert.assertTrue(result.isVerfiyFail());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
```



#### 导入结果ExcelImportResult

导入之后返回一个ExcelImportResult 对象,比我们平时返回的list多了一些元素



    /**
         * 结果集
         */
    private List<T>  list;
    /**
     * 是否存在校验失败
     */
    private boolean  verfiyFail;
    
    /**
     * 数据源
     */
    private Workbook workbook;

一个是集合,是一个是是否有校验失败的数据,一个原本的文档,但是在文档后面追加了错误信息

注意,这里的list,有两种返回

> 一种是只返回正确的数据
> 一种是返回全部的数据,但是要求这个对象必须实现IExcelModel接口,如下
>
> ```java
> IExcelModel
> 
> public class ExcelVerifyEntityOfMode extends ExcelVerifyEntity implements IExcelModel {
>  private String errorMsg;
> 
>  @Override
>  public String getErrorMsg() {
>      return errorMsg;
>  }
> 
>  @Override
>  public void setErrorMsg(String errorMsg) {
>      this.errorMsg = errorMsg;
>  }
> }
> ```
>
> 




##### IExcelDataModel

###### 获取错误数据的行号



    public interface IExcelDataModel {
        /**
        * 获取行号
        * @return
        */
        public int getRowNum();
    
        /**
        *  设置行号
        * @param rowNum
        */
        public void setRowNum(int rowNum);
    }



###### 需要对象实现这个接口

每行的错误数据也会填到这个错误信息中,方便用户后面自定义处理
看下代码

```java
@Test
    public void baseModetest() {
        try {
            ImportParams params = new ImportParams();
            params.setNeedVerfiy(true);
            ExcelImportResult<ExcelVerifyEntityOfMode> result = ExcelImportUtil.importExcelMore(
                    new FileInputStream(new File(PoiPublicUtil.getWebRootPath("import/verfiy.xlsx"))),
                ExcelVerifyEntityOfMode.class, params);
            FileOutputStream fos = new FileOutputStream("D:/excel/baseModetest.xlsx");
            result.getWorkbook().write(fos);
            fos.close();
            for (int i = 0; i < result.getList().size(); i++) {
                System.out.println(ReflectionToStringBuilder.toString(result.getList().get(i)));
            }
            Assert.assertTrue(result.getList().size() == 4);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
        }
    }
```



### 定制化修改

有时候，我们需要定制化一些信息，比如，导出Excel时，我们需要统一Excel的字体的大小，字型等。我们可以通过实现**IExcelExportStyler**接口或者继承**ExcelExportStylerDefaultImpl**类来实现。如下所示：我们定义了一个ExcelStyleUtil工具类继承了ExcelExportStylerDefaultImpl（样式的默认实现类）,并且将列头，标题，单元格的字体都设置为了宋体。



    public class ExcelStyleUtil extends ExcelExportStylerDefaultImpl {
        public ExcelStyleUtil(Workbook workbook) {
            super(workbook);
        }
        /**
         * 标题样式
         */	
        @Override
        public CellStyle getTitleStyle(short color) {
            CellStyle cellStyle = super.getTitleStyle(color);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 间隔行的样式
         */
        @Override
        public CellStyle stringSeptailStyle(Workbook workbook, boolean isWarp) {
            CellStyle cellStyle = super.stringSeptailStyle(workbook, isWarp);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 列表头样式
         */
        @Override
        public CellStyle getHeaderStyle(short color) {
            CellStyle cellStyle =  super.getHeaderStyle(color);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 单元格的样式
         */
        @Override
        public CellStyle stringNoneStyle(Workbook workbook, boolean isWarp) {
            CellStyle cellStyle = super.stringNoneStyle(workbook, isWarp);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
    
        /**
         * 字体样式
         *
         * @param size   字体大小
         * @param isBold 是否加粗
         * @return
         */
        private Font getFont(Workbook workbook, int size, boolean isBold) {
            Font font = workbook.createFont();
            //字体样式
            font.setFontName("宋体");
            //是否加粗
            font.setBold(isBold);
            //字体大小
            font.setFontHeightInPoints((short) size);
            return font;
        }
    }


然后就是对ExcelExportUtil类进行包装，如下所示：




    /**
     *
     */
    private static final Integer EXPORT_EXCEL_MAX_NUM = 20000;
    
    /**
     * 获取导出的Workbook对象
     *
     * @param sheetName 页签名
     * @param clazz     类对象
     * @param list      导出的数据集合
     * @return
     * @author xiagwei
     * @date 2020/2/10 4:45 PM
     */
    public static Workbook getWorkbook(String sheetName, Class clazz, List<?> list) {
        //判断数据是否为空
        if (CollectionUtils.isEmpty(list)) {
            log.info("***********导出数据行数为空!");
            list = new ArrayList<>();
        }
        if (list.size() > EXPORT_EXCEL_MAX_NUM) {
            log.info("***********导出数据行数超过:" + EXPORT_EXCEL_MAX_NUM + "条,无法导出、请添加导出条件!");
            list = new ArrayList<>();
        }
        log.info("***********"+sheetName+"的导出数据行数为"+list.size()+"");
        //获取导出参数
        ExportParams exportParams = new ExportParams();
        //设置导出样式
        exportParams.setStyle(ExcelStyleUtil.class);
        //设置sheetName
        exportParams.setSheetName(sheetName);
        //输出workbook流
        return ExcelExportUtil.exportExcel(exportParams, clazz, list);
    }


使用的话，我们只需要获取需要导出的数据，然后调用OfficeExportUtil的getWorkbook方法。

总结

**本文主要介绍了EasyPOI的使用和相关属性，EasyPOI使用起来还是蛮简单的。但是有个缺点是导入导出大批量数据时性能没那么好。**

