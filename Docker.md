## Docker介绍

### 什么是 Docker？

说实话关于 Docker 是什么并不太好说，下面我通过四点向你说明 Docker 到底是个什么东西。

- Docker 是世界领先的软件容器平台，基于 **Go 语言** 进行开发实现。
- Docker 能够自动执行重复性任务，例如搭建和配置开发环境，从而解放开发人员。
- 用户可以方便地创建和使用容器，把自己的应用放入容器。容器还可以进行版本管理、复制、分享、修改，就像管理普通的代码一样。
- Docker 可以**对进程进行封装隔离，属于操作系统层面的虚拟化技术。** 由于隔离的进程独立于宿主和其它的隔离的进程，因此也称其为容器。

官网地址：https://www.docker.com/ 。

![认识容器](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/container.png)

### 为什么要用 Docker?

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app），更重要的是容器性能开销极低。

传统的开发流程中，我们的项目通常需要使用 MySQL、Redis、FastDFS 等等环境，这些环境都是需要我们手动去进行下载并配置的，安装配置流程极其复杂，而且不同系统下的操作也不一样。

Docker 的出现完美地解决了这一问题，我们可以在容器中安装 MySQL、Redis 等软件环境，使得应用和环境架构分开，它的优势在于：

1. 一致的运行环境，能够更轻松地迁移
2. 对进程进行封装隔离，容器与容器之间互不影响，更高效地利用系统资源
3. 可以通过镜像复制多个一致的容器

另外，[《Docker 从入门到实践》open in new window](https://yeasy.gitbook.io/docker_practice/introduction/why) 这本开源书籍中也已经给出了使用 Docker 的原因。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210412220015698.png)

## Docker 的安装

### Windows

接下来对 Docker 进行安装，以 Windows 系统为例，访问 Docker 的官网：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-4e3146984adaee0067bdc5e9b1d757bb479.png)

然后点击`Get Started`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-96adfbfebe3e59097c8ba25e55f68ba7908.png)

在此处点击`Download for Windows`即可进行下载。

如果你的电脑是`Windows 10 64位专业版`的操作系统，则在安装 Docker 之前需要开启一下`Hyper-V`，开启方式如下。打开控制面板，选择程序：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-73ce678240826de0f49225250a970b4d205.png)

点击`启用或关闭Windows功能`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-9c7a96c332e56b9506325a1f1fdb608a659.png)

勾选上`Hyper-V`，点击确定即可：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-aad4a58c5e917f7185908d6320d7fb06861.png)

完成更改后需要重启一下计算机。

开启了`Hyper-V`后，我们就可以对 Docker 进行安装了，打开安装程序后，等待片刻点击`Ok`即可：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-62ac3c9184bdc21387755294613ff5054c6.png)

安装完成后，我们仍然需要重启计算机，重启后，若提示如下内容：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-3585c7d6a4632134ed925493a7d43e14a43.png)

它的意思是询问我们是否使用 WSL2，这是基于 Windows 的一个 Linux 子系统，这里我们取消即可，它就会使用我们之前勾选的`Hyper-V`虚拟机。

因为是图形界面的操作，这里就不介绍 Docker Desktop 的具体用法了。

### Mac

直接使用 Homebrew 安装即可



```bash
brew install --cask docker
```

### Linux

下面来看看 Linux 中如何安装 Docker，这里以 CentOS7 为例。

在测试或开发环境中，Docker 官方为了简化安装流程，提供了一套便捷的安装脚本，执行这个脚本后就会自动地将一切准备工作做好，并且把 Docker 的稳定版本安装在系统中。



```bash
curl -fsSL get.docker.com -o get-docker.sh
```



```bash
sh get-docker.sh --mirror Aliyun
```



安装完成后直接启动服务：



```bash
systemctl start docker
```

推荐设置开机自启，执行指令：



```bash
systemctl enable docker
```

## Docker 中的几个概念

在正式学习 Docker 之前，我们需要了解 Docker 中的几个核心概念：

### 镜像

镜像就是一个只读的模板，镜像可以用来创建 Docker 容器，一个镜像可以创建多个容器

### 容器

容器是用镜像创建的运行实例，Docker 利用容器独立运行一个或一组应用。它可以被启动、开始、停止、删除，每个容器都是相互隔离的、保证安全的平台。 可以把容器看作是一个简易的 Linux 环境和运行在其中的应用程序。容器的定义和镜像几乎一模一样，也是一堆层的统一视角，唯一区别在于容器的最上面那一层是可读可写的

### 仓库

仓库是集中存放镜像文件的场所。仓库和仓库注册服务器是有区别的，仓库注册服务器上往往存放着多个仓库，每个仓库中又包含了多个镜像，每个镜像有不同的标签。 仓库分为公开仓库和私有仓库两种形式，最大的公开仓库是 DockerHub，存放了数量庞大的镜像供用户下载，国内的公开仓库有阿里云、网易云等

### 总结

通俗点说，一个镜像就代表一个软件；而基于某个镜像运行就是生成一个程序实例，这个程序实例就是容器；而仓库是用来存储 Docker 中所有镜像的。

其中仓库又分为远程仓库和本地仓库，和 Maven 类似，倘若每次都从远程下载依赖，则会大大降低效率，为此，Maven 的策略是第一次访问依赖时，将其下载到本地仓库，第二次、第三次使用时直接用本地仓库的依赖即可，Docker 的远程仓库和本地仓库的作用也是类似的。

## Docker 初体验

下面我们来对 Docker 进行一个初步的使用，这里以下载一个 MySQL 的镜像为例`(在CentOS7下进行)`。

和 GitHub 一样，Docker 也提供了一个 DockerHub 用于查询各种镜像的地址和安装教程，为此，我们先访问 DockerHub：[https://hub.docker.com/open in new window](https://hub.docker.com/)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-37d083cc92fe36aad829e975646b9d27fa0.png)

在左上角的搜索框中输入`MySQL`并回车：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-ced37002391a059754def9b3a6c2aa4e342.png)

可以看到相关 MySQL 的镜像非常多，若右上角有`OFFICIAL IMAGE`标识，则说明是官方镜像，所以我们点击第一个 MySQL 镜像：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-48ba3fdc99c93a96e18b929195ca8e93c6c.png)

右边提供了下载 MySQL 镜像的指令为`docker pull MySQL`，但该指令始终会下载 MySQL 镜像的最新版本。

若是想下载指定版本的镜像，则点击下面的`View Available Tags`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-ed601649275c6cfe65bbe422b463c263a64.png)

这里就可以看到各种版本的镜像，右边有下载的指令，所以若是想下载 5.7.32 版本的 MySQL 镜像，则执行：



```bash
docker pull MySQL:5.7.32
```



然而下载镜像的过程是非常慢的，所以我们需要配置一下镜像源加速下载，访问`阿里云`官网：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-0a46effd262d3db1b613a0db597efa31f34.png)

点击控制台：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-60f198e0106be6b43044969d2900272504f.png)

然后点击左上角的菜单，在弹窗的窗口中，将鼠标悬停在产品与服务上，并在右侧搜索容器镜像服务，最后点击容器镜像服务：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-2f6706a979b405dab01bc44a29bb6b26fc4.png)

点击左侧的镜像加速器，并依次执行右侧的配置指令即可。



```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://679xpnpz.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



### **使用官方安装脚本自动安装**

安装命令如下：

```
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

也可以使用国内 daocloud 一键安装命令：

```
curl -sSL https://get.daocloud.io/docker | sh
```

------

### **手动安装**	

#### **卸载旧版本**

较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

```
$ sudo yum remove docker \
         docker-client \
         docker-client-latest \
         docker-common \
         docker-latest \
         docker-latest-logrotate \
         docker-logrotate \
         docker-engine
```

#### **安装 Docker Engine-Community**

#### **使用 Docker 仓库进行安装**

在新主机上首次安装 Docker Engine-Community 之前，需要设置 Docker 仓库。之后，您可以从仓库安装和更新 Docker。

**设置仓库**

安装所需的软件包。yum-utils 提供了 yum-config-manager ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。

$ **sudo** **yum install** -y yum-utils \
 device-mapper-persistent-data \
 lvm2

使用以下命令来设置稳定的仓库。

#### **使用官方源地址（比较慢）**

```
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

可以选择国内的一些源地址：

#### **阿里云**

```
$ sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

#### **清华大学源**

```
$ sudo yum-config-manager \
    --add-repo \
    https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
```

#### **安装 Docker Engine-Community**

安装最新版本的 Docker Engine-Community 和 containerd，或者转到下一步安装特定版本：

```
$ sudo yum install docker-ce docker-ce-cli containerd.io
```

如果提示您接受 GPG 密钥，请选是。

> **有多个 Docker 仓库吗？**
>
> 如果启用了多个 Docker 仓库，则在未在 yum install 或 yum update 命令中指定版本的情况下，进行的安装或更新将始终安装最高版本，这可能不适合您的稳定性需求。

Docker 安装完默认未启动。并且已经创建好 docker 用户组，但该用户组下没有用户。

**要安装特定版本的 Docker Engine-Community，请在存储库中列出可用版本，然后选择并安装：**

1、列出并排序您存储库中可用的版本。此示例按版本号（从高到低）对结果进行排序。

$ **yum list** docker-ce --showduplicates **|** **sort** -r

docker-ce.x86_64  3:18.09.1-3.el7           docker-ce-stable
docker-ce.x86_64  3:18.09.0-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.1.ce-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.0.ce-3.el7           docker-ce-stable

2、通过其完整的软件包名称安装特定版本，该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。例如：docker-ce-18.09.1。

```
$ sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
```

启动 Docker。

```
$ sudo systemctl start docker
```

通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

```
$ sudo docker run hello-world
```

#### **卸载 docker**

#### **启动并加入开机启动**

```
$ sudo systemctl start docker
$ sudo systemctl enable docker
```

**删除安装包：**

```
yum remove docker-ce
```

**删除镜像、容器、配置文件等内容：**

```
rm -rf /var/lib/docker
```

#### 镜像加速

```
科大镜像：https://docker.mirrors.ustc.edu.cn/
网易：https://hub-mirror.c.163.com/
阿里云：https://<你的ID>.mirror.aliyuncs.com
七牛云加速器：https://reg-mirror.qiniu.com
```



对于使用 systemd 的系统，请在 /etc/docker/daemon.json 中写入如下内容（如果文件不存在请新建该文件）：

```json
{"registry-mirrors":["https://reg-mirror.qiniu.com/"]}
```

之后重新启动服务：

```shell
systemctl daemon-reload
systemctl restart docker
```



## **Docker 基本命令**

### 1、启动或停止docker命令

```shell
sudo service docker start 启动docker centos6.x的命令sudo service docker restart 重启docker centos6.x的命令sudo service docker stop 关闭docker centos6.x的命令sudo systemctl start docker 启动docker centos7.x 命令sudo systemctl restart  docker 重启docker centos7.x 命令sudo systemctl stop docker 关闭docker centos7.x 命令
```

### 2、查看docker版本

```
docker -v
```

### 3、查看docker下载的镜像

```
docker images
```

![9a0eed828034a8c16a8d796922353ca3.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/9a0eed828034a8c16a8d796922353ca3.png)

### 4、設置docker开机自启动

```
systemctl enable docker
```

### 5、查看容器启动日志

```shell
①、docker logs -f -t --tail 10 smartbus #实时查看docker容器名为smartbus的最后10行日志
②、docker logs -f -t --since="2020-08-06" --tail=100 smartbus #查看指定时间后的日志，只显示最后100行
③、docker logs --since 30m smartbus #查看最近30分钟的日志
④、docker logs -t --since="2020-08-06T13:13:13" smartbus #查看某时间之后的日志
⑤、docker logs -t --since="2020-08-01T13:13:13" --until "2020-08-06 13:13:13" smartbus #查看某时间段的日志⑥、docker logs -f -t --since="2020-08-01" smartbus | grep error >>logs_error.txt #将错误日志写入文件
```

### 6、进入容器

```shell
docker exec -it [容器id] /bin/bash
```

从docker容器进入镜像容器后台例如进行mysql容器,例如：docker exec -it b30062adc08c /bin/bash 这样子就可以操作mysql容器

### 7、查看docker正在运行的容器

```
docker ps
```

![9e89323794a8b5109c3ec8d9284ee76d.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/9e89323794a8b5109c3ec8d9284ee76d.png)

### 8、停止容器

```
docker stop 
```

### 9、重启容器

```
docker restart 
```

### 10、拉取远程镜像

```
docker pull 镜像名<:tags># 从远程仓库抽取镜像,<:tags>是指镜像的版本,不加就是下载最新的镜像 例如：docker pull tomcat:7
```

### 11、创建容器，并且启动容器

```
docker run 镜像名<:tags> #如果不需要启动容器的话，直接用docker create 镜像名<:tags>
```

### 12、删除指定容器

```
docker rm  容器id#如果这个容器还在运行的情况下，加上-f这个参数代表强制删除
```

### 13、删除指定版本的镜像

```
docker rmi  镜像名:#加-f就是强制删除，即使这个镜像有对应的容器
```

### 14、查看所有的镜像和容器存储在宿主机的哪个位置 默认是存储在这个地址 cd /var/lib/docker

![0a3a5abbef058d5d4259a30922fe4693.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/0a3a5abbef058d5d4259a30922fe4693.png)

### 15、删除docker所有容器

```
docker rm $(docker ps -aq)
```

![c7fd62c92e74d54f23d8d7b2b25e1d46.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/c7fd62c92e74d54f23d8d7b2b25e1d46.png)



Docker 需要频繁地操作相关的镜像，所以我们先来了解一下 Docker 中的镜像指令。

若想查看 Docker 中当前拥有哪些镜像，则可以使用 `docker images` 命令。



```bash
[root@izrcf5u3j3q8xaz ~]# docker images
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
MySQL         5.7.32    f07dfa83b528   11 days ago     448MB
tomcat        latest    feba8d001e3f   2 weeks ago     649MB
nginx         latest    ae2feff98a0c   2 weeks ago     133MB
hello-world   latest    bf756fb1ae65   12 months ago   13.3kB
```

其中`REPOSITORY`为镜像名，`TAG`为版本标志，`IMAGE ID`为镜像 id(唯一的)，`CREATED`为创建时间，注意这个时间并不是我们将镜像下载到 Docker 中的时间，而是镜像创建者创建的时间，`SIZE`为镜像大小。

该指令能够查询指定镜像名：



```bash
docker image MySQL
```

若如此做，则会查询出 Docker 中的所有 MySQL 镜像：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images MySQL
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
MySQL        5.6       0ebb5600241d   11 days ago     302MB
MySQL        5.7.32    f07dfa83b528   11 days ago     448MB
MySQL        5.5       d404d78aa797   20 months ago   205MB
```

该指令还能够携带`-q`参数：`docker images -q` ， `-q`表示仅显示镜像的 id：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images -q
0ebb5600241d
f07dfa83b528
feba8d001e3f
d404d78aa797
```

若是要下载镜像，则使用：



```bash
docker pull MySQL:5.7
```

`docker pull`是固定的，后面写上需要下载的镜像名及版本标志；若是不写版本标志，而是直接执行`docker pull MySQL`，则会下载镜像的最新版本。

一般在下载镜像前我们需要搜索一下镜像有哪些版本才能对指定版本进行下载，使用指令：



```bash
docker search MySQL
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-559083ae80e7501e86e95fbbad25b6d571a.png)

不过该指令只能查看 MySQL 相关的镜像信息，而不能知道有哪些版本，若想知道版本，则只能这样查询：



```bash
docker search MySQL:5.5
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-68394e25f652964bb042571151c5e0fd2e9.png)

若是查询的版本不存在，则结果为空：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-abfdd51b9ad2ced3711268369f52b077b12.png)

删除镜像使用指令：



```bash
docker image rm MySQL:5.5
```

若是不指定版本，则默认删除的也是最新版本。

还可以通过指定镜像 id 进行删除：



```bash
docker image rm bf756fb1ae65
```

然而此时报错了：



```bash
[root@izrcf5u3j3q8xaz ~]# docker image rm bf756fb1ae65
Error response from daemon: conflict: unable to delete bf756fb1ae65 (must be forced) - image is being used by stopped container d5b6c177c151
```

这是因为要删除的`hello-world`镜像正在运行中，所以无法删除镜像，此时需要强制执行删除：



```bash
docker image rm -f bf756fb1ae65
```

该指令将镜像和通过该镜像执行的容器全部删除，谨慎使用。

Docker 还提供了删除镜像的简化版本：`docker rmi 镜像名:版本标志` 。

此时我们即可借助`rmi`和`-q`进行一些联合操作，比如现在想删除所有的 MySQL 镜像，那么你需要查询出 MySQL 镜像的 id，并根据这些 id 一个一个地执行`docker rmi`进行删除，但是现在，我们可以这样：



```bash
docker rmi -f $(docker images MySQL -q)
```

首先通过`docker images MySQL -q`查询出 MySQL 的所有镜像 id，`-q`表示仅查询 id，并将这些 id 作为参数传递给`docker rmi -f`指令，这样所有的 MySQL 镜像就都被删除了。



### 16、拷贝文件



##### 1、从容器里面拷文件到宿主机？

   答：在宿主机里面执行以下命令   **docker cp 容器名：要拷贝的文件在容器里面的路径    要拷贝到宿主机的相应路径** 

   示例： 假设容器名为testtomcat,要从容器里面拷贝的文件路为：/usr/local/tomcat/webapps/test/js/test.js, 现在要将test.js从容器里面拷到宿主机的/opt路径下面，在宿主机上面执行命令

```
docker cp testtomcat：/usr/local/tomcat/webapps/test/js/test.js /opt
```

 

##### 2、从宿主机拷文件到容器里面		

   答：在宿主机里面执行如下命令 **docker cp 要拷贝的文件路径 容器名：要拷贝到容器里面对应的路径**

​    示例：假设容器名为testtomcat,现在要将宿主机/opt/test.js文件拷贝到容器里面的/usr/local/tomcat/webapps/test/js路径下面，在宿主机上面执行如下命令   

```
docker cp /opt/test.js testtomcat：/usr/local/tomcat/webapps/test/js
```

### 重启docker服务



```sh
#systemcsystemctl ⽅式
#守护进程重启
sudo systemctl restart docker
#关闭docker
sudo systemctl stop docker
#service ⽅式
#重启docker服务
sudo service docker restart
#关闭docker
```

sudo service docker stop
--------------------------------------------------------

作者：铁头采蓝知识集
链接：https://wenku.baidu.com/view/0b6377901937f111f18583d049649b6648d70969.html
来源：百度文库
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## Docker 安装 Oracle



### 1、拉取镜像

> docker pull registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g

镜像详情：https://dev.aliyun.com/detail.html?spm=5176.1972343.2.8.E6Cbr1&repoId=1969

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325141.jpg)

由于镜像我已经拉取，所以此处显示已存在，查看镜像信息

> docker iamges

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325142.jpg)

### 2、创建并容器信息

> docker run -d -p 1521:1521 --name oracle_11g registry.aliyuncs.com/helowin/oracle_11g

由于此处我的容器已经创建（命令如想，容器名称 oracle_11g）此处我直接启动即可。

> docker start oracle_11g

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325143.jpg)

### 3、进入控制台设置用户信息

> docker exec -it oracle_11g bash

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325144.jpg)

登录sqlplus，此处发现sqlplus命令不可用，所以需要进行相关配置，操作步骤如下：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325145.jpg)

（1）、切换到root用户模式下

> su root

输入密码helowin

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325146.jpg)

(2)、编辑profile文件配置ORACLE环境变量

vi /etc/profile 并在文件最后添加如下命令

> export ORACLE_HOME=/home/oracle/app/oracle/product/11.2.0/dbhome_2
>
> export ORACLE_SID=helowin
>
> export PATH=$ORACLE_HOME/bin:$PATH

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325147.jpg)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325148.jpg)

>\#刷新配置 source /etc/profile

推出并保存。

（3）、软件连接

> ln -s $ORACLE_HOME/bin/sqlplus /usr/bin

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325149.jpg)

因为我已经创建过所以包标志已存在。

（4）、切换到oracle 用户

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325150.jpg)

登录sqlplus并修改sys、system用户密码

> sqlplus /nolog
>
> conn /as sysdba

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325153.jpg)

接着执行下面命令

> alter user system identified by oracle;
>
> alter user sys identified by oracle;
>
> ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325154.jpg)

### 4、登录验证

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325155.jpg)

**登录成功**

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325156.jpg)

#### 5、提交修改

​	docker commit 容器名称或ID 新的镜像名称:版本









## Docker 安装 MySQL



MySQL 是世界上最受欢迎的开源数据库。凭借其可靠性、易用性和性能，MySQL 已成为 Web 应用程序的数据库优先选择。

### 1、查看可用的 MySQL 版本

访问 MySQL 镜像库地址：https://hub.docker.com/_/mysql?tab=tags 。

可以通过 Sort by 查看其他版本的 MySQL，默认是最新版本 **mysql:latest** 。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql1.png)

你也可以在下拉列表中找到其他你想要的版本：

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql2.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql2.png)

此外，我们还可以用 **docker search mysql** 命令来查看可用版本：

```
$ docker search mysql
NAME                     DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
mysql                    MySQL is a widely used, open-source relati...   2529      [OK]       
mysql/mysql-server       Optimized MySQL Server Docker images. Crea...   161                  [OK]
centurylink/mysql        Image containing mysql. Optimized to be li...   45                   [OK]
sameersbn/mysql                                                          36                   [OK]
google/mysql             MySQL server for Google Compute Engine          16                   [OK]
appcontainers/mysql      Centos/Debian Based Customizable MySQL Con...   8                    [OK]
marvambass/mysql         MySQL Server based on Ubuntu 14.04              6                    [OK]
drupaldocker/mysql       MySQL for Drupal                                2                    [OK]
azukiapp/mysql           Docker image to run MySQL by Azuki - http:...   2                    [OK]
...
```

### 2、拉取 MySQL 镜像

这里我们拉取官方的最新版本的镜像：

```
$ docker pull mysql:latest
```

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql3.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql3.png)

### 3、查看本地镜像

使用以下命令来查看是否已安装了 mysql：

```
$ docker images
```

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql6.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql6.png)

在上图中可以看到我们已经安装了最新版本（latest）的 mysql 镜像。

### 4、运行容器

安装完成后，我们可以使用以下命令来运行 mysql 容器：

```
$ docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

参数说明：

- **-p 3306:3306** ：映射容器服务的 3306 端口到宿主机的 3306 端口，外部主机可以直接通过 **宿主机ip:3306** 访问到 MySQL 的服务。
- **MYSQL_ROOT_PASSWORD=123456**：设置 MySQL 服务 root 用户的密码。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql4.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql4.png)

### 5、安装成功

通过 **docker ps** 命令查看是否安装成功：

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql5.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql5.png)

本机可以通过 root 和密码 123456 访问 MySQL 服务。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql7.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql7.png)

### **问题**

#### 一、无法连接到 docker hub

```
docker: error pulling image configuration: Get https://registry-1.docker.io/v2/library/mysql/: TLS handshake timeout.
```

##### **解决：**

```
systemctl stop docker

echo "DOCKER_OPTS=\"\$DOCKER_OPTS --registry-mirror=http://f2d6cb40.m.daocloud.io\"" | sudo tee -a /etc/default/docker

service docker restart
```

#### **二、docker加载新的镜像后repository和tag名称都为none**

​	**解决：**

```
docker tag [image id] [name]:[版本]
```

#### **三、删除镜像报错**

```
Error response from daemon: conflict: unable to delete
```

**解决：**

当通过该镜像创建的容器未被销毁时，镜像是无法被删除的。为了验证这一点，我们来做个试验。首先，我们通过 `docker pull alpine` 命令，拉取一个最新的 `alpine` 镜像, 然后启动镜像，让其输出 `hello, docker!`:

![Docker run alpine](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271716522566.jpeg)

接下来，我们来删除这个镜像试试：

![Docker 删除镜像](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271728646226.jpeg)

可以看到提示信息，无法删除该镜像，因为有容器正在引用他！同时，这段信息还告诉我们，除非通过添加 `-f` 子命令，也就是强制删除，才能移除掉该镜像！

```bash
docker rmi -f docker.io/alpine
```

但是，我们一般不推荐这样暴力的做法，正确的做法应该是：

1. 先删除引用这个镜像的容器；
2. 再删除这个镜像；

也就是，根据上图中提示的，引用该镜像的容器 ID (`9d59e2278553`), 执行删除命令：

```bash
docker rm 9d59e2278553
```

然后，再执行删除镜像的命令：

```bash
docker rmi 5cb3aa00f899
```

![Docker 删除镜像](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271780037954.jpeg)

这个时候，就能正常删除了！

#### 四、主机无法连接虚拟机库

```
ERROR 1130: Host X.X.X.X is not allowed to connect to this MySQL serve
```

​		默认服务器直能支持本机用户登录，如果需要用工具链接需要给账号权限才行，具体操作如下

​       创建一个用户，xxxxx是用户名 % 代表任意地址都行123456 是密码

```sql
CREATE USER 'XXXXX'@'%' IDENTIFIED BY '123456';
```

​        授权用户，让他具有所有数据库的操作权限

```sql
grant all privileges on *.* to 'XXXXX'@'%';
```

​		授权用户，让他对所有的数据库具有SUPER 的权限

```sql
grant SUPER on *.* to 'XXXXX'@'%';
```

​		刷新权限

```sql
flush privileges;
```

​        这样在链接就好了，

​		如果root用户还不行

​		![image-20210927084510250](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20210927084510250.png)

将Host改为%，代表所有IP可连接

改完刷新权限

#### **五、MySQL 5.7root用户密码修改**

在MySQL 5.7 password字段已从mysql.user表中删除，新的字段名是“authenticalion_string”。

选择数据库

```sql
use mysql;
```

更新root的密码：

```sql
update user set authentication_string=password('新密码') where user='root' and Host='localhost';
```

刷新权限：

```sql
flush privileges;
```



## CentOS 7.0防火墙



CentOS 7.0默认使用的是firewall作为防火墙



查看防火墙状态

```
firewall-cmd --state
```

停止firewall

```
systemctl stop firewalld.service
```

禁止firewall开机启动

```
systemctl disable firewalld.service 
```



## 问题

### docker基础容器中bash: vi: command not found

​	**这是因为vim没有安装。**

```shell
apt-get update
apt-get install vim
```



### CentOS 8 安装 docker 文件冲突

- 卸载旧版本的docker或者docker-engine，最后你下载完的docker可以在/var/lib/docker中找到，然后这个文件夹中包含了images、container、volumes和networks，docker引擎的包叫做docker-ce。

```bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

- 继续别跳步骤，通用方法安装repository，输入下面两条命令。

```bash
sudo yum install -y yum-utils

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

- 如果你想安装nightly或者test repositories，输入前两条命令即可，第三条命令是禁用命令，同时，想要了解nightly、test repository的请戳这个链接看一下[Install Docker Engine | Docker Documentation](https://docs.docker.com/engine/install/)：

```bash
sudo yum-config-manager --enable docker-ce-nightly

sudo yum-config-manager --enable docker-ce-test

sudo yum-config-manager --disable docker-ce-nightly
```

- 在安装docker engine时，我遇到了一个问题就是有多个文件冲突，这时候网上很多说需要先安装lvm2，再继续安装docker engine：

```bash
yum install lvm2 -y
```

- 但是我在安装之后，并没有什么卵用，可以看到错误是和podman文件有关，然后查了一下podman是linux自带的容器，与docker冲突了，这样我们就知道问题出在哪了，只需要移除这个即可：

```bash
rpm -q podman  # 看一下自己的podman版本

yum remove podman  # 移除podman
```

- 正式安装docker，这里还会出现一些安装不上的东西，但这些东西并不影响正常的docker运行，我们在安装命令后加上 --allowerasing，如下图所示：

```bash
//第一种方式，直接安装最新版的docker engine
sudo yum install docker-ce docker-ce-cli containerd.io --allowerasing

//第二种安装指定版本，首先要看一下目前docker engine都有哪些版本，输入下列命令：

yum list docker-ce --showduplicates | sort -r

//然后你就取中间那一列的某一行，自己选一下，比如说3:20.10.0-3.el8，去掉“3：”和“-3.el8”只要“20.10.0”，接着输入命令：

sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io --allowerasing

//注意这里的VERSION_STRING要替换成你选的那个版本号，例如上述“20.10.0”，输入的时候不要加双引号，同时去掉尖括号
```

 