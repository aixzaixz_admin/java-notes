## 常用语句

### mapper里的转义字符

| 特殊字符 | 特殊字符转义一   | 特殊字符转义二  |
| -------- | ---------------- | --------------- |
| >=       | &gt;=            | <![CDATA[>= ]]> |
| <=       | &lt;=            | <![CDATA[<= ]]> |
| !=       | <![CDATA[ <> ]]> | <![CDATA[!= ]]> |

![image-20220516113951563](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/image-20220516113951563.png)

#### **JdbcType和Oracle以及MySQL，相互之间的映射关系**

|          | JdbcType      | Oracle         | MySql              |
| :------- | :------------ | :------------- | :----------------- |
| JdbcType | ARRAY         |                |                    |
| JdbcType | BIGINT        |                | BIGINT             |
| JdbcType | BINARY        |                |                    |
| JdbcType | BIT           |                | BIT                |
| JdbcType | BLOB          | BLOB           | BLOB               |
| JdbcType | BOOLEAN       |                |                    |
| JdbcType | CHAR          | CHAR           | CHAR               |
| JdbcType | CLOB          | CLOB           | 修改为TEXT         |
| JdbcType | CURSOR        |                |                    |
| JdbcType | DATE          | DATE           | DATE               |
| JdbcType | DECIMAL       | DECIMAL        | DECIMAL            |
| JdbcType | DOUBLE        | NUMBER         | DOUBLE             |
| JdbcType | FLOAT         | FLOAT          | FLOAT              |
| JdbcType | INTEGER       | INTEGER        | INTEGER            |
| JdbcType | LONGVARBINARY |                |                    |
| JdbcType | LONGVARCHAR   | LONG VARCHAR   |                    |
| JdbcType | NCHAR         | NCHAR          |                    |
| JdbcType | NCLOB         | NCLOB          |                    |
| JdbcType | NULL          |                |                    |
| JdbcType | NUMERIC       | NUMERIC/NUMBER | NUMERIC/           |
| JdbcType | NVARCHAR      |                |                    |
| JdbcType | OTHER         |                |                    |
| JdbcType | REAL          | REAL           | REAL               |
| JdbcType | SMALLINT      | SMALLINT       | SMALLINT           |
| JdbcType | STRUCT        |                |                    |
| JdbcType | TIME          |                | TIME               |
| JdbcType | TIMESTAMP     | TIMESTAMP      | TIMESTAMP/DATETIME |
| JdbcType | TINYINT       |                | TINYINT            |
| JdbcType | UNDEFINED     |                |                    |
| JdbcType | VARBINARY     |                |                    |
| JdbcType | VARCHAR       | VARCHAR        | VARCHAR            |



### if test 判断字符串

```xml
<if test="sex=='Y'.toString()">
<if test = 'sex== "Y"'>
```

### 判断数组

```xml
<if test="array != null and array.length >0">
```

### 判断List

```xml
<if test="regions != null and regions.size() >0">
```



### 日志配置

```yml
mybatis-plus:
configuration:
log-impl: org.apache.ibatis.logging.stdout.StdOutImpl

mybatis:
configuration:
log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```

### 驼峰命名

```yaml
mybatis-plus:
  configuration:
    mapUnderscoreToCamelCase: true
```



## 问题

### 传入Integer 数据 if 标签判断不通过



```xml
<if test="provincePointNo != null and provincePointNo != ''">   
```



**不能用空字符串来判断，因为空字符串会判断与0相等**



### 分页无效、查询数据慢

```java
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        // 分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.ORACLE);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }
```

将插件配置注入注入容器



### 使用foreach批量更新数据报无效字符错误

改动前：

```xml
<update id="updateByECBoxIdBatch" parameterType="java.util.List">
    <foreach collection="list" item="item" index="index" open="" close=";" separator=";">
        update EC_ENTRUST_BOX
        set REALMONEY = #{item.money,jdbcType=DECIMAL}
        where BOX_ID = #{item.boxId,jdbcType=VARCHAR}
        and ENTRUST_MONEY_ID = #{item.ecEntrustMoneyId,jdbcType=VARCHAR}
    </foreach>
</update>
```

后台执行的SQL：

```xml
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;
123456789101112131415
```

报错：无效字符

==========================================================
改动后：（加了begin，end）

```xml
<update id="updateByECBoxIdBatch" parameterType="java.util.List">
    <foreach collection="list" item="item" index="index" open="begin" close=";end;" separator=";">
        update EC_ENTRUST_BOX
        set REALMONEY = #{item.money,jdbcType=DECIMAL}
        where BOX_ID = #{item.boxId,jdbcType=VARCHAR}
        and ENTRUST_MONEY_ID = #{item.ecEntrustMoneyId,jdbcType=VARCHAR}
    </foreach>
</update>
12345678
```

后台执行的SQL：

```xml
begin
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;
update EC_ENTRUST_BOX
set REALMONEY = ?
where BOX_ID = ?
and ENTRUST_MONEY_ID = ?
;end;
```