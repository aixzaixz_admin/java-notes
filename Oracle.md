## 常用语句

### 查询锁表

```sql
select t2.username,
       t2.sid,
       t2.serial#,
       t3.object_name,
       t2.OSUSER,
       t2.MACHINE,
       t2.PROGRAM,
       t2.LOGON_TIME,
       t2.COMMAND,
       t2.LOCKWAIT,
       t2.SADDR,
       t2.PADDR,
       t2.TADDR,
       t2.SQL_ADDRESS,
       t1.LOCKED_MODE
  from v$locked_object t1, v$session t2, dba_objects t3
 where t1.session_id = t2.sid
   and t1.object_id = t3.object_id
 order by t2.logon_time;
```

### 解决锁表

```sql
alter system kill session 'sid,seial#';
```

### 创建表空间

```sql
create tablespace TEST_DATA 
logging 
datafile 'E:\oracle\oradata\orcl\TEST_DATA.dbf' 
size 50m 
autoextend on 
next 50m maxsize UNLIMITED 
extent management local; 
```

### 创建临时表空间

```sql
create temporary tablespace TEST_TEMP
tempfile 'E:\oracle\oradata\orcl\TEST_TEMP.dbf' 
size 50m 
autoextend on 
next 50m maxsize UNLIMITED 
extent management local;
```

### 创建用户

```sql
create user test_username identified by 123456 
default tablespace TEST_DATA 
temporary tablespace TEST_TEMP; 
```

### 分配权限

```sql
grant connect,resource,dba to test_username; 
```

### 创建同义词

#### **一、定义**

同义词顾名思义，是数据库方案对象的一个别名。这里的数据库方案对象指表、视图、序列、存储过程、包等。

> 相当于alias(别名),比如把user1.table1在user2中建一个同义词table1
> create synonym table1 for user1.table1;
> 这样当你在user2中查select * from table1时就相当于查select * from user1.table1;

#### **二、同义词的好处**

>1、不占内存空间，节省大量的数据库空间
>2、简化了数据库对象的访问
>3、提高了数据库对象访问的安全性
>4、扩展的数据库的使用范围，能够在不同的数据库用户之间实现无缝交互;同义词可以创建在不同一个数据库服务器上，通过网络实现连接

#### **三、创建同义词语法**

```
create public synonym table_a for user.table_a;
```

#### **四、同义词的删除语法**

因为同义词也是对象 ，删除语法统表一样

```
drop public synonym table_a;
```

#### **五、扩展**

如果要访问不同数据库下或者不同用户下的表table_a，当然也可以使用同义词，但需要先创建一个Database Link(数据库连接)来扩展访问，然后在使用如下语句创建数据库同义词：

```
create synonym table_a for table_a @DB_Link;
```

#### **六、实例演示**

##### **1、同库不同用户演示**

如图所示 在ctdev用户下有 表employees
![image-20220617100001741](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100001741.png)
但是在ctcheck用户没有这张表，在ctcheck用户下如图不能访问表employees；

![image-20220617100014988](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100014988.png)

创建同义词

```
create public synonym employees for ctdev.employees;
```

创建好后就可以在ctcheck用户下访问ctdev用户的表employees
![image-20220617100051460](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100051460.png)

##### **2、跨库同义词演示**

在10.248.100.81库下用户cwuser中并不能访问employees ，如下图所示。
![image-20220617100113422](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100113422.png)
已知10.248.100.81库下用户cwuser用户下已有访问 库10.1.2.1用户ctdev的dblink名称为to_ctdev。如下图所示

![image-20220617100126443](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100126443.png)

在cwuser用户下创建访问ctdev下employees 表的同义词

```
create synonym employees for employees@TO_CTDEV
```

即可在cwuser下访问ctdev下的表employees ，如下图所示

![image-20220617100152524](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100152524.png)



### 查询当前用户下某些字段在那个表

```sql
select table_name,column_name from user_tab_columns where column_name like '%column_name %';  
```

### 回滚误操作

```sql
select * from 表名 as of timestamp to_timestamp('2017-7-24 17:35:50','yyyy-mm-dd hh24:mi:ss');
```

### 修改用户名

```sql
 UPDATE USER$ SET NAME='JXZJXT1231SIT' WHERE user#=ZXZJXT1231SIT;
     COMMIT;
```

### Expdp Impdp 数据泵导入导出

**导出** 

```sql
expdp csdz/Csdz#321@orcl DIRECTORY=DATA_PUMP_DIR SCHEMAS=csdz dumpfile=Csdz20240322.dmp logfile=Csdz20240322.log  compression=all
```

高版本兼容低版本

```sql
expdp system/oracle@ip:1521/dbname directory=dmp11g logfile=xxx.log dumpfile=xxx_%U.dmp parallel=8 schemas=user1,user2 compression=all version=11.2.0.0.0 cluster=n

impdp system/oracle directory=dmp11g logfile=xxx.09011.log dumpfile=xxx_%U.dmp parallel=8 schemas=user1,user2 cluster=n
```



新建逻辑目录，Oracle不会自动创建实际的物理目录“D:\temp”（务必先手动创建此目录然后再执行oracle 导出目录定义），仅仅是进行定义逻辑路径dmp_dir；  

```bash
sql>conn username/password@orcl as sysdba;
sql>create directory dmp_dir as 'd:\temp';
sql>select * from dba_directories;
```

oracle 定义路径前必须创建物理地址，不然会出现如下错误。  

```sql
    ORA-39002: invalid operation
    ORA-39070: Unable to open the log file.
    ORA-29283: invalid file operation
    ORA-06512: at "SYS.UTL_FILE", line 536
    ORA-29283: invalid file operation
```

确认目录存在，当出现如下错误，看看是否有授权 

```sql
ORA-39002: invalid operation
ORA-39070: Unable to open the log file.
ORA-39087: directory name DUMP_DIR is invalid
```

解决方法：

```sql
grant read,write on directory dump_dir to username;
```

导出时出现如下错误，表示没有给用户授权（grant exp_full_database to username;）

```sql
    ORA-31631: privileges are required

    ORA-39161: Full database jobs require privileges
```

导入时出现如下错误，表示没有给用户授权（grant imp_full_database to username;）

```sql
    ORA-31631: privileges are required

    ORA-39122: Unprivileged users may not perform REMAP_SCHEMA remappings.
```

表空间已满提示错误：

```sql
    ORA-31626: job does not exist
    ORA-31633: unable to create master table "DRM_PFM.SYS_EXPORT_FULL_06"
    ORA-06512: at "SYS.DBMS_SYS_ERROR", line 95
    ORA-06512: at "SYS.KUPV$FT", line 1020
    ORA-01658: unable to create INITIAL extent for segment in tablespace TS_PFM
```

查看表空间情况：

```sql
SELECT a.tablespace_name "表空间名",
       total / 1024 / 1024 "表空间大小单位[M]",
       free / 1024 / 1024 "表空间剩余大小单位[M]",
       (total - free) / 1024 / 1024 "表空间使用大小单位[M]",
       Round((total - free) / total, 4) * 100 "使用率   [%]"
  FROM (SELECT tablespace_name, Sum(bytes) free
          FROM DBA_FREE_SPACE
         GROUP BY tablespace_name) a,
       (SELECT tablespace_name, Sum(bytes) total
          FROM DBA_DATA_FILES
         GROUP BY tablespace_name) b
 WHERE a.tablespace_name = b.tablespace_name;
```

解决方法：

```
-- 增加表空间容量
ALTER DATABASE DATAFILE 'D:\DEVELOP\DATABASE\TABLESPACE\TS_PFM.DBF' RESIZE 200M;

-- 修改表空间自动扩展
ALTER DATABASE DATAFILE 'D:\DEVELOP\DATABASE\TABLESPACE\TS_PFM.DBF' AUTOEXTEND ON NEXT 100M MAXSIZE 1G;
```

​    如果表空间设置了自动扩展，查询到的表空间容量快满了，手动执行表空间容量增加；我的就是设置了表空间自动扩展的，但是expdp 导出的时候还是报错，通过手动增加容量解决问题。  

文件已存在提示错误，删除之前因为出现错误创建的文件重新执行即可。

```sql
    ORA-39001: invalid argument value
    ORA-39000: bad dump file specification
    ORA-31641: unable to create dump file "d:\temp\oracle\drm_pfm0711.dmp"
    ORA-27038: created file already exists
    OSD-04010: <create> option specified, file already exists
```

根据用户导出数据提示

```sql
ORA-31626: job does not exist
ORA-31633: unable to create master table "DRM_PFM.SYS_EXPORT_SCHEMA_05"
ORA-06512: at "SYS.DBMS_SYS_ERROR", line 95
ORA-06512: at "SYS.KUPV$FT", line 1020
ORA-01950: no privileges on tablespace 'TS_PFM'
```

解决办法

```sql
SQL> alter user jnzjxt quota unlimited on tablespace_name ; 
```

 

**导入**

```sql
Impdp jnzjxt/jnzjxt@orcl directory=dmp_dir dumpfile=jnzjxt_expdp.dmp logfile=jnzjxt_impdp.log schemas=jnzjxt table_exists_action=append
```



角色确实不存在忽略即可，如果还是导入不了看看是否是表空间不足。

```sql
IMP-00058: ORACLE error 1653 encountered
ORA-01653: unable to extend table user.testTable by 8 in tablespace TS_test
IMP-00028: partial import of previous table rolled back: 10 rows rolled back
IMP-00017: following statement failed with ORACLE error 1917: 
           "GRANT SELECT ON "testTable" TO "testUser""
IMP-00003: ORACLE error 1917 encountered
ORA-01917: user or role 'testUser' does not exist
```

 

```sql
ORA-39006: internal error
ORA-39213: Metadata processing is notavailable
SQL> execute  sys.dbms_metadata_util.load_stylesheets;
```

###  表、字段加注释

```sql
COMMENT ON TABLE ZQ_STRESS_SCENARIO IS '债券压力情景设计表';
```

### ID自增

```sql
------创建序列

CREATE SEQUENCE ZQ_STRESS_SEQ
 
MINVALUE 1 --最小值
 
NOMAXVALUE --不设置最大值
 
START WITH 1 --从1开始计数
 
INCREMENT BY 1 --每次加1个
 
NOCYCLE --一直累加，不循环
 
NOCACHE; --不建缓冲区


-----创建触发器
CREATE OR REPLACE TRIGGER ZQ_STRESS_IS_TRIGGER
BEFORE INSERT ON ZQ_STRESS_SCENARIO FOR EACH ROW
BEGIN
SELECT ZQ_STRESS_SEQ.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
```

### 扩字段

```sql
alter table 表名 modify 列名 数据类型
alter table 表名 ADD 列名 数据类型 #新增字段
```

### 查看表空间的名称及大小 

```sql
SELECT t.tablespace_name, round(SUM(bytes / (1024 * 1024)), 0) ts_size 
FROM dba_tablespaces t, dba_data_files d 
WHERE t.tablespace_name = d.tablespace_name 
GROUP BY t.tablespace_name; 
```

### 创建索引、查询索引

##### 1、创建索引

```sql
create index 索引名 on 表名(列名);
```

##### 2、删除索引

```sql
drop index 索引名;
```

 

##### 3、创建组合索引

```sql
    create index 索引名 on 表名(列名1,,列名2);
```

 

```sql
*查看目标表中已添加的索引
*
*/
--在数据库中查找表名
select` `* ``from` `user_tables ``where` `table_name ``like` `'tablename%'``;
```

 

```sql
--查看该表的所有索引
select` `* ``from` `all_indexes ``where` `table_name = ``'tablename'``;
```

 

```sql
--查看该表的所有索引列
select``* ``from` `all_ind_columns ``where` `table_name = ``'tablename'``;
```

### 数值处理函数

```sql
1.绝对值：abs()

   select abs(-2) value from dual;

2.取整函数（大）：ceil（）

   select ceil(-2.001) value from dual;(-2)

3.取整函数（小）：floor（）

   select floor(-2.001) value from dual;(-3)

4.取整函数（截取）：trunc（）

   select trunc(-2.001) value from dual; (-2)

5.四舍五入：round（）

   select round(1.234564) value from dual;(1.2346)

6.取平方：Power（m,n）

   select power(4,2) value from dual;(16)

7.取平方根:SQRT()

   select sqrt(16) value from dual;(4)

8.取随机数:dbms_random(minvalue,maxvalue)

   select sys.dbms.random.value(0,1) value from dual;

9.取符号：Sign()

   select sign(-3) value from dual;(-)

10,取集合的最大值:greatest(value)

   select greatest(-1,3,5,7,9) value from dual;(9)

11.取集合的最小值:least(value)

   select least(-1,3,5,7,9) value from dual;(-1)

12.处理Null值：nvl(空值，代替值)

   select  nvl(null,10) value from dual;(10)

13.求字符序号:ascii()

    select ascii(a) value from dual;

14.求序号字符:chr()

    select chr(97) value from dual;

15.链接：concat()

    select concat("11","22") value from dual;(1122)

16.获取系统时间:sysdate()

    select sysdate value from dual;

17.求日期

    select trunc(sysdate) from dual;

18.求时间

    select  to_char(sysdate,"hh24:mm:ss") from dual;

19.首字母大写：InitCAP()

    select INITCAP(abc def ghi) value from dual;(Abc Def Ghi)
```

### 查看版本号

```sql
select * from v$version;
```

### 查询表注释

```sql
select * from user_tab_comments
```

### 修改数据库字符集

> 1.先查看客户端和服务端的编码集
> 客户端：
>
> SELECT * FROM V$NLS_PARAMETERS;
> 1
> 服务端
>
> SELECT * FROM NLS_DATABASE_PARAMETERS;



```sql

SQL> shutdown immediate;
数据库已经关闭。
已经卸载数据库。
ORACLE 例程已经关闭。
SQL> startup mount;
ORACLE 例程已经启动。
 
Total System Global Area 1071333376 bytes
Fixed Size                  1375792 bytes
Variable Size             536871376 bytes
Database Buffers          528482304 bytes
Redo Buffers                4603904 bytes
数据库装载完毕。
SQL> alter session set sql_trace=true;
 
会话已更改。
 
SQL> alter system enable restricted session;
 
系统已更改。
 
SQL>
SQL> alter system set job_queue_processes=0;
 
系统已更改。
 
SQL> alter system set aq_tm_processes=0;
 
系统已更改。
 
SQL> alter database open;
 
数据库已更改。
 
SQL> ALTER DATABASE character set INTERNAL_USE AL32UTF8;
 
数据库已更改。
 
SQL> shutdown immediate;
数据库已经关闭。
已经卸载数据库。
ORACLE 例程已经关闭。
SQL> startup
ORACLE 例程已经启动。
```



### instr字符查找函数

#### 语法

**instr( string1, string2, start_position,nth_appearance )**

#### 参数

● string1：源字符串，要在此字符串中查找。

●string2：要在string1中查找的字符串 。

●start_position：代表string1 的哪个位置开始查找。此参数可选，如果省略默认为1. 字符串索引从1开始。如果此参数为正，从左到右开始检索，如果此参数为负，从右到左检索，返回要查找的字符串在源字符串中的开始索引。

●nth_appearance：代表要查找第几次出现的string2. 此参数可选，如果省略，默认为 1.如果为负数系统会报错。 [2] 

#### 注意

位置索引号从1开始。

如果String2在String1中没有找到，instr函数返回0。　

#### 示例

```
SELECT instr('syranmo','s') FROM dual; -- 返回 1

SELECT instr('syranmo','ra') FROM dual; -- 返回 3

SELECT instr('syran mo','at',1,2) FROM dual; -- 返回 0
```



### 去重

```sql
create table test(
    id int primary key not null,
    name varchar(10) not null,
    age int not null
);


insert into test values (1,'张三'，20);
insert into test values (2,'张三'，20);
insert into test values (3,'李四'，20);
insert into test values (4,'李四'，30);
insert into test values (5,'王五'，40);
insert into test values (6,'王五'，40);
commit;
```

1.distinct

```sql
select distinct name,age from test
```

2.group by 

```sql
select name,age from test
group by name,age;
```

3.rowid（伪列去重）

```sql
select id,name,age from test t1
where t1.rowid in (select min(rowid) from test t2 where t1.name=t2.name and t1.age=t2.age);
```

4.窗口函数row_number () over()

```sql
select t.id ,t.name,t.age from 
(select row_number() over(partition by name,age order by age) rank,test.* from test)t
where t.rank = 1;
```



### Over() Partition By

group by是分组函数，partition by是分区函数（像sum()等是[聚合函数](https://so.csdn.net/so/search?q=聚合函数&spm=1001.2101.3001.7020)），注意区分。

#### 1、over函数的写法：

```
over（partition by cno order by degree ）
1
```

先对cno 中相同的进行分区，在cno 中相同的情况下对degree 进行排序

#### 2、分区函数Partition By与rank()的用法“对比”分区函数Partition By与row_number()的用法

例：查询每名课程的第一名的成绩

###### （1）使用rank()

```
SELECT	* 
FROM	(select sno,cno,degree,
      	rank()over(partition by cno order by degree desc) mm 
      	from score) 
where mm = 1;
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190323180421725.png)

###### （2）使用row_number()

```
SELECT * 
FROM   (select sno,cno,degree,
       row_number()over(partition by cno order by degree desc) mm 
       from score) 
where mm = 1;
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190323180844735.png)

###### （3）rank()与row_number()的区别

由以上的例子得出，在求第一名成绩的时候，不能用row_number()，因为如果同班有两个并列第一，row_number()只返回一个结果。

#### 3、分区函数Partition By与rank()的用法“对比”分区函数Partition By与dense_rank()的用法

例：查询课程号为‘3-245’的成绩与排名

###### （1） 使用rank()

```
SELECT * 
FROM   (select sno,cno,degree,
       rank()over(partition by cno order by degree desc) mm 
       from score) 
where cno = '3-245'
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190325162112385.png)

###### （2） 使用dense_rank()

```
SELECT * 
FROM   (select sno,cno,degree,
       dense_rank()over(partition by cno order by degree desc) mm 
       from score) 
where cno = '3-245'
12345
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190325162956912.png)

###### （3）rank()与dense_rank()的区别

由以上的例子得出，rank()和dense_rank()都可以将并列第一名的都查找出来；但rank()是跳跃排序，有两个第一名时接下来是第三名；而dense_rank()是非跳跃排序，有两个第一名时接下来是第二名。



### Decode

#### 1、decode的含义

```
decode(条件,值1,返回值1,值2,返回值2,…值n,返回值n,缺省值)
```

这个是decode的[表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)，具体的含义解释为：

```java
IF 条件=值1 THEN
　　　　RETURN(翻译值1)
ELSIF 条件=值2 THEN
　　　　RETURN(翻译值2)
　　　　......
ELSIF 条件=值n THEN
　　　　RETURN(翻译值n)
ELSE
　　　　RETURN(缺省值)
END IF
```

#### 2.decode的用法



##### 2.1 翻译值

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143638977.png)
需求：查询出的数据，1表示男生，2表示女生

```sql
select t.id,
       t.name,
       t.age,
       decode(t.sex, '1', '男生', '2', '女生', '其他') as sex
  from STUDENT2 t
12345
```

结果：![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143719566.png)

##### 2.2 decode比较大小

说明：sign(value)函数会根据value的值为0，正数，负数，分别返回0，1,-1

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143759114.png)
需求：年龄在20以上的显示20以上，20以下的显示20以下，20的显示正好20

```sql
select t.id,
       t.name,
       t.age,
       decode(sign(t.age - 20),
              1,
              '20以上',
              -1,
              '20以下',
              0,
              '正好20',
              '未知') as sex
  from STUDENT2 t
123456789101112
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143835462.png)

##### 2.3 decode分段

数据暂无
需求：工资大于5000为高薪，工资介于3000到5000为中等，工资小于3000为底薪

```sql
select name,
       sal,
       decode(sign(sal - 5000),
              1,
              '高薪',
              0,
              '高薪',
              -1,
              decode(sign(sal - 3000), 1, '中等', 0, '中等', -1, '低薪')) as salname
  from person;
12345678910
```

##### 2.4 搜索字符串

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143936574.png)
需求：找到含有三的姓名

```sql
select t.id,
       decode(instr(t.name, '三'), 0, '姓名不含有三', '姓名含有三') as name,
       t.age,
       t.sex
  from STUDENT2 t
12345
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144009575.png)

##### 2.5 判断是否为空

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144032102.png)
需求：性别为空显示“暂无数据”，不为空原样输出

```sql
select t.id,
       t.name,
       t.age,
       decode(t.sex,NULL,'暂无数据',t.sex) as sex
  from STUDENT2 t
12345
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144108973.png)



### Case when

> Case具有两种格式。简单Case函数和Case搜索函数。
>
> **Case when 在Oracle 中的用法：**
>
> （a）以case开头，以end 结尾；
>
> （b）分之中when后跟条件，then 显示结果；
>
> （c）else 为除此之外的默认情况，类似于高级语言程序中的 switc case 的default可以不加；
>
> （d）end后面跟别名；
>
> **Case 有两种表达式：**
>
> （A）简单case表达式试用表达式确定返回值；
>
> （B）搜索case表达式，使用条件确定返回值；



第一种 格式 : 简单Case函数 :

格式说明

```
case` `列名``when` `条件值1 ``then` `选项1``when` `条件值2 ``then` `选项2.......``else` `默认值 ``end
```

eg:

```
select``case` `　　job_level``when` `'1'` `then` `'1111'``when``　 ``'2'` `then` `'1111'``when``　 ``'3'` `then` `'1111'``else` `'eee'` `end``from` `dbo.employee
```

第二种 格式 :Case搜索函数

格式说明

```
case``when` `列名= 条件值1 ``then` `选项1``when` `列名=条件值2 ``then` `选项2.......``else` `默认值 ``end
```

eg:

```
update` `employee``set` `e_wage =``case``when` `job_level = ``'1'` `then` `e_wage*1.97``when` `job_level = ``'2'` `then` `e_wage*1.07``when` `job_level = ``'3'` `then` `e_wage*1.06``else` `e_wage*1.05``end
```

### replace 字符串替换

```sql
replace(要更新的字段,'要替换的字符串'，'替换后字符串')
```

```sql
#替换手机号中间的4位为*号	
replace(phone,substr(phone,4,4),'****')
substr(username,1,3)||'****'||substr(username,-4,4)
```



### length() 

> 返回字符串的长度



### 字符串截取

```sql
//返回截取的字符串
substr(字符串,截取开始位置,截取长度) 
```



### 替换数组存入数据库后多出的","

```sql
CASE
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',',LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1) > 0
        AND instr(point.FZ_ADOPT_CONTROL_MEASURES, ',,') > 0
        THEN REPLACE(SUBSTR(point.FZ_ADOPT_CONTROL_MEASURES, 0, LENGTH 
            (point.FZ_ADOPT_CONTROL_MEASURES) - 1), ',,',',')
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',',LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1) > 0
        THEN SUBSTR(point.FZ_ADOPT_CONTROL_MEASURES, 0, LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1)
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',,') > 0
        THEN REPLACE(point.FZ_ADOPT_CONTROL_MEASURES, ',,',',')
    END FZ_ADOPT_CONTROL_MEASURES
```



### 修改字段类型为clob

```sql
–增加大字段项
alter table t add a_copy CLOB;
–将需要改成大字段的项内容copy到大字段中
update t set a_copy= a;
–删除原有字段
alter table t DROP COLUMN a;
–将大字段名改成原字段名
alter table t RENAME COLUMN a_copy TO a;
最后commit
```



### connect by prior

CONNECT BY PRIOR 这个子句主要是用于B树结构类型的数据递归查询,通俗点讲就是当表中存在ID和父类ID时可以通过子节点或者父节点查到相应的数据。

实际例子：以299499为父结点，遍历其子结点



```
SELECT FG.CODE,FG.NAME,LEVEL,FG.PARENT_ID FROM CMS.FND_GROUP FG 
START WITH FG.PARENT_ID = '299499'
CONNECT BY PRIOR FG.ID = FG.PARENT_ID
ORDER BY LEVEL DESC
(WHERE FG.ID )
```

start with 子句：遍历起始条件，

connect by 子句：连接条件。关键词prior，跟父类字段连接就向父节点遍历，parentid、id两列谁放在“=”前都无所谓，关键是prior跟谁在一起。上面就是向子类遍历。

order by 子句：排序，根据级别降序。



### update 多表关联更新

先看例子

```sql
select t.*, t.rowid from T1 t;
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708091836624.png)

```sql
select t.*, t.rowid from T2 t;
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708091956979.png)

**错误示范：**

```sql
update t1 set t1.money = (select t2.money from t2 where t2.name = t1.name);
```

**结果：**
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708093109444.png)
因更新条件不够，可以看到name不相等的money为空了，所以要排除name不相等的情况，只更name相等的

#### 方法一：update

```sql
update t1 
   set t1.money = (select t2.money 
				     from t2 
				    where t2.name = t1.name
				   )
where exists (select 1 from t2 where t2.name = t1.name);
```

#### 方法二：内联视图更新

```sql
update (
        select t1.money money1,t2.money money2 from t1,t2 where t1.name = t2.name
       ) t
   set t.money1 = t.money2;
```

**注意：** 括号里通过关联两表建立一个视图，set中设置好更新的字段。这个解决方法比写法较直观且执行速度快。但表t2的主键一定要在where条件中，并且是以“=”来关联被更新表，否则报错误：ORA-01779: 无法修改与非键值保存表对应的列。

#### 方法三：merge into更新

```sql
    merge into t1
           using (select t2.name,t2.money from t2) t
              on (t.name = t1.name)
    when matched then 
    update  set t1.money = t.money;
```

#### 方法四：快速游标更新法

（参考：https://www.cnblogs.com/jingbf-BI/p/4909612.html）
begin for cr in (查询语句) loop –-循环 --更新语句（根据查询出来的结果集合） endloop; --结束循环 end; oracle支持快速游标，不需要定义直接把游标写到for循环中，这样就方便了我们批量更新数据。再加上oracle的rowid物理字段（oracle默认给每个表都有rowid这个字段，并且是唯一索引），可以快速定位到要更新的记录上。
例：

```sql
begin
for cr in (select a.rowid,b.join_state 
             from t_join_situation a,t_people_info b
            where a.people_number=b.people_number and
                  a.year='2011'and 
                  a.city_number='M00000'and 
                  a.town_number='M51000'
           )
loop
  update t_join_situation set join_state=cr.join_state where
  rowid = cr.rowid;
end loop;
end;
```

结论 方案 建议 标准update语法 单表更新或较简单的语句采用使用此方案更优。 inline view更新法 两表关联且被更新表通过关联表主键关联的，采用此方案更优。 merge更新法 两表关联且被更新表不是通过关联表主键关联的，采用此方案更优。 快速游标更新法 多表关联且逻辑复杂的，采用此方案更优。



### to_date

**TO_DATE格式(以时间:2007-11-02 13:45:25为例)**

**Year**:

> yy    two digits  两位年        显示值:07
>
> yyy   three digits 三位年        显示值:007
>
> yyyy  four digits 四位年        显示值:2007

**Month**:

> mm   number     两位月          显示值:11
>
> mon   abbreviated  字符集表示      显示值:11月,若是英文版,显示nov
>
> month spelled out  字符集表示      显示值:11月,若是英文版,显示november

**Day**:

> dd   number     当月第几天       显示值:02
>
> ddd  number     当年第几天       显示值:02
>
> dy   abbreviated  当周第几天简写   显示值:星期五,若是英文版,显示fri
>
> day  spelled out   当周第几天全写   显示值:星期五,若是英文版,显示friday 

**Hour**:

> hh    two digits   12小时进制    显示值:01
>
> hh24  two digits   24小时进制    显示值:13

**Minute**:

> mi  two digits   60进制      显示值:45

**Second**:

> ss  two digits   60进制      显示值:25

**其它**

> Q    digit      季度          显示值:4
>
> WW  digit      当年第几周     显示值:44
>
> W    digit      当月第几周     显示值:1

24小时格式下时间范围为： 0:00:00 - 23:59:59....

12小时格式下时间范围为： 1:00:00 - 12:59:59....



#### 1. 日期和字符转换函数用法（to_date,to_char）

```sql
select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') as nowTime from dual;   //日期转化为字符串  
select to_char(sysdate,'yyyy')  as nowYear   from dual;   //获取时间的年  
select to_char(sysdate,'mm')    as nowMonth  from dual;   //获取时间的月  
select to_char(sysdate,'dd')    as nowDay    from dual;   //获取时间的日  
select to_char(sysdate,'hh24')  as nowHour   from dual;   //获取时间的时  
select to_char(sysdate,'mi')    as nowMinute from dual;   //获取时间的分  
select to_char(sysdate,'ss')    as nowSecond from dual;   //获取时间的秒
```



#### 2. 字符串和时间互转

```sql
select to_date('2004-05-07 13:23:44','yyyy-mm-dd hh24:mi:ss') from dual
select to_char( to_date(222,'J'),'Jsp') from dual //显示Two Hundred Twenty-Two    
```



#### 3.求某天是星期几

```sql
select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day') from dual;     //星期一     
select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day',
'NLS_DATE_LANGUAGE = American') from dual;   // monday   
//设置日期语言     
ALTER SESSION SET NLS_DATE_LANGUAGE='AMERICAN';     
//也可以这样     
TO_DATE ('2002-08-26', 'YYYY-mm-dd', 'NLS_DATE_LANGUAGE = American')    
```



#### 4. 两个日期间的天数

```sql
select floor(sysdate - to_date('20020405','yyyymmdd')) from dual;
```



#### 5. 时间为null的用法

```sql
select id, active_date from table1     
UNION    
select 1, TO_DATE(null) from dual;  //注意要用TO_DATE(null)
```



#### 6.月份差

```sql
a_date between to_date('20011201','yyyymmdd') and to_date('20011231','yyyymmdd')     
//那么12月31号中午12点之后和12月1号的12点之前是不包含在这个范围之内的。     
//所以，当时间需要精确的时候，觉得to_char还是必要的
```



#### 7. 日期格式冲突问题

输入的格式要看你安装的ORACLE字符集的类型, 比如: US7ASCII, date格式的类型就是: '01-Jan-01'

```sql
alter system set NLS_DATE_LANGUAGE = American     
alter session set NLS_DATE_LANGUAGE = American     
//或者在to_date中写     
select to_char(to_date('2002-08-26','yyyy-mm-dd'),
   'day','NLS_DATE_LANGUAGE = American') from dual;     
//注意我这只是举了NLS_DATE_LANGUAGE，当然还有很多，可查看     
select * from nls_session_parameters     
select * from V$NLS_PARAMETERS    
```



#### 8.查询特殊条件天数

```sql
select count(*)     
from ( select rownum-1 rnum     
   from all_objects     
   where rownum <= to_date('2002-02-28','yyyy-mm-dd') - to_date('2002-     
   02-01','yyyy-mm-dd')+1     
  )     
where to_char( to_date('2002-02-01','yyyy-mm-dd')+rnum-1, 'D' )     
    not in ( '1', '7' )     
//查找2002-02-28至2002-02-01间除星期一和七的天数     
//在前后分别调用DBMS_UTILITY.GET_TIME, 让后将结果相减(得到的是1/100秒, 而不是毫秒).    
```



#### 9. 查找月份

```sql
select months_between(to_date('01-31-1999','MM-DD-YYYY'),
to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;     
//结果为：1     
select months_between(to_date('02-01-1999','MM-DD-YYYY'),
to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;     
//结果为：1.03225806451613
```



#### 10. Next_day的用法

```sql
Next_day(date, day)     
Monday-Sunday, for format code DAY     
Mon-Sun, for format code DY     
1-7, for format code D    
```



#### 11.获得小时数

```sql
//extract()找出日期或间隔值的字段值
SELECT EXTRACT(HOUR FROM TIMESTAMP '2001-02-16 2:38:40') from offer     
select sysdate ,to_char(sysdate,'hh') from dual;     
 
SYSDATE               TO_CHAR(SYSDATE,'HH')     
-------------------- ---------------------     
2003-10-13 19:35:21   07     
select sysdate ,to_char(sysdate,'hh24') from dual;     
 
SYSDATE               TO_CHAR(SYSDATE,'HH24')     
-------------------- -----------------------     
2003-10-13 19:35:21   19    
```



#### 12.年月日的处理

```sql
SELECT
  older_date,
  newer_date,
  years,
  months,
  ABS (
    TRUNC (
      newer_date - ADD_MONTHS (older_date, years * 12 + months)
    )
  ) days
FROM
  (
    SELECT
      TRUNC (
        MONTHS_BETWEEN (newer_date, older_date) / 12
      ) YEARS,
      MOD (
        TRUNC (
          MONTHS_BETWEEN (newer_date, older_date)
        ),
        12
      ) MONTHS,
      newer_date,
      older_date
    FROM
      (
        SELECT
          hiredate older_date,
          ADD_MONTHS (hiredate, ROWNUM) + ROWNUM newer_date
        FROM
          emp
      )
  )   
```



#### 13.处理月份天数不定的办法

```sql
select to_char(add_months(last_day(sysdate) +1, -2), 'yyyymmdd'),last_day(sysdate) from dual    
```



#### 14.找出今年的天数

```sql
select add_months(trunc(sysdate,'year'), 12) - trunc(sysdate,'year') from dual    
   //闰年的处理方法     
  to_char( last_day( to_date('02'    | | :year,'mmyyyy') ), 'dd' )     
   //如果是28就不是闰年 

```



#### 15.yyyy与rrrr的区别

```sql
   YYYY99  TO_C     
   ------- ----     
   yyyy 99 0099     
   rrrr 99 1999     
   yyyy 01 0001     
   rrrr 01 2001   
```



#### 16.不同时区的处理

```sql
 select to_char( NEW_TIME( sysdate, 'GMT','EST'), 'dd/mm/yyyy hh:mi:ss') ,
   sysdate   from dual;    
```



#### 17. 5秒钟一个间隔

```sql
   Select TO_DATE(FLOOR(TO_CHAR(sysdate,'SSSSS')/300) * 300,'SSSSS') ,
   TO_CHAR(sysdate,'SSSSS')   from dual    
   //2002-11-1 9:55:00 35786     
   //SSSSS表示5位秒数    
```



#### 18.一年的第几天

```sql
    select TO_CHAR(SYSDATE,'DDD'),sysdate from dual   
   //310  2002-11-6 10:03:51    
```



#### 19.计算小时,分,秒,毫秒

```sql
     SELECT
      Days,
      A,
      TRUNC (A * 24) Hours,
      TRUNC (A * 24 * 60 - 60 * TRUNC(A * 24)) Minutes,
      TRUNC (
        A * 24 * 60 * 60 - 60 * TRUNC (A * 24 * 60)
      ) Seconds,
      TRUNC (
        A * 24 * 60 * 60 * 100 - 100 * TRUNC (A * 24 * 60 * 60)
      ) mSeconds
    FROM
      (
        SELECT
          TRUNC (SYSDATE) Days,
          SYSDATE - TRUNC (SYSDATE) A
        FROM
          dual
      ) SELECT
        *
      FROM
        tabname
      ORDER BY
        DECODE (MODE, 'FIFO', 1 ,- 1) * TO_CHAR (rq, 'yyyymmddhh24miss')
      
   //   floor((date2-date1) /365) 作为年     
   //  floor((date2-date1, 365) /30) 作为月     
   //  d(mod(date2-date1, 365), 30)作为日.
 
```



#### 20.next_day函数

```sql
   //返回下个星期的日期,day为1-7或星期日-星期六,1表示星期日
   next_day(sysdate,6)是从当前开始下一个星期五。后面的数字是从星期日开始算起。     
   // 1  2  3  4  5  6  7     
   //日 一 二 三 四 五 六   
   select (sysdate-to_date('2003-12-03 12:55:45','yyyy-mm-dd hh24:mi:ss'))*24*60*60 from dual
   //日期 返回的是天 然后 转换为ss
```



#### 21,round[舍入到最接近的日期](day:舍入到最接近的星期日)

```sql
   select sysdate S1,
   round(sysdate) S2 ,
   round(sysdate,'year') YEAR,
   round(sysdate,'month') MONTH ,
   round(sysdate,'day') DAY from dual
```



#### 22,trunc[截断到最接近的日期,单位为天],返回的是日期类型

```sql
   select sysdate S1,                    
     trunc(sysdate) S2,                 //返回当前日期,无时分秒
     trunc(sysdate,'year') YEAR,        //返回当前年的1月1日,无时分秒
     trunc(sysdate,'month') MONTH ,     //返回当前月的1日,无时分秒
     trunc(sysdate,'day') DAY           //返回当前星期的星期天,无时分秒
   from dual
```



#### 23,返回日期列表中最晚日期

```sql
   select greatest('01-1月-04','04-1月-04','10-2月-04') from dual
```



#### 24.计算时间差

```sql
     注:oracle时间差是以天数为单位,所以换算成年月,日
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))/365) as spanYears from dual        //时间差-年
      select ceil(moths_between(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))) as spanMonths from dual           //时间差-月
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))) as spanDays from dual             //时间差-天
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24) as spanHours from dual         //时间差-时
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24*60) as spanMinutes from dual    //时间差-分
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24*60*60) as spanSeconds from dual //时间差-秒
```



#### 25.更新时间

```sql
 //oracle时间加减是以天数为单位,设改变量为n,所以换算成年月,日
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
    to_char(sysdate+n*365,'yyyy-mm-dd hh24:mi:ss') as newTime from dual        //改变时间-年
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     add_months(sysdate,n) as newTime from dual                                 //改变时间-月
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n,'yyyy-mm-dd hh24:mi:ss') as newTime from dual            //改变时间-日
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24,'yyyy-mm-dd hh24:mi:ss') as newTime from dual         //改变时间-时
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual      //改变时间-分
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24/60/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual   //改变时间-秒
```



#### 26.查找月的第一天,最后一天

```sql
     SELECT Trunc(Trunc(SYSDATE, 'MONTH') - 1, 'MONTH') First_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') - 1 / 86400 Last_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') First_Day_Cur_Month,
       LAST_DAY(Trunc(SYSDATE, 'MONTH')) + 1 - 1 / 86400 Last_Day_Cur_Month
   FROM dual;
```



### wm_concat

```sql
select id，wm_concat(name) from tab1 group by id;
```

![下载](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202208171132486.png)

> 说明：wm_concat 用于纵转成一横非常有用，然后每个数据之间自动打上逗号，
> 但wm_concat 参数有一个限制，参数字符串不能超过30k字节，如果超过30k，就需要手工写一个函数了



### 更新clob字段

```sql
--使用PL/SQL语法，采取绑定变量的方式解决，而不是直接拼接SQL
application.yaml
```

### LISTAGG 函数 （多个行值合并成一个字符串）

Oracle LISTAGG 函数用于将一列中的多个行值合并成一个字符串。以下是 LISTAGG 函数的语法：

```
sqlCopy code
LISTAGG (column_name, delimiter) WITHIN GROUP (ORDER BY column_name)
```

其中，`column_name` 是要合并的列名，`delimiter` 是用于分隔每个值的字符。`WITHIN GROUP (ORDER BY column_name)` 用于指定在合并结果中出现值的顺序。

以下是一个示例，假设有一个名为 `employee` 的表，其中包含了员工名字和所在部门的信息：

```
markdownCopy codeEmployee
------------------
Name    | Department
------------------
John    | Sales
Jane    | HR
Bob     | Sales
Alice   | Marketing
```

要将部门为 Sales 的员工名字合并成一个字符串，可以使用以下 SQL 查询：

```
sqlCopy codeSELECT LISTAGG(Name, ',') WITHIN GROUP (ORDER BY Name)
FROM Employee
WHERE Department = 'Sales';
```

运行以上 SQL 查询的结果是：

```
Copy code
Bob,John
```

可以看到，LISTAGG 函数将部门为 Sales 的员工名字合并成了一个字符串，并用逗号分隔每个名字。

 

### 删除用户下的所有表

```sql
SELECT 'drop  table '|| table_name || ';' FROM USER_TABLES ORDER BY TABLE_NAME;
```

## 附件

### 数据类型

![image-20220609200341416](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220609200341416.png)



### 日期时间

| 字母 | 日期或时间元素   | 表示              | 示例                                  |
| :--- | :--------------- | :---------------- | :------------------------------------ |
| G    | Era标志符        | Text              | AD                                    |
| y    | 年               | Year              | 1996; 96                              |
| M    | 年中的月份       | Month             | July; Jul;07                          |
| w    | 年中的周数       | Number            | 27                                    |
| W    | 月份中的周数     | Number            | 2                                     |
| D    | 年中的天数       | Number            | 189                                   |
| d    | 月份中的天数     | Number            | 10                                    |
| F    | 月份中的星期     | Number            | 2                                     |
| E    | 星期中的天数     | Text              | Tuesday; Tue                          |
| a    | Am/pm 标记       | Text              | PM                                    |
| H    | 一天中的小时数   | （0-23）          | Number 0                              |
| k    | 一天中的小时数   | （1-24）          | Number 24                             |
| K    | am/pm 中的小时数 | （0-11）          | Number 0                              |
| h    | am/pm 中的小时数 | （1-12）          | Number 12                             |
| m    | 小时中的分钟数   | Number            | 30                                    |
| s    | 分钟中的秒数     | Number            | 55                                    |
| S    | 毫秒数           | Number            | 978                                   |
| z    | 时区             | General time zone | Pacific Standard Time; PST; GMT-08:00 |
| Z    | 时区             | RFC 822 time zone | -0800                                 |



## 问题

### ORA-00001

```
违反唯一约束条件
```

### Impdp 导入报错



```
高版本向低版本导入必须加版本号
dmp文件放到创建的directory=dmp_dir 路径下
```



### ORA-01940 无法删除当前已连接的用户



首先查询一下数据中有没有用户在使用

```sh
select username,sid,serial#,paddr from v$session where username='ECITY';
USERNAME                                    SID    SERIAL# PADDR
------------------------------ ---------- -------------------------------------------------
ECITY                                       634        7   00000000C028D198
SQL> select PROGRAM from v$process where addr='00000000C028D198';
PROGRAM
----------------------------------------------------------------------------------------------------------
Oracle@oradb01 (DW00)
```

其次杀掉系统中的这个进程

```sh
SQL> alter system kill session '634,7';
System altered.
```

然后执行删除操作，即可完成

```sh
SQL> select saddr,sid,serial#,paddr,username,status from v$session where username is not null;
SQL> drop user ecity CASCADE;
User dropped.
```

问题解决，记得KILL进程前，先看看是啥进程，哪台机连过来的，能否KILL等等。避免杀掉其他进程



### ORA-28547

> 连接工具⾃带的oci.dll并不⽀持oracle版本，需要去官⽹下载⽀持的版本。
>
> 1. 先⽤你的IDEA或者别⼈的连接到oracle数据库（为了查询版本）
>    1.1 查询版本SQL：select * from v$version;
>    1.2 IDEA连接Oracle数据库（会的跳到下⼀步）
> 2. 去oracle下载对应的oci.dll⽂件
> 3. 重启连接工具并连接Oracl



### 分页有重复数据

> **在Oracle数据库中进行分页查询时，如果排序条件所对应的字段值有大量重复或为空时，则可能会出现分页数据重复的情况。如果要避免出现分页数据重复的情况，则一定要在排序时加上唯一值字段为排序条件。**



