## 一、Spring Boot 入门

### 1、Spring Boot 简介

> 简化Spring应用开发的一个框架；
>
> 整个Spring技术栈的一个大整合；
>
> J2EE开发的一站式解决方案；

### 2、微服务

2014，martin fowler

微服务：架构风格（服务微化）

一个应用应该是一组小型服务；可以通过HTTP的方式进行互通；

单体应用：ALL IN ONE

微服务：每一个功能元素最终都是一个可独立替换和独立升级的软件单元；

[详细参照微服务文档](https://martinfowler.com/articles/microservices.html#MicroservicesAndSoa)



### 3、环境准备

http://www.gulixueyuan.com/ 谷粒学院

环境约束

> –jdk1.8：Spring Boot 推荐jdk1.7及以上；java version "1.8.0_112"
>
> –maven3.x：maven 3.3以上版本；Apache Maven 3.3.9
>
> –IntelliJIDEA2017：IntelliJ IDEA 2017.2.2 x64、STS
>
> –SpringBoot 1.5.9.RELEASE：1.5.9；

统一环境；



#### 1、MAVEN设置；

给maven 的settings.xml配置文件的profiles标签添加

```xml
<profile>
  <id>jdk-1.8</id>
  <activation>
    <activeByDefault>true</activeByDefault>
    <jdk>1.8</jdk>
  </activation>
  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
  </properties>
</profile>
```

#### 2、IDEA设置

整合maven进来；

![idea设置](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129151045.png)



![images/](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129151112.png)

### 4、Spring Boot HelloWorld

一个功能：

浏览器发送hello请求，服务器接受请求并处理，响应Hello World字符串；



#### 1、创建一个maven工程；（jar）

#### 2、导入spring boot相关的依赖

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
```

#### 3、编写一个主程序；启动Spring Boot应用

```java
/**
 *  @SpringBootApplication 来标注一个主程序类，说明这是一个Spring Boot应用
 */
@SpringBootApplication
public class HelloWorldMainApplication {

    public static void main(String[] args) {

        // Spring应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class,args);
    }
}
```

#### 4、编写相关的Controller、Service

```java
@Controller
public class HelloController {

    @ResponseBody
    @RequestMapping("/hello")
    public String hello(){
        return "Hello World!";
    }
}

```



#### 5、运行主程序测试

#### 6、简化部署

```xml
 <!-- 这个插件，可以将应用打包成一个可执行的jar包；-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
```

将这个应用打成jar包，直接使用java -jar的命令进行执行；

### 5、Hello World探究

#### 1、POM文件

##### 1、父项目

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>1.5.9.RELEASE</version>
</parent>

他的父项目是
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-dependencies</artifactId>
  <version>1.5.9.RELEASE</version>
  <relativePath>../../spring-boot-dependencies</relativePath>
</parent>
他来真正管理Spring Boot应用里面的所有依赖版本；

```

Spring Boot的版本仲裁中心；

以后我们导入依赖默认是不需要写版本；（没有在dependencies里面管理的依赖自然需要声明版本号）

##### 2、启动器

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

**spring-boot-starter**-==web==：

​	spring-boot-starter：spring-boot场景启动器；帮我们导入了web模块正常运行所依赖的组件；



Spring Boot将所有的功能场景都抽取出来，做成一个个的starters（启动器），只需要在项目里面引入这些starter相关场景的所有依赖都会导入进来。要用什么功能就导入什么场景的启动器



#### 2、主程序类，主入口类

```java
/**
 *  @SpringBootApplication 来标注一个主程序类，说明这是一个Spring Boot应用
 */
@SpringBootApplication
public class HelloWorldMainApplication {

    public static void main(String[] args) {

        // Spring应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class,args);
    }
}

```

@**SpringBootApplication**:    Spring Boot应用标注在某个类上说明这个类是SpringBoot的主配置类，SpringBoot就应该运行这个类的main方法来启动SpringBoot应用；



```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = {
      @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
      @Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication {
```

@**SpringBootConfiguration**:Spring Boot的配置类；

​		标注在某个类上，表示这是一个Spring Boot的配置类；

​		@**Configuration**:配置类上来标注这个注解；

​			配置类 -----  配置文件；配置类也是容器中的一个组件；@Component



@**EnableAutoConfiguration**：开启自动配置功能；

​		以前我们需要配置的东西，Spring Boot帮我们自动配置；@**EnableAutoConfiguration**告诉SpringBoot开启自动配置功能；这样自动配置才能生效；

```java
@AutoConfigurationPackage
@Import(EnableAutoConfigurationImportSelector.class)
public @interface EnableAutoConfiguration {
```

​      	@**AutoConfigurationPackage**：自动配置包

​		@**Import**(AutoConfigurationPackages.Registrar.class)：

​		Spring的底层注解@Import，给容器中导入一个组件；导入的组件由AutoConfigurationPackages.Registrar.class；

==将主配置类（@SpringBootApplication标注的类）的所在包及下面所有子包里面的所有组件扫描到Spring容器；==

​	@**Import**(EnableAutoConfigurationImportSelector.class)；

​		给容器中导入组件？

​		**EnableAutoConfigurationImportSelector**：导入哪些组件的选择器；

​		将所有需要导入的组件以全类名的方式返回；这些组件就会被添加到容器中；

​		会给容器中导入非常多的自动配置类（xxxAutoConfiguration）；就是给容器中导入这个场景需要的所有组件，并配置好这些组件；		![自动配置类](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129224104.png)

有了自动配置类，免去了我们手动编写配置注入功能组件等的工作；

​		SpringFactoriesLoader.loadFactoryNames(EnableAutoConfiguration.class,classLoader)；



==Spring Boot在启动的时候从类路径下的META-INF/spring.factories中获取EnableAutoConfiguration指定的值，将这些值作为自动配置类导入到容器中，自动配置类就生效，帮我们进行自动配置工作；==以前我们需要自己配置的东西，自动配置类都帮我们；

J2EE的整体整合解决方案和自动配置都在spring-boot-autoconfigure-1.5.9.RELEASE.jar；



​		

==Spring注解版（谷粒学院）==



### 6、使用Spring Initializer快速创建Spring Boot项目

#### 1、IDEA：使用 Spring Initializer快速创建项目

IDE都支持使用Spring的项目创建向导快速创建一个Spring Boot项目；

选择我们需要的模块；向导会联网创建Spring Boot项目；

默认生成的Spring Boot项目；

- 主程序已经生成好了，我们只需要我们自己的逻辑
- resources文件夹中目录结构
  - static：保存所有的静态资源； js css  images；
  - templates：保存所有的模板页面；（Spring Boot默认jar包使用嵌入式的Tomcat，默认不支持JSP页面）；可以使用模板引擎（freemarker、thymeleaf）；
  - application.properties：Spring Boot应用的配置文件；可以修改一些默认设置；

#### 2、STS使用 Spring Starter Project快速创建项目



-------------



## 二、配置文件

### 1、配置文件

SpringBoot使用一个全局的配置文件，配置文件名是固定的；

•application.properties

•application.yml



配置文件的作用：修改SpringBoot自动配置的默认值；SpringBoot在底层都给我们自动配置好；



YAML（YAML Ain't Markup Language）

​	YAML  A Markup Language：是一个标记语言

​	YAML   isn't Markup Language：不是一个标记语言；

标记语言：

​	以前的配置文件；大多都使用的是  **xxxx.xml**文件；

​	YAML：**以数据为中心**，比json、xml等更适合做配置文件；

​	YAML：配置例子

```yaml
server:
  port: 8081
```

​	XML：

```xml
<server>
	<port>8081</port>
</server>
```



### 2、YAML语法：

#### 1、基本语法

k:(空格)v：表示一对键值对（空格必须有）；

以**空格**的缩进来控制层级关系；只要是左对齐的一列数据，都是同一个层级的

```yaml
server:
    port: 8081
    path: /hello
```

属性和值也是大小写敏感；



#### 2、值的写法

##### 字面量：普通的值（数字，字符串，布尔）

​	k: v：字面直接来写；

​		字符串默认不用加上单引号或者双引号；

​		""：双引号；不会转义字符串里面的特殊字符；特殊字符会作为本身想表示的意思

​				name:   "zhangsan \n lisi"：输出；zhangsan 换行  lisi

​		''：单引号；会转义特殊字符，特殊字符最终只是一个普通的字符串数据

​				name:   ‘zhangsan \n lisi’：输出；zhangsan \n  lisi



##### 对象、Map（属性和值）（键值对）：

​	k: v：在下一行来写对象的属性和值的关系；注意缩进

​		对象还是k: v的方式

```yaml
friends:
		lastName: zhangsan
		age: 20
```

行内写法：

```yaml
friends: {lastName: zhangsan,age: 18}
```



##### 数组（List、Set）：

用- 值表示数组中的一个元素

```yaml
pets:
 - cat
 - dog
 - pig
```

行内写法

```yaml
pets: [cat,dog,pig]
```



### 3、配置文件值注入

配置文件

```yaml
person:    lastName: hello    age: 18    boss: false    birth: 2017/12/12    maps: {k1: v1,k2: 12}    lists:      - lisi      - zhaoliu    dog:      name: 小狗      age: 12
```

javaBean：

```java
/**
 * 将配置文件中配置的每一个属性的值，映射到这个组件中
 * @ConfigurationProperties：告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定；
 *      prefix = "person"：配置文件中哪个下面的所有属性进行一一映射
 *
 * 只有这个组件是容器中的组件，才能容器提供的@ConfigurationProperties功能；
 *
 */
@Component
@ConfigurationProperties(prefix = "person")
public class Person {

    private String lastName;
    private Integer age;
    private Boolean boss;
    private Date birth;

    private Map<String,Object> maps;
    private List<Object> lists;
    private Dog dog;

```



我们可以导入配置文件处理器，以后编写配置就有提示了

```xml
<!--导入配置文件处理器，配置文件进行绑定就会有提示-->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
```

#### 1、properties配置文件在idea中默认utf-8可能会乱码

调整

![idea配置乱码](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180130161620.png)

#### 2、@Value获取值和@ConfigurationProperties获取值比较

|                      | @ConfigurationProperties | @Value     |
| -------------------- | ------------------------ | ---------- |
| 功能                 | 批量注入配置文件中的属性 | 一个个指定 |
| 松散绑定（松散语法） | 支持                     | 不支持     |
| SpEL                 | 不支持                   | 支持       |
| JSR303数据校验       | 支持                     | 不支持     |
| 复杂类型封装         | 支持                     | 不支持     |

配置文件yml还是properties他们都能获取到值；

如果说，我们只是在某个业务逻辑中需要获取一下配置文件中的某项值，使用@Value；

如果说，我们专门编写了一个javaBean来和配置文件进行映射，我们就直接使用@ConfigurationProperties；



#### 3、配置文件注入值数据校验

```java
@Component@ConfigurationProperties(prefix = "person")@Validatedpublic class Person {    /**     * <bean class="Person">     *      <property name="lastName" value="字面量/${key}从环境变量、配置文件中获取值/#{SpEL}"></property>     * <bean/>     */   //lastName必须是邮箱格式    @Email    //@Value("${person.last-name}")    private String lastName;    //@Value("#{11*2}")    private Integer age;    //@Value("true")    private Boolean boss;    private Date birth;    private Map<String,Object> maps;    private List<Object> lists;    private Dog dog;
```



#### 4、@PropertySource&@ImportResource&@Bean

@**PropertySource**：加载指定的配置文件；

```java
/**
 * 将配置文件中配置的每一个属性的值，映射到这个组件中
 * @ConfigurationProperties：告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定；
 *      prefix = "person"：配置文件中哪个下面的所有属性进行一一映射
 *
 * 只有这个组件是容器中的组件，才能容器提供的@ConfigurationProperties功能；
 *  @ConfigurationProperties(prefix = "person")默认从全局配置文件中获取值；
 *
 */
@PropertySource(value = {"classpath:person.properties"})
@Component
@ConfigurationProperties(prefix = "person")
//@Validated
public class Person {

    /**
     * <bean class="Person">
     *      <property name="lastName" value="字面量/${key}从环境变量、配置文件中获取值/#{SpEL}"></property>
     * <bean/>
     */

   //lastName必须是邮箱格式
   // @Email
    //@Value("${person.last-name}")
    private String lastName;
    //@Value("#{11*2}")
    private Integer age;
    //@Value("true")
    private Boolean boss;

```



@**ImportResource**：导入Spring的配置文件，让配置文件里面的内容生效；

Spring Boot里面没有Spring的配置文件，我们自己编写的配置文件，也不能自动识别；

想让Spring的配置文件生效，加载进来；@**ImportResource**标注在一个配置类上

```java
@ImportResource(locations = {"classpath:beans.xml"})
导入Spring的配置文件让其生效
```



不来编写Spring的配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">


    <bean id="helloService" class="com.atguigu.springboot.service.HelloService"></bean>
</beans>
```

SpringBoot推荐给容器中添加组件的方式；推荐使用全注解的方式

1、配置类**@Configuration**------>Spring配置文件

2、使用**@Bean**给容器中添加组件

```java
/** * @Configuration：指明当前类是一个配置类；就是来替代之前的Spring配置文件 * * 在配置文件中用<bean><bean/>标签添加组件 * */@Configurationpublic class MyAppConfig {    //将方法的返回值添加到容器中；容器中这个组件默认的id就是方法名    @Bean    public HelloService helloService02(){        System.out.println("配置类@Bean给容器中添加组件了...");        return new HelloService();    }}
```

##4、配置文件占位符

##### 1、随机数

```java
${random.value}、${random.int}、${random.long}${random.int(10)}、${random.int[1024,65536]}
```



##### 2、占位符获取之前配置的值，如果没有可以是用:指定默认值

```properties
person.last-name=张三${random.uuid}
person.age=${random.int}
person.birth=2017/12/15
person.boss=false
person.maps.k1=v1
person.maps.k2=14
person.lists=a,b,c
person.dog.name=${person.hello:hello}_dog
person.dog.age=15
```



### 5、Profile

#### 1、多Profile文件

我们在主配置文件编写的时候，文件名可以是   application-{profile}.properties/yml

默认使用application.properties的配置；



#### 2、yml支持多文档块方式

```yml
server:
  port: 8081
spring:
  profiles:
    active: prod

---
server:
  port: 8083
spring:
  profiles: dev


---

server:
  port: 8084
spring:
  profiles: prod  #指定属于哪个环境
```





#### 3、激活指定profile

​	1、在配置文件中指定  spring.profiles.active=dev

​	2、命令行：

​		java -jar spring-boot-02-config-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev；

​		可以直接在测试的时候，配置传入命令行参数

​	3、虚拟机参数；

​		-Dspring.profiles.active=dev



### 6、配置文件加载位置

springboot 启动会扫描以下位置的application.properties或者application.yml文件作为Spring boot的默认配置文件

–file:./config/

–file:./

–classpath:/config/

–classpath:/

优先级由高到底，高优先级的配置会覆盖低优先级的配置；

SpringBoot会从这四个位置全部加载主配置文件；**互补配置**；



==我们还可以通过spring.config.location来改变默认的配置文件位置==

**项目打包好以后，我们可以使用命令行参数的形式，启动项目的时候来指定配置文件的新位置；指定配置文件和默认加载的这些配置文件共同起作用形成互补配置；**

java -jar spring-boot-02-config-02-0.0.1-SNAPSHOT.jar --spring.config.location=G:/application.properties

### 7、外部配置加载顺序

**==SpringBoot也可以从以下位置加载配置； 优先级从高到低；高优先级的配置覆盖低优先级的配置，所有的配置会形成互补配置==**

**1.命令行参数**

所有的配置都可以在命令行上进行指定

java -jar spring-boot-02-config-02-0.0.1-SNAPSHOT.jar --server.port=8087  --server.context-path=/abc

多个配置用空格分开； --配置项=值



2.来自java:comp/env的JNDI属性

3.Java系统属性（System.getProperties()）

4.操作系统环境变量

5.RandomValuePropertySource配置的random.*属性值



==**由jar包外向jar包内进行寻找；**==

==**优先加载带profile**==

**6.jar包外部的application-{profile}.properties或application.yml(带spring.profile)配置文件**

**7.jar包内部的application-{profile}.properties或application.yml(带spring.profile)配置文件**



==**再来加载不带profile**==

**8.jar包外部的application.properties或application.yml(不带spring.profile)配置文件**

**9.jar包内部的application.properties或application.yml(不带spring.profile)配置文件**



10.@Configuration注解类上的@PropertySource

11.通过SpringApplication.setDefaultProperties指定的默认属性

所有支持的配置加载来源；

[参考官方文档](https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#boot-features-external-config)

### 8、自动配置原理

配置文件到底能写什么？怎么写？自动配置原理；

[配置文件能配置的属性参照](https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#common-application-properties)



#### 1、**自动配置原理：**

1）、SpringBoot启动的时候加载主配置类，开启了自动配置功能 ==@EnableAutoConfiguration==

**2）、@EnableAutoConfiguration 作用：**

 - 利用EnableAutoConfigurationImportSelector给容器中导入一些组件？

- 可以查看selectImports()方法的内容；

- List<String> configurations = getCandidateConfigurations(annotationMetadata,      attributes);获取候选的配置

  - ```java
    SpringFactoriesLoader.loadFactoryNames()
    扫描所有jar包类路径下  META-INF/spring.factories
    把扫描到的这些文件的内容包装成properties对象
    从properties中获取到EnableAutoConfiguration.class类（类名）对应的值，然后把他们添加在容器中
    
    ```

    

**==将 类路径下  META-INF/spring.factories 里面配置的所有EnableAutoConfiguration的值加入到了容器中；==**

```properties
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration,\
org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration,\
org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration,\
org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration,\
org.springframework.boot.autoconfigure.cloud.CloudAutoConfiguration,\
org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration,\
org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration,\
org.springframework.boot.autoconfigure.couchbase.CouchbaseAutoConfiguration,\
org.springframework.boot.autoconfigure.dao.PersistenceExceptionTranslationAutoConfiguration,\
org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.cassandra.CassandraRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.couchbase.CouchbaseDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.couchbase.CouchbaseRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.ldap.LdapDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.ldap.LdapRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.neo4j.Neo4jRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.solr.SolrRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration,\
org.springframework.boot.autoconfigure.elasticsearch.jest.JestAutoConfiguration,\
org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration,\
org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration,\
org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration,\
org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration,\
org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration,\
org.springframework.boot.autoconfigure.hazelcast.HazelcastJpaDependencyAutoConfiguration,\
org.springframework.boot.autoconfigure.info.ProjectInfoAutoConfiguration,\
org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration,\
org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.JndiDataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.XADataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration,\
org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.JndiConnectionFactoryAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.activemq.ActiveMQAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.artemis.ArtemisAutoConfiguration,\
org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration,\
org.springframework.boot.autoconfigure.groovy.template.GroovyTemplateAutoConfiguration,\
org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration,\
org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration,\
org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration,\
org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapAutoConfiguration,\
org.springframework.boot.autoconfigure.ldap.LdapAutoConfiguration,\
org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration,\
org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration,\
org.springframework.boot.autoconfigure.mail.MailSenderValidatorAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.DeviceResolverAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.DeviceDelegatingViewResolverAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.SitePreferenceAutoConfiguration,\
org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration,\
org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration,\
org.springframework.boot.autoconfigure.mustache.MustacheAutoConfiguration,\
org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration,\
org.springframework.boot.autoconfigure.reactor.ReactorAutoConfiguration,\
org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration,\
org.springframework.boot.autoconfigure.security.SecurityFilterAutoConfiguration,\
org.springframework.boot.autoconfigure.security.FallbackWebSecurityAutoConfiguration,\
org.springframework.boot.autoconfigure.security.oauth2.OAuth2AutoConfiguration,\
org.springframework.boot.autoconfigure.sendgrid.SendGridAutoConfiguration,\
org.springframework.boot.autoconfigure.session.SessionAutoConfiguration,\
org.springframework.boot.autoconfigure.social.SocialWebAutoConfiguration,\
org.springframework.boot.autoconfigure.social.FacebookAutoConfiguration,\
org.springframework.boot.autoconfigure.social.LinkedInAutoConfiguration,\
org.springframework.boot.autoconfigure.social.TwitterAutoConfiguration,\
org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration,\
org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration,\
org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration,\
org.springframework.boot.autoconfigure.transaction.jta.JtaAutoConfiguration,\
org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration,\
org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration,\
org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration,\
org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.web.HttpEncodingAutoConfiguration,\
org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration,\
org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration,\
org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration,\
org.springframework.boot.autoconfigure.web.WebClientAutoConfiguration,\
org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration,\
org.springframework.boot.autoconfigure.websocket.WebSocketMessagingAutoConfiguration,\
org.springframework.boot.autoconfigure.webservices.WebServicesAutoConfiguration
```

每一个这样的  xxxAutoConfiguration类都是容器中的一个组件，都加入到容器中；用他们来做自动配置；

3）、每一个自动配置类进行自动配置功能；

4）、以**HttpEncodingAutoConfiguration（Http编码自动配置）**为例解释自动配置原理；

```java
@Configuration   //表示这是一个配置类，以前编写的配置文件一样，也可以给容器中添加组件@EnableConfigurationProperties(HttpEncodingProperties.class)  //启动指定类的ConfigurationProperties功能；将配置文件中对应的值和HttpEncodingProperties绑定起来；并把HttpEncodingProperties加入到ioc容器中@ConditionalOnWebApplication //Spring底层@Conditional注解（Spring注解版），根据不同的条件，如果满足指定的条件，整个配置类里面的配置就会生效；    判断当前应用是否是web应用，如果是，当前配置类生效@ConditionalOnClass(CharacterEncodingFilter.class)  //判断当前项目有没有这个类CharacterEncodingFilter；SpringMVC中进行乱码解决的过滤器；@ConditionalOnProperty(prefix = "spring.http.encoding", value = "enabled", matchIfMissing = true)  //判断配置文件中是否存在某个配置  spring.http.encoding.enabled；如果不存在，判断也是成立的//即使我们配置文件中不配置pring.http.encoding.enabled=true，也是默认生效的；public class HttpEncodingAutoConfiguration {    	//他已经和SpringBoot的配置文件映射了  	private final HttpEncodingProperties properties;     //只有一个有参构造器的情况下，参数的值就会从容器中拿  	public HttpEncodingAutoConfiguration(HttpEncodingProperties properties) {		this.properties = properties;	}      @Bean   //给容器中添加一个组件，这个组件的某些值需要从properties中获取	@ConditionalOnMissingBean(CharacterEncodingFilter.class) //判断容器没有这个组件？	public CharacterEncodingFilter characterEncodingFilter() {		CharacterEncodingFilter filter = new OrderedCharacterEncodingFilter();		filter.setEncoding(this.properties.getCharset().name());		filter.setForceRequestEncoding(this.properties.shouldForce(Type.REQUEST));		filter.setForceResponseEncoding(this.properties.shouldForce(Type.RESPONSE));		return filter;	}
```

根据当前不同的条件判断，决定这个配置类是否生效？

一但这个配置类生效；这个配置类就会给容器中添加各种组件；这些组件的属性是从对应的properties类中获取的，这些类里面的每一个属性又是和配置文件绑定的；







5）、所有在配置文件中能配置的属性都是在xxxxProperties类中封装者‘；配置文件能配置什么就可以参照某个功能对应的这个属性类

```java
@ConfigurationProperties(prefix = "spring.http.encoding")  //从配置文件中获取指定的值和bean的属性进行绑定
public class HttpEncodingProperties {

   public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
```





**精髓：**

​	**1）、SpringBoot启动会加载大量的自动配置类**

​	**2）、我们看我们需要的功能有没有SpringBoot默认写好的自动配置类；**

​	**3）、我们再来看这个自动配置类中到底配置了哪些组件；（只要我们要用的组件有，我们就不需要再来配置了）**

​	**4）、给容器中自动配置类添加组件的时候，会从properties类中获取某些属性。我们就可以在配置文件中指定这些属性的值；**



xxxxAutoConfigurartion：自动配置类；

给容器中添加组件

xxxxProperties:封装配置文件中相关属性；



#### 2、细节



##### 1、@Conditional派生注解（Spring注解版原生的@Conditional作用）

作用：必须是@Conditional指定的条件成立，才给容器中添加组件，配置配里面的所有内容才生效；

| @Conditional扩展注解            | 作用（判断是否满足当前指定条件）                 |
| ------------------------------- | ------------------------------------------------ |
| @ConditionalOnJava              | 系统的java版本是否符合要求                       |
| @ConditionalOnBean              | 容器中存在指定Bean；                             |
| @ConditionalOnMissingBean       | 容器中不存在指定Bean；                           |
| @ConditionalOnExpression        | 满足SpEL表达式指定                               |
| @ConditionalOnClass             | 系统中有指定的类                                 |
| @ConditionalOnMissingClass      | 系统中没有指定的类                               |
| @ConditionalOnSingleCandidate   | 容器中只有一个指定的Bean，或者这个Bean是首选Bean |
| @ConditionalOnProperty          | 系统中指定的属性是否有指定的值                   |
| @ConditionalOnResource          | 类路径下是否存在指定资源文件                     |
| @ConditionalOnWebApplication    | 当前是web环境                                    |
| @ConditionalOnNotWebApplication | 当前不是web环境                                  |
| @ConditionalOnJndi              | JNDI存在指定项                                   |

**自动配置类必须在一定的条件下才能生效；**

我们怎么知道哪些自动配置类生效；

**==我们可以通过启用  debug=true属性；来让控制台打印自动配置报告==**，这样我们就可以很方便的知道哪些自动配置类生效；

```java
=========================
AUTO-CONFIGURATION REPORT
=========================


Positive matches:（自动配置类启用的）
-----------------

   DispatcherServletAutoConfiguration matched:
      - @ConditionalOnClass found required class 'org.springframework.web.servlet.DispatcherServlet'; @ConditionalOnMissingClass did not find unwanted class (OnClassCondition)
      - @ConditionalOnWebApplication (required) found StandardServletEnvironment (OnWebApplicationCondition)
        
    
Negative matches:（没有启动，没有匹配成功的自动配置类）
-----------------

   ActiveMQAutoConfiguration:
      Did not match:
         - @ConditionalOnClass did not find required classes 'javax.jms.ConnectionFactory', 'org.apache.activemq.ActiveMQConnectionFactory' (OnClassCondition)

   AopAutoConfiguration:
      Did not match:
         - @ConditionalOnClass did not find required classes 'org.aspectj.lang.annotation.Aspect', 'org.aspectj.lang.reflect.Advice' (OnClassCondition)
        
```





## 三、日志

### 1、日志框架

 小张；开发一个大型系统；

​		1、System.out.println("")；将关键数据打印在控制台；去掉？写在一个文件？

​		2、框架来记录系统的一些运行时信息；日志框架 ；  zhanglogging.jar；

​		3、高大上的几个功能？异步模式？自动归档？xxxx？  zhanglogging-good.jar？

​		4、将以前框架卸下来？换上新的框架，重新修改之前相关的API；zhanglogging-prefect.jar；

​		5、JDBC---数据库驱动；

​			写了一个统一的接口层；日志门面（日志的一个抽象层）；logging-abstract.jar；

​			给项目中导入具体的日志实现就行了；我们之前的日志框架都是实现的抽象层；



**市面上的日志框架；**

JUL、JCL、Jboss-logging、logback、log4j、log4j2、slf4j....

| 日志门面  （日志的抽象层）                                   | 日志实现                                             |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| ~~JCL（Jakarta  Commons Logging）~~    SLF4j（Simple  Logging Facade for Java）    **~~jboss-logging~~** | Log4j  JUL（java.util.logging）  Log4j2  **Logback** |

左边选一个门面（抽象层）、右边来选一个实现；

日志门面：  SLF4J；

日志实现：Logback；



SpringBoot：底层是Spring框架，Spring框架默认是用JCL；‘

​	**==SpringBoot选用 SLF4j和logback；==**



### 2、SLF4j使用

#### 1、如何在系统中使用SLF4j   https://www.slf4j.org

以后开发的时候，日志记录方法的调用，不应该来直接调用日志的实现类，而是调用日志抽象层里面的方法；

给系统里面导入slf4j的jar和  logback的实现jar

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    logger.info("Hello World");
  }
}
```

图示；

![images/concrete-bindings.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/concrete-bindings.png)

每一个日志的实现框架都有自己的配置文件。使用slf4j以后，**配置文件还是做成日志实现框架自己本身的配置文件；**

#### 2、遗留问题

a（slf4j+logback）: Spring（commons-logging）、Hibernate（jboss-logging）、MyBatis、xxxx

统一日志记录，即使是别的框架和我一起统一使用slf4j进行输出？

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/legacy.png)

**如何让系统中所有的日志都统一到slf4j；**

==1、将系统中其他日志框架先排除出去；==

==2、用中间包来替换原有的日志框架；==

==3、我们导入slf4j其他的实现==



### 3、SpringBoot日志关系

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
```



SpringBoot使用它来做日志功能；

```xml
	<dependency>			<groupId>org.springframework.boot</groupId>			<artifactId>spring-boot-starter-logging</artifactId>		</dependency>
```

底层依赖关系

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180131220946.png)

总结：

​	1）、SpringBoot底层也是使用slf4j+logback的方式进行日志记录

​	2）、SpringBoot也把其他的日志都替换成了slf4j；

​	3）、中间替换包？

```java
@SuppressWarnings("rawtypes")public abstract class LogFactory {    static String UNSUPPORTED_OPERATION_IN_JCL_OVER_SLF4J = "http://www.slf4j.org/codes.html#unsupported_operation_in_jcl_over_slf4j";    static LogFactory logFactory = new SLF4JLogFactory();
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180131221411.png)



​	4）、如果我们要引入其他框架？一定要把这个框架的默认日志依赖移除掉？

​			Spring框架用的是commons-logging；

```xml
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
```

**==SpringBoot能自动适配所有的日志，而且底层使用slf4j+logback的方式记录日志，引入其他框架的时候，只需要把这个框架依赖的日志框架排除掉即可；==**

### 4、日志使用；

#### 1、默认配置

SpringBoot默认帮我们配置好了日志；

```java
	//记录器
	Logger logger = LoggerFactory.getLogger(getClass());
	@Test
	public void contextLoads() {
		//System.out.println();

		//日志的级别；
		//由低到高   trace<debug<info<warn<error
		//可以调整输出的日志级别；日志就只会在这个级别以以后的高级别生效
		logger.trace("这是trace日志...");
		logger.debug("这是debug日志...");
		//SpringBoot默认给我们使用的是info级别的，没有指定级别的就用SpringBoot默认规定的级别；root级别
		logger.info("这是info日志...");
		logger.warn("这是warn日志...");
		logger.error("这是error日志...");


	}
```



        日志输出格式：
    		%d表示日期时间，
    		%thread表示线程名，
    		%-5level：级别从左显示5个字符宽度
    		%logger{50} 表示logger名字最长50个字符，否则按照句点分割。 
    		%msg：日志消息，
    		%n是换行符
        -->
        %d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n

SpringBoot修改日志的默认配置

```properties
logging.level.com.atguigu=trace


#logging.path=
# 不指定路径在当前项目下生成springboot.log日志
# 可以指定完整的路径；
#logging.file=G:/springboot.log

# 在当前磁盘的根路径下创建spring文件夹和里面的log文件夹；使用 spring.log 作为默认文件
logging.path=/spring/log

#  在控制台输出的日志的格式
logging.pattern.console=%d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n
# 指定文件中日志输出的格式
logging.pattern.file=%d{yyyy-MM-dd} === [%thread] === %-5level === %logger{50} ==== %msg%n
```

| logging.file | logging.path | Example  | Description                        |
| ------------ | ------------ | -------- | ---------------------------------- |
| (none)       | (none)       |          | 只在控制台输出                     |
| 指定文件名   | (none)       | my.log   | 输出日志到my.log文件               |
| (none)       | 指定目录     | /var/log | 输出到指定目录的 spring.log 文件中 |

#### 2、指定配置

给类路径下放上每个日志框架自己的配置文件即可；SpringBoot就不使用他默认配置的了

| Logging System          | Customization                                                |
| ----------------------- | ------------------------------------------------------------ |
| Logback                 | `logback-spring.xml`, `logback-spring.groovy`, `logback.xml` or `logback.groovy` |
| Log4j2                  | `log4j2-spring.xml` or `log4j2.xml`                          |
| JDK (Java Util Logging) | `logging.properties`                                         |

logback.xml：直接就被日志框架识别了；

**logback-spring.xml**：日志框架就不直接加载日志的配置项，由SpringBoot解析日志配置，可以使用SpringBoot的高级Profile功能

```xml
<springProfile name="staging">
    <!-- configuration to be enabled when the "staging" profile is active -->
  	可以指定某段配置只在某个环境下生效
</springProfile>

```

如：

```xml
<appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
        <!--
        日志输出格式：
			%d表示日期时间，
			%thread表示线程名，
			%-5level：级别从左显示5个字符宽度
			%logger{50} 表示logger名字最长50个字符，否则按照句点分割。 
			%msg：日志消息，
			%n是换行符
        -->
        <layout class="ch.qos.logback.classic.PatternLayout">
            <springProfile name="dev">
                <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} ----> [%thread] ---> %-5level %logger{50} - %msg%n</pattern>
            </springProfile>
            <springProfile name="!dev">
                <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} ==== [%thread] ==== %-5level %logger{50} - %msg%n</pattern>
            </springProfile>
        </layout>
    </appender>
```



如果使用logback.xml作为日志配置文件，还要使用profile功能，会有以下错误

 `no applicable action for [springProfile]`

### 5、切换日志框架

可以按照slf4j的日志适配图，进行相关的切换；

slf4j+log4j的方式；

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
  <exclusions>
    <exclusion>
      <artifactId>logback-classic</artifactId>
      <groupId>ch.qos.logback</groupId>
    </exclusion>
    <exclusion>
      <artifactId>log4j-over-slf4j</artifactId>
      <groupId>org.slf4j</groupId>
    </exclusion>
  </exclusions>
</dependency>

<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-log4j12</artifactId>
</dependency>

```



切换为log4j2

```xml
   <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <exclusion>
                    <artifactId>spring-boot-starter-logging</artifactId>
                    <groupId>org.springframework.boot</groupId>
                </exclusion>
            </exclusions>
        </dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-log4j2</artifactId>
</dependency>
```

-----------------

### 附件

#### Logback.xml配置

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<!-- 级别从高到低 OFF 、 FATAL 、 ERROR 、 WARN 、 INFO 、 DEBUG 、 TRACE 、 ALL -->
<!-- 日志输出规则 根据当前ROOT 级别，日志输出时，级别高于root默认的级别时 会输出 -->
<!-- 以下 每个配置的 filter 是过滤掉输出文件里面，会出现高级别文件，依然出现低级别的日志信息，通过filter 过滤只记录本级别的日志 -->
<!-- scan 当此属性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true。 -->
<!-- scanPeriod 设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位是毫秒。当scan为true时，此属性生效。默认的时间间隔为1分钟。 -->
<!-- debug 当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。 -->
<configuration scan="true" scanPeriod="60 seconds" debug="false">

    <!-- 动态日志级别 -->
    <jmxConfigurator/>

    <!--*****************************************************************************-->
    <!--自定义项 开始-->
    <!--*****************************************************************************-->

    <!-- 定义日志文件 输出位置 -->
    <property name="log.home_dir" value="D:\\Logs"/>
    <property name="log.app_name" value="tableNameReplace"/>
    <!-- 日志最大的历史 30天 -->
    <property name="log.maxHistory" value="30"/>
    <property name="log.maxSize" value="5MB"/>
    <!-- 日志界别 -->
    <property name="log.level" value="info"/>
    <!-- 打印sql语句 需要指定dao层包的位置 -->
    <property name="mapper.package" value="com.rers.table_name_replce.mapper" />

    <!--*****************************************************************************-->
    <!--自定义项 结束-->
    <!--*****************************************************************************-->

    <!-- ConsoleAppender 控制台输出日志 -->
    
     <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <!-- 默认情况下，每个日志事件都会立即刷新到基础输出流。 这种默认方法更安全，因为如果应用程序在没有正确关闭appender的情况下退出，则日志事件不会丢失。
         但是，为了显着增加日志记录吞吐量，您可能希望将immediateFlush属性设置为false -->
         <!--<immediateFlush>true</immediateFlush>-->
         <encoder>
            <!-- %37():如果字符没有37个字符长度,则左侧用空格补齐 -->
             <!-- %-37():如果字符没有37个字符长度,则右侧用空格补齐 -->
             <!-- %15.15():如果记录的线程字符长度小于15(第一个)则用空格在左侧补齐,如果字符长度大于15(第二个),则从开头开始截断多余的字符 -->
             <!-- %-40.40():如果记录的logger字符长度小于40(第一个)则用空格在右侧补齐,如果字符长度大于40(第二个),则从开头开始截断多余的字符 -->
             <!-- %msg：日志打印详情 -->
             <!-- %n:换行符 -->
             <!-- %highlight():转换说明符以粗体红色显示其级别为ERROR的事件，红色为WARN，BLUE为INFO，以及其他级别的默认颜色。 -->
             <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} %highlight(%-5level) --- [%15.15(%thread)] %cyan(%-40.40(%logger{40})) : %msg%n</pattern>
             <!-- 控制台也要使用UTF-8，不要使用GBK，否则会中文乱码 -->
             <charset>UTF-8</charset>
        </encoder>
    </appender>


    <!-- ERROR级别日志 -->
    <!-- 滚动记录文件，先将日志记录到指定文件，当符合某个条件时，将日志记录到其他文件 RollingFileAppender -->
    <appender name="ERROR" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!-- 过滤器，只记录WARN级别的日志 -->
        <!-- 果日志级别等于配置级别，过滤器会根据onMath 和 onMismatch接收或拒绝日志。 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 设置过滤级别 -->
            <level>ERROR</level>
            <!-- 用于配置符合过滤条件的操作 -->
            <onMatch>ACCEPT</onMatch>
            <!-- 用于配置不符合过滤条件的操作 -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <!-- 最常用的滚动策略，它根据时间来制定滚动策略.既负责滚动也负责触发滚动 -->
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <!--日志输出位置 可相对、和绝对路径 -->
            <fileNamePattern>
                ${log.home_dir}/error/%d{yyyy-MM-dd}/${log.app_name}-%i.log
            </fileNamePattern>
            <!-- 可选节点，控制保留的归档文件的最大数量，超出数量就删除旧文件,假设设置每个月滚动，且<maxHistory>是6，
            则只保存最近6个月的文件，删除之前的旧文件。注意，删除旧文件是，那些为了归档而创建的目录也会被删除 -->
            <maxHistory>${log.maxHistory}</maxHistory>
            <!--日志文件最大的大小-->
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>
                <!-- 设置日志输出格式 -->
                %d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n
            </pattern>
        </encoder>
    </appender>


    <!-- WARN级别日志 appender -->
    <appender name="WARN" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!-- 过滤器，只记录WARN级别的日志 -->
        <!-- 果日志级别等于配置级别，过滤器会根据onMath 和 onMismatch接收或拒绝日志。 -->
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <!-- 设置过滤级别 -->
            <level>WARN</level>
            <!-- 用于配置符合过滤条件的操作 -->
            <onMatch>ACCEPT</onMatch>
            <!-- 用于配置不符合过滤条件的操作 -->
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <!--日志输出位置 可相对、和绝对路径 -->
            <fileNamePattern>${log.home_dir}/warn/%d{yyyy-MM-dd}/${log.app_name}-%i.log</fileNamePattern>
            <maxHistory>${log.maxHistory}</maxHistory>
            <!--当天的日志大小 超过MaxFileSize时,压缩日志并保存-->
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n</pattern>
        </encoder>
    </appender>


    <!-- INFO级别日志 appender -->
    <appender name="INFO" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>INFO</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>${log.home_dir}/info/%d{yyyy-MM-dd}/${log.app_name}-%i.log</fileNamePattern>
            <maxHistory>${log.maxHistory}</maxHistory>
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5level] %logger - %msg%n</pattern>
        </encoder>
    </appender>


    <!-- DEBUG级别日志 appender -->
    <appender name="DEBUG" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>DEBUG</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>${log.home_dir}/debug/%d{yyyy-MM-dd}/${log.app_name}-%i.log</fileNamePattern>
            <maxHistory>${log.maxHistory}</maxHistory>
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5level] %logger - %msg%n</pattern>
        </encoder>
    </appender>


    <!-- TRACE级别日志 appender -->
    <appender name="TRACE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>TRACE</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>${log.home_dir}/trace/%d{yyyy-MM-dd}/${log.app_name}-%i.log</fileNamePattern>
            <maxHistory>${log.maxHistory}</maxHistory>
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5level] %logger - %msg%n</pattern>
        </encoder>
    </appender>


    <!--设置一个向上传递的appender,所有级别的日志都会输出-->
    <appender name="app" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <rollingPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
            <fileNamePattern>${log.home_dir}/app/%d{yyyy-MM-dd}/${log.app_name}-%i.log</fileNamePattern>
            <maxHistory>${log.maxHistory}</maxHistory>
            <MaxFileSize>${log.maxSize}</MaxFileSize>
        </rollingPolicy>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%-5level] %logger - %msg%n</pattern>
        </encoder>
    </appender>


    <!--org.springframework.web包下的类的日志输出-->
    <logger name="org.springframework.web" additivity="false" level="WARN">
        <appender-ref ref="WARN"/>
    </logger>
    <!--dao层包下的类的日志输出-->
    <logger name="${mapper.package}" additivity="false" level="DEBUG">
        <appender-ref ref="app"/>
        <appender-ref ref="ERROR"/>
        <!--打印控制台-->
        <appender-ref ref="CONSOLE"/>
    </logger>


    <!-- root级别   DEBUG -->
    <root>
        <!-- 打印debug级别日志及以上级别日志 -->
        <level value="${log.level}"/>
        <!-- 控制台输出 -->
        <appender-ref ref="CONSOLE"/>
        <!-- 不管什么包下的日志都输出文件 -->
<!--        <appender-ref ref="ERROR"/>-->
<!--        <appender-ref ref="INFO"/>-->
<!--        <appender-ref ref="WARN"/>-->
<!--        <appender-ref ref="DEBUG"/>-->
<!--        <appender-ref ref="TRACE"/>-->
<!--        不分级别输出-->
        <appender-ref ref="app"/>
    </root>

</configuration>
```





## 四、Web开发

### 1、简介



使用SpringBoot；

**1）、创建SpringBoot应用，选中我们需要的模块；**

**2）、SpringBoot已经默认将这些场景配置好了，只需要在配置文件中指定少量配置就可以运行起来**

**3）、自己编写业务代码；**



**自动配置原理？**

这个场景SpringBoot帮我们配置了什么？能不能修改？能修改哪些配置？能不能扩展？xxx

```
xxxxAutoConfiguration：帮我们给容器中自动配置组件；
xxxxProperties:配置类来封装配置文件的内容；

```



### 2、SpringBoot对静态资源的映射规则；

```java
@ConfigurationProperties(prefix = "spring.resources", ignoreUnknownFields = false)
public class ResourceProperties implements ResourceLoaderAware {
  //可以设置和静态资源有关的参数，缓存时间等
```



```java
	WebMvcAuotConfiguration：
		@Override
		public void addResourceHandlers(ResourceHandlerRegistry registry) {
			if (!this.resourceProperties.isAddMappings()) {
				logger.debug("Default resource handling disabled");
				return;
			}
			Integer cachePeriod = this.resourceProperties.getCachePeriod();
			if (!registry.hasMappingForPattern("/webjars/**")) {
				customizeResourceHandlerRegistration(
						registry.addResourceHandler("/webjars/**")
								.addResourceLocations(
										"classpath:/META-INF/resources/webjars/")
						.setCachePeriod(cachePeriod));
			}
			String staticPathPattern = this.mvcProperties.getStaticPathPattern();
          	//静态资源文件夹映射
			if (!registry.hasMappingForPattern(staticPathPattern)) {
				customizeResourceHandlerRegistration(
						registry.addResourceHandler(staticPathPattern)
								.addResourceLocations(
										this.resourceProperties.getStaticLocations())
						.setCachePeriod(cachePeriod));
			}
		}

        //配置欢迎页映射
		@Bean
		public WelcomePageHandlerMapping welcomePageHandlerMapping(
				ResourceProperties resourceProperties) {
			return new WelcomePageHandlerMapping(resourceProperties.getWelcomePage(),
					this.mvcProperties.getStaticPathPattern());
		}

       //配置喜欢的图标
		@Configuration
		@ConditionalOnProperty(value = "spring.mvc.favicon.enabled", matchIfMissing = true)
		public static class FaviconConfiguration {

			private final ResourceProperties resourceProperties;

			public FaviconConfiguration(ResourceProperties resourceProperties) {
				this.resourceProperties = resourceProperties;
			}

			@Bean
			public SimpleUrlHandlerMapping faviconHandlerMapping() {
				SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
				mapping.setOrder(Ordered.HIGHEST_PRECEDENCE + 1);
              	//所有  **/favicon.ico 
				mapping.setUrlMap(Collections.singletonMap("**/favicon.ico",
						faviconRequestHandler()));
				return mapping;
			}

			@Bean
			public ResourceHttpRequestHandler faviconRequestHandler() {
				ResourceHttpRequestHandler requestHandler = new ResourceHttpRequestHandler();
				requestHandler
						.setLocations(this.resourceProperties.getFaviconLocations());
				return requestHandler;
			}

		}

```



==1）、所有 /webjars/** ，都去 classpath:/META-INF/resources/webjars/ 找资源；==

​	webjars：以jar包的方式引入静态资源；

http://www.webjars.org/

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180203181751.png)

localhost:8080/webjars/jquery/3.3.1/jquery.js

```xml
<!--引入jquery-webjar-->在访问的时候只需要写webjars下面资源的名称即可
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>jquery</artifactId>
			<version>3.3.1</version>
		</dependency>
```



==2）、"/**" 访问当前项目的任何资源，都去（静态资源的文件夹）找映射==

```
"classpath:/META-INF/resources/", 
"classpath:/resources/",
"classpath:/static/", 
"classpath:/public/" 
"/"：当前项目的根路径
```

localhost:8080/abc ===  去静态资源文件夹里面找abc

==3）、欢迎页； 静态资源文件夹下的所有index.html页面；被"/**"映射；==

​	localhost:8080/   找index页面

==4）、所有的 **/favicon.ico  都是在静态资源文件下找；==



### 3、模板引擎

JSP、Velocity、Freemarker、Thymeleaf

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/template-engine.png)



SpringBoot推荐的Thymeleaf；

语法更简单，功能更强大；



#### 1、引入thymeleaf；

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
          	2.1.6
		</dependency>
切换thymeleaf版本
<properties>
		<thymeleaf.version>3.0.9.RELEASE</thymeleaf.version>
		<!-- 布局功能的支持程序  thymeleaf3主程序  layout2以上版本 -->
		<!-- thymeleaf2   layout1-->
		<thymeleaf-layout-dialect.version>2.2.2</thymeleaf-layout-dialect.version>
  </properties>
```



#### 2、Thymeleaf使用

```java
@ConfigurationProperties(prefix = "spring.thymeleaf")
public class ThymeleafProperties {

	private static final Charset DEFAULT_ENCODING = Charset.forName("UTF-8");

	private static final MimeType DEFAULT_CONTENT_TYPE = MimeType.valueOf("text/html");

	public static final String DEFAULT_PREFIX = "classpath:/templates/";

	public static final String DEFAULT_SUFFIX = ".html";
  	//
```

只要我们把HTML页面放在classpath:/templates/，thymeleaf就能自动渲染；

使用：

1、导入thymeleaf的名称空间

```xml
<html lang="en" xmlns:th="http://www.thymeleaf.org">
```

2、使用thymeleaf语法；

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>成功！</h1>
    <!--th:text 将div里面的文本内容设置为 -->
    <div th:text="${hello}">这是显示欢迎信息</div>
</body>
</html>
```

#### 3、语法规则

1）、th:text；改变当前元素里面的文本内容；

​	th：任意html属性；来替换原生属性的值

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2018-02-04_123955.png)



2）、表达式？

```properties
Simple expressions:（表达式语法）
    Variable Expressions: ${...}：获取变量值；OGNL；
    		1）、获取对象的属性、调用方法
    		2）、使用内置的基本对象：
    			#ctx : the context object.
    			#vars: the context variables.
                #locale : the context locale.
                #request : (only in Web Contexts) the HttpServletRequest object.
                #response : (only in Web Contexts) the HttpServletResponse object.
                #session : (only in Web Contexts) the HttpSession object.
                #servletContext : (only in Web Contexts) the ServletContext object.
                
                ${session.foo}
            3）、内置的一些工具对象：
#execInfo : information about the template being processed.
#messages : methods for obtaining externalized messages inside variables expressions, in the same way as they would be obtained using #{…} syntax.
#uris : methods for escaping parts of URLs/URIs
#conversions : methods for executing the configured conversion service (if any).
#dates : methods for java.util.Date objects: formatting, component extraction, etc.
#calendars : analogous to #dates , but for java.util.Calendar objects.
#numbers : methods for formatting numeric objects.
#strings : methods for String objects: contains, startsWith, prepending/appending, etc.
#objects : methods for objects in general.
#bools : methods for boolean evaluation.
#arrays : methods for arrays.
#lists : methods for lists.
#sets : methods for sets.
#maps : methods for maps.
#aggregates : methods for creating aggregates on arrays or collections.
#ids : methods for dealing with id attributes that might be repeated (for example, as a result of an iteration).

    Selection Variable Expressions: *{...}：选择表达式：和${}在功能上是一样；
    	补充：配合 th:object="${session.user}：
   <div th:object="${session.user}">
    <p>Name: <span th:text="*{firstName}">Sebastian</span>.</p>
    <p>Surname: <span th:text="*{lastName}">Pepper</span>.</p>
    <p>Nationality: <span th:text="*{nationality}">Saturn</span>.</p>
    </div>
    
    Message Expressions: #{...}：获取国际化内容
    Link URL Expressions: @{...}：定义URL；
    		@{/order/process(execId=${execId},execType='FAST')}
    Fragment Expressions: ~{...}：片段引用表达式
    		<div th:insert="~{commons :: main}">...</div>
    		
Literals（字面量）
      Text literals: 'one text' , 'Another one!' ,…
      Number literals: 0 , 34 , 3.0 , 12.3 ,…
      Boolean literals: true , false
      Null literal: null
      Literal tokens: one , sometext , main ,…
Text operations:（文本操作）
    String concatenation: +
    Literal substitutions: |The name is ${name}|
Arithmetic operations:（数学运算）
    Binary operators: + , - , * , / , %
    Minus sign (unary operator): -
Boolean operations:（布尔运算）
    Binary operators: and , or
    Boolean negation (unary operator): ! , not
Comparisons and equality:（比较运算）
    Comparators: > , < , >= , <= ( gt , lt , ge , le )
    Equality operators: == , != ( eq , ne )
Conditional operators:条件运算（三元运算符）
    If-then: (if) ? (then)
    If-then-else: (if) ? (then) : (else)
    Default: (value) ?: (defaultvalue)
Special tokens:
    No-Operation: _ 
```

### 4、SpringMVC自动配置

https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications

#### 1. Spring MVC auto-configuration

Spring Boot 自动配置好了SpringMVC

以下是SpringBoot对SpringMVC的默认配置:**==（WebMvcAutoConfiguration）==**

- Inclusion of `ContentNegotiatingViewResolver` and `BeanNameViewResolver` beans.

  - 自动配置了ViewResolver（视图解析器：根据方法的返回值得到视图对象（View），视图对象决定如何渲染（转发？重定向？））
  - ContentNegotiatingViewResolver：组合所有的视图解析器的；
  - ==如何定制：我们可以自己给容器中添加一个视图解析器；自动的将其组合进来；==

- Support for serving static resources, including support for WebJars (see below).静态资源文件夹路径,webjars

- Static `index.html` support. 静态首页访问

- Custom `Favicon` support (see below).  favicon.ico

  

- 自动注册了 of `Converter`, `GenericConverter`, `Formatter` beans.

  - Converter：转换器；  public String hello(User user)：类型转换使用Converter
  - `Formatter`  格式化器；  2017.12.17===Date；

```java
		@Bean
		@ConditionalOnProperty(prefix = "spring.mvc", name = "date-format")//在文件中配置日期格式化的规则
		public Formatter<Date> dateFormatter() {
			return new DateFormatter(this.mvcProperties.getDateFormat());//日期格式化组件
		}
```

​	==自己添加的格式化器转换器，我们只需要放在容器中即可==

- Support for `HttpMessageConverters` (see below).

  - HttpMessageConverter：SpringMVC用来转换Http请求和响应的；User---Json；

  - `HttpMessageConverters` 是从容器中确定；获取所有的HttpMessageConverter；

    ==自己给容器中添加HttpMessageConverter，只需要将自己的组件注册容器中（@Bean,@Component）==

    

- Automatic registration of `MessageCodesResolver` (see below).定义错误代码生成规则

- Automatic use of a `ConfigurableWebBindingInitializer` bean (see below).

  ==我们可以配置一个ConfigurableWebBindingInitializer来替换默认的；（添加到容器）==

  ```
  初始化WebDataBinder；
  请求数据=====JavaBean；
  ```

**org.springframework.boot.autoconfigure.web：web的所有自动场景；**

If you want to keep Spring Boot MVC features, and you just want to add additional [MVC configuration](https://docs.spring.io/spring/docs/4.3.14.RELEASE/spring-framework-reference/htmlsingle#mvc) (interceptors, formatters, view controllers etc.) you can add your own `@Configuration` class of type `WebMvcConfigurerAdapter`, but **without** `@EnableWebMvc`. If you wish to provide custom instances of `RequestMappingHandlerMapping`, `RequestMappingHandlerAdapter` or `ExceptionHandlerExceptionResolver` you can declare a `WebMvcRegistrationsAdapter` instance providing such components.

If you want to take complete control of Spring MVC, you can add your own `@Configuration` annotated with `@EnableWebMvc`.

#### 2、扩展SpringMVC

```xml
    <mvc:view-controller path="/hello" view-name="success"/>
    <mvc:interceptors>
        <mvc:interceptor>
            <mvc:mapping path="/hello"/>
            <bean></bean>
        </mvc:interceptor>
    </mvc:interceptors>
```

**==编写一个配置类（@Configuration），是WebMvcConfigurerAdapter类型；不能标注@EnableWebMvc==**;

既保留了所有的自动配置，也能用我们扩展的配置；

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
@Configuration
//spring 5 采用 WebMvcConfigurer  WebMvcConfigurerAdapter已弃用
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }
}
```

**spring 5 采用 WebMvcConfigurer  WebMvcConfigurerAdapter以弃用**

```java
@Configuration
public class MyConfig implements WebMvcConfigurer {
}
```



原理：

​	1）、WebMvcAutoConfiguration是SpringMVC的自动配置类

​	2）、在做其他自动配置时会导入；@Import(**EnableWebMvcConfiguration**.class)

```java
    @Configuration
	public static class EnableWebMvcConfiguration extends DelegatingWebMvcConfiguration {
      private final WebMvcConfigurerComposite configurers = new WebMvcConfigurerComposite();

	 //从容器中获取所有的WebMvcConfigurer
      @Autowired(required = false)
      public void setConfigurers(List<WebMvcConfigurer> configurers) {
          if (!CollectionUtils.isEmpty(configurers)) {
              this.configurers.addWebMvcConfigurers(configurers);
            	//一个参考实现；将所有的WebMvcConfigurer相关配置都来一起调用；  
            	@Override
             // public void addViewControllers(ViewControllerRegistry registry) {
              //    for (WebMvcConfigurer delegate : this.delegates) {
               //       delegate.addViewControllers(registry);
               //   }
              }
          }
	}
```

​	3）、容器中所有的WebMvcConfigurer都会一起起作用；

​	4）、我们的配置类也会被调用；

​	效果：SpringMVC的自动配置和我们的扩展配置都会起作用；

#### 3、全面接管SpringMVC；

SpringBoot对SpringMVC的自动配置不需要了，所有都是我们自己配置；所有的SpringMVC的自动配置都失效了

**我们需要在配置类中添加@EnableWebMvc即可；**

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
@EnableWebMvc
@Configuration
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }
}
```

原理：

为什么@EnableWebMvc自动配置就失效了；

1）@EnableWebMvc的核心

```java
@Import(DelegatingWebMvcConfiguration.class)
public @interface EnableWebMvc {
```

2）、

```java
@Configuration
public class DelegatingWebMvcConfiguration extends WebMvcConfigurationSupport {
```

3）、

```java
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({ Servlet.class, DispatcherServlet.class,
		WebMvcConfigurerAdapter.class })
//容器中没有这个组件的时候，这个自动配置类才生效
@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 10)
@AutoConfigureAfter({ DispatcherServletAutoConfiguration.class,
		ValidationAutoConfiguration.class })
public class WebMvcAutoConfiguration {
```

4）、@EnableWebMvc将WebMvcConfigurationSupport组件导入进来；

5）、导入的WebMvcConfigurationSupport只是SpringMVC最基本的功能；



### 5、如何修改SpringBoot的默认配置

模式：

​	1）、SpringBoot在自动配置很多组件的时候，先看容器中有没有用户自己配置的（@Bean、@Component）如果有就用用户配置的，如果没有，才自动配置；如果有些组件可以有多个（ViewResolver）将用户配置的和自己默认的组合起来；

​	2）、在SpringBoot中会有非常多的xxxConfigurer帮助我们进行扩展配置

​	3）、在SpringBoot中会有很多的xxxCustomizer帮助我们进行定制配置

### 6、RestfulCRUD

#### 1）、默认访问首页

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
//@EnableWebMvc   不要接管SpringMVC
@Configuration
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }

    //所有的WebMvcConfigurerAdapter组件都会一起起作用
    @Bean //将组件注册在容器
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(){
        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
            }
        };
        return adapter;
    }
}

```

#### 2）、国际化

**1）、编写国际化配置文件；**

2）、使用ResourceBundleMessageSource管理国际化资源文件

3）、在页面使用fmt:message取出国际化内容



步骤：

1）、编写国际化配置文件，抽取页面需要显示的国际化消息

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180211130721.png)



2）、SpringBoot自动配置好了管理国际化资源文件的组件；

```java
@ConfigurationProperties(prefix = "spring.messages")public class MessageSourceAutoConfiguration {        /**	 * Comma-separated list of basenames (essentially a fully-qualified classpath	 * location), each following the ResourceBundle convention with relaxed support for	 * slash based locations. If it doesn't contain a package qualifier (such as	 * "org.mypackage"), it will be resolved from the classpath root.	 */	private String basename = "messages";      //我们的配置文件可以直接放在类路径下叫messages.properties；        @Bean	public MessageSource messageSource() {		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();		if (StringUtils.hasText(this.basename)) {            //设置国际化资源文件的基础名（去掉语言国家代码的）			messageSource.setBasenames(StringUtils.commaDelimitedListToStringArray(					StringUtils.trimAllWhitespace(this.basename)));		}		if (this.encoding != null) {			messageSource.setDefaultEncoding(this.encoding.name());		}		messageSource.setFallbackToSystemLocale(this.fallbackToSystemLocale);		messageSource.setCacheSeconds(this.cacheSeconds);		messageSource.setAlwaysUseMessageFormat(this.alwaysUseMessageFormat);		return messageSource;	}
```



3）、去页面获取国际化的值；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180211134506.png)



```html
<!DOCTYPE html>
<html lang="en"  xmlns:th="http://www.thymeleaf.org">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Signin Template for Bootstrap</title>
		<!-- Bootstrap core CSS -->
		<link href="asserts/css/bootstrap.min.css" th:href="@{/webjars/bootstrap/4.0.0/css/bootstrap.css}" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="asserts/css/signin.css" th:href="@{/asserts/css/signin.css}" rel="stylesheet">
	</head>

	<body class="text-center">
		<form class="form-signin" action="dashboard.html">
			<img class="mb-4" th:src="@{/asserts/img/bootstrap-solid.svg}" src="asserts/img/bootstrap-solid.svg" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">Please sign in</h1>
			<label class="sr-only" th:text="#{login.username}">Username</label>
			<input type="text" class="form-control" placeholder="Username" th:placeholder="#{login.username}" required="" autofocus="">
			<label class="sr-only" th:text="#{login.password}">Password</label>
			<input type="password" class="form-control" placeholder="Password" th:placeholder="#{login.password}" required="">
			<div class="checkbox mb-3">
				<label>
          		<input type="checkbox" value="remember-me"/> [[#{login.remember}]]
        </label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit" th:text="#{login.btn}">Sign in</button>
			<p class="mt-5 mb-3 text-muted">© 2017-2018</p>
			<a class="btn btn-sm">中文</a>
			<a class="btn btn-sm">English</a>
		</form>

	</body>

</html>
```

效果：根据浏览器语言设置的信息切换了国际化；



原理：

​	国际化Locale（区域信息对象）；LocaleResolver（获取区域信息对象）；

```java
		@Bean		@ConditionalOnMissingBean		@ConditionalOnProperty(prefix = "spring.mvc", name = "locale")		public LocaleResolver localeResolver() {			if (this.mvcProperties					.getLocaleResolver() == WebMvcProperties.LocaleResolver.FIXED) {				return new FixedLocaleResolver(this.mvcProperties.getLocale());			}			AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();			localeResolver.setDefaultLocale(this.mvcProperties.getLocale());			return localeResolver;		}默认的就是根据请求头带来的区域信息获取Locale进行国际化
```

4）、点击链接切换国际化

```java
/**
 * 可以在连接上携带区域信息
 */
public class MyLocaleResolver implements LocaleResolver {
    
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String l = request.getParameter("l");
        Locale locale = Locale.getDefault();
        if(!StringUtils.isEmpty(l)){
            String[] split = l.split("_");
            locale = new Locale(split[0],split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}


 @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}


```

#### 3）、登陆

开发期间模板引擎页面修改以后，要实时生效

1）、禁用模板引擎的缓存

```
# 禁用缓存
spring.thymeleaf.cache=false 
```

2）、页面修改完成以后ctrl+f9：重新编译；



登陆错误消息的显示

```html
<p style="color: red" th:text="${msg}" th:if="${not #strings.isEmpty(msg)}"></p>
```



#### 4）、拦截器进行登陆检查

拦截器

```java
/** * 登陆检查， */public class LoginHandlerInterceptor implements HandlerInterceptor {    //目标方法执行之前    @Override    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {        Object user = request.getSession().getAttribute("loginUser");        if(user == null){            //未登陆，返回登陆页面            request.setAttribute("msg","没有权限请先登陆");            request.getRequestDispatcher("/index.html").forward(request,response);            return false;        }else{            //已登陆，放行请求            return true;        }    }    @Override    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {    }    @Override    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {    }}
```



注册拦截器

```java
  //所有的WebMvcConfigurerAdapter组件都会一起起作用    @Bean //将组件注册在容器    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(){        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter() {            @Override            public void addViewControllers(ViewControllerRegistry registry) {                registry.addViewController("/").setViewName("login");                registry.addViewController("/index.html").setViewName("login");                registry.addViewController("/main.html").setViewName("dashboard");            }            //注册拦截器            @Override            public void addInterceptors(InterceptorRegistry registry) {                //super.addInterceptors(registry);                //静态资源；  *.css , *.js                //SpringBoot已经做好了静态资源映射                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")                        .excludePathPatterns("/index.html","/","/user/login");            }        };        return adapter;    }
```

#### 5）、CRUD-员工列表

实验要求：

1）、RestfulCRUD：CRUD满足Rest风格；

URI：  /资源名称/资源标识       HTTP请求方式区分对资源CRUD操作

|      | 普通CRUD（uri来区分操作） | RestfulCRUD       |
| ---- | ------------------------- | ----------------- |
| 查询 | getEmp                    | emp---GET         |
| 添加 | addEmp?xxx                | emp---POST        |
| 修改 | updateEmp?id=xxx&xxx=xx   | emp/{id}---PUT    |
| 删除 | deleteEmp?id=1            | emp/{id}---DELETE |

2）、实验的请求架构;

| 实验功能                             | 请求URI | 请求方式 |
| ------------------------------------ | ------- | -------- |
| 查询所有员工                         | emps    | GET      |
| 查询某个员工(来到修改页面)           | emp/1   | GET      |
| 来到添加页面                         | emp     | GET      |
| 添加员工                             | emp     | POST     |
| 来到修改页面（查出员工进行信息回显） | emp/1   | GET      |
| 修改员工                             | emp     | PUT      |
| 删除员工                             | emp/1   | DELETE   |

3）、员工列表：

##### thymeleaf公共页面元素抽取

```html
1、抽取公共片段<div th:fragment="copy">&copy; 2011 The Good Thymes Virtual Grocery</div>2、引入公共片段<div th:insert="~{footer :: copy}"></div>~{templatename::selector}：模板名::选择器~{templatename::fragmentname}:模板名::片段名3、默认效果：insert的公共片段在div标签中如果使用th:insert等属性进行引入，可以不用写~{}：行内写法可以加上：[[~{}]];[(~{})]；
```



三种引入公共片段的th属性：

**th:insert**：将公共片段整个插入到声明引入的元素中

**th:replace**：将声明引入的元素替换为公共片段

**th:include**：将被引入的片段的内容包含进这个标签中



```html
<footer th:fragment="copy">&copy; 2011 The Good Thymes Virtual Grocery</footer>引入方式<div th:insert="footer :: copy"></div><div th:replace="footer :: copy"></div><div th:include="footer :: copy"></div>效果<div>    <footer>    &copy; 2011 The Good Thymes Virtual Grocery    </footer></div><footer>&copy; 2011 The Good Thymes Virtual Grocery</footer><div>&copy; 2011 The Good Thymes Virtual Grocery</div>
```



引入片段的时候传入参数： 

```html
<nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">    <div class="sidebar-sticky">        <ul class="nav flex-column">            <li class="nav-item">                <a class="nav-link active"                   th:class="${activeUri=='main.html'?'nav-link active':'nav-link'}"                   href="#" th:href="@{/main.html}">                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                        <polyline points="9 22 9 12 15 12 15 22"></polyline>                    </svg>                    Dashboard <span class="sr-only">(current)</span>                </a>            </li><!--引入侧边栏;传入参数--><div th:replace="commons/bar::#sidebar(activeUri='emps')"></div>
```

#### 6）、CRUD-员工添加

添加页面

```html
<form>
    <div class="form-group">
        <label>LastName</label>
        <input type="text" class="form-control" placeholder="zhangsan">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" placeholder="zhangsan@atguigu.com">
    </div>
    <div class="form-group">
        <label>Gender</label><br/>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender"  value="1">
            <label class="form-check-label">男</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender"  value="0">
            <label class="form-check-label">女</label>
        </div>
    </div>
    <div class="form-group">
        <label>department</label>
        <select class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>
    <div class="form-group">
        <label>Birth</label>
        <input type="text" class="form-control" placeholder="zhangsan">
    </div>
    <button type="submit" class="btn btn-primary">添加</button>
</form>
```

提交的数据格式不对：生日：日期；

2017-12-12；2017/12/12；2017.12.12；

日期的格式化；SpringMVC将页面提交的值需要转换为指定的类型;

2017-12-12---Date； 类型转换，格式化;

默认日期是按照/的方式；

#### 7）、CRUD-员工修改

修改添加二合一表单

```html
<!--需要区分是员工修改还是添加；-->
<form th:action="@{/emp}" method="post">
    <!--发送put请求修改员工数据-->
    <!--
1、SpringMVC中配置HiddenHttpMethodFilter;（SpringBoot自动配置好的）
2、页面创建一个post表单
3、创建一个input项，name="_method";值就是我们指定的请求方式
-->
    <input type="hidden" name="_method" value="put" th:if="${emp!=null}"/>
    <input type="hidden" name="id" th:if="${emp!=null}" th:value="${emp.id}">
    <div class="form-group">
        <label>LastName</label>
        <input name="lastName" type="text" class="form-control" placeholder="zhangsan" th:value="${emp!=null}?${emp.lastName}">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="email" type="email" class="form-control" placeholder="zhangsan@atguigu.com" th:value="${emp!=null}?${emp.email}">
    </div>
    <div class="form-group">
        <label>Gender</label><br/>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" value="1" th:checked="${emp!=null}?${emp.gender==1}">
            <label class="form-check-label">男</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" value="0" th:checked="${emp!=null}?${emp.gender==0}">
            <label class="form-check-label">女</label>
        </div>
    </div>
    <div class="form-group">
        <label>department</label>
        <!--提交的是部门的id-->
        <select class="form-control" name="department.id">
            <option th:selected="${emp!=null}?${dept.id == emp.department.id}" th:value="${dept.id}" th:each="dept:${depts}" th:text="${dept.departmentName}">1</option>
        </select>
    </div>
    <div class="form-group">
        <label>Birth</label>
        <input name="birth" type="text" class="form-control" placeholder="zhangsan" th:value="${emp!=null}?${#dates.format(emp.birth, 'yyyy-MM-dd HH:mm')}">
    </div>
    <button type="submit" class="btn btn-primary" th:text="${emp!=null}?'修改':'添加'">添加</button>
</form>
```

#### 8）、CRUD-员工删除

```html
<tr th:each="emp:${emps}">    <td th:text="${emp.id}"></td>    <td>[[${emp.lastName}]]</td>    <td th:text="${emp.email}"></td>    <td th:text="${emp.gender}==0?'女':'男'"></td>    <td th:text="${emp.department.departmentName}"></td>    <td th:text="${#dates.format(emp.birth, 'yyyy-MM-dd HH:mm')}"></td>    <td>        <a class="btn btn-sm btn-primary" th:href="@{/emp/}+${emp.id}">编辑</a>        <button th:attr="del_uri=@{/emp/}+${emp.id}" class="btn btn-sm btn-danger deleteBtn">删除</button>    </td></tr><script>    $(".deleteBtn").click(function(){        //删除当前员工的        $("#deleteEmpForm").attr("action",$(this).attr("del_uri")).submit();        return false;    });</script>
```



### 7、错误处理机制

#### 1）、SpringBoot默认的错误处理机制

默认效果：

​		1）、浏览器，返回一个默认的错误页面

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226173408.png)

  浏览器发送请求的请求头：

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226180347.png)

​		2）、如果是其他客户端，默认响应一个json数据

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226173527.png)

​		![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226180504.png)

原理：

​	可以参照ErrorMvcAutoConfiguration；错误处理的自动配置；

  	给容器中添加了以下组件

​	1、DefaultErrorAttributes：

```java
帮我们在页面共享信息；@Override	public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes,			boolean includeStackTrace) {		Map<String, Object> errorAttributes = new LinkedHashMap<String, Object>();		errorAttributes.put("timestamp", new Date());		addStatus(errorAttributes, requestAttributes);		addErrorDetails(errorAttributes, requestAttributes, includeStackTrace);		addPath(errorAttributes, requestAttributes);		return errorAttributes;	}
```



​	2、BasicErrorController：处理默认/error请求

```java
@Controller@RequestMapping("${server.error.path:${error.path:/error}}")public class BasicErrorController extends AbstractErrorController {        @RequestMapping(produces = "text/html")//产生html类型的数据；浏览器发送的请求来到这个方法处理	public ModelAndView errorHtml(HttpServletRequest request,			HttpServletResponse response) {		HttpStatus status = getStatus(request);		Map<String, Object> model = Collections.unmodifiableMap(getErrorAttributes(				request, isIncludeStackTrace(request, MediaType.TEXT_HTML)));		response.setStatus(status.value());                //去哪个页面作为错误页面；包含页面地址和页面内容		ModelAndView modelAndView = resolveErrorView(request, response, status, model);		return (modelAndView == null ? new ModelAndView("error", model) : modelAndView);	}	@RequestMapping	@ResponseBody    //产生json数据，其他客户端来到这个方法处理；	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {		Map<String, Object> body = getErrorAttributes(request,				isIncludeStackTrace(request, MediaType.ALL));		HttpStatus status = getStatus(request);		return new ResponseEntity<Map<String, Object>>(body, status);	}
```



​	3、ErrorPageCustomizer：

```java
	@Value("${error.path:/error}")	private String path = "/error";  系统出现错误以后来到error请求进行处理；（web.xml注册的错误页面规则）
```



​	4、DefaultErrorViewResolver：

```java
@Override	public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status,			Map<String, Object> model) {		ModelAndView modelAndView = resolve(String.valueOf(status), model);		if (modelAndView == null && SERIES_VIEWS.containsKey(status.series())) {			modelAndView = resolve(SERIES_VIEWS.get(status.series()), model);		}		return modelAndView;	}	private ModelAndView resolve(String viewName, Map<String, Object> model) {        //默认SpringBoot可以去找到一个页面？  error/404		String errorViewName = "error/" + viewName;                //模板引擎可以解析这个页面地址就用模板引擎解析		TemplateAvailabilityProvider provider = this.templateAvailabilityProviders				.getProvider(errorViewName, this.applicationContext);		if (provider != null) {            //模板引擎可用的情况下返回到errorViewName指定的视图地址			return new ModelAndView(errorViewName, model);		}        //模板引擎不可用，就在静态资源文件夹下找errorViewName对应的页面   error/404.html		return resolveResource(errorViewName, model);	}
```



​	步骤：

​		一但系统出现4xx或者5xx之类的错误；ErrorPageCustomizer就会生效（定制错误的响应规则）；就会来到/error请求；就会被**BasicErrorController**处理；

​		1）响应页面；去哪个页面是由**DefaultErrorViewResolver**解析得到的；

```java
protected ModelAndView resolveErrorView(HttpServletRequest request,      HttpServletResponse response, HttpStatus status, Map<String, Object> model) {    //所有的ErrorViewResolver得到ModelAndView   for (ErrorViewResolver resolver : this.errorViewResolvers) {      ModelAndView modelAndView = resolver.resolveErrorView(request, status, model);      if (modelAndView != null) {         return modelAndView;      }   }   return null;}
```

#### 2）、如果定制错误响应：

##### 	**1）、如何定制错误的页面；**

​			**1）、有模板引擎的情况下；error/状态码;** 【将错误页面命名为  错误状态码.html 放在模板引擎文件夹里面的 error文件夹下】，发生此状态码的错误就会来到  对应的页面；

​			我们可以使用4xx和5xx作为错误页面的文件名来匹配这种类型的所有错误，精确优先（优先寻找精确的状态码.html）；		

​			页面能获取的信息；

​				timestamp：时间戳

​				status：状态码

​				error：错误提示

​				exception：异常对象

​				message：异常消息

​				errors：JSR303数据校验的错误都在这里

​			2）、没有模板引擎（模板引擎找不到这个错误页面），静态资源文件夹下找；

​			3）、以上都没有错误页面，就是默认来到SpringBoot默认的错误提示页面；



##### 	2）、如何定制错误的json数据；

​		1）、自定义异常处理&返回定制json数据；

```java
@ControllerAdvice
public class MyExceptionHandler {    @ResponseBody    @ExceptionHandler(UserNotExistException.class)    public Map<String,Object> handleException(Exception e){        Map<String,Object> map = new HashMap<>();        map.put("code","user.notexist");        map.put("message",e.getMessage());        return map;    }}//没有自适应效果...
```



​		2）、转发到/error进行自适应响应效果处理

```java
 @ExceptionHandler(UserNotExistException.class)    public String handleException(Exception e, HttpServletRequest request){        Map<String,Object> map = new HashMap<>();        //传入我们自己的错误状态码  4xx 5xx，否则就不会进入定制错误页面的解析流程        /**         * Integer statusCode = (Integer) request         .getAttribute("javax.servlet.error.status_code");         */        request.setAttribute("javax.servlet.error.status_code",500);        map.put("code","user.notexist");        map.put("message",e.getMessage());        //转发到/error        return "forward:/error";    }
```

##### 	3）、将我们的定制数据携带出去；

出现错误以后，会来到/error请求，会被BasicErrorController处理，响应出去可以获取的数据是由getErrorAttributes得到的（是AbstractErrorController（ErrorController）规定的方法）；

​	1、完全来编写一个ErrorController的实现类【或者是编写AbstractErrorController的子类】，放在容器中；

​	2、页面上能用的数据，或者是json返回能用的数据都是通过errorAttributes.getErrorAttributes得到；

​			容器中DefaultErrorAttributes.getErrorAttributes()；默认进行数据处理的；

自定义ErrorAttributes

```java
//给容器中加入我们自己定义的ErrorAttributes@Componentpublic class MyErrorAttributes extends DefaultErrorAttributes {    @Override    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {        Map<String, Object> map = super.getErrorAttributes(requestAttributes, includeStackTrace);        map.put("company","atguigu");        return map;    }}
```

最终的效果：响应是自适应的，可以通过定制ErrorAttributes改变需要返回的内容，

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180228135513.png)



### 8、配置嵌入式Servlet容器

SpringBoot默认使用Tomcat作为嵌入式的Servlet容器；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180301142915.png)



问题？

#### 1）、如何定制和修改Servlet容器的相关配置；

1、修改和server有关的配置（ServerProperties【也是EmbeddedServletContainerCustomizer】）；

```properties
server.port=8081server.context-path=/crudserver.tomcat.uri-encoding=UTF-8//通用的Servlet容器设置server.xxx//Tomcat的设置server.tomcat.xxx
```

2、编写一个**EmbeddedServletContainerCustomizer**：嵌入式的Servlet容器的定制器；来修改Servlet容器的配置

```java
@Bean  //一定要将这个定制器加入到容器中
public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer(){    return new EmbeddedServletContainerCustomizer() {        //定制嵌入式的Servlet容器相关的规则        @Override        
    public void customize(ConfigurableEmbeddedServletContainer container) {            container.setPort(8083);        }    };}
```

**EmbeddedServletContainerCustomizer** 被**WebServerFactoryCustomizer**替换

```java
@Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer(){
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                factory.setPort(8081);
            }
        };
    }
```



#### 2）、注册Servlet三大组件【Servlet、Filter、Listener】

由于SpringBoot默认是以jar包的方式启动嵌入式的Servlet容器来启动SpringBoot的web应用，没有web.xml文件。

注册三大组件用以下方式

ServletRegistrationBean

```java
//注册三大组件@Beanpublic ServletRegistrationBean myServlet(){    ServletRegistrationBean registrationBean = new ServletRegistrationBean(new MyServlet(),"/myServlet");    return registrationBean;}
```

FilterRegistrationBean

```java
@Bean
public FilterRegistrationBean myFilter(){    FilterRegistrationBean registrationBean = new FilterRegistrationBean();    registrationBean.setFilter(new MyFilter());    registrationBean.setUrlPatterns(Arrays.asList("/hello","/myServlet"));    return registrationBean;}
```

ServletListenerRegistrationBean

```java
@Bean
public ServletListenerRegistrationBean myListener(){    ServletListenerRegistrationBean<MyListener> registrationBean = new ServletListenerRegistrationBean<>(new MyListener());    return registrationBean;}
```



SpringBoot帮我们自动SpringMVC的时候，自动的注册SpringMVC的前端控制器；DIspatcherServlet；

DispatcherServletAutoConfiguration中：

```java
@Bean(name = DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME)@ConditionalOnBean(value = DispatcherServlet.class, name = DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)public ServletRegistrationBean dispatcherServletRegistration(      DispatcherServlet dispatcherServlet) {   ServletRegistrationBean registration = new ServletRegistrationBean(         dispatcherServlet, this.serverProperties.getServletMapping());    //默认拦截： /  所有请求；包静态资源，但是不拦截jsp请求；   /*会拦截jsp    //可以通过server.servletPath来修改SpringMVC前端控制器默认拦截的请求路径       registration.setName(DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);   registration.setLoadOnStartup(         this.webMvcProperties.getServlet().getLoadOnStartup());   if (this.multipartConfig != null) {      registration.setMultipartConfig(this.multipartConfig);   }   return registration;}
```

2）、SpringBoot能不能支持其他的Servlet容器；

#### 3）、替换为其他嵌入式Servlet容器

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302114401.png)

默认支持：

Tomcat（默认使用）

```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   引入web模块默认就是使用嵌入式的Tomcat作为Servlet容器；
</dependency>
```

Jetty

```xml
<!-- 引入web模块 -->
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   <exclusions>
      <exclusion>
         <artifactId>spring-boot-starter-tomcat</artifactId>
         <groupId>org.springframework.boot</groupId>
      </exclusion>
   </exclusions>
</dependency>

<!--引入其他的Servlet容器-->
<dependency>
   <artifactId>spring-boot-starter-jetty</artifactId>
   <groupId>org.springframework.boot</groupId>
</dependency>
```

Undertow

```xml
<!-- 引入web模块 -->
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   <exclusions>
      <exclusion>
         <artifactId>spring-boot-starter-tomcat</artifactId>
         <groupId>org.springframework.boot</groupId>
      </exclusion>
   </exclusions>
</dependency>

<!--引入其他的Servlet容器-->
<dependency>
   <artifactId>spring-boot-starter-undertow</artifactId>
   <groupId>org.springframework.boot</groupId>
</dependency>
```

#### 4）、嵌入式Servlet容器自动配置原理；



EmbeddedServletContainerAutoConfiguration：嵌入式的Servlet容器自动配置？

```java
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)@Configuration@ConditionalOnWebApplication@Import(BeanPostProcessorsRegistrar.class)//导入BeanPostProcessorsRegistrar：Spring注解版；给容器中导入一些组件//导入了EmbeddedServletContainerCustomizerBeanPostProcessor：//后置处理器：bean初始化前后（创建完对象，还没赋值赋值）执行初始化工作public class EmbeddedServletContainerAutoConfiguration {        @Configuration	@ConditionalOnClass({ Servlet.class, Tomcat.class })//判断当前是否引入了Tomcat依赖；	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)//判断当前容器没有用户自己定义EmbeddedServletContainerFactory：嵌入式的Servlet容器工厂；作用：创建嵌入式的Servlet容器	public static class EmbeddedTomcat {		@Bean		public TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory() {			return new TomcatEmbeddedServletContainerFactory();		}	}        /**	 * Nested configuration if Jetty is being used.	 */	@Configuration	@ConditionalOnClass({ Servlet.class, Server.class, Loader.class,			WebAppContext.class })	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)	public static class EmbeddedJetty {		@Bean		public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory() {			return new JettyEmbeddedServletContainerFactory();		}	}	/**	 * Nested configuration if Undertow is being used.	 */	@Configuration	@ConditionalOnClass({ Servlet.class, Undertow.class, SslClientAuthMode.class })	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)	public static class EmbeddedUndertow {		@Bean		public UndertowEmbeddedServletContainerFactory undertowEmbeddedServletContainerFactory() {			return new UndertowEmbeddedServletContainerFactory();		}	}
```

1）、EmbeddedServletContainerFactory（嵌入式Servlet容器工厂）

```java
public interface EmbeddedServletContainerFactory {   //获取嵌入式的Servlet容器   EmbeddedServletContainer getEmbeddedServletContainer(         ServletContextInitializer... initializers);}
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302144835.png)

2）、EmbeddedServletContainer：（嵌入式的Servlet容器）

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302144910.png)



3）、以**TomcatEmbeddedServletContainerFactory**为例

```java
@Overridepublic EmbeddedServletContainer getEmbeddedServletContainer(      ServletContextInitializer... initializers) {    //创建一个Tomcat   Tomcat tomcat = new Tomcat();        //配置Tomcat的基本环节   File baseDir = (this.baseDirectory != null ? this.baseDirectory         : createTempDir("tomcat"));   tomcat.setBaseDir(baseDir.getAbsolutePath());   Connector connector = new Connector(this.protocol);   tomcat.getService().addConnector(connector);   customizeConnector(connector);   tomcat.setConnector(connector);   tomcat.getHost().setAutoDeploy(false);   configureEngine(tomcat.getEngine());   for (Connector additionalConnector : this.additionalTomcatConnectors) {      tomcat.getService().addConnector(additionalConnector);   }   prepareContext(tomcat.getHost(), initializers);        //将配置好的Tomcat传入进去，返回一个EmbeddedServletContainer；并且启动Tomcat服务器   return getTomcatEmbeddedServletContainer(tomcat);}
```

4）、我们对嵌入式容器的配置修改是怎么生效？

```
ServerProperties、EmbeddedServletContainerCustomizer
```



**EmbeddedServletContainerCustomizer**：定制器帮我们修改了Servlet容器的配置？

怎么修改的原理？

5）、容器中导入了**EmbeddedServletContainerCustomizerBeanPostProcessor**

```java
//初始化之前@Overridepublic Object postProcessBeforeInitialization(Object bean, String beanName)      throws BeansException {    //如果当前初始化的是一个ConfigurableEmbeddedServletContainer类型的组件   if (bean instanceof ConfigurableEmbeddedServletContainer) {       //      postProcessBeforeInitialization((ConfigurableEmbeddedServletContainer) bean);   }   return bean;}private void postProcessBeforeInitialization(			ConfigurableEmbeddedServletContainer bean) {    //获取所有的定制器，调用每一个定制器的customize方法来给Servlet容器进行属性赋值；    for (EmbeddedServletContainerCustomizer customizer : getCustomizers()) {        customizer.customize(bean);    }}private Collection<EmbeddedServletContainerCustomizer> getCustomizers() {    if (this.customizers == null) {        // Look up does not include the parent context        this.customizers = new ArrayList<EmbeddedServletContainerCustomizer>(            this.beanFactory            //从容器中获取所有这葛类型的组件：EmbeddedServletContainerCustomizer            //定制Servlet容器，给容器中可以添加一个EmbeddedServletContainerCustomizer类型的组件            .getBeansOfType(EmbeddedServletContainerCustomizer.class,                            false, false)            .values());        Collections.sort(this.customizers, AnnotationAwareOrderComparator.INSTANCE);        this.customizers = Collections.unmodifiableList(this.customizers);    }    return this.customizers;}ServerProperties也是定制器
```

步骤：

1）、SpringBoot根据导入的依赖情况，给容器中添加相应的EmbeddedServletContainerFactory【TomcatEmbeddedServletContainerFactory】

2）、容器中某个组件要创建对象就会惊动后置处理器；EmbeddedServletContainerCustomizerBeanPostProcessor；

只要是嵌入式的Servlet容器工厂，后置处理器就工作；

3）、后置处理器，从容器中获取所有的**EmbeddedServletContainerCustomizer**，调用定制器的定制方法



###5）、嵌入式Servlet容器启动原理；

什么时候创建嵌入式的Servlet容器工厂？什么时候获取嵌入式的Servlet容器并启动Tomcat；

获取嵌入式的Servlet容器工厂：

1）、SpringBoot应用启动运行run方法

2）、refreshContext(context);SpringBoot刷新IOC容器【创建IOC容器对象，并初始化容器，创建容器中的每一个组件】；如果是web应用创建**AnnotationConfigEmbeddedWebApplicationContext**，否则：**AnnotationConfigApplicationContext**

3）、refresh(context);**刷新刚才创建好的ioc容器；**

```java
public void refresh() throws BeansException, IllegalStateException {
   synchronized (this.startupShutdownMonitor) {
      // Prepare this context for refreshing.
      prepareRefresh();

      // Tell the subclass to refresh the internal bean factory.
      ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

      // Prepare the bean factory for use in this context.
      prepareBeanFactory(beanFactory);

      try {
         // Allows post-processing of the bean factory in context subclasses.
         postProcessBeanFactory(beanFactory);

         // Invoke factory processors registered as beans in the context.
         invokeBeanFactoryPostProcessors(beanFactory);

         // Register bean processors that intercept bean creation.
         registerBeanPostProcessors(beanFactory);

         // Initialize message source for this context.
         initMessageSource();

         // Initialize event multicaster for this context.
         initApplicationEventMulticaster();

         // Initialize other special beans in specific context subclasses.
         onRefresh();

         // Check for listener beans and register them.
         registerListeners();

         // Instantiate all remaining (non-lazy-init) singletons.
         finishBeanFactoryInitialization(beanFactory);

         // Last step: publish corresponding event.
         finishRefresh();
      }

      catch (BeansException ex) {
         if (logger.isWarnEnabled()) {
            logger.warn("Exception encountered during context initialization - " +
                  "cancelling refresh attempt: " + ex);
         }

         // Destroy already created singletons to avoid dangling resources.
         destroyBeans();

         // Reset 'active' flag.
         cancelRefresh(ex);

         // Propagate exception to caller.
         throw ex;
      }

      finally {
         // Reset common introspection caches in Spring's core, since we
         // might not ever need metadata for singleton beans anymore...
         resetCommonCaches();
      }
   }
}
```

4）、  onRefresh(); web的ioc容器重写了onRefresh方法

5）、webioc容器会创建嵌入式的Servlet容器；**createEmbeddedServletContainer**();

**6）、获取嵌入式的Servlet容器工厂：**

EmbeddedServletContainerFactory containerFactory = getEmbeddedServletContainerFactory();

​	从ioc容器中获取EmbeddedServletContainerFactory 组件；**TomcatEmbeddedServletContainerFactory**创建对象，后置处理器一看是这个对象，就获取所有的定制器来先定制Servlet容器的相关配置；

7）、**使用容器工厂获取嵌入式的Servlet容器**：this.embeddedServletContainer = containerFactory      .getEmbeddedServletContainer(getSelfInitializer());

8）、嵌入式的Servlet容器创建对象并启动Servlet容器；

**先启动嵌入式的Servlet容器，再将ioc容器中剩下没有创建出的对象获取出来；**

**==IOC容器启动创建嵌入式的Servlet容器==**



### 9、使用外置的Servlet容器

嵌入式Servlet容器：应用打成可执行的jar

​		优点：简单、便携；

​		缺点：默认不支持JSP、优化定制比较复杂（使用定制器【ServerProperties、自定义EmbeddedServletContainerCustomizer】，自己编写嵌入式Servlet容器的创建工厂【EmbeddedServletContainerFactory】）；



外置的Servlet容器：外面安装Tomcat---应用war包的方式打包；

#### 步骤

1）、必须创建一个war项目；（利用idea创建好目录结构）

2）、将嵌入式的Tomcat指定为provided；

```xml
<dependency>   <groupId>org.springframework.boot</groupId>   <artifactId>spring-boot-starter-tomcat</artifactId>   <scope>provided</scope></dependency>
```

3）、必须编写一个**SpringBootServletInitializer**的子类，并调用configure方法

```java
public class ServletInitializer extends SpringBootServletInitializer {

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
       //传入SpringBoot应用的主程序
      return application.sources(SpringBoot04WebJspApplication.class);
   }

}
```

4）、启动服务器就可以使用；

#### 原理

jar包：执行SpringBoot主类的main方法，启动ioc容器，创建嵌入式的Servlet容器；

war包：启动服务器，**服务器启动SpringBoot应用**【SpringBootServletInitializer】，启动ioc容器；



servlet3.0（Spring注解版）：

8.2.4 Shared libraries / runtimes pluggability：

规则：

​	1）、服务器启动（web应用启动）会创建当前web应用里面每一个jar包里面ServletContainerInitializer实例：

​	2）、ServletContainerInitializer的实现放在jar包的META-INF/services文件夹下，有一个名为javax.servlet.ServletContainerInitializer的文件，内容就是ServletContainerInitializer的实现类的全类名

​	3）、还可以使用@HandlesTypes，在应用启动的时候加载我们感兴趣的类；



流程：

1）、启动Tomcat

2）、org\springframework\spring-web\4.3.14.RELEASE\spring-web-4.3.14.RELEASE.jar!\META-INF\services\javax.servlet.ServletContainerInitializer：

Spring的web模块里面有这个文件：**org.springframework.web.SpringServletContainerInitializer**

3）、SpringServletContainerInitializer将@HandlesTypes(WebApplicationInitializer.class)标注的所有这个类型的类都传入到onStartup方法的Set<Class<?>>；为这些WebApplicationInitializer类型的类创建实例；

4）、每一个WebApplicationInitializer都调用自己的onStartup；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302221835.png)

5）、相当于我们的SpringBootServletInitializer的类会被创建对象，并执行onStartup方法

6）、SpringBootServletInitializer实例执行onStartup的时候会createRootApplicationContext；创建容器

```java
protected WebApplicationContext createRootApplicationContext(
      ServletContext servletContext) {
    //1、创建SpringApplicationBuilder
   SpringApplicationBuilder builder = createSpringApplicationBuilder();
   StandardServletEnvironment environment = new StandardServletEnvironment();
   environment.initPropertySources(servletContext, null);
   builder.environment(environment);
   builder.main(getClass());
   ApplicationContext parent = getExistingRootWebApplicationContext(servletContext);
   if (parent != null) {
      this.logger.info("Root context already created (using as parent).");
      servletContext.setAttribute(
            WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, null);
      builder.initializers(new ParentContextApplicationContextInitializer(parent));
   }
   builder.initializers(
         new ServletContextApplicationContextInitializer(servletContext));
   builder.contextClass(AnnotationConfigEmbeddedWebApplicationContext.class);
    
    //调用configure方法，子类重写了这个方法，将SpringBoot的主程序类传入了进来
   builder = configure(builder);
    
    //使用builder创建一个Spring应用
   SpringApplication application = builder.build();
   if (application.getSources().isEmpty() && AnnotationUtils
         .findAnnotation(getClass(), Configuration.class) != null) {
      application.getSources().add(getClass());
   }
   Assert.state(!application.getSources().isEmpty(),
         "No SpringApplication sources have been defined. Either override the "
               + "configure method or add an @Configuration annotation");
   // Ensure error pages are registered
   if (this.registerErrorPageFilter) {
      application.getSources().add(ErrorPageFilterConfiguration.class);
   }
    //启动Spring应用
   return run(application);
}
```

7）、Spring的应用就启动并且创建IOC容器

```java
public ConfigurableApplicationContext run(String... args) {
   StopWatch stopWatch = new StopWatch();
   stopWatch.start();
   ConfigurableApplicationContext context = null;
   FailureAnalyzers analyzers = null;
   configureHeadlessProperty();
   SpringApplicationRunListeners listeners = getRunListeners(args);
   listeners.starting();
   try {
      ApplicationArguments applicationArguments = new DefaultApplicationArguments(
            args);
      ConfigurableEnvironment environment = prepareEnvironment(listeners,
            applicationArguments);
      Banner printedBanner = printBanner(environment);
      context = createApplicationContext();
      analyzers = new FailureAnalyzers(context);
      prepareContext(context, environment, listeners, applicationArguments,
            printedBanner);
       
       //刷新IOC容器
      refreshContext(context);
      afterRefresh(context, applicationArguments);
      listeners.finished(context, null);
      stopWatch.stop();
      if (this.logStartupInfo) {
         new StartupInfoLogger(this.mainApplicationClass)
               .logStarted(getApplicationLog(), stopWatch);
      }
      return context;
   }
   catch (Throwable ex) {
      handleRunFailure(context, listeners, analyzers, ex);
      throw new IllegalStateException(ex);
   }
}
```

**==启动Servlet容器，再启动SpringBoot应用==**



## 五、SpringBoot与数据访问

### 1、JDBC

```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
```



```yaml
spring:
  datasource:
    username: root
    password: 123456
    url: jdbc:mysql://192.168.15.22:3306/jdbc
    driver-class-name: com.mysql.jdbc.Driver
```

效果：

​	默认是用org.apache.tomcat.jdbc.pool.DataSource作为数据源；

​	数据源的相关配置都在DataSourceProperties里面；

自动配置原理：

org.springframework.boot.autoconfigure.jdbc：

1、参考DataSourceConfiguration，根据配置创建数据源，默认使用Tomcat连接池；可以使用spring.datasource.type指定自定义的数据源类型；

2、SpringBoot默认可以支持；

```
org.apache.tomcat.jdbc.pool.DataSource、HikariDataSource、BasicDataSource、
```

3、自定义数据源类型

```java
/**
 * Generic DataSource configuration.
 */
@ConditionalOnMissingBean(DataSource.class)
@ConditionalOnProperty(name = "spring.datasource.type")
static class Generic {

   @Bean
   public DataSource dataSource(DataSourceProperties properties) {
       //使用DataSourceBuilder创建数据源，利用反射创建响应type的数据源，并且绑定相关属性
      return properties.initializeDataSourceBuilder().build();
   }

}
```

4、**DataSourceInitializer：ApplicationListener**；

​	作用：

​		1）、runSchemaScripts();运行建表语句；

​		2）、runDataScripts();运行插入数据的sql语句；

默认只需要将文件命名为：

```properties
schema-*.sql、data-*.sql
默认规则：schema.sql，schema-all.sql；
可以使用   
	schema:
      - classpath:department.sql
      指定位置
      
      

      
```

```yaml
2.x需要添加配置
sql:
    init:
      mode: always
      
 2.5+
   sql:
    init:
      mode: always
      schema-locations:
        - classpath:employee.sql
        - classpath:department.sql
```



5、操作数据库：自动配置了JdbcTemplate操作数据库

### 2、整合Druid数据源

```java
导入druid数据源
@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druid(){
       return  new DruidDataSource();
    }

    //配置Druid的监控
    //1、配置一个管理后台的Servlet
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        Map<String,String> initParams = new HashMap<>();

        initParams.put("loginUsername","admin");
        initParams.put("loginPassword","123456");
        initParams.put("allow","");//默认就是允许所有访问
        initParams.put("deny","192.168.15.21");

        bean.setInitParameters(initParams);
        return bean;
    }


    //2、配置一个web监控的filter
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());

        Map<String,String> initParams = new HashMap<>();
        initParams.put("exclusions","*.js,*.css,/druid/*");

        bean.setInitParameters(initParams);

        bean.setUrlPatterns(Arrays.asList("/*"));

        return  bean;
    }
}

```

yaml 配置

```properties
spring:
  datasource:
    #1.JDBC
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/springboot?useUnicode=true&characterEncoding=utf8
    username: root
    password: 123
    druid:
      #2.连接池配置
      #初始化连接池的连接数量 大小，最小，最大
      initial-size: 5
      min-idle: 5
      max-active: 20
      #配置获取连接等待超时的时间
      max-wait: 60000
       #配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
      time-between-eviction-runs-millis: 60000
      # 配置一个连接在池中最小生存的时间，单位是毫秒
      min-evictable-idle-time-millis: 30000
      validation-query: SELECT 1 FROM DUAL
      test-while-idle: true
      test-on-borrow: true
      test-on-return: false
      # 是否缓存preparedStatement，也就是PSCache  官方建议MySQL下建议关闭   个人建议如果想用SQL防火墙 建议打开
      pool-prepared-statements: true
      max-pool-prepared-statement-per-connection-size: 20
      # 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
      filter:
        stat:
          merge-sql: true
          slow-sql-millis: 5000
      #3.基础监控配置
      web-stat-filter:
        enabled: true
        url-pattern: /*
        #设置不统计哪些URL
        exclusions: "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*"
        session-stat-enable: true
        session-stat-max-count: 100
      stat-view-servlet:
        enabled: true
        url-pattern: /druid/*
        reset-enable: true
        #设置监控页面的登录名和密码
        login-username: admin
        login-password: admin
        allow: 127.0.0.1
        #deny: 192.168.1.100
```



#### 问题

##### 数据库连接池连接失败后一直重连

将改 `break-after-acquire-failure` 设置成 `true`,在 application.properties 文件如下配置：

```
spring.datasource.druid.break-after-acquire-failure=true
```

如果想多尝试连接几次，需要设置 `connection-error-retry-attempts` ,当 `errorCount` 大于 `connectionErrorRetryAttempts` 才会进入到 条件内，才会中断循环。在 application.properties 文件如下配置：

```
spring.datasource.druid.connection-error-retry-attempts=3
```

> druid 数据库连接失败，是因为在使用多线程连接数据时使用了无限制循环连接，需要在连接失败中断连接，需要设置 `break-after-acquire-failure` 为 `true`。设置之后数据库连接不成功也不会不断的重试。如果要设置重连次数要设置 `connection-error-retry-attempts`

##### 监控界面404

> 查看一下自己的项目，是否配置了项目路径
>
> 如果配置了项目路径，访问[http://localhost](https://links.jianshu.com/go?to=http%3A%2F%2Flocalhost):服务端口/项目路径/druid/login.html即可



### 3、整合MyBatis

```xml
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>1.3.1</version>
		</dependency>
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180305194443.png)

步骤：

​	1）、配置数据源相关属性（见上一节Druid）

​	2）、给数据库建表

​	3）、创建JavaBean

#### 	4）、注解版

```java
//指定这是一个操作数据库的mapper
@Mapper
public interface DepartmentMapper {

    @Select("select * from department where id=#{id}")
    public Department getDeptById(Integer id);

    @Delete("delete from department where id=#{id}")
    public int deleteDeptById(Integer id);

    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Insert("insert into department(departmentName) values(#{departmentName})")
    public int insertDept(Department department);

    @Update("update department set departmentName=#{departmentName} where id=#{id}")
    public int updateDept(Department department);
}
```

问题：

自定义MyBatis的配置规则；给容器中添加一个ConfigurationCustomizer；

```java
@org.springframework.context.annotation.Configuration
public class MyBatisConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer(){
        return new ConfigurationCustomizer(){

            @Override
            public void customize(Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }
}
```



```java
使用MapperScan批量扫描所有的Mapper接口；
@MapperScan(value = "com.atguigu.springboot.mapper")
@SpringBootApplication
public class SpringBoot06DataMybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot06DataMybatisApplication.class, args);
	}
}
```

#### 5）、配置文件版

```yaml
mybatis:
  config-location: classpath:mybatis/mybatis-config.xml 指定全局配置文件的位置
  mapper-locations: classpath:mybatis/mapper/*.xml  指定sql映射文件的位置
```

开启驼峰映射

```xml
<configuration>  
    <settings>  
        <setting name="mapUnderscoreToCamelCase" value="true" />  
    </settings>  
</configuration>
```



更多使用参照

http://www.mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/



### 4、整合SpringData JPA

#### 1）、SpringData简介

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306105412.png)

#### 2）、整合SpringData JPA

JPA:ORM（Object Relational Mapping）；

1）、编写一个实体类（bean）和数据表进行映射，并且配置好映射关系；

```java
//使用JPA注解配置映射关系
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "tbl_user") //@Table来指定和哪个数据表对应;如果省略默认表名就是user；
public class User {

    @Id //这是一个主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键
    private Integer id;

    @Column(name = "last_name",length = 50) //这是和数据表对应的一个列
    private String lastName;
    @Column //省略默认列名就是属性名
    private String email;
```

2）、编写一个Dao接口来操作实体类对应的数据表（Repository）

```java
//继承JpaRepository来完成对数据库的操作
public interface UserRepository extends JpaRepository<User,Integer> {
}

```

3）、基本的配置JpaProperties

```yaml
spring:  
 jpa:
    hibernate:
#     更新或者创建数据表结构
      ddl-auto: update
#    控制台显示SQL
    show-sql: true
```

#### 3)问题

##### No serializer found for class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyIn

###### **@JsonIgnoreProperties(vh4alue={"hibernateLazyInitializer"})** 属性

在此标记不生成json对象的属性

因为jsonplugin用的是java的内审机制.hibernate会给被管理的pojo加入一个hibernateLazyInitializer属性,jsonplugin会把hibernateLazyInitializer也拿出来操作,并读取里面一个不能被反射操作的属性就产生了这个异常.  



用annotation来排除hibernateLazyInitializer 这个属性

在你的pojo类声明加上：

```java
@JsonIgnoreProperties(value={"hibernateLazyInitializer"})  
@JsonIgnoreProperties就是标注 哪个属性 不用转化为json的
```



## 六、启动配置原理



几个重要的事件回调机制

配置在META-INF/spring.factories

**ApplicationContextInitializer**

**SpringApplicationRunListener**



只需要放在ioc容器中

**ApplicationRunner**

**CommandLineRunner**



启动流程：

### **1、创建SpringApplication对象**

```java
initialize(sources);
private void initialize(Object[] sources) {
    //保存主配置类
    if (sources != null && sources.length > 0) {
        this.sources.addAll(Arrays.asList(sources));
    }
    //判断当前是否一个web应用
    this.webEnvironment = deduceWebEnvironment();
    //从类路径下找到META-INF/spring.factories配置的所有ApplicationContextInitializer；然后保存起来
    setInitializers((Collection) getSpringFactoriesInstances(
        ApplicationContextInitializer.class));
    //从类路径下找到ETA-INF/spring.factories配置的所有ApplicationListener
    setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
    //从多个配置类中找到有main方法的主配置类
    this.mainApplicationClass = deduceMainApplicationClass();
}
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306145727.png)

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306145855.png)

### 2、运行run方法

```java
public ConfigurableApplicationContext run(String... args) {
   StopWatch stopWatch = new StopWatch();
   stopWatch.start();
   ConfigurableApplicationContext context = null;
   FailureAnalyzers analyzers = null;
   configureHeadlessProperty();
    
   //获取SpringApplicationRunListeners；从类路径下META-INF/spring.factories
   SpringApplicationRunListeners listeners = getRunListeners(args);
    //回调所有的获取SpringApplicationRunListener.starting()方法
   listeners.starting();
   try {
       //封装命令行参数
      ApplicationArguments applicationArguments = new DefaultApplicationArguments(
            args);
      //准备环境
      ConfigurableEnvironment environment = prepareEnvironment(listeners,
            applicationArguments);
       		//创建环境完成后回调SpringApplicationRunListener.environmentPrepared()；表示环境准备完成
       
      Banner printedBanner = printBanner(environment);
       
       //创建ApplicationContext；决定创建web的ioc还是普通的ioc
      context = createApplicationContext();
       
      analyzers = new FailureAnalyzers(context);
       //准备上下文环境;将environment保存到ioc中；而且applyInitializers()；
       //applyInitializers()：回调之前保存的所有的ApplicationContextInitializer的initialize方法
       //回调所有的SpringApplicationRunListener的contextPrepared()；
       //
      prepareContext(context, environment, listeners, applicationArguments,
            printedBanner);
       //prepareContext运行完成以后回调所有的SpringApplicationRunListener的contextLoaded（）；
       
       //s刷新容器；ioc容器初始化（如果是web应用还会创建嵌入式的Tomcat）；Spring注解版
       //扫描，创建，加载所有组件的地方；（配置类，组件，自动配置）
      refreshContext(context);
       //从ioc容器中获取所有的ApplicationRunner和CommandLineRunner进行回调
       //ApplicationRunner先回调，CommandLineRunner再回调
      afterRefresh(context, applicationArguments);
       //所有的SpringApplicationRunListener回调finished方法
      listeners.finished(context, null);
      stopWatch.stop();
      if (this.logStartupInfo) {
         new StartupInfoLogger(this.mainApplicationClass)
               .logStarted(getApplicationLog(), stopWatch);
      }
       //整个SpringBoot应用启动完成以后返回启动的ioc容器；
      return context;
   }
   catch (Throwable ex) {
      handleRunFailure(context, listeners, analyzers, ex);
      throw new IllegalStateException(ex);
   }
}
```

### 3、事件监听机制

配置在META-INF/spring.factories

**ApplicationContextInitializer**

```java
public class HelloApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println("ApplicationContextInitializer...initialize..."+applicationContext);
    }
}

```

**SpringApplicationRunListener**

```java
public class HelloSpringApplicationRunListener implements SpringApplicationRunListener {

    //必须有的构造器
    public HelloSpringApplicationRunListener(SpringApplication application, String[] args){

    }

    @Override
    public void starting() {
        System.out.println("SpringApplicationRunListener...starting...");
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
        Object o = environment.getSystemProperties().get("os.name");
        System.out.println("SpringApplicationRunListener...environmentPrepared.."+o);
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        System.out.println("SpringApplicationRunListener...contextPrepared...");
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        System.out.println("SpringApplicationRunListener...contextLoaded...");
    }

    @Override
    public void finished(ConfigurableApplicationContext context, Throwable exception) {
        System.out.println("SpringApplicationRunListener...finished...");
    }
}

```

配置（META-INF/spring.factories）

```properties
org.springframework.context.ApplicationContextInitializer=\com.atguigu.springboot.listener.HelloApplicationContextInitializerorg.springframework.boot.SpringApplicationRunListener=\com.atguigu.springboot.listener.HelloSpringApplicationRunListener
```





只需要放在ioc容器中

**ApplicationRunner**

```java
@Component
public class HelloApplicationRunner implements ApplicationRunner {
    @Override
        public void run(ApplicationArguments args) throws Exception {
        System.out.println("ApplicationRunner...run....");
    }
}
```



**CommandLineRunner**

```java
@Component
public class HelloCommandLineRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("CommandLineRunner...run..."+ Arrays.asList(args));
    }
}
```



### 七、自定义starter

starter：

​	1、这个场景需要使用到的依赖是什么？

​	2、如何编写自动配置

```java
@Configuration  //指定这个类是一个配置类
@ConditionalOnXXX  //在指定条件成立的情况下自动配置类生效
@AutoConfigureAfter  //指定自动配置类的顺序
@Bean  //给容器中添加组件

@ConfigurationPropertie结合相关xxxProperties类来绑定相关的配置
@EnableConfigurationProperties //让xxxProperties生效加入到容器中

自动配置类要能加载
将需要启动就加载的自动配置类，配置在META-INF/spring.factories
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
```

​	3、模式：

启动器只用来做依赖导入；

专门来写一个自动配置模块；

启动器依赖自动配置；别人只需要引入启动器（starter）

mybatis-spring-boot-starter；自定义启动器名-spring-boot-starter



步骤：

1）、启动器模块

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.atguigu.starter</groupId>
    <artifactId>atguigu-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!--启动器-->
    <dependencies>

        <!--引入自动配置模块-->
        <dependency>
            <groupId>com.atguigu.starter</groupId>
            <artifactId>atguigu-spring-boot-starter-autoconfigurer</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
    </dependencies>

</project>
```

2）、自动配置模块

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>

   <groupId>com.atguigu.starter</groupId>
   <artifactId>atguigu-spring-boot-starter-autoconfigurer</artifactId>
   <version>0.0.1-SNAPSHOT</version>
   <packaging>jar</packaging>

   <name>atguigu-spring-boot-starter-autoconfigurer</name>
   <description>Demo project for Spring Boot</description>

   <parent>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-parent</artifactId>
      <version>1.5.10.RELEASE</version>
      <relativePath/> <!-- lookup parent from repository -->
   </parent>

   <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
      <java.version>1.8</java.version>
   </properties>

   <dependencies>

      <!--引入spring-boot-starter；所有starter的基本配置-->
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter</artifactId>
      </dependency>

   </dependencies>



</project>

```



```java
package com.atguigu.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "atguigu.hello")
public class HelloProperties {

    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}

```

```java
package com.atguigu.starter;

public class HelloService {

    HelloProperties helloProperties;

    public HelloProperties getHelloProperties() {
        return helloProperties;
    }

    public void setHelloProperties(HelloProperties helloProperties) {
        this.helloProperties = helloProperties;
    }

    public String sayHellAtguigu(String name){
        return helloProperties.getPrefix()+"-" +name + helloProperties.getSuffix();
    }
}

```

```java
package com.atguigu.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnWebApplication //web应用才生效
@EnableConfigurationProperties(HelloProperties.class)
public class HelloServiceAutoConfiguration {

    @Autowired
    HelloProperties helloProperties;
    @Bean
    public HelloService helloService(){
        HelloService service = new HelloService();
        service.setHelloProperties(helloProperties);
        return service;
    }
}

```



## 七、SpringBoot与缓存



### 1、JSR107



**Java Caching定义了5个核心接口，分别是CachingProvider, CacheManager, Cache, Entry 和 Expiry。**

•**CachingProvider**定义了创建、配置、获取、管理和控制多个**CacheManager**。一个应用可以在运行期访问多个CachingProvider。

•**CacheManager**定义了创建、配置、获取、管理和控制多个唯一命名的**Cache**，这些Cache存在于CacheManager的上下文中。一个CacheManager仅被一个CachingProvider所拥有。

•**Cache**是一个类似Map的数据结构并临时存储以Key为索引的值。一个Cache仅被一个CacheManager所拥有。

•**Entry**是一个存储在Cache中的key-value对。

​		Expiry 每一个存储在Cache中的条目有一个定义的有效期。一旦超过这个时间，条目为过期的状态。一旦过期，条目将不可访问、更新和删除。缓存有效期可以通过ExpiryPolicy设置

![image-20220325104246314](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325104246314.png)

### 2、Spring缓存抽象



**Spring从3.1开始定义了org.springframework.cache.Cache 和 org.springframework.cache.CacheManager接口来统一不同的缓存技术；并支持使用JCache（JSR-107）注解简化我们开发；**

•Cache接口为缓存的组件规范定义，包含缓存的各种操作集合；

•Cache接口下Spring提供了各种xxxCache的实现；如RedisCache，EhCacheCache , ConcurrentMapCache等；

•每次调用需要缓存功能的方法时，Spring会检查检查指定参数的指定的目标方法是否已经被调用过；如果有就直接从缓存中获取方法调用后的结果，如果没有就调用方法并缓存结果后返回给用户。下次调用直接从缓存中获取。

•使用Spring缓存抽象时我们需要关注以下两点；

​		1、确定方法需要被缓存以及他们的缓存策略

​		2、从缓存中读取之前缓存存储的数据

### 3、几个重要概念&缓存注解



| **Cache**          | **缓存接口，定义缓存操作。实现有：****RedisCache****、****EhCacheCache****、****ConcurrentMapCache****等** |
| ------------------ | ------------------------------------------------------------ |
| **CacheManager**   | **缓存管理器，管理各种缓存（****Cache****）组件**            |
| **@Cacheable**     | **主要针对方法配置，能够根据方法的请求参数对其结果进行缓存** |
| **@CacheEvict**    | **清空缓存**                                                 |
| **@CachePut**      | **保证方法被调用，又希望结果被缓存。**                       |
| **@EnableCaching** | **开启基于注解的缓存**                                       |
| **keyGenerator**   | **缓存数据时****key****生成策略**                            |
| **serialize**      | **缓存数据时****value****序列化策略**                        |



| **@Cacheable/@CachePut/@CacheEvict** **主要的参数** |                                                              |
| --------------------------------------------------- | ------------------------------------------------------------ |
| value                                               | 缓存的名称，在  spring 配置文件中定义，必须指定至少一个      |
| key                                                 | 缓存的  key，可以为空，如果指定要按照  **SpEL 表达式**编写，如果不指定，则缺省按照方法的所有参数进行组合 |
| condition                                           | 缓存的条件，可以为空，使用  SpEL 编写，返回  true 或者 false，只有为  true 才进行缓存/清除缓存，在调用方法之前之后都能判断 |
| allEntries  (**@CacheEvict**  )                     | 是否清空所有缓存内容，缺省为  false，如果指定为 true，则方法调用后将立即清空所有缓存 |
| beforeInvocation  **(@CacheEvict)**                 | 是否在方法执行前就清空，缺省为  false，如果指定为 true，则在方法还没有执行的时候就清空缓存，缺省情况下，如果方法执行抛出异常，则不会清空缓存 |
| unless  **(@CachePut)**  **(@Cacheable)**           | 用于否决缓存的，不像condition，该表达式只在方法执行之后判断，此时可以拿到返回值result进行判断。条件为true不会缓存，fasle才缓存 |



**Cache** **SpEL** **规则**

| **名字**        | **位置**           | **描述**                                                     | **示例**             |
| --------------- | ------------------ | ------------------------------------------------------------ | -------------------- |
| methodName      | root object        | 当前被调用的方法名                                           | #root.methodName     |
| method          | root object        | 当前被调用的方法                                             | #root.method.name    |
| target          | root object        | 当前被调用的目标对象                                         | #root.target         |
| targetClass     | root object        | 当前被调用的目标对象类                                       | #root.targetClass    |
| args            | root object        | 当前被调用的方法的参数列表                                   | #root.args[0]        |
| caches          | root object        | 当前方法调用使用的缓存列表（如@Cacheable(value={"cache1",  "cache2"})），则有两个cache | #root.caches[0].name |
| *argument name* | evaluation context | 方法参数的名字. 可以直接 #参数名 ，也可以使用 #p0或#a0 的形式，0代表参数的索引； | #iban 、 #a0 、 #p0  |
| result          | evaluation context | 方法执行后的返回值（仅当方法执行之后的判断有效，如‘unless’，’cache put’的表达式 ’cache evict’的表达式beforeInvocation=false） | #result              |



### 4、缓存使用



#### 1）缓存Demo

```
@CacheConfig(cacheNames="emp"/*,cacheManager = "employeeCacheManager"*/) //抽取缓存的公共配置
@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    /**
     * 将方法的运行结果进行缓存；以后再要相同的数据，直接从缓存中获取，不用调用方法；
     * CacheManager管理多个Cache组件的，对缓存的真正CRUD操作在Cache组件中，每一个缓存组件有自己唯一一个名字；
     *

     *
     * 原理：
     *   1、自动配置类；CacheAutoConfiguration
     *   2、缓存的配置类
     *   org.springframework.boot.autoconfigure.cache.GenericCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.JCacheCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.EhCacheCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.HazelcastCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.InfinispanCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.CouchbaseCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.RedisCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.CaffeineCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.GuavaCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.SimpleCacheConfiguration【默认】
     *   org.springframework.boot.autoconfigure.cache.NoOpCacheConfiguration
     *   3、哪个配置类默认生效：SimpleCacheConfiguration；
     *
     *   4、给容器中注册了一个CacheManager：ConcurrentMapCacheManager
     *   5、可以获取和创建ConcurrentMapCache类型的缓存组件；他的作用将数据保存在ConcurrentMap中；
     *
     *   运行流程：
     *   @Cacheable：
     *   1、方法运行之前，先去查询Cache（缓存组件），按照cacheNames指定的名字获取；
     *      （CacheManager先获取相应的缓存），第一次获取缓存如果没有Cache组件会自动创建。
     *   2、去Cache中查找缓存的内容，使用一个key，默认就是方法的参数；
     *      key是按照某种策略生成的；默认是使用keyGenerator生成的，默认使用SimpleKeyGenerator生成key；
     *          SimpleKeyGenerator生成key的默认策略；
     *                  如果没有参数；key=new SimpleKey()；
     *                  如果有一个参数：key=参数的值
     *                  如果有多个参数：key=new SimpleKey(params)；
     *   3、没有查到缓存就调用目标方法；
     *   4、将目标方法返回的结果，放进缓存中
     *
     *   @Cacheable标注的方法执行之前先来检查缓存中有没有这个数据，默认按照参数的值作为key去查询缓存，
     *   如果没有就运行方法并将结果放入缓存；以后再来调用就可以直接使用缓存中的数据；
     *
     *   核心：
     *      1）、使用CacheManager【ConcurrentMapCacheManager】按照名字得到Cache【ConcurrentMapCache】组件
     *      2）、key使用keyGenerator生成的，默认是SimpleKeyGenerator
     *
     *
     *   几个属性：
     *      cacheNames/value：指定缓存组件的名字;将方法的返回结果放在哪个缓存中，是数组的方式，可以指定多个缓存；
     *
     *      key：缓存数据使用的key；可以用它来指定。默认是使用方法参数的值  1-方法的返回值
     *              编写SpEL； #i d;参数id的值   #a0  #p0  #root.args[0]
     *              getEmp[2]
     *
     *      keyGenerator：key的生成器；可以自己指定key的生成器的组件id
     *              key/keyGenerator：二选一使用;
     *
     *
     *      cacheManager：指定缓存管理器；或者cacheResolver指定获取解析器
     *
     *      condition：指定符合条件的情况下才缓存；
     *              ,condition = "#id>0"
     *          condition = "#a0>1"：第一个参数的值》1的时候才进行缓存
     *
     *      unless:否定缓存；当unless指定的条件为true，方法的返回值就不会被缓存；可以获取到结果进行判断
     *              unless = "#result == null"
     *              unless = "#a0==2":如果第一个参数的值是2，结果不缓存；
     *      sync：是否使用异步模式
     * @param id
     * @return
     *
     */
    @Cacheable(value = {"emp"}/*,keyGenerator = "myKeyGenerator",condition = "#a0>1",unless = "#a0==2"*/)
    public Employee getEmp(Integer id){
        System.out.println("查询"+id+"号员工");
        Employee emp = employeeMapper.getEmpById(id);
        return emp;
    }

    /**
     * @CachePut：既调用方法，又更新缓存数据；同步更新缓存
     * 修改了数据库的某个数据，同时更新缓存；
     * 运行时机：
     *  1、先调用目标方法
     *  2、将目标方法的结果缓存起来
     *
     * 测试步骤：
     *  1、查询1号员工；查到的结果会放在缓存中；
     *          key：1  value：lastName：张三
     *  2、以后查询还是之前的结果
     *  3、更新1号员工；【lastName:zhangsan；gender:0】
     *          将方法的返回值也放进缓存了；
     *          key：传入的employee对象  值：返回的employee对象；
     *  4、查询1号员工？
     *      应该是更新后的员工；
     *          key = "#employee.id":使用传入的参数的员工id；
     *          key = "#result.id"：使用返回后的id
     *             @Cacheable的key是不能用#result
     *      为什么是没更新前的？【1号员工没有在缓存中更新】
     *
     */
    @CachePut(/*value = "emp",*/key = "#result.id")
    public Employee updateEmp(Employee employee){
        System.out.println("updateEmp:"+employee);
        employeeMapper.updateEmp(employee);
        return employee;
    }

    /**
     * @CacheEvict：缓存清除
     *  key：指定要清除的数据
     *  allEntries = true：指定清除这个缓存中所有的数据
     *  beforeInvocation = false：缓存的清除是否在方法之前执行
     *      默认代表缓存清除操作是在方法执行之后执行;如果出现异常缓存就不会清除
     *
     *  beforeInvocation = true：
     *      代表清除缓存操作是在方法运行之前执行，无论方法是否出现异常，缓存都清除
     *
     *
     */
    @CacheEvict(value="emp",beforeInvocation = true/*key = "#id",*/)
    public void deleteEmp(Integer id){
        System.out.println("deleteEmp:"+id);
        //employeeMapper.deleteEmpById(id);
        int i = 10/0;
    }

    // @Caching 定义复杂的缓存规则
    @Caching(
         cacheable = {
             @Cacheable(/*value="emp",*/key = "#lastName")
         },
         put = {
             @CachePut(/*value="emp",*/key = "#result.id"),
             @CachePut(/*value="emp",*/key = "#result.email")
         }
    )
    public Employee getEmpByLastName(String lastName){
        return employeeMapper.getEmpByLastName(lastName);
    }




}

```

#### 2）整合redis

#####  引入starter

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

##### 配置redis客户端IP/端口

```yaml
spring:
  redis:
    host: 192.168.153.4
```

##### 自定义redis 的序列化机制为Json

```java
/**
 * @author aixz
 */
@Configuration
public class RedisConfig {

    private final Jackson2JsonRedisSerializer<Object> jsonSerializer = new Jackson2JsonRedisSerializer<>(Object.class);


    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object>  template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        // 使用JSON格式序列化对象，对缓存数据key和value进行转换

        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);

        jsonSerializer.setObjectMapper(om);

        // 设置RedisTemplate模板API的序列化方式为JSON
        template.setDefaultSerializer(jsonSerializer);
        //开启事务
        template.setEnableTransactionSupport(true);
        return template;
    }

    @Bean
    public RedisCacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        // 分别创建String和JSON格式序列化对象，对缓存数据key和value进行转换
        RedisSerializer<String> strSerializer = new StringRedisSerializer();
        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);

        jsonSerializer.setObjectMapper(om);

        // 定制缓存数据序列化方式及时效
        //.entryTtl(Duration.ofDays(1)) 缓存数据有效期设为一天
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofDays(1))
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(strSerializer))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jsonSerializer))
                .disableCachingNullValues();

                return RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(config.entryTtl(Duration.ofDays(1))).build();
    }
}
```



## 八、SpringBoot与消息



### 1、概述



**1.大多应用中，可通过消息服务中间件来提升系统异步通信、扩展解耦能力**

**2.消息服务中两个重要概念：**

​    消息代理（message broker）和目的地（destination）

​	当消息发送者发送消息以后，将由消息代理接管，消息代理保证消息传递到指定目的地。

**3.消息队列主要有两种形式的目的地**

​		**1.队列（queue）：点对点消息通信（point-to-point）**

​		**2.主题（topic）：发布（publish）/订阅（subscribe）消息通信**



![image-20220325110317361](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325110317361.png)



![image-20220325110352331](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325110352331.png)



**4.点对点式：**

​	–消息发送者发送消息，消息代理将其放入一个队列中，消息接收者从队列中获取消息内容，消息读取后被移出队列

​	–消息只有唯一的发送者和接受者，但并不是说只能有一个接收者

**5.发布订阅式：**

​	–发送者（发布者）发送消息到主题，多个接收者（订阅者）监听（订阅）这个主题，那么就会在消息到达时同时收到消息

**6.JMS（Java Message Service）JAVA消息服务：**

​	–基于JVM消息代理的规范。ActiveMQ、HornetMQ是JMS实现

**7.AMQP（Advanced Message Queuing Protocol）**

​	–高级消息队列协议，也是一个消息代理的规范，兼容JMS

​	–RabbitMQ是AMQP的实现



|              | JMS                                                          | AMQP                                                         |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 定义         | Java  api                                                    | 网络线级协议                                                 |
| 跨语言       | 否                                                           | 是                                                           |
| 跨平台       | 否                                                           | 是                                                           |
| Model        | 提供两种消息模型：  （1）、Peer-2-Peer  （2）、Pub/sub       | 提供了五种消息模型：  （1）、direct  exchange  （2）、fanout  exchange  （3）、topic  change  （4）、headers  exchange  （5）、system  exchange  本质来讲，后四种和JMS的pub/sub模型没有太大差别，仅是在路由机制上做了更详细的划分； |
| 支持消息类型 | 多种消息类型：  TextMessage  MapMessage  BytesMessage  StreamMessage  ObjectMessage  Message  （只有消息头和属性） | byte[]  当实际应用时，有复杂的消息，可以将消息序列化后发送。 |
| 综合评价     | JMS  定义了JAVA  API层面的标准；在java体系中，多个client均可以通过JMS进行交互，不需要应用修改代码，但是其对跨平台的支持较差； | AMQP定义了wire-level层的协议标准；天然具有跨平台、跨语言特性。 |

**8.Spring支持**

​	–**spring-****jms****提供了对****JMS****的支持**

​	–**spring-rabbit****提供了对****AMQP****的支持**

​	–**需要****ConnectionFactory****的实现来连接消息代理**

​	–**提供****JmsTemplate****、****RabbitTemplate****来发送消息**

​	–**@JmsListener****（****JMS****）、****@RabbitListener****（****AMQP****）注解在方法上监听消息代理发布的消息**

​	–**@EnableJms****、****@EnableRabbit****开启支持**

**9.Spring Boot自动配置**

​	–**JmsAutoConfiguration**

​	–**RabbitAutoConfiguration**



### 2、RabbitMQ简介



**RabbitMQ简介：**

​		RabbitMQ是一个由erlang开发的AMQP(Advanved Message Queue Protocol)的开源实现。

**核心概念**

**Message**

​		消息，消息是不具名的，它由消息头和消息体组成。消息体是不透明的，而消息头则由一系列的可选属性组成，这些属性包括routing-key（路由键）、priority（相对于其他消息的优先权）、delivery-mode（指出该消息可能需要持久性存储）等。

**Publisher**

​		消息的生产者，也是一个向交换器发布消息的客户端应用程序。

**Exchange**

​		交换器，用来接收生产者发送的消息并将这些消息路由给服务器中的队列。

​		Exchange有4种类型：direct(默认)，fanout, topic, 和headers，不同类型的Exchange转发消息的策略有所区别

**Queue**

​		消息队列，用来保存消息直到发送给消费者。它是消息的容器，也是消息的终点。一个消息可投入一个或多个队列。消息一直在队列里面，等待消费者连接到这个队列将其取走。

**Binding**

​		绑定，用于消息队列和交换器之间的关联。一个绑定就是基于路由键将交换器和消息队列连接起来的路由规则，所以可以将交换器理解成一个由绑定构成的路由表。

Exchange 和Queue的绑定可以是多对多的关系。

**Connection**

​		网络连接，比如一个TCP连接。

**Channel**

​		信道，多路复用连接中的一条独立的双向数据流通道。信道是建立在真实的TCP连接内的虚拟连接，AMQP 命令都是通过信道发出去的，不管是发布消息、订阅队列还是接收消息，这些动作都是通过信道完成。因为对于操作系统来说建立和销毁 TCP 都是非常昂贵的开销，所以引入了信道的概念，以复用一条 TCP 连接。

**Consumer**

​		消息的消费者，表示一个从消息队列中取得消息的客户端应用程序。

**Virtual Host**

​		虚拟主机，表示一批交换器、消息队列和相关对象。虚拟主机是共享相同的身份认证和加密环境的独立服务器域。每个 vhost 本质上就是一个 mini 版的 RabbitMQ 服务器，拥有自己的队列、交换器、绑定和权限机制。vhost 是 AMQP 概念的基础，必须在连接时指定，RabbitMQ 默认的 vhost 是 / 。

**Broker**

​		表示消息队列服务器实体

![image-20220325111102337](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111102337.png)



### 3、RabbitMQ运行机制



**AMQP 中的消息路由**

​			•AMQP 中消息的路由过程和 Java 开发者熟悉的 JMS 存在一些差别，AMQP 中增加了 **Exchange** 和 **Binding** 的角色。生产者把消息发布到 Exchange 上，消息最终到达队列并被消费者接收，而 Binding 决定交换器的消息应该发送到那个队列。

![image-20220325111215116](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111215116.png)



**Exchange 类型**

​		•**Exchange**分发消息时根据类型的不同分发策略有区别，目前共四种类型：**direct****、****fanout****、****topic****、****headers** 。headers 匹配 AMQP 消息的 header 而不是路由键， headers 交换器和 direct 交换器完全一致，但性能差很多，目前几乎用不到了，所以直接看另外三种类型：

![image-20220325111323518](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111323518.png)

消息中的路由键（routing key）如果和 Binding 中的 binding key 一致， 交换器就将消息发到对应的队列中。路由键与队列名完全匹配，如果一个队列绑定到交换机要求路由键为“dog”，则只转发 routing key 标记为“dog”的消息，不会转发“dog.puppy”，也不会转发“dog.guard”等等。它是完全匹配、单播的模式。



![image-20220325111347553](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111347553.png)

每个发到 fanout 类型交换器的消息都会分到所有绑定的队列上去。fanout 交换器不处理路由键，只是简单的将队列绑定到交换器上，每个发送到交换器的消息都会被转发到与该交换器绑定的所有队列上。很像子网广播，每台子网内的主机都获得了一份复制的消息。fanout 类型转发消息是最快的。



![image-20220325111418489](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111418489.png)

topic 交换器通过模式匹配分配消息的路由键属性，将路由键和某个模式进行匹配，此时队列需要绑定到一个模式上。它将路由键和绑定键的字符串切分成单词，这些**单词之间用点隔开**。它同样也会识别两个通配符：符号“#”和符号“**”**。**#**匹配**0**个或多个单词**，****匹配一个单词。



### 4、RabbitMQ整合



**1.引入 spring-boot-starter-amqp**

**2.application.yml配置**

**3.测试RabbitMQ**

​		1)AmqpAdmin：管理组件

​		2)RabbitTemplate：消息发送处理组件

#### 缓存Demo

1、主配置文件开启注解支持

```java
@EnableRabbit
@SpringBootApplication
public class BootAmqp01Application {

    public static void main(String[] args) {
        SpringApplication.run(BootAmqp01Application.class, args);
    }

}
```

2、发布端发送消息

```java
@SpringBootTest
class BootAmqp01ApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    AmqpAdmin amqpAdmin;


    @Test
    void textAmqpAdmin(){
        //创建一个交换器
        amqpAdmin.declareExchange(new FanoutExchange("amqp.ex"));
        //创建一个队列
        amqpAdmin.declareQueue(new Queue("amqp.queue",true));
        //创建一个绑定
        amqpAdmin.declareBinding(new Binding("amqp.queue",QUEUE,"amqp.ex","amqp",null));
    }



    @Test
    void contextLoads() {
        Map<String,Object> mes =  new HashMap<>();
        mes.put("name","Jask");
        mes.put("sex","M");
        mes.put("students", Arrays.asList("zhangsan","lisi","wangwu"));

       // rabbitTemplate.convertAndSend("aixz.direct","aixz.hello",mes);

        rabbitTemplate.convertAndSend("aixz.direct","aixz.hello","hello word");
    }

    /**
     * 测试收取消息
     */
    @Test
    void testRabbit(){
        Object o = rabbitTemplate.receiveAndConvert("aixz.hello");
        assert o != null;
        System.out.println(o.getClass());
        System.out.println(o);
    }

    /**
     *测试发送对象
     */
    @Test
    void testObjectRabbit(){
        Book book = new Book("西游记","吴承恩");
        rabbitTemplate.convertAndSend("amqp.ex","amqp",book);
    }
}
```

3、订阅端使用@RabbitListener订阅并消费信息

```java
@Service()
public class BookService {

    @RabbitListener(queues="amqp.queue")
    public void receive(Book book){
        System.out.println(book);
    }
}
```

4、将Jackson2JsonMessageConverter 转换器, 显式声明 转换bean

```
@Configuration
public class JacksonConverterConfig {
    public static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
    }

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter(objectMapper);
    }
}
```



#### [rabbitmq的序列化反序列化格式](https://www.cnblogs.com/gne-hwz/p/15668947.html)



SpringBoot封装了rabbitmq中，发送对象和接收对象时，会统一将对象和消息互相转换

会用到MessageConverter转换接口

**发送消息时，**

会将Object转换成Message Message createMessage(Object object, MessageProperties messageProperties)

**接收消息时**

SimpleMessageListenerContainer容器监听消息时，会调用SimpleMessageListenerContainer.messageListener的消息转换器将Message转换成对象 Object fromMessage(Message message)



Springboot中，默认的rabbitMq的序列化类是：SimpleMessageConverter

SimpleMessageConverter

将Object对象 和 Message 互相转换的规则



```
如果是 byte[] 或者 String 类型，直接获取字节 String.getBytes(this.defaultCharset)
设置messageProperties的 contentType = text/plain

如果是其他对象，调用 SerializationUtils.serialize(object) 进行序列化
设置messageProperties的 contentType = application/x-java-serialized-object
```

这里存在一个问题

如果发送方： object类是 com.zwh.user

但是接受方没有这个路径的类时，会抛出异常，not found class

 

所以发送消息时，最好手动将消息转换成String



Jackson2JsonMessageConverter 转换器



```
序列化时，Object 转成 json字符串在设置到 Message.body()中。 调用的是 jackson的序列化方式
反序列化时，将 Message.body()内容反序列化成 Object
```

这个方式不会出现上面SimpleMessageConverter转换器的 not found class错误



如果项目中要统一的序列化格式, 需要显示声明 转换bean



```
@Bean
public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
    return new Jackson2JsonMessageConverter();
}
```

因为自动配置类**RabbitAutoConfiguration** 和 **RabbitAnnotationDrivenConfiguration**

**RabbitAutoConfiguration**

![image-20220328155837858](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220328155837858.png)

**RabbitAnnotationDrivenConfiguration**

![image-20220328160005061](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220328160005061.png)

会自动获取Spring 容器中MessageConverter实现类bean数据

并将转换器设置到 RabbitTemplate 和 SimpleRabbitListenerContainerFactory.setMessageConverter() 中



```
RabbitAutoConfiguration 配置 发送端 RabbitTemplate 
RabbitAnnotationDrivenConfiguration 配置接收端 SimpleRabbitListenerContainerFactory
```

 

注意：如果 RabbitTemplate 配置的 jackson 序列化，而 listener没有配置（默认SimpleMessageConverter），则接受消息转换成object时将会报错

```
ListenerExecutionFailedException: Listener method could not be invoked with the incoming message
Fatal message conversion error; message rejected; it will be dropped or routed to a dead letter exchange
消息转换错误，消息被拒绝
```



## 九、SpringBoot与检索

我们的应用经常需要添加检索功能，开源的 [ElasticSearch](https://www.elastic.co/) 是目前全文搜索引擎的首选。他可以快速的存储、搜索和分析海量数据。Spring Boot通过整合Spring Data ElasticSearch为我们提供了非常便捷的检索功能支持；



Elasticsearch是一个分布式搜索服务，提供Restful API，底层基于Lucene，采用多shard（分片）的方式保证数据安全，并且提供自动resharding的功能，github等大型的站点也是采用了ElasticSearch作为其搜索服务，

### 一、概念



**•以 *员工文档* 的形式存储为例：一个文档代表一个员工数据。存储数据到 ElasticSearch 的行为叫做 *索引* ，但在索引一个文档之前，需要确定将文档存储在哪里。**

•一个 ElasticSearch 集群可以 包含多个 *索引* ，相应的每个索引可以包含多个 *类型* 。 这些不同的类型存储着多个 *文档* ，每个文档又有 多个 *属性* 。

**•类似关系：**

​				**–索引-数据库**

​				**–类型-表**

​				**–文档-表中的记录**

​				**–属性-列**

### 二、安装

#### **（一）Elastic search**

\1. 拉取镜像：docker pull elasticsearch:7.4.2

\2. 创建容器：docker run -d --name es -p 9200:9200 -p 9300:9300 -e ES_JAVA_OPTS="-Xms512m -Xmx512m" -e "discovery.type=single-node" elasticsearch:7.4.2

\3. 进入es容器，在文件config/elasticsearch.yml中增加以下跨域信息：

```yaml
http.cors.enabled: true

http.cors.allow-origin: "*"

xpack.security.enabled: false
```

\4. 重启es容器

 

#### **（二）Es-head 可视化插件**

\1. 拉取镜像：docker pull elasticsearch-head:5

\2. 创建容器：docker run -d -p 9100:9100 --name es-head mobz/elasticsearch-head:5

\3. 访问http://localhost:9100，如下图所示代表连接成功

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135206014-806376828.png)

\4. 新建索引时，若报如下错误：

{"error":"Content-Type header [application/x-www-form-urlencoded] is not supported","status":406}

解决方法：进入es-head容器中-》进入_site文件夹-》修改vendor.js两处代码

①　第6686行：

contentType: "application/x-www-form-urlencoded", 

-》

contentType: "application/json;charset=UTF-8",

②　第7574行：

var inspectData = s.contentType === "application/x-www-form-urlencoded" &&

-》

var inspectData = s.contentType === "application/json;charset=UTF-8" &&

 

#### **（三）Kibana**

\1. 拉取镜像(版本与es一致)：docker pull kibana:7.4.2

\2. 创建容器：docker run -d -p 5601:5601 --name kibana kibana:7.4.2

\3. 进入kibana容器，在文件config/kibana.yml中，修改hosts地址

[http://elasticserach:9200](http://elasticserach:9200/) 改为

http://172.17.0.1:9200

\4. 重启kibana容器后访问http://localhost:5601，若访问报错：Kibana server is not ready yet，耐心等待一会，若还报错则进入容器执行以下两个命令后再次重启kibana容器：

①　curl -u elastic:changeme -XDELETE 172.17.0.1:9200/_xpack/security/privilege/kibana-.kibana/space_all

②　curl -u elastic:changeme -XDELETE 172.17.0.1:9200/_xpack/security/privilege/kibana-.kibana/space_read

\5. 汉化设置：进入kibana容器中，在config/kibana.yml文件最后增加：i18n.locale: "zh-CN"。然后重启kibana容器即可

 

#### **（四）Logstash**

\1. 拉取镜像(版本与es一致)：docker pull logstash:7.4.2

\2. 在本地创建logstash/logstash.yml，输入以下内容

**path.config**: /usr/share/logstash/conf.d/*.conf
**path.logs**: /var/log/logstash

\3. 在本地创建logstash/conf.d/logstash_dev.conf

\4. 创建容器：

docker run -d --restart=always --log-driver json-file --log-opt max-size=100m --log-opt max-file=2 -p 5044:5044 --name logstash -v ./logstash/logstash.yml:/usr/share/logstash/config/logstash.yml -v ./logstash/conf.d/:/usr/share/logstash/conf.d/ logstash:7.4.2

 

#### **（五）IK分词器**

\1. 安装

l 进入es容器plugins文件夹中，下载ik7.4.2：

wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.4.2/elasticsearch-analysis-ik-7.4.2.zip

l 解压至ik7.4.2文件夹中：

unzip elasticsearch-analysis-ik-7.4.2.zip -d ik7.4.2

l 删除下载包及修改ik7.4.2权限

rm -rf ik-7.4.2.zip

chmod -R 777 ik7.4.2/

l 重启es容器

\2. 增加分词

l 进入到ik7.4.2/config 文件夹中

l 新增一个 xxx.dic文件，写入想要的分词

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134623767-1831195315.png)

 

l 打开IKAnalyzer.cfg.xml文件，引入刚刚新增的dic文件

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134640638-2132027795.png)

 

l 重启es容器

\3. 分词使用

打开kibana-》开发工具

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134701960-1835594534.png)

从上图可以看出 “可安” 被es当成了一个词组

 

### **三、ES基本操作**

#### **（一）Rest风格说明**

一种软件架构风格，并不是一个死规定，只是提供了一组设计原则和约束条件，用于客户端与服务端交互的软件。基于此风格设计的软件可以更简介、更有层次、更易于实现缓存等机制。

基于rest的es api设计：

| **method** | **url地址**                           | **描述**               |
| ---------- | ------------------------------------- | ---------------------- |
| PUT        | 域名/索引名称/类型名称/文档id         | 创建文档（指定文档id） |
| POST       | 域名/索引名称/类型名称                | 创建文档（随机文档id） |
| POST       | 域名/索引名称/类型名称/文档id/_update | 修改文档               |
| DELETE     | 域名/索引名称/类型名称/文档id         | 删除文档               |
| GET        | 域名/索引名称/类型名称/文档id         | 查询指定id的文档       |
| POST       | 域名/索引名称/类型名称/_search        | 查询所有数据           |

 

#### **（二）基本操作**

\1. 创建

①　创建文档数据（不推荐）：

PUT /索引名称/类型名称/文档id

{请求体}

在kibana工具中执行新增索引命令

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134738776-442433999.png)

 

在es-head工具中查看到数据

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134757210-643625934.png)

 

②　创建指定类型的索引规则：

PUT /索引名称

{请求体}

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134828429-1971627203.png)

 

③　创建默认类型的文档数据（推荐！！！）

说明：如果文档字段没有被指定，那么es会默认指定字段类型。

PUT /索引名称/_doc/文档id

{请求体}

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134853283-1862474769.png)

 

\2. 获取

①　索引文档字段类型

GET /索引名称

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134915986-97794123.png)

 

②　指定的文档数据

GET /索引名称/_doc/文档id

③　所有文档数据

POST /索引名称/_doc/_search

④　简单条件查询

GET /索引名称/_doc/_search?q=字段名:字段值

⑤　扩展命令

获取es当前信息(版本、健康值等)：GET _cat/

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134935032-1031347550.png)

 

\3. 修改：

①　PUT（不推荐），需要写全所有字段

PUT /索引名称/_doc/文档id

{请求体}

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134950034-1640205914.png)

 

②　POST(推荐！！！)

POST /索引名称/_doc/文档id/_update

{请求体}

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135012780-2023033979.png)

 

 

\4. 删除：

①　删除索引：DELETE /索引名称

②　删除文档：DELETE /索引名称/_doc/文档id

 

#### **（三）复杂查询**

文档地址：（接口太多了，就不一一举例了）https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135042971-1489584238.png)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135055860-1008394726.png)



#### （四）整合ElasticSearch



**•引入spring-boot-starter-data-elasticsearch**

**•安装Spring Data 对应版本的ElasticSearch**

**•application.yml配置**

**•Spring Boot自动配置的**

 **ElasticsearchRepository、ElasticsearchTemplate、Jest**

**测试ElasticSearch**



### 问题

#### 1、docker安装es-head后无法查看数据



在docker中安装es-head后，使用es-head查看索引时，无法显示数据，F12查看调用请求，发现Content-Type错误了
看一下网络流可知报406 错误

```json
{
  "error" : "Content-Type header [application/x-www-form-urlencoded] is not supported",
  "status" : 406
}
1234
```

在网上查找了相关资料，解决办法如下：

进入head插件安装目录 ，编辑/usr/src/app/_site/vendor.js

修改共有两处：

> 第6886行 : contentType: "application/x-www-form-urlencoded
> 改为 contentType: “application/json;charset=UTF-8”
> 第7573行: var inspectData = s.contentType === “application/x-www-form-urlencoded” &&
> 改为 var inspectData = s.contentType === “application/json;charset=UTF-8” &&

#### 2. received plaintext http traffic on an https channel, closing connection Netty4HttpChannel

**[2022-02-16T21:08:50,085][WARN ][o.e.x.s.t.n.SecurityNetty4HttpServerTransport] [DESKTOP-VCT39JM] received plaintext http traffic on an https channel, closing connection Netty4HttpChannel{localAddress=/[0:0:0:0:0:0:0:1]:9200, remoteAddress=/[0:0:0:0:0:0:0:1]:1172}**

解决

是因为开启了 ssl 认证。
在 ES/config/elasticsearch.yml 文件中把 `xpack.security.http.ssl:enabled` 设置成 `false` 即可

```yml
# Enable encryption for HTTP API client connections, such as Kibana, Logstash, and Agents
xpack.security.http.ssl:
  enabled: false
  keystore.path: certs/http.p12
```

#### 3. elasticsearch 账号密码

windows 下直接启动 ElasticSearch ，见到 started 为成功启动，访问 htttp://localhost:9200 需要输入密码，是因为开启了密码验证模式。
找了一轮没看到有账号密码，干脆就设置免密登录就好。

解决

找到 `elasticsearch.yml` 文件， 把 `xpack.security.enabled` 属性设置为 `false` 即可。

```yml
# Enable security features
xpack.security.enabled: false
```

#### 4. 设置内存大小

ES 的内存是自己调节的。在 `config/jvm.options` 文件中直接设置就好(追加):

```yml
-Xms512m
-Xmx2048m
```

#### 5. windows Could not rename log file ‘logs/gc.log’ to ‘logs/gc.log.14’ (Permission denied).

ES 在 windows 中只允许打开一个应用程序，当你再想去创建一个 ES 应用程序的时候，就会显示 `Permission denied`，即使是使`用 cmd 管理员运行 elasticsearch.bat 文件`也是一样的错误。

- 解决： 注意查看是否已经在别的地方已经打开 ES 服务，实在不行则进行电脑重启

#### 6. org/elasticsearch/action/ActionRequest has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0

```java
public static void main(String[] args) throws IOException {
    // 创建 ES 客户端
    RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(new HttpHost(Constants.HOST, Constants.PORT, Constants.HTTP))
    );

    client.close();
}
12345678
```

通过上述代码，使用 RestHighLevelCilent 访问 ES 客户端的时候，出现以下错误：`Exception in thread "main" java.lang.UnsupportedClassVersionError:`org/elasticsearch/action/ActionRequest has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0

看到注释，我们知道是版本不兼容的问题，查找资料看到这样一个表：

```js
Major version numbers map to Java versions:

45 = Java 1.1
46 = Java 1.2
47 = Java 1.3
48 = Java 1.4
49 = Java 5
50 = Java 6
51 = Java 7
52 = Java 8
53 = Java 9
54 = Java 10
55 = Java 11
56 = Java 12
57 = Java 13
```

讲道理在这个`版本任你发，我用Java8` 的年代，RestHighLevelClient 肯定是兼容Java 8 的，那么就只有 ES 版本太高了，把 `pom.xml` 的 `ES 依赖版本`降到跟 `elasticsearch-rest-high-level-client` 一样就可以了

```xml
<properties>
    <elasticsearch.version>7.17.0</elasticsearch.version>   	 																	<elasticsearch.client.version>7.17.0</elasticsearch.client.version>
</properties>

<dependies>
    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch</artifactId>
        <version>${elasticsearch.version}</version>
    </dependency>
    <dependency>
        <groupId>org.elasticsearch.client</groupId>
        <artifactId>elasticsearch-rest-high-level-client</artifactId>
        <version>${elasticsearch.client.version}</version>
    </dependency>
</dependies>
```

- 解决：把 `pom.xml` 的 `elasticsearch` 和 `elasticsearch-rest-high-level-client` 版本一致即可。

## 十、SpringBoot与任务

### **一、异步任务**

在Java应用中，绝大多数情况下都是通过同步的方式来实现交互处理的；但是在处理与第三方系统交互的时候，容易造成响应迟缓的情况，之前大部分都是使用多线程来完成此类任务，其实，在Spring 3.x之后，就已经内置了@Async来完美解决这个问题。



**两个注解：**

**@EnableAysnc、@Aysnc**

#### Demo

```java
@EnableAsync
@SpringBootApplication
public class Async01Application {

    public static void main(String[] args) {
        SpringApplication.run(Async01Application.class, args);
    }

}


@Async
    public void say(){
        try {
            logger.info("-----------------------开始发送邮件"+new Date().toString());
            sendMail();
            logger.info("-----------------------邮件发送结束"+new Date().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```



#### 线程池配置

```java
/**
 * @PackageName:com.dmo.config
 * @ClassName:ThreadPoolExecutorConfig
 * @Description: 配置线程池
 * @Author: john
 */
@Configuration
public class ThreadPoolExecutorConfig {
    //核心线程数 = 当前cpu线程数量 +1
    //setDaemon 守护线程
    private static final int THREADS = Runtime.getRuntime().availableProcessors() + 1;
    final ThreadFactory threadFactory = new ThreadFactoryBuilder()
            .setNameFormat("My_Thread -> %d")
            .setDaemon(true)
            .build();

    //线程池名
    @Bean("myTaskExecutor")
    public Executor myTaskExecutor() {
        //7 个参数 =
        // 核心线程数，
        // 最大线程数，
        // 线程存活时间，
        // 单位，
        // 线程存放阻塞队列，
        // 线程创建工厂
        // 超出线程的拒绝策略 （4种,默认超出报 Exception ）
        return new ThreadPoolExecutor(THREADS,
                                            9,
                                            5,
                             TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(10),
                                threadFactory,
                         (runnable, executor) -> {
                        // 打印日志,添加监控等
                        System.out.println("超出最大线程数,已被拒绝！"); });
    }
}
```



#### 自定义线程池

```java
@Async("myTaskExecutor")
```



>**七大参数：**    
>
>​	1 corePoolSize：线程池核心线程数量，核心线程不会被回收，即使没有任务执行，也会保持空闲状态。如果线程池中的线程少于此数目，则在执行任务时创建。    
>
>​	2 maximumPoolSize：池允许最大的线程数，当线程数量达到corePoolSize，且workQueue队列塞满任务了之后，继续创建线程。    	3 keepAliveTime：超过corePoolSize之后的“临时线程”的存活时间。    
>
>​	4 unit：keepAliveTime的单位。    
>
>​	5 workQueue：当前线程数超过corePoolSize时，新的任务会处在等待状态，并存在workQueue中，BlockingQueue是一个先进先出的阻塞式队列实现，底层实现会涉及Java并发的AQS机制，有关于AQS的相关知识，我会单独写一篇，敬请期待。    
>
>​	6 threadFactory：创建线程的工厂类，通常我们会自顶一个threadFactory设置线程的名称，这样我们就可以知道线程是由哪个工厂类创建的，可以快速定位。    
>
>​	7 handler：线程池执行拒绝策略，当线数量达到maximumPoolSize大小，并且workQueue也已经塞满了任务的情况下，线程池会调用handler拒绝策略来处理请求。     
>
>**四大拒绝策略：**    
>
>​	1 AbortPolicy：为线程池默认的拒绝策略，该策略直接抛异常处理。    
>
>​	2 DiscardPolicy：直接抛弃不处理。    
>
>​	3 DiscardOldestPolicy：抛弃队列头部（最旧）的一个任务，并执行当前任务    
>
>​	4 CallerRunsPolicy：使用当前调用的线程来执行此任务





#### 问题

##### 异步调用不生效

> 被调用方法 和 调用处的代码都处在同一个类，所以只是相当于本类调用，并没有使用代理类 从而@Async并没有产生效果。



### 二、定时任务 



项目开发中经常需要执行一些定时任务，比如需要在每天凌晨时候，分析一次前一天的日志信息。Spring为我们提供了异步执行任务调度的方式，提供TaskExecutor 、TaskScheduler 接口。

**两个注解：**@EnableScheduling、@Scheduled

​	**cron表达式**

| **字段** | **允许值**             | **允许的特殊字符** |
| -------- | ---------------------- | ------------------ |
| 秒       | 0-59                   | , -  * /           |
| 分       | 0-59                   | , -  * /           |
| 小时     | 0-23                   | , -  * /           |
| 日期     | 1-31                   | , -  * ? / L W C   |
| 月份     | 1-12                   | , -  * /           |
| 星期     | 0-7或SUN-SAT  0,7是SUN | , -  * ? / L C #   |

| **特殊字符** | **代表含义**               |
| ------------ | -------------------------- |
| ,            | 枚举                       |
| -            | 区间                       |
| *            | 任意                       |
| /            | 步长                       |
| ?            | 日/星期冲突匹配            |
| L            | 最后                       |
| W            | 工作日                     |
| C            | 和calendar联系后计算过的值 |
| #            | 星期，4#2，第2个星期四     |



#### Demo

```java
@EnableScheduling
@SpringBootApplication
public class Async01Application {

    public static void main(String[] args) {
        SpringApplication.run(Async01Application.class, args);
    }

}


@Service
public class ScheduledService {

    /**
     * second(秒), minute（分）, hour（时）, day of month（日）, month（月）, day of week（周几）.
     * 0 * * * * MON-FRI
     *  【0 0/5 14,18 * * ?】 每天14点整，和18点整，每隔5分钟执行一次
     *  【0 15 10 ? * 1-6】 每个月的周一至周六10:15分执行一次
     *  【0 0 2 ? * 6L】每个月的最后一个周六凌晨2点执行一次
     *  【0 0 2 LW * ?】每个月的最后一个工作日凌晨2点执行一次
     *  【0 0 2-4 ? * 1#1】每个月的第一个周一凌晨2点到4点期间，每个整点都执行一次；
     */
   // @Scheduled(cron = "0 * * * * MON-SAT")
    //@Scheduled(cron = "0,1,2,3,4 * * * * MON-SAT")
   // @Scheduled(cron = "0-4 * * * * MON-SAT")
    @Scheduled(cron = "0/4 * * * * MON-SAT")  //每4秒执行一次
    public void hello(){
        System.out.println("hello ... ");
    }
}
```



### 三、邮件任务 



**•邮件发送需要引入spring-boot-starter-mail**

**•Spring Boot 自动配置MailSenderAutoConfiguration**

**•定义MailProperties内容，配置在application.yml中**

**•自动装配JavaMailSender**

**•测试邮件发送**

![image-20220331191203537](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220331191203537.png)

现在发送邮件是一个网站必备的功能，比如注册激活，或者忘记密码等等都需要发送邮件。正常我们会用JavaMail相关api来写发送邮件的相关代码，但现在springboot提供了一套更简易使用的封装。也就是我们使用SpringBoot来发送邮件的话，代码就简单许多，毕竟SpringBoot是开箱即用的 ，它提供了什么，我们了解之后觉得可行，就使用它，自己没必要再写原生的。

##### 发送邮件流程图

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312101648110.png)
现在的Java项目大多都是按照MVC架构模型来开发的，但是这里不使用Controller层，是直接在测试中发送邮件，假如测试中发送邮件成功，那么改为用户发送请求来发送邮件也会成功。发送邮件，也就是要有发送方和接收方，我们需要在系统中配置发送方的信息，而接受方的信息我们需要根据从客户端发来的请求决定。作为发送方的我们，需要被授权，被谁授权？

1. 假如我使用QQ邮箱发送邮件，那么我就需要开启QQ邮箱授权码。
2. 假如我使用网易邮箱发送邮件，那我就需要开启网易邮箱授权码。

我们接下来就一 一开启这两个邮箱的授权码

1. 开启QQ邮箱授权码
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312111820127.png)
2. 开启网易邮箱授权码
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312111125605.png)

上图的步骤都会生成授权码，我们需要记住它，可以选择保存在本地电脑上，因为我们接下来配置发送方信息需要授权码。
接下来我们就开始编写代码发送邮件，我们使用IDEA创建SpringBoot项目，下篇博客可以快速了解如何在IDEA中创建SpringBoot项目。
[SpringBoot快速入门](https://blog.csdn.net/weixin_44176169/article/details/103982951)

##### 导入依赖

`spring-boot-starter-mail`：这个启动器内部封装了发送邮件的代码，必须引入的。
`spring-boot-starter-web`：这个启动器整合了JavaWeb常用的功能，如果只是简单在测试中发送邮件，那么就不需要引入它。
`spring-boot-starter-thymeleaf`：模板引擎依赖，如果需要将数据渲染到页面上，就需要引入它。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
123456789101112
```

##### 配置发送方信息

我们需要在SpringBoot配置文件(application.yml)中配置发送方的信息，包括以下信息：

1. 用户名：也就是我们的邮箱账号
2. 密码：我们刚刚生成的授权码
3. 邮箱主机：配置使用哪个邮箱服务器发送邮件
   还有等等其他可选配置，在下面配置代码中，大家需要填写自己的用户名、密码、from(和用户名一样)。

```
QQ邮箱配置
spring:
  mail:
    host: smtp.qq.com #发送邮件服务器
    username:  #发送邮件的邮箱地址
    password:   #客户端授权码，不是邮箱密码，这个在qq邮箱设置里面自动生成的
    properties.mail.smtp.port: 465 #端口号465或587
    from:  # 发送邮件的地址，和上面username一致
    properties.mail.smtp.starttls.enable: true
    properties.mail.smtp.starttls.required: true
    properties.mail.smtp.ssl.enable: true
    default-encoding: utf-8
1234567891011
网易邮箱配置
spring:
  mail:
    host: smtp.163.com #发送邮件服务器
    username:  #发送邮件的邮箱地址
    password: #客户端授权码，不是邮箱密码,网易的是自己设置的
    properties.mail.smtp.port: 994 #465或者994
    from:  # 发送邮件的地址，和上面username一致
    properties.mail.smtp.starttls.enable: true
    properties.mail.smtp.starttls.required: true
    properties.mail.smtp.ssl.enable: true
    default-encoding: utf-8
1234567891011
```

##### EmailService

我们需要自己封装邮件服务，这个服务只是便于Controller层调用，也就是根据客户端发送的请求来调用，封装三种邮件服务

1. 发送普通文本邮件
2. 发送HTML邮件：一般在注册激活时使用
3. 发送带附件的邮件：可以发送图片等等附件

```java
public interface EmailService {

/**
 * 发送文本邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 */
void sendSimpleMail(String to, String subject, String content);

/**
 * 发送HTML邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 */
void sendHtmlMail(String to, String subject, String content);

/**
 * 发送带附件的邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 * @param filePath 附件
 */
public void sendAttachmentsMail(String to, String subject, String content, String filePath);
}
123456789101112131415161718192021222324252627
```

##### EmailServiceImpl

我们需要实现上面的邮件服务，在具体的实现里面，则是调用了SpringBoot提供的JavaMailSender(封装了发送邮件的代码)。

```java
@Service
public class EmailServiceImpl implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender javaMailSender;
	
	//注入配置文件中配置的信息——>from
    @Value("${spring.mail.from}")
    private String from;

    @Override
    public void sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        //发件人
        message.setFrom(from);
        //收件人
        message.setTo(to);
        //邮件主题
        message.setSubject(subject);
        //邮件内容
        message.setText(content);
        //发送邮件
        javaMailSender.send(message);
    }

    @Override
    public void sendHtmlMail(String to, String subject, String content) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message,true);
            messageHelper.setFrom(from);
            messageHelper.setTo(to);
            message.setSubject(subject);
            messageHelper.setText(content,true);
            javaMailSender.send(message);
            logger.info("邮件已经发送！");
        } catch (MessagingException e) {
            logger.error("发送邮件时发生异常："+e);
        }
    }

    @Override
    public void sendAttachmentsMail(String to, String subject, String content, String filePath) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message,true);
            messageHelper.setFrom(from);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(content,true);
            //携带附件
            FileSystemResource file = new FileSystemResource(filePath);
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            messageHelper.addAttachment(fileName,file);

            javaMailSender.send(message);
            logger.info("邮件加附件发送成功！");
        } catch (MessagingException e) {
            logger.error("发送失败："+e);
        }
    }

}
12345678910111213141516171819202122232425262728293031323334353637383940414243444546474849505152535455565758596061626364656667
```

##### EmailServiceTest

我们直接在测试中发送邮件，如果测试中发送邮件没问题，那么转为用户请求发送邮件也没有问题的。

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest{

    @Autowired
    private EmailService emailService;

    @Test
    public void sendSimpleEmail(){
        String content = "你好，恭喜你...";
        emailService.sendSimpleMail("XXX@qq.com","祝福邮件",content);
    }

    @Test
    public void sendMimeEmail(){
        String content = "<a href='https://blog.csdn.net/'>你好，欢迎注册网站，请点击链接激活</a>";
        emailService.sendHtmlMail("XXX@163.com","激活邮件",content);
    }

    @Test
    public void sendAttachment(){
        emailService.sendAttachmentsMail("XX@qq.com","发送附件","这是Java体系图","F:/图片/爱旅行.jpg");
    }
}
123456789101112131415161718192021222324
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312122149108.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312122316154.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312123000765.png)
这样我们就实现了提供Java代码来发送邮件。发送附件功能其实就是读取我们本地电脑的文件，然后添加到发送流中即可。
[发送邮件附件的源代码](https://github.com/ZhengWenQi98/SpringBoot)





## 十一、SpringBoot与安全



### 一、安全



**Spring Security是针对Spring项目的安全框架，也是Spring Boot底层安全模块默认的技术选型。他可以实现强大的web安全控制。对于安全控制，我们仅需引入spring-boot-starter-security模块，进行少量的配置，即可实现强大的安全管理。**

****

 **几个类：**

**WebSecurityConfigurerAdapter：自定义Security策略**

**AuthenticationManagerBuilder：自定义认证策略**

**@EnableWebSecurity：开启WebSecurity模式**



**•应用程序的两个主要区域是“认证”和“授权”（或者访问控制）。这两个主要区域是Spring Security 的两个目标。**

**•“认证”（Authentication），是建立一个他声明的主体的过程（一个“主体”一般是指用户，设备或一些可以在你的应用程序中执行动作的其他系统）。**

**•“授权”（Authorization）指确定一个主体是否允许在你的应用程序执行一个动作的过程。为了抵达需要授权的店，主体的身份已经有认证过程建立。**

**•这个概念是通用的而不只在Spring Security中。**



### 二、Web&安全 



##### 1.登陆/注销

​	**–HttpSecurity配置登陆、注销功能**

##### 2.Thymeleaf提供的SpringSecurity标签支持

```html
<html xmlns:th="http://www.thymeleaf.org"
	  xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
```



```js
sec:authentication=“name”获得当前用户的用户名
sec:authentication="authorities" 角色
–sec:authorize=“hasRole(‘ADMIN’)” 当前用户必须拥有ADMIN权限时才会显示标签内容
```

[表达式说明地址](https://docs.spring.io/spring-security/reference/servlet/authorization/expression-based.html#el-access-web)

##### 3.remember me

​	**–表单添加remember-me的checkbox**

​	**–配置启用remember-me功能**

##### 4.CSRF（Cross-site request forgery）跨站请求伪造

​	**HttpSecurity启用csrf功能，会为表单添加_csrf的值，提交携带来预防CSRF；**

### 三、Demo



#### 动态登陆状态

```html
<!--未登录,显示登录按钮-->
    <div sec:authorize="!isAuthenticated()">
        <a class="item" th:href="@{/toLogin}">
            <i class="address card icon"></i> 登录
        </a>
    </div>
<!--登陆后,显示用户名,注销-->
     <div sec:authorize="isAuthenticated()">
         <a class="item">
             用户名: <span sec:authentication="name"></span>
             角色: <span sec:authentication="authorities"></span>
         </a>
         <a class="item" th:href="@{/logout}">
         	<i class="sign-out icon"></i> 注销
         </a>
      </div>
```

#### 配置类

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder()).withUser("zhangsan").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1")
                .and().withUser("lisi").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1","VIP2").and()
                .withUser("wangwu").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1","VIP2","VIP3");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");
        //开启用户登录表单
        http.formLogin().loginPage("/userlogin").passwordParameter("pwd");
        //开启记住我
        http.rememberMe().rememberMeParameter("reme");
        //开启注销
        http.logout().logoutSuccessUrl("/");
    }

}
```

#### 跟据权限动态显示:

```html
<div class="column" sec:authorize="hasRole('vip1')"> 11 </div>
<!--权限为vip1,可见-->
```





### 问题

#### There is no PasswordEncoder mapped for the id “null”异常解决办法

##### 一. 问题描述

Spring security 5.0中新增了多种加密方式,也改变了默认的密码格式.

我们来看一下官方文档:

```python
The general format for a password is:

{id}encodedPassword

Such that id is an identifier used to look up which PasswordEncoder should be used and encodedPassword is the original encoded password for the selected PasswordEncoder. The id must be at the beginning of the password, start with { and end with }. If the id cannot be found, the id will be null. For example, the following might be a list of passwords encoded using different id. All of the original passwords are "password".

{bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.20cQQubK3.HZWzG3YB1tlRy.fqvM/BG 

{noop}password 

{pbkdf2}5d923b44a6d129f3ddf3e3c8d29412723dcbde72445e8ef6bf3b508fbf17fa4ed4d6b99ca763d8dc 

{scrypt}$e0801$8bWJaSu2IKSn9Z9kM+TPXfOc/9bdYSrN1oD9qfVThWEwdRTnO7re7Ei+fUZRJ68k9lTyuTeUp4of4g24hHnazw==$OAOec05+bXxvuu/1qZ6NUR+xQYvYv7BeL1QxwRpY5Pc=  

{sha256}97cde38028ad898ebc02e690819fa220e88c62e0699403e94fff291cfffaf8410849f27605abcbc0
```

**这段话的意思是说,现如今Spring Security中密码的存储格式是“{id}…………”.前面的id是加密方式,id可以是bcrypt、sha256等,后面跟着的是加密后的密码.也就是说,程序拿到传过来的密码的时候,会首先查找被“{”和“}”包括起来的id,来确定后面的密码是被怎么样加密的,如果找不到就认为id是null.**这也就是为什么我们的程序会报错:There is no PasswordEncoder mapped for the id “null”.官方文档举的例子中是各种加密方式针对同一密码加密后的存储形式,原始密码都是“password”.

##### 二. 解决办法

需要修改一下configure中的代码,我们要将前端传过来的密码进行某种方式加密,Spring Security 官方推荐的是使用bcrypt加密方式.

###### **1. 在内存中存取密码的修改方式**

修改后是这样的:

```java
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  //inMemoryAuthentication 从内存中获取
  auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder()).withUser("user1").password(new BCryptPasswordEncoder().encode("123")).roles("USER");
}
```

inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())",这相当于登陆时用BCrypt加密方式对用户密码进行处理.以前的".password("123")" 变成了 ".password(new BCryptPasswordEncoder().encode("123"))",这相当于对内存中的密码进行Bcrypt编码加密.如果比对时一致,说明密码正确,才允许登陆.

###### **2. 在数据库中存取密码的修改方式**

如果你用的是在数据库中存储用户名和密码,那么一般是要在用户注册时就使用BCrypt编码将用户密码加密处理后存储在数据库中,并且修改configure()方法,加入".passwordEncoder(new BCryptPasswordEncoder())",保证用户登录时使用bcrypt对密码进行处理再与数据库中的密码比对.如下:

```javascript
//注入userDetailsService的实现类



auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
```

 





## 十二、Spring Boot与分布式



### 一、分布式应用

**在分布式系统中，国内常用zookeeper+dubbo组合，而Spring Boot推荐使用全栈的Spring，Spring Boot+Spring Cloud。**

**分布式系统：**

![image-20220403185156186](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403185156186.png)



**单一应用架构**

当网站流量很小时，只需一个应用，将所有功能都部署在一起，以减少部署节点和成本。此时，用于简化增删改查工作量的数据访问框架(ORM)是关键。

**垂直应用架构**

当访问量逐渐增大，单一应用增加机器带来的加速度越来越小，将应用拆成互不相干的几个应用，以提升效率。此时，用于加速前端页面开发的Web框架(MVC)是关键。

**分布式服务架构**

当垂直应用越来越多，应用之间交互不可避免，将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，使前端应用能更快速的响应多变的市场需求。此时，用于提高业务复用及整合的分布式服务框架(RPC)是关键。

**流动计算架构**

当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需增加一个调度中心基于访问压力实时管理集群容量，提高集群利用率。此时，用于提高机器利用率的资源调度和治理中心(SOA)是关键。



### 二、Zookeeper和Dubbo



**ZooKeeper**

ZooKeeper 是一个分布式的，开放源码的分布式应用程序协调服务。它是一个为分布式应用提供一致性服务的软件，提供的功能包括：配置维护、域名服务、分布式同步、组服务等。

**Dubbo**

Dubbo是Alibaba开源的分布式服务框架，它最大的特点是按照分层的方式来架构，使用这种方式可以使各个层之间解耦合（或者最大限度地松耦合）。从服务模型的角度来看，Dubbo采用的是一种非常简单的模型，要么是提供方提供服务，要么是消费方消费服务，所以基于这一点可以抽象出服务提供方（Provider）和服务消费方（Consumer）两个角色。

![image-20220403185326708](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403185326708.png)



#### 1、安装zookeeper作为注册中心

​		docker 安装

#### 2、引入依赖

```xml
<properties>
        <java.version>1.8</java.version>
        <curator.version>5.2.1</curator.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>${curator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>${curator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-x-discovery</artifactId>
            <version>${curator.version}</version>
        </dependency>

        <!-- Dubbo Spring Boot Starter -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>2.7.8</version>
        </dependency>

        <dependency>
            <groupId>org.apache.zookeeper</groupId>
            <artifactId>zookeeper</artifactId>
            <version>3.8.0</version>
        </dependency>

    </dependencies>
```



#### 3、编写服务提供者



##### 	1）配置

​	

```yaml
spring:
  application:
    name: provider_book

demo:
  service:
    version: 1.0.0

embedded:
  zookeeper:
    port: 2181

dubbo:
  application:
    name: provider_book
  registry:
    address: zookeeper://192.168.153.4:${embedded.zookeeper.port}
    client: curator
    timeout: 10000
  scan:
    base-packages: aixz.study.provider.service
```



##### 2) 编写服务接口及实现

```java
public interface BookService {
    /**
     *购买书籍接口
     * @param name
     * @return
     */
    public String getBook(String name);
}



@DubboService(version = "${demo.service.version}",interfaceClass = BookService.class)
@Service
public class BookServiceImpl implements BookService{
    @Override
    public String getBook(String name) {
        return name+"购买了《三国演艺》";
    }
}

```

#### 4、编写服务消费者

##### 1）配置

​	

```yaml
spring:
  application:
    name: consumer_readers

demo:
  service:
    version: 1.0.0

embedded:
  zookeeper:
    port: 2181


dubbo:
  application:
    name: consumer_readers
  registry:
    address: zookeeper://192.168.153.4:${embedded.zookeeper.port}
    client: curator
    register: false #是否向此注册中心注册服务，如果设为false，将只订阅，不注册
    timeout: 10000

server:
  port: 8081
```



##### 2) 编写服务消费实现

复制服务方代码到同包下

![image-20220403190429832](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403190429832.png)



```java
@RestController
public class HelloController {

    @DubboReference(version = "1.0.0")
    BookService bookServiceImpl;


    @RequestMapping("/buy/{name}")
    public String BuyBook(@PathVariable String name){
       return bookServiceImpl.getBook(name);
    }
}

```



### 三、Spring Boot和Spring Cloud



**Spring Cloud**

Spring Cloud是一个分布式的整体解决方案。Spring Cloud 为开发者提供了**在分布式系统（配置管理，服务发现，熔断，路由，微代理，控制总线，一次性****token****，全局琐，****leader****选举，分布式****session****，集群状态）中快速构建的工具**，使用Spring Cloud的开发者可以快速的启动服务或构建应用、同时能够快速和云平台资源进行对接。



**•SpringCloud分布式开发五大常用组件**

​	**•服务发现——Netflix Eureka**

​	**•客服端负载均衡——Netflix Ribbon**

​	**•断路器——Netflix Hystrix**

​	**•服务网关——Netflix Zuul**

​	**•分布式配置——Spring Cloud Config**

![image-20220403190835342](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403190835342.png)

#### Spring Cloud 入门



[Spring Cloud 官方文档](https://spring.io/projects/spring-cloud-netflix#learn)

##### 1、引入Eureka注册中心



##### 1、创建provider

配置注册中心

```yml
server:
  port: 8001

eureka:
  client:
    serviceUrl:
      defaultZone: http://127.0.0.1:8761/eureka/

spring:
  application:
    name: provide-car

```



引入依赖

```xml
<properties>
        <java.version>1.8</java.version>
        <spring-cloud.version>2021.0.1</spring-cloud.version>
        <eureka.version>3.1.1</eureka.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
            <version>${eureka.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

开启服务发现

```java
@EnableDiscoveryClient
@SpringBootApplication
public class ProvideCarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvideCarApplication.class, args);
    }

}
```

编写控制器

```java
@RestController
public class BuyController {

    @Autowired
    CarService carServiceImpl;

    @RequestMapping("/buy/{name}")
    public String buy(@PathVariable String name){
        return   carServiceImpl.buyCar(name);
    };
}
```

2、创建consumer

引入依赖

开启服务发现

引入Ribbon进行客户端负载均衡

```java
@EnableDiscoveryClient
@SpringBootApplication
public class ConsumerDriverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerDriverApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}

```



编写逻辑调用服务

```java
@RestController
public class BuyCarController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    DiscoveryClient client;

    @Autowired
    LoadBalancerClient balancerClient;

    @RequestMapping("/hello")
    public String buyCar(){
        List<ServiceInstance> instances = client.getInstances("PROVIDE_CAR");
        ServiceInstance instance = instances.get(0);
        String uri = String.valueOf(instance.getUri());
        String name = "张三";
        System.out.println(uri+"/buy/"+name);
        ResponseEntity<String> entity = restTemplate.getForEntity(uri+"/buy/"+name, String.class);
        //System.out.println(entity.getStatusCode());
        return entity.getBody();
        /*ServiceInstance instance = balancerClient.choose("PROVIDE_CAR");
        String  uri = String.valueOf(instance.getUri());
        System.out.println(uri);*/
        //return restTemplate.getForObject("http://PROVIDE-CAR/buy/张三", String.class);

    }
}
```

负载均衡

```
return restTemplate.getForObject("http://PROVIDE-CAR/buy/张三", String.class);
```





#### 问题

##### 1、**Request URI does not contain a valid hostname**

问题原因：

```
在注册服务的时候，properties文件中的服务名（spring.application.name）带上了下划线（如：PRODUCT_SERVICE）
```

解决办法：

```
将下划线换成横杠即可。(PRODUCT-SERVICE)
```





## 十三、Spring Boot与开发热部署



**在开发中我们修改一个Java文件后想看到效果不得不重启应用，这导致大量时间花费，我们希望不重启应用的情况下，程序可以自动部署（热部署）。有以下四种情况，如何能实现热部署。**



### 1、模板引擎

**–在Spring Boot中开发情况下禁用模板引擎的cache**

**–页面模板改变ctrl+F9可以重新编译当前页面并生效**

### 2、Spring Loaded

**Spring官方提供的热部署程序，实现修改类文件的热部署**

**–下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）**

**–添加运行时参数；**

**-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify**

### 3、JRebel

**–收费的一个热部署软件**

**–安装插件使用即可**

### 4、Spring Boot Devtools（推荐）

**–引入依赖**

```xml
<dependency>  
       <groupId>org.springframework.boot</groupId>  
       <artifactId>spring-boot-devtools</artifactId>   
</dependency> 

```

**–IDEA使用ctrl+F9**

**–或做一些小调整**

 ***Intellij* *IEDA和Eclipse不同，Eclipse设置了自动编译之后，修改类它会自动编译，而IDEA在非RUN或DEBUG情况下才会自动编译（前提是你已经设置了Auto-Compile）。***

**•设置自动编译（settings-compiler-make project automatically）**

**•ctrl+shift+alt+/（maintenance）**

**•勾选compiler.automake.allow.when.app.running**



## 十四、Spring Boot与监控管理

[官方文档](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator)



### 1. 启用生产就绪功能

该[`spring-boot-actuator`](https://github.com/spring-projects/spring-boot/tree/v2.6.6/spring-boot-project/spring-boot-actuator)模块提供了 Spring Boot 的所有生产就绪功能。`spring-boot-starter-actuator`启用这些功能的推荐方法是添加对“Starter”的依赖。

执行器的定义

致动器是一个制造术语，指的是用于移动或控制某物的机械装置。执行器可以通过微小的变化产生大量的运动。

要将执行器添加到基于 Maven 的项目中，请添加以下“Starter”依赖项：

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
</dependencies>
```

对于 Gradle，使用以下声明：

```gradle
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
}
```

### 2. 端点

执行器端点使您可以监视应用程序并与之交互。Spring Boot 包含许多内置端点，并允许您添加自己的端点。例如，`health`端点提供基本的应用程序健康信息。

您可以[启用或禁用](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.enabling)每个单独的端点并[通过 HTTP 或 JMX 公开它们（使它们可以远程访问）](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.exposing)。当端点被启用和公开时，它被认为是可用的。内置端点仅在可用时才会自动配置。大多数应用程序选择通过 HTTP 公开，其中端点的 ID 和前缀`/actuator`映射到 URL。例如，默认情况下，`health`端点映射到`/actuator/health`.

|      | 要了解有关执行器端点及其请求和响应格式的更多信息，请参阅单独的 API 文档（[HTML](https://docs.spring.io/spring-boot/docs/2.6.6/actuator-api/htmlsingle)或[PDF](https://docs.spring.io/spring-boot/docs/2.6.6/actuator-api/pdf/spring-boot-actuator-web-api.pdf)）。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

以下与技术无关的端点可用：

| ID                 | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| `auditevents`      | 公开当前应用程序的审计事件信息。需要一个`AuditEventRepository`豆子。 |
| `beans`            | 显示应用程序中所有 Spring bean 的完整列表。                  |
| `caches`           | 公开可用的缓存。                                             |
| `conditions`       | 显示在配置和自动配置类上评估的条件以及它们匹配或不匹配的原因。 |
| `configprops`      | 显示所有`@ConfigurationProperties`.                          |
| `env`              | 公开 Spring 的`ConfigurableEnvironment`.                     |
| `flyway`           | 显示已应用的任何 Flyway 数据库迁移。需要一个或多个`Flyway`豆子。 |
| `health`           | 显示应用程序运行状况信息。                                   |
| `httptrace`        | 显示 HTTP 跟踪信息（默认情况下，最近 100 个 HTTP 请求-响应交换）。需要一个`HttpTraceRepository`豆子。 |
| `info`             | 显示任意应用程序信息。                                       |
| `integrationgraph` | 显示 Spring 集成图。需要依赖`spring-integration-core`.       |
| `loggers`          | 显示和修改应用程序中记录器的配置。                           |
| `liquibase`        | 显示已应用的任何 Liquibase 数据库迁移。需要一个或多个`Liquibase`豆子。 |
| `metrics`          | 显示当前应用程序的“指标”信息。                               |
| `mappings`         | 显示所有`@RequestMapping`路径的整理列表。                    |
| `quartz`           | 显示有关 Quartz 调度程序作业的信息。                         |
| `scheduledtasks`   | 显示应用程序中的计划任务。                                   |
| `sessions`         | 允许从 Spring Session 支持的会话存储中检索和删除用户会话。需要使用 Spring Session 的基于 servlet 的 Web 应用程序。 |
| `shutdown`         | 让应用程序正常关闭。默认禁用。                               |
| `startup`          | 显示由. [_ ](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.spring-application.startup-tracking)`ApplicationStartup`需要`SpringApplication`配置`BufferingApplicationStartup`. |
| `threaddump`       | 执行线程转储。                                               |

如果您的应用程序是 Web 应用程序（Spring MVC、Spring WebFlux 或 Jersey），您可以使用以下附加端点：

| ID           | 描述                                                         |
| :----------- | :----------------------------------------------------------- |
| `heapdump`   | 返回一个堆转储文件。在 HotSpot JVM 上，`HPROF`返回一个 -format 文件。在 OpenJ9 JVM 上，`PHD`返回一个 -format 文件。 |
| `jolokia`    | 当 Jolokia 在类路径上时，通过 HTTP 公开 JMX bean（不适用于 WebFlux）。需要依赖`jolokia-core`. |
| `logfile`    | 返回日志文件的内容（如果已设置`logging.file.name`或属性）。`logging.file.path`支持使用 HTTP`Range`标头检索部分日志文件内容。 |
| `prometheus` | 以 Prometheus 服务器可以抓取的格式公开指标。需要依赖`micrometer-registry-prometheus`. |

#### 2.1。启用端点

默认情况下，除了`shutdown`启用之外的所有端点。要配置端点的启用，请使用其`management.endpoint.<id>.enabled`属性。以下示例启用`shutdown`端点：

特性

yaml

```properties
management.endpoint.shutdown.enabled=true
```

如果您希望端点启用是选择加入而不是选择退出，请将`management.endpoints.enabled-by-default`属性设置为`false`并使用单个端点`enabled`属性来选择重新加入。以下示例启用`info`端点并禁用所有其他端点：

特性

yaml

```properties
management.endpoints.enabled-by-default=false
management.endpoint.info.enabled=true
```

|      | 禁用的端点完全从应用程序上下文中删除。如果您只想更改暴露端点的技术，请改用[`include`and`exclude`属性](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.exposing)。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

#### 2.2. 暴露端点

由于端点可能包含敏感信息，您应该仔细考虑何时公开它们。下表显示了内置端点的默认曝光：

| ID                 | JMX    | 网络 |
| :----------------- | :----- | :--- |
| `auditevents`      | 是的   | 不   |
| `beans`            | 是的   | 不   |
| `caches`           | 是的   | 不   |
| `conditions`       | 是的   | 不   |
| `configprops`      | 是的   | 不   |
| `env`              | 是的   | 不   |
| `flyway`           | 是的   | 不   |
| `health`           | 是的   | 是的 |
| `heapdump`         | 不适用 | 不   |
| `httptrace`        | 是的   | 不   |
| `info`             | 是的   | 不   |
| `integrationgraph` | 是的   | 不   |
| `jolokia`          | 不适用 | 不   |
| `logfile`          | 不适用 | 不   |
| `loggers`          | 是的   | 不   |
| `liquibase`        | 是的   | 不   |
| `metrics`          | 是的   | 不   |
| `mappings`         | 是的   | 不   |
| `prometheus`       | 不适用 | 不   |
| `quartz`           | 是的   | 不   |
| `scheduledtasks`   | 是的   | 不   |
| `sessions`         | 是的   | 不   |
| `shutdown`         | 是的   | 不   |
| `startup`          | 是的   | 不   |
| `threaddump`       | 是的   | 不   |

要更改公开的端点，请使用以下技术特定`include`和`exclude`属性：

| 财产                                        | 默认     |
| :------------------------------------------ | :------- |
| `management.endpoints.jmx.exposure.exclude` |          |
| `management.endpoints.jmx.exposure.include` | `*`      |
| `management.endpoints.web.exposure.exclude` |          |
| `management.endpoints.web.exposure.include` | `health` |

该`include`属性列出了公开的端点的 ID。该`exclude`属性列出不应公开的端点的 ID。`exclude`财产优先于财产`include`。您可以使用端点 ID 列表来配置`include`和`exclude`属性。

例如，要停止通过 JMX 公开所有端点并仅公开`health`和`info`端点，请使用以下属性：

特性

yaml

```properties
management.endpoints.jmx.exposure.include=health,info
```

`*`可用于选择所有端点。例如，要通过 HTTP 公开除`env`和`beans`端点之外的所有内容，请使用以下属性：

特性

yaml

```properties
management.endpoints.web.exposure.include=*
management.endpoints.web.exposure.exclude=env,beans
```

|      | `*`在 YAML 中具有特殊含义，因此如果要包含（或排除）所有端点，请务必添加引号。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

|      | 如果您的应用程序是公开的，我们强烈建议您也[保护您的端点](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.security)。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

|      | 如果您想在端点暴露时实现自己的策略，您可以注册一个`EndpointFilter`bean。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |









## 附录

------



### 1、**语言简称表**

| 语言               | 简称  |
| ------------------ | ----- |
| 简体中文(中国)     | zh_CN |
| 繁体中文(中国台湾) | zh_TW |
| 繁体中文(中国香港) | zh_HK |
| 英语(中国香港)     | en_HK |
| 英语(美国)         | en_US |
| 英语(英国)         | en_GB |
| 英语(全球)         | en_WW |
| 英语(加拿大)       | en_CA |
| 英语(澳大利亚)     | en_AU |
| 英语(爱尔兰)       | en_IE |
| 英语(芬兰)         | en_FI |
| 芬兰语(芬兰)       | fi_FI |
| 英语(丹麦)         | en_DK |
| 丹麦语(丹麦)       | da_DK |
| 英语(以色列)       | en_IL |
| 希伯来语(以色列)   | he_IL |
| 英语(南非)         | en_ZA |
| 英语(印度)         | en_IN |
| 英语(挪威)         | en_NO |
| 英语(新加坡)       | en_SG |
| 英语(新西兰)       | en_NZ |
| 英语(印度尼西亚)   | en_ID |
| 英语(菲律宾)       | en_PH |
| 英语(泰国)         | en_TH |
| 英语(马来西亚)     | en_MY |
| 英语(阿拉伯)       | en_XA |
| 韩文(韩国)         | ko_KR |
| 日语(日本)         | ja_JP |
| 荷兰语(荷兰)       | nl_NL |
| 荷兰语(比利时)     | nl_BE |
| 葡萄牙语(葡萄牙)   | pt_PT |
| 葡萄牙语(巴西)     | pt_BR |
| 法语(法国)         | fr_FR |
| 法语(卢森堡)       | fr_LU |
| 法语(瑞士)         | fr_CH |
| 法语(比利时)       | fr_BE |
| 法语(加拿大)       | fr_CA |
| 西班牙语(拉丁美洲) | es_LA |
| 西班牙语(西班牙)   | es_ES |
| 西班牙语(阿根廷)   | es_AR |
| 西班牙语(美国)     | es_US |
| 西班牙语(墨西哥)   | es_MX |
| 西班牙语(哥伦比亚) | es_CO |
| 西班牙语(波多黎各) | es_PR |
| 德语(德国)         | de_DE |
| 德语(奥地利)       | de_AT |
| 德语(瑞士)         | de_CH |
| 俄语(俄罗斯)       | ru_RU |
| 意大利语(意大利)   | it_IT |
| 希腊语(希腊)       | el_GR |
| 挪威语(挪威)       | no_NO |
| 匈牙利语(匈牙利)   | hu_HU |
| 土耳其语(土耳其)   | tr_TR |
| 捷克语(捷克共和国) | cs_CZ |
| 斯洛文尼亚语       | sl_SL |
| 波兰语(波兰)       | pl_PL |
| 瑞典语(瑞典)       | sv_SE |
| 西班牙语(智利)     | es_CL |

### 2、spring boot启动图像

####  	1.首先进入下面网址生成一个你想要的图像

```ruby
http://patorjk.com/software/taag/#p=display&h=3&v=3&f=4Max&t=itcast%20Spring%20Boot
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191116550.png)

#### 2.在自己的工程中建立banner.txt文件

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191440620.png)

#### 3.把生成图像复制进banner.txt

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191614928.png)

#### 4.启动项目即可！

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191731144.png)

搞定！

备注：代码复制下面这个就行，用你的图案代替一下中间的图案！

```html
${AnsiColor.BLUE}

██╗  ██╗██╗   ██╗██╗    ██╗███████╗██╗
╚██╗██╔╝██║   ██║██║    ██║██╔════╝██║
 ╚███╔╝ ██║   ██║██║ █╗ ██║█████╗  ██║
 ██╔██╗ ██║   ██║██║███╗██║██╔══╝  ██║
██╔╝ ██╗╚██████╔╝╚███╔███╔╝███████╗██║
╚═╝  ╚═╝ ╚═════╝  ╚══╝╚══╝ ╚══════╝╚═╝

------版本号------${spring-boot.version}
```

###  3、MVC请求重定向、转发

#### 请求重定向

```java
redirect:
```

#### 请求转发

```
forward:
```

## 问题

### 1、from 发送其他请求无效

SpringMVC使用get/post以外提交方式，例如put等需要具备以下条件：

配置HiddenHttpMethodFilter
页面创建一个post表单
创建一个input项，name=”_method” Value=“put”，值就是指定的请求方式

#### 原因：

​		**springboot自动配置，帮我们省略了第一步的配置，上面代码方法就是为了实现自动配置，但是因为注解@ConditionalOnProperty限制了自动配置，默认false不开启配置，所以页面的put提交无法使用。**

#### **解决：**

```yaml
spring.mvc.hiddenmethod.filter.enabled=true
```

