# Extjs

## 一、字段注释

### 1、 region

页面布局：主要使用一个控件panel，感觉就相当于HTML中的div。位置通过region来设置。

有一点需要注意的是，使用region来布局，位置一定要设成north→center→south或者west→center→east。如果只有两块就west→center而不能westr→east。 

```
region: 'west',//自动排位置
```

### 2、显示边界

```js
style:'border:1px solid #3399ff;',
```

### 3、表格自动产生序号

```js
new Ext.grid.RowNumberer({width:30}),
```

### 4、设置表单项不可用

```js
disabled:true,
```

### 5、嵌套表头

```js
var zqsd_item = [
    { header: '期限',align : 'left',width : 100,sortable : true,dataIndex: 'email' },
    {
        text:"滚动情景",
        columns:[
            { header: '轻度',align : 'left',width : 100,sortable : true,dataIndex: 'email'},
            { header: '中度',align : 'left',width : 100,sortable : true,dataIndex: 'email'},
            { header: '重度',align : 'left',width : 100,sortable : true,dataIndex: 'email'}
        ]
    }
]
```

### 6、自动高度

```js
autoHeight : 'true',
```

### 7、表格分组

```js
// 1) store 里声明分组字段
groupField: 'name',
// 2) store 通过不同的 name 区分

// 3）Panel 定义添加 features 属性
    var gmjjGrid =new Ext.grid.Panel({
        id:functionName+"gmjjGrid",
        autoHeight : 'true',
        region:'center',
        layout: 'fit',
        bufferedRenderer : false,//防止grid刷新出现空白
        border: true,
        columns: zqsd_item,
        features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: "{name}",
            startCollapsed:false,// 分组后，所有组是展开还是收缩
            hideGroupedHeader: true,
            enableGroupingMenu: false
        }],
        store:simpsonsStore
    });
```

### 8、使用新的store/columns重新生成grid

```js
reconfigure( Ext.data.Store store, Object[] columns )
//使用一个新的store/columns配置项进行重新配置生成grid。 如果你不想改变store或者columns，他们任意一项均可以省略。
```



### 9、动态设置store的值

```js
zqStore.removeAll();
zqStore.getProxy().data  = jsonObj.dblist;
zqStore.reload();
```



### 10、reset（）

**重置表单项的当前值为最初加载的值并清除验证信息**



### 11、setDisabled（）



```js
setDisabled( Boolean disabled )
设置使用当前action的所有组件的禁用状态。 enable 和 disable 的快捷方式。

Parameters
disabled : Boolean
True 将禁用组件, false 将启用它
```



### 12、日期选择框

```js
//字段定义
xtype : "datefield",
value:sys.datetime
//取值
var scene_date = Ext.getCmp(functionName+'sceneDate').getRawValue();
```



# Node.js





![nodejs](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/nodejs.jpg)

简单的说 Node.js 就是运行在服务端的 JavaScript。

Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。

Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎，V8引擎执行Javascript的速度非常快，性能非常好。





## NPM 使用介绍



NPM是随同NodeJS一起安装的包管理工具，能解决NodeJS代码部署上的很多问题，常见的使用场景有以下几种：

- 允许用户从NPM服务器下载别人编写的第三方包到本地使用。
- 允许用户从NPM服务器下载并安装别人编写的命令行程序到本地使用。
- 允许用户将自己编写的包或命令行程序上传到NPM服务器供别人使用。

由于新版的nodejs已经集成了npm，所以之前npm也一并安装好了。同样可以通过输入 **"npm -v"** 来测试是否成功安装。命令如下，出现版本提示表示安装成功:

```
$ npm -v
2.3.0
```

如果你安装的是旧版本的 npm，可以很容易得通过 npm 命令来升级，命令如下：

```
$ sudo npm install npm -g
/usr/local/bin/npm -> /usr/local/lib/node_modules/npm/bin/npm-cli.js
npm@2.14.2 /usr/local/lib/node_modules/npm
```

如果是 Window 系统使用以下命令即可：

```
npm install npm -g
```

> 使用淘宝镜像的命令：
>
> ```
> npm install -g cnpm --registry=https://registry.npm.taobao.org
> ```

------

### 使用 npm 命令安装模块

npm 安装 Node.js 模块语法格式如下：

```
$ npm install <Module Name>
```

以下实例，我们使用 npm 命令安装常用的 Node.js web框架模块 **express**:

```
$ npm install express
```

安装好之后，express 包就放在了工程目录下的 node_modules 目录中，因此在代码中只需要通过 **require('express')** 的方式就好，无需指定第三方包路径。

```
var express = require('express');
```

------

### 全局安装与本地安装

npm 的包安装分为本地安装（local）、全局安装（global）两种，从敲的命令行来看，差别只是有没有-g而已，比如

```
npm install express          # 本地安装
npm install express -g   # 全局安装
```

如果出现以下错误：

```
npm err! Error: connect ECONNREFUSED 127.0.0.1:8087 
```

解决办法为：

```
$ npm config set proxy null
```

#### 本地安装

- \1. 将安装包放在 ./node_modules 下（运行 npm 命令时所在的目录），如果没有 node_modules 目录，会在当前执行 npm 命令的目录下生成 node_modules 目录。
- \2. 可以通过 require() 来引入本地安装的包。

#### 全局安装

- \1. 将安装包放在 /usr/local 下或者你 node 的安装目录。
- \2. 可以直接在命令行里使用。

如果你希望具备两者功能，则需要在两个地方安装它或使用 **npm link**。

接下来我们使用全局方式安装 express

```
$ npm install express -g
```

安装过程输出如下内容，第一行输出了模块的版本号及安装位置。



## Nodejs 服务后台常驻



### 一、利用 forever



forever是一个nodejs守护进程，完全由命令行操控。forever会监控nodejs服务，并在服务挂掉后进行重启。

1、安装 forever

```shell
npm install forever -g
```

2、启动服务

```shell
service forever start
```

3、使用 forever 启动 js 文件

```shell
forever start index.js
```

4、停止 js 文件

```shell
forever stop index.js
```

5、启动js文件并输出日志文件

```shell
forever start -l forever.log -o out.log -e err.log index.js
```

6、重启js文件

```shell
forever restart index.js
```

7、查看正在运行的进程

```shell
forever list
```



### 二、PM2



pm2是一个进程管理工具,可以用它来管理你的node进程,并查看node进程的状态,当然也支持性能监控,进程守护,负载均衡等功能

```shell
npm install -g pm2
pm2 start app.js        // 启动
pm2 start app.js -i max //启动 使用所有CPU核心的集群
pm2 stop app.js         // 停止
pm2 stop all            // 停止所有
pm2 restart app.js      // 重启
pm2 restart all         // 重启所有
pm2 delete  app.js      // 关闭
```

### 三、nohub



nodejs 自带node.js自带服务nohub，不需要安装别的包。
缺点：存在无法查询日志等问题,关闭终端后服务也就关闭了， 经测试是这样的。

```shell
nohup node ***.js &
```





## 问题



# JavaScript 



[javascript 教程](https://wangdoc.com/javascript/)

## 一、常用函数、方法



### 1、String转int



方案一代码：

```
Number(str) 
```

方案二代码：

```
//parseInt 方法都有两个参数, 第一个参数就是要转换的对象, 第二个参数是进制基数, 可以是 2, 8, 10, 16, 默认以 10 进制处理
parseInt(str)
```

方案一与方案二对比：

```
var str='1250' ;
alert( Number(str) );  //得到1250
alert(parseInt(str));  //得到1250
var str1='00100';
alert( Number(str1) );  //得到100
alert(parseInt(str1));  //得到64
```

注意：parseInt方法在format'00'开头的数字时会当作2进制转10进制的方法进行转换；

所以建议string转int最好用Number方法；

### 2、map()

`map()`方法将数组的所有成员依次传入参数函数，然后把每一次的执行结果组成一个新数组返回。

```
var numbers = [1, 2, 3];

numbers.map(function (n) {
  return n + 1;
});
// [2, 3, 4]

numbers
// [1, 2, 3]
```

上面代码中，`numbers`数组的所有成员依次执行参数函数，运行结果组成一个新数组返回，原数组没有变化。

`map()`方法接受一个函数作为参数。该函数调用时，`map()`方法向它传入三个参数：当前成员、当前位置和数组本身。

```
[1, 2, 3].map(function(elem, index, arr) {
  return elem * index;
});
// [0, 2, 6]
```

上面代码中，`map()`方法的回调函数有三个参数，`elem`为当前成员的值，`index`为当前成员的位置，`arr`为原数组（`[1, 2, 3]`）。

`map()`方法还可以接受第二个参数，用来绑定回调函数内部的`this`变量（详见《this 变量》一章）。

```
var arr = ['a', 'b', 'c'];

[1, 2].map(function (e) {
  return this[e];
}, arr)
// ['b', 'c']
```

上面代码通过`map()`方法的第二个参数，将回调函数内部的`this`对象，指向`arr`数组。

如果数组有空位，`map()`方法的回调函数在这个位置不会执行，会跳过数组的空位。

```
var f = function (n) { return 'a' };

[1, undefined, 2].map(f) // ["a", "a", "a"]
[1, null, 2].map(f) // ["a", "a", "a"]
[1, , 2].map(f) // ["a", , "a"]
```

上面代码中，`map()`方法不会跳过`undefined`和`null`，但是会跳过空位。

### 3、forEach()

`forEach()`方法与`map()`方法很相似，也是对数组的所有成员依次执行参数函数。但是，`forEach()`方法不返回值，只用来操作数据。这就是说，如果数组遍历的目的是为了得到返回值，那么使用`map()`方法，否则使用`forEach()`方法。

`forEach()`的用法与`map()`方法一致，参数是一个函数，该函数同样接受三个参数：当前值、当前位置、整个数组。

```
function log(element, index, array) {
  console.log('[' + index + '] = ' + element);
}

[2, 5, 9].forEach(log);
// [0] = 2
// [1] = 5
// [2] = 9
```

上面代码中，`forEach()`遍历数组不是为了得到返回值，而是为了在屏幕输出内容，所以不必使用`map()`方法。

`forEach()`方法也可以接受第二个参数，绑定参数函数的`this`变量。

```
var out = [];

[1, 2, 3].forEach(function(elem) {
  this.push(elem * elem);
}, out);

out // [1, 4, 9]
```

上面代码中，空数组`out`是`forEach()`方法的第二个参数，结果，回调函数内部的`this`关键字就指向`out`。

注意，`forEach()`方法无法中断执行，总是会将所有成员遍历完。如果希望符合某种条件时，就中断遍历，要使用`for`循环。

```
var arr = [1, 2, 3];

for (var i = 0; i < arr.length; i++) {
  if (arr[i] === 2) break;
  console.log(arr[i]);
}
// 1
```

上面代码中，执行到数组的第二个成员时，就会中断执行。`forEach()`方法做不到这一点。

`forEach()`方法也会跳过数组的空位。

```
var log = function (n) {
  console.log(n + 1);
};

[1, undefined, 2].forEach(log)
// 2
// NaN
// 3

[1, null, 2].forEach(log)
// 2
// 1
// 3

[1, , 2].forEach(log)
// 2
// 3
```

上面代码中，`forEach()`方法不会跳过`undefined`和`null`，但会跳过空位。



## 二、常用教程

### 1、动态key获取对象属性

```js
var obj = {name:"张三",value:10}
//key动态拼接
key ="";
var value = obj[key];
```





# VUE



## 1、常用代码



### 处理手机号中间四位

```vue
<template>
  <div>
    <div v-for="item in person" :key="item.id">
    {{ item.iphone | phoneFilter }}
 
    </div>
  </div>
</template>
 
<script>
export default {
  data() {
    return {
      person: [
        {
          iphone: "18220550305",
        },
        {
          iphone: "18220550305",
        },
        {
          iphone: "18220550305",
        },
      ],
    };
  },
 
  filters: {
    phoneFilter(val) {
      let reg = /^(.{3}).*(.{4})$/;
      return val.replace(reg, "$1****$2");
    },
  },
};
</script>
```

