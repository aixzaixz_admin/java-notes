## 	拉取 CentOS7 镜像

>docker pull centos:7.9.2009

## 把 CentOS7 镜像运行为 Docker 容器：

```sh
docker run --name=dm8demo2 --hostname=dm8demo2 --restart=always -e TZ=Asia/Shanghai --net mynetwork --ip 172.172.1.102 -d --privileged=true -p 5236:5236 -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e INSTANCE_NAME=dm8demo2 -e PAGE_SIZE=32 -e EXTENT_SIZE=32 -e LOG_SIZE=2048 -v /data/dm8demo2/dmdbms:/opt/dmdbms -it eeb6ee3f44bd /usr/sbin/init
```

> **eeb6ee3f44bd 为容器ID**

## 新建 dmdba 用户

> **需进入 dm8demo2 容器**

### 进入容器

```sh
docker exec -it DM8DEMO1 /bin/bash
docker exec -it DM8DEMO2 /bin/bash
docker exec -it DMMONITOR /bin/bash
```

### 设置中文环境编码

#### 安装语言包

```sql
yum install kde-l10n-Chinese -y
yum install glibc-common -y
```

#### 转化语言环境和字符集

```sql
localedef -c -f UTF-8 -i zh_CN zh_CN.utf8
```

#### 添加定义到系统环境变量

```sh
vi /etc/profile
```

**添加如下配置**

> export LC_ALL=zh_CN.utf8

#### 执行生效

```sh
source /etc/profile
```

### 创建用户所在的组

```sh
groupadd dinstall
```

### 创建用户

```sh
useradd -g dinstall -m -d /home/dmdba -s /bin/bash dmdba
```

**说明**

>-g, --gid GROUP 新建账户所属组的名称或 ID
>
>-m, --create-home 创建用户的主目录
>
>-d, --home-dir HOME_DIR 新账户的主目录
>
>-s, --shell SHELL 新建账户登录的 shell

### 修改新建账户的密码

```sh
passwd dmdba
```

> xiaoluo666

### 修改文件打开最大数

**重启服务器（本场景下是重启 dm8demo2 容器）后永久生效**

用 vi 编辑器（需进入容器内）打开 **/etc/security/limits.conf** 文件，在最后面添加以下四条语句：

```sh
vi /etc/security/limits.conf

dmdba hard nofile 65536
dmdba soft nofile 65536
dmdba hard stack 32768
dmdba soft stack 16384
```

切换到 dmdba 用户，查看是否生效： 

```sh
su - dmdba
ulimit -a
```

**设置参数临时生效【不推荐】**

可使用 dmdba 用户执行如下命令，使设置临时生效

```sh
ulimit -n 65536
```

### 把获取到的 DM 数据库安装包传入容器

-  解压 dm8_20230104_x86_rh6_64.iso 后得到 DMInstall.bin 文 件 

- 将解压后的 DMInstall.bin 文件拷贝到容器

  >**方法一：**
  >
  >​		在宿主机下利用 MobaXterm 之类的工具连接到容器后，采用文件上传的方式将 DMInstall.bin 文件上传到容器；
  >
  >**方法二：**
  >
  >​		在宿主机下利用 Docker cp 命令将解压后的 DMInstall.bin 文件拷贝到容器；
  >
  >```sh
  >docker cp /home/OracleFile/DMInstall.bin DM8DEMO1:/home/dmdba/
  >docker cp /home/OracleFile/DMInstall.bin DM8DEMO2:/home/dmdba/
  >docker cp /home/OracleFile/DMInstall.bin DMMONITOR:/home/dmdba/
  >```

- 修改安装目录权限

  >假设 DMInstall.bin 文件存放于容器的 /dm8/ 路径下
  >
  >1) 将安装路径目录 /dm8/ 权限的用户修改为 dmdba，用户组修改为 dinstall。命令如下：
  >
  >   ```sh
  >   chown dmdba:dinstall -R /home/dmdba/
  >   ```
  >
  >2) 给安装路径下的文件设置 755 权限。命令如下：
  >
  >   ```sql
  >   chmod -R 755 /home/dmdba
  >   ```

## **数据库安装** 

**切换至 dmdba 用户下，在 /dm8 目录下使用命令行安装数据库程序，依次执行以下命令安装 DM 数据库：** 

```sh
docker exec -it dm8demo3 /bin/bash
su - dmdba
cd /home/dmdba
./DMInstall.bin -i
```

>**报错**
>
>Not support Chinese or UTF-8 in current environment, please set environment variable LANG.
>Please select the installer's language
>
>**设置中文**
>
>export LANG=zh_CN.UTF-8

**按需求依次选择【安装语言，默认为中文】，试用版安装选择【不输入 Key 文件】，选择【默认时区 21】**

![image-20230704163938895](C:/Users/wuzp/AppData/Roaming/Typora/typora-user-images/image-20230704163938895.png)

**选择【1-典型安装】，按已规划的安装目录完（可自己选择安装目录，一般默认即可）成数据库软件安装**

![image-20230704164116941](C:/Users/wuzp/AppData/Roaming/Typora/typora-user-images/image-20230704164116941.png)

### 配置环境变量

切换到 root 用户进入 dmdba 用户的根目录下，配置对应的环境变量。DM_HOME 变量和动态链接库文件的加载路径在程序安装成功后会自动导入

```sh
export PATH=$PATH:$DM_HOME/bin:$DM_HOME/tool
```

**编辑 .bash_profile使其最终效果如下图所示：**

```sh
cd /home/dmdba/
vi .bash_profile
```

**切换至 dmdba 用户下，执行以下命令，使环境变量生效**

```sh
su - dmdba
source .bash_profile
```



## **配置实例** 

使用 dmdba 用户配置实例，进入到 DM 数据库安装目录下的bin 目录中，使用 dminit 命令初始化实例。dminit 命令可设置多种参数，可执行 dminit help 命令查看可配置参数

```sh
/home/dmdba/dmdbms/bin/dminit INSTANCE_NAME=DM8DEMO1 PATH=/home/dmdba/dmdbms/data PAGE_SIZE=32 EXTENT_SIZE=32 LOG_SIZE=2048 SYSDBA_PWD=dmdba#123

/home/dmdba/dmdbms/bin/dminit INSTANCE_NAME=DM8DEMO2 PATH=/home/dmdba/dmdbms/data PAGE_SIZE=32 EXTENT_SIZE=32 LOG_SIZE=2048 SYSDBA_PWD=dmdba#123

/home/dmdba/dmdbms/bin/dminit INSTANCE_NAME=DMMONITOR PATH=/home/dmdba/dmdbms/data PAGE_SIZE=32 EXTENT_SIZE=32 LOG_SIZE=2048 SYSDBA_PWD=dmdba#123
```

>1、实例化前，需要用 root 用户将 /opt/dmdbms/data 目录权限授予 dmdba 用户；
>
>2、如果此处自定义了初始化参数，在后面的注册服务和启动数据库等步骤中，请按实际的自定义参数进行操作

**主要参数说明：**

>**DB_NAME** 数据库名(缺省使用 DAMENG)
>
>**INSTANCE_NAME** 实例名(缺省使用 DMSERVER)
>
>**PATH** 数据库实例化目录；
>
>**PAGE_SIZE** 数据文件使用的页大小，可以为 4 KB、8 KB、16 KB 或32 KB 之一，选择的页大小越大，则 DM 支持的元组长度也越大，但同时空间利用率可能下降，缺省使用 8 KB；
>
>**EXTENT_SIZE** 指数据文件使用的簇大小，即每次分配新的段空间时连续的页数。只能是 16 页或 32 页或 64 页之一，缺省使用 16 页；
>
>**LOG_SIZE** 日志文件大小，单位为 MB，缺省使用 256MB；
>
>**CHARSET** 字符集选项。0 代表 GB18030；1 代表 UTF-8；2 代表韩文字符集 EUC-KR；取值 0、1 或 2 之一。默认值为 0；
>
>**SYSDBA_PWD** 设定 SYSDBA 的密码，缺省使用 SYSDBA；

## 注册服务

>注册服务需使用 root 用户进行注册。使用 root 用户进入数据库安装目录的 /script/root 下，如下所示：

```sh
cd /home/dmdba/dmdbms/script/root
./dm_service_installer.sh -t dmserver -dm_ini /home/dmdba/dmdbms/data/DAMENG/dm.ini -p DMSERVER
```

## 命令行启停数据库

### 启动数据库

```sh
systemctl start DmServiceDMSERVER.service
/home/dmdba/dmdbms/bin/DmServiceDMSERVER start
```

### 停止数据库

```sh
systemctl stop DmServiceDMSERVER.service
/home/dmdba/dmdbms/bin/DmServiceDMSERVER stop
```

### 重启数据库

```sh
systemctl restart DmServiceDMSERVER.service
 /home/dmdba/dmdbms/bin/DmServiceDMSERVER restart
```

### 查看数据库服务状态

```sh
systemctl status DmServiceDMSERVER.service
 /home/dmdba/dmdbms/bin/DmServiceDMSERVER status
 ps -ef | grep dmserver
```

**可前台启动（即不通过服务来启动），进入 DM 安装目录下的 bin 目录下**

```sh
./dmserver /home/dmdba/dmdbms/data/DAMENG/dm.ini
```

**该启动方式为前台启动，若想关闭数据库，则输入 exit 即可。也可进入 DM 安装目录下的 bin 目录下，启动/停止/重启数据库/查看数据库状态**

```sh
./DmServiceDMSERVER start/stop/restart/status
```

### 连接数据库

```sh
su - dmdba

#分步连接
disql SYSDBA
dmdba#123

#直接连接
disql SYSDBA/dmdba#123
```

### 启动监视器

```sh
/home/dmdba/dmdbms/bin/dmmonitor /home/dmdba/dmdbms/data/DAMENG/dmmonitor.ini
```



## 集群

```sh
docker run --name=DM8DEMO1 --hostname=DM8DEMO1 --restart=always -e TZ=Asia/Shanghai --net mynetwork --ip 172.172.1.101 -d --privileged=true -p 5236:5236 -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e INSTANCE_NAME=DM8DEMO1 -e PAGE_SIZE=32 -e EXTENT_SIZE=32 -e LOG_SIZE=2048 -v /data/dm8demo1/dmdbms:/opt/dmdbms -it eeb6ee3f44bd /usr/sbin/init

docker run --name=DM8DEMO2 --hostname=DM8DEMO2 --restart=always -e TZ=Asia/Shanghai --net mynetwork --ip 172.172.1.102 -d --privileged=true -p 5237:5236 -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e INSTANCE_NAME=DM8DEMO2 -e PAGE_SIZE=32 -e EXTENT_SIZE=32 -e LOG_SIZE=2048 -v /data/dm8demo2/dmdbms:/opt/dmdbms -it eeb6ee3f44bd /usr/sbin/init

docker run --name=DMMONITOR --hostname=DMMONITOR --restart=always -e TZ=Asia/Shanghai --net mynetwork --ip 172.172.1.103 -d --privileged=true -p 5238:5236 -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e INSTANCE_NAME=DMMONITOR -e PAGE_SIZE=32 -e EXTENT_SIZE=32 -e LOG_SIZE=2048 -v /data//dmmonitor/dmdbms:/opt/dmdbms -it eeb6ee3f44bd /usr/sbin/init
```

### **配置 A 机器** 

> **安装 -> 实例化 ->启动服务**

#### 脱机备份数据库

##### 停止数据库

```sh
/home/dmdba/dmdbms/bin/DmServiceDMSERVER stop
```

**在主机 GRP1RW01 上执行以下命令，确认主库 dmap 服务已启动：**

```sh
ps -ef | grep dmap
```

**若未启动，则先启动 DMAP 服务，在 dmdba 用户下执行以下命令**

```sh
/home/dmdba/dmdbms/bin/DmAPService start
```

#### 启动 dmrman 工具

```sh
/home/dmdba/dmdbms/bin/dmrman use_ap=2
```

#### 执行 backup 备份全库

```sh
backup database '/home/dmdba/dmdbms/data/DAMENG/dm.ini' backupset '/home/dmdba/dmdbms/bakfull';
```

#### 进入备份目录

```SH
 cd /home/dmdba/dmdbms/bakfull
```

#### 拷贝文件

```sh
docker cp DM8DEMO1:/home/dmdba/dmdbms/bakfull /data/dm8demo1/dmdbms/data/
```

### **配置 B机器** 

> **安装 -> 实例化 ->启动服务->停止服务** 

```sh
/home/dmdba/dmdbms/bin/DmServiceDMSERVER stop
```

#### 备份还原来自主机的备库

**将来自主机的备库拷贝到这台服务器上**

```sh
docker cp /data/dm8demo1/dmdbms/data/bakfull DM8DEMO2:/home/dmdba/dmdbms 
```

**在这台服务器上使用 dmrman 工具还原备库，可在 dmdba 用户下执行以下命令：**

```sh
dmrman use_ap=2
```

**进入 dmrman 工具交互界面后，再分别执行以下指令进行还原操作**

##### 执行 restore

```sh
restore database '/home/dmdba/dmdbms/data/DAMENG/dm.ini' from backupset '/home/dmdba/dmdbms/bakfull';
```

##### 执行 recover

```sh
recover database '/home/dmdba/dmdbms/data/DAMENG/dm.ini' from backupset '/home/dmdba/dmdbms/bakfull';
```

##### 执行 recover update db_magic

```sh
recover database '/home/dmdba/dmdbms/data/DAMENG/dm.ini' update db_magic;
```

### 修改 dm.ini 参数

**注意：所有节点都要修改**

**修改方式有两种：**

- 直接进入到容器里执行 vi  /home/dmdba/dmdbms/data/DAMENG/dm.ini 进行修改
- 在宿主机直接修 改 ， 此 时 修 改 的 路 径 是 宿 主 机 路 径 ， 如/data/dm8demo1/dmdbms/data/DAMENG/dm.ini（具体路径以创建容器时映射的路径为准）

#### 主库 GRP1RW1 上修改

**在一搬模式下使用,搜索**

```sh
/INSTANCE_NAME
```

```sh
INSTANCE_NAME = DM8DEMO1
MAL_INI = 1
ARCH_INI = 1
ALTER_MODE_STATUS = 1
ENABLE_OFFLINE_TS = 2
```

#### 在备库 GRP1RW2 上修改以下参数值

```sh
INSTANCE_NAME = DM8DEMO2 
MAL_INI = 1
ARCH_INI = 1
ALTER_MODE_STATUS = 1
ENABLE_OFFLINE_TS = 2
```

>集群配置完成后 ALTER_MODE_STATUS = 0 配置修改回去

>若不止一台备库，其他备库以此类推去修改！

### **配置归档文件 dmarch.ini** 

#### 在主库 GRP1RW1 上配置

>在 实 例 目 录 下 新 建 dmarch.ini 归 档 配 置 文 件 （ 宿 主 机 路 径为/data/dm8demo1/dmdbms/data/DAMENG/dmarch.ini，容器内路径为/home/dmdba/dmdbms/data/DAMENG/dmarch.ini）REALTIME 自动切换模式下，需要指定 ARCH_WAIT_APPLY=1 保证主备事务一致

```sh
vi dmarch.ini
```

**具体配置内容如下所示：**

```sh
ARCH_WAIT_APPLY=1 # REALTIME 自动切换模式
[ARCHIVE_REALTIME1]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO2 # 实时归档目标实例名（备库实例名）
[ARCHIVE_LOCAL1]
ARCH_TYPE = LOCAL # 本地归档类型
ARCH_DEST = /home/dmdba/dmdbms/data/DAMENG/arch # 本地归档文件存放路径
ARCH_FILE_SIZE = 512 # 单位 MB，本地单个归档文件最大值
ARCH_SPACE_LIMIT = 2048 # 单位 MB，0 表示无限制，范围1024~4294967294 MB
```

#### 在备库 GRP1RW2 上配置

```ssh
ARCH_WAIT_APPLY=1 # REALTIME 自动切换模式
[ARCHIVE_REALTIME1]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO1 # 实时归档目标实例名（主库实例名）
[ARCHIVE_LOCAL1]
ARCH_TYPE = LOCAL # 本地归档类型
ARCH_DEST = /home/dmdba/dmdbms/data/DAMENG/arch # 本地归档文件存放路径
ARCH_FILE_SIZE = 512 # 单位 MB，本地单个归档文件最大值
ARCH_SPACE_LIMIT = 2048 # 单位 MB，0 表示无限制，范围1024~4294967294 MB
```

**若不止一个备库，假设还有第 2 个备库（甚至更多，以此类推），则实时归档类型的配置内容会存在些许差异，如下所示**

##### 主库

```sh
ARCH_WAIT_APPLY=1 # REALTIME 自动切换模式
[ARCHIVE_REALTIME1]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO2 # 实时归档目标实例名（备库实例名）
#  差异部分 start
[ARCHIVE_REALTIME2]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO3 # 实时归档目标实例名（备库实例名）
#  差异部分 end
[ARCHIVE_LOCAL1]
ARCH_TYPE = LOCAL # 本地归档类型
ARCH_DEST =/home/dmdba/dmdbms/data/DAMENG/arch # 本地归档文件存放路径
ARCH_FILE_SIZE = 512 # 单位 MB，本地单个归档文件最大值
ARCH_SPACE_LIMIT = 2048 # 单位 MB，0 表示无限制，范围1024~4294967294 MB
```

##### 备库 1：

```sh
ARCH_WAIT_APPLY=1 # REALTIME 自动切换模式
[ARCHIVE_REALTIME1]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO1 # 实时归档目标实例名（主库实例名）
#  差异部分 start
[ARCHIVE_REALTIME2]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO3 # 实时归档目标实例名（其他备库实例名）
#  差异部分 end
[ARCHIVE_LOCAL1]
ARCH_TYPE = LOCAL # 本地归档类型
ARCH_DEST = /home/dmdba/dmdbms/data/DAMENG/arch # 本地归档文件存放路径
ARCH_FILE_SIZE = 512 # 单位 MB，本地单个归档文件最大值
ARCH_SPACE_LIMIT = 2048 # 单位 MB，0 表示无限制，范围1024~4294967294 MB
```

##### 备库 2：

```sh
ARCH_WAIT_APPLY=1 # REALTIME 自动切换模式
[ARCHIVE_REALTIME1]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO1 # 实时归档目标实例名（主库实例名）
[ARCHIVE_REALTIME2]
ARCH_TYPE = REALTIME # 实时归档类型
ARCH_DEST = DM8DEMO2 # 实时归档目标实例名（其他备库实例名）
[ARCHIVE_LOCAL1]
ARCH_TYPE = LOCAL # 本地归档类型
ARCH_DEST = /home/dmdba/dmdbms/data/DAMENG/arch # 本地归档文件存放路径
ARCH_FILE_SIZE = 512 # 单位 MB，本地单个归档文件最大值
ARCH_SPACE_LIMIT = 2048 # 单位 MB，0 表示无限制，范围1024~4294967294 MB
```

##### 授权

```sh
chmod -R 755 dmarch.ini
```



### 配置 MAL 文件 dmmal.ini

```sh
cd /home/dmdba/dmdbms/data/DAMENG
vi dmmal.ini
```

在实例目录下新建 dmmal.ini 归 档 配 置 文 件 (宿 主 机 路 径 为/data/dm8demo1/dmdbms/data/DAMENG

/dmmal.ini，容器内路径为/home/dmdba/dmdbms/data/DAMENG/dmmal.ini）主备库各节点的文件内容要保持相同，如下所示

```sh
MAL_CHECK_INTERVAL = 5
MAL_CONN_FAIL_INTERVAL = 15
[MAL_INST1]
 MAL_INST_NAME = DM8DEMO1
 MAL_HOST = 172.172.1.101
 MAL_PORT = 7336
 MAL_INST_HOST = 172.172.1.101
 MAL_INST_PORT = 5236
 MAL_DW_PORT = 7436
 MAL_INST_DW_PORT = 7536
[MAL_INST2]
 MAL_INST_NAME = DM8DEMO2 # 与 dm.ini 中的INSTANCE_NAME 一致
 MAL_HOST = 172.172.1.102 # MAL 系统监听 TCP 内部网络 IP
 MAL_PORT = 7336 # MAL 系统监听 TCP 连接的端口
 MAL_INST_HOST = 172.172.1.102 # 实例的对外服务 IP 地址
 MAL_INST_PORT = 5236 # 与 dm.ini 中的 PORT_NUM 一致
 MAL_DW_PORT = 7436 # 实例对应的守护进程监听TCP 端口
 MAL_INST_DW_PORT = 7536
```

##### 授权

```sh
chmod -R 755 dmmal.ini
```

##### 拷贝文件

```sh
docker cp DM8DEMO1:/home/dmdba/dmdbms/data/DAMENG/dmmal.ini  /data/dm8demo1/dmdbms/data/

docker cp /data/dm8demo1/dmdbms/data/dmmal.ini DM8DEMO1:/home/dmdba/dmdbms/data/DAMENG/
docker cp /data/dm8demo1/dmdbms/data/dmmal.ini DM8DEMO2:/home/dmdba/dmdbms/data/DAMENG/

chown dmdba:dinstall -R dmmal.ini
chmod -R 755 dmmal.ini
```





**若不止一个备库，假设还有第 2 个备库（甚至更多，以此类推）**

### 配置守护进程配置文件**dmwatcher.ini** 

**在实例目录下新建 dmwatcher.ini 归档配置文件**

- 宿主机路径  为/data/dm8demo1/dmdbms/data/DAMENG/dmwatcher.ini，
- 容器内路径为/home/dmdba/dmdbms/data/DAMENG/dmwatcher.ini

**主备库各节点的文件内容要保持相同，如下所示**

```sh
[GRP_RW] 
DW_TYPE = GLOBAL #全局守护类型
DW_MODE = AUTO #自动切换模式
DW_ERROR_TIME = 30 #远程守护进程故障认定时间
INST_RECOVER_TIME = 60 #主库守护进程启动恢复的间隔时间
INST_ERROR_TIME = 20 #本地实例故障认定时间
INST_OGUID = 453331 #守护系统唯一 OGUID 值
INST_INI = /home/dmdba/dmdbms/data/DAMENG/dm.ini #dm.ini 配置文件路径
INST_AUTO_RESTART = 1 #打开实例的自动启动功能
INST_STARTUP_CMD = /home/dmdba/dmdbms/bin/DmServiceDMSERVER start #自动启动服务的命令
```

##### 授权

```sh
chmod -R 755 dmwatcher.ini
```

##### 拷贝文件

```sh
docker cp DM8DEMO1:/home/dmdba/dmdbms/data/DAMENG/dmwatcher.ini /data/dm8demo1/dmdbms/data/

docker cp /data/dm8demo1/dmdbms/data/dmwatcher.ini DM8DEMO1:/home/dmdba/dmdbms/data/DAMENG/
docker cp /data/dm8demo1/dmdbms/data/dmwatcher.ini DM8DEMO2:/home/dmdba/dmdbms/data/DAMENG/

chown dmdba:dinstall -R dmwatcher.ini
chmod -R 755 dmwatcher.ini
```



**配置完成后，进入容器并以 mount 方式启动数据库实例，在 dmdba 用户下执行以下命令（主备库都要执行）**

```sh
/home/dmdba/dmdbms/bin/dmserver /home/dmdba/dmdbms/data/DAMENG/dm.ini mount
```

**打开一个新的终端，并使用 disql 工具连接到数据库（注意密码中含特殊字符时的转义处理）**

```sh
disql sysdba
dmdba#123
```

**在主备库上都设置守护进程唯一 OGUID 值，执行以下命令**

```sql
sp_set_oguid(453331);
```

**在主库上修改数据库模式为 primary，执行以下命令**

```sql
alter database primary;
```

**在备库上修改数据库模式为 standby，执行以下命令**

```sql
alter database standby;
```

##### 启动守护进程

**在 dmdba 用户下执行以下命令（主备库都要执行）**

```sh
/home/dmdba/dmdbms/bin/dmwatcher /home/dmdba/dmdbms/data/DAMENG/dmwatcher.ini
```

守护进程启动后，会将 mount 的实例 open。

### **配置监视器** 

> 为监视器开启一个新 docker 容器->安装（略）->实例化（略，须进入到容器内操作）

**实例化时请务必将 INSTANCE_NAME 设置成划好的名字DMMONITOR**

也可以修改配置文件

```sh
vi  /home/dmdba/dmdbms/data/DAMENG/dm.ini
```

#### 启动服务

```sh
/home/dmdba/dmdbms/bin/dmserver /home/dmdba/dmdbms/data/DAMENG/dm.ini
```

**出现 system is ready 后输入 exit 停止数据库**

#### 启动监视器

守护进程配置为自动切换时，必须配置确认监视器（需安与主备心跳网络端口开放）。在实例目录下新建 dmmonitor.ini 监视配置文件

- 宿主机路径为/data/dm8monitor/dmdbms/data/DAMENG/dmmonitor.ini ，
- 容器内路径为/home/dmdba/dmdbms/data/DAMENG/dmmonitor.ini

文件内容如下所示

```sh
MON_DW_CONFIRM = 1  #确认监视器模式
MON_LOG_PATH = /home/dmdba/dmdbms/log  #监视器日志文件存放路径
MON_LOG_INTERVAL = 60  #每隔 60s 定时记录系统信息到日志文件
MON_LOG_FILE_SIZE = 200  #单位MB，每个日志文件最大占用空间
MON_LOG_SPACE_LIMIT = 1024  #单位MB，限定日志文件总占用空间
[GRP_RW]
MON_INST_OGUID = 453331  #组 GRP_RW 的唯一 OGUID 值
# 以下配置为监视器到组 GRP_RW 的守护进程的连接信息，以“IP:PORT”的形式配置
# IP 对应 dmmal.ini 中的 MAL_HOST，PORT 对应 dmmal.ini 中的 MAL_DW_PORT
MON_DW_IP = 172.172.1.101:7436
MON_DW_IP = 172.172.1.102:7436
```

**完成配置后，执行以下命令启动监视器**

```sh
/home/dmdba/dmdbms/bin/dmmonitor /home/dmdba/dmdbms/data/DAMENG/dmmonitor.ini
```



### 验证主备集群同步状态

#### 监视器查看读写分离集群状态



> 如上面所说，在启动并进入监视器终端后，可通过以下途径进行各有关操作：输入 show 命令查看集群状态，其中守护进程状态 WSTATUS 为 OPEN，实例状态 ISTATUS 为 OPEN，归档类型 RTYPE 为 REALTIME，归档状态RSTAT 为 VALID；

**输入choose switchover 命令查看可用于主备切换的备选服务器列表**

> 输入switchover [组名.]<实例名>命令使用指定组的指定实例，切换为主机，其中的组名可以省略，实例名必须是真实有效的备选服务器，即位于通过choose switchover 命令查看的备选服务器列表中

#### 使用 disql 客户端验证

打开一个新的终端，并使用 disql 工具连接到主数据库，创建测试表，插入数据，步骤如下

```sql
CREATE TABLE T_DEMO (ID INT);
INSERT INTO T_DEMO VALUES(101);
COMMIT;
SELECT * FROM T_DEMO;
```

打开一个新的终端，并使用 disql 工具连接到备数据库，查询测试表，以验证主数据库数据是否已经同步，步骤如下

```sql
SELECT * FROM T_DEMO;
```



## 空间数据的存储与应用

### 数据库的存储



**用 户 在 使 用 DMGEO 包 之 前 ， 需 要 提 前 调 用 系 统 过 程 SP_INIT_GEO_SYS(1)创建 DMGEO 包，包创建成功后就可以使用空间数 据类型以及包提供的方法**

```sql
SP_INIT_GEO_SYS(1);
```

如果 dmgeo 包已存在，调用 SP_INIT_GEO_SYS(2)，系统将只重建 dmgeo 包方法，而不影响现有的空间数据类型以及数据：

```sql
SP_INIT_GEO_SYS(2);
```

如 果 dmgeo 包 已 存 在 ， 调 用 SP_INIT_GEO_SYS(3) 将只重建 SPATIAL_REF_SYS 表，而不影响现有的空间数据类型以及数据：

```sql'
SP_INIT_GEO_SYS(3);
```

调用系统过程 SP_INIT_GEO_SYS(0)可以删除 DMGEO 包，任何与空 间数据类型相关的表、函数、过程、触发器等对象均会被级联删除：

```sql
SP_INIT_GEO_SYS(0);
```

创建 DMGEO 包后，系统会创建表 GEOMETRY_COLUMNS 和 SPATIAL_REF_SYS，这两张表分别记录空间数据类型列创建情况和空间参 考 坐 标 系 信 息 。 GEOMETRY_COLUMNS 内 容 不 需 要 用 户 干 预 ， SPATIAL_REF_SYS 内容可以通过 DMGEO 包提供的存储过程 ST_ADD_SPATIAL_REF 和 ST_DEL_SPATIAL_REF 增加或删除。 

**建议在不需要保留现有空间数据时, 使用下列语句来重建 GEO 包：**

```sql
SP_INIT_GEO_SYS(0);
SP_INIT_GEO_SYS(1);
```







## 问题

------



### 授权

```sh
chown dmdba:dinstall -R /home/dmdba
chmod -R 755 /home/dmdba
```

### 中文乱码

```sh
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
```

### 退出mount模式

```sql
ALTER DATABASE OPEN;
ALTER DATABASE OPEN FORCE;
```

### 取消容器自启动

```sh
docker stop DMMONITOR
docker stop DM8DEMO2
docker stop DM8DEMO1

docker update --restart=no DM8DEMO1
docker update --restart=no DM8DEMO2
docker update --restart=no DMMONITOR
```

