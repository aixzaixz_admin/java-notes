# Java

## 基础知识

### 	IO

Java 中 IO 流分为几种?

- 按照流的流向分，可以分为输入流和输出流；
- 按照操作单元划分，可以划分为字节流和字符流；
- 按照流的角色划分为节点流和处理流。

Java IO 流共涉及 40 多个类，这些类看上去很杂乱，但实际上很有规则，而且彼此之间存在非常紧密的联系， Java IO 流的 40 多个类都是从如下 4 个抽象类基类中派生出来的。

- InputStream/Reader: 所有的输入流的基类，前者是字节输入流，后者是字符输入流。
- OutputStream/Writer: 所有输出流的基类，前者是字节输出流，后者是字符输出流。

按操作方式分类结构图：

![IO-操作方式分类](https://my-blog-to-use.oss-cn-beijing.aliyuncs.com/2019-6/IO-%E6%93%8D%E4%BD%9C%E6%96%B9%E5%BC%8F%E5%88%86%E7%B1%BB.png)

按操作对象分类结构图：

![IO-操作对象分类](https://my-blog-to-use.oss-cn-beijing.aliyuncs.com/2019-6/IO-%E6%93%8D%E4%BD%9C%E5%AF%B9%E8%B1%A1%E5%88%86%E7%B1%BB.png)

## 常用代码

### **判断集合是否为空/不为空**

**org.apache.commons.collections.CollectionUtils**

**例1: 判断集合是否为空:**

```java
　　CollectionUtils.isEmpty(null): true
　　CollectionUtils.isEmpty(new ArrayList()): true　　
　　CollectionUtils.isEmpty({a,b}): false
```

**例2: 判断集合是否不为空:**

```java
　　CollectionUtils.isNotEmpty(null): false
　　CollectionUtils.isNotEmpty(new ArrayList()): false
　　CollectionUtils.isNotEmpty({a,b}): true
```

### 获取Oracle 返回的 BigDecimal类型

```java
 //设备总数
            BigDecimal warningNum = new BigDecimal(item.get("warningNum") == null ? "0" : item.get("warningNum").toString());
            //正常设备数
            BigDecimal normalNum = new BigDecimal(item.get("normalNum") == null ? "0" : item.get("normalNum").toString());
           
```

### Bigdecimal类型相除并保留两位小数返回百分比

```java
//计算设备正常率如果不能整除保留6位小数并四舍五入
            BigDecimal rate = normalNum.divide(warningNum,6,RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            //计算结果四舍五入保留两位小数拼接%
            String normalRate = rate.setScale(2, RoundingMode.HALF_UP).toPlainString() + "%";
```



### 判断Bigdecimal类型是否等于0的方法

**判断相等**

```java
compareTo
```

Bigdecimal的equals方法不仅仅比较值的大小是否相等，首先比较的是scale（scale是bigdecimal的保留小数点位数，比如 new Bigdecimal("1.001"),scale为3），也就是说，不但值得大小要相等，保留位数也要相等，equals才能返回true。

Bigdecimal b = new Bigdecimal("0") 和 Bigdecimal c = new Bigdecimal("0.0"),用equals比较，返回就是false。

Bigdecimal.ZERO的scale为0。

所以，用equals方法要注意这一点。

```java
b.compareTo(BigDecimal.ZERO)==0
```

可以比较是否等于0，返回true则等于0，返回false，则不等于0

### Lambda forEach 关于 return 的使用

> JDK8 中新增的 Lambda [表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)对于 for 循环的操作变得非常简洁
> 但其中的 [forEach](https://so.csdn.net/so/search?q=forEach&spm=1001.2101.3001.7020) 和 for 之间存在一定差异
> 比如 forEach 无法使用 break 和 continue

```java
```

#### forEach 实现和 contiune 一样的效果

1. 参见以下代码可知，在 **forEach** 中 `return` 可实现和 `contiune` 一样的效果

```java
int[] arrs = new int[]{1, 3, 9, 2};

arrs.forEach(arr -> {
    if (arr > 4) {
        return;
    }

    // 输出 1 3 2
    System.out.println(arr);
})
```



###  list使用[stream](https://so.csdn.net/so/search?q=stream&spm=1001.2101.3001.7020)排序(多字段)

```java
List<类> list; 代表某集合
 
//返回 对象集合以类属性一升序排序
 
list.stream().sorted(Comparator.comparing(类::属性一));
 
//返回 对象集合以类属性一降序排序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed());//先以属性一升序,结果进行属性一降序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()));//以属性一降序
 
//返回 对象集合以类属性一升序 属性二升序
 
list.stream().sorted(Comparator.comparing(类::属性一).thenComparing(类::属性二));
 
//返回 对象集合以类属性一降序 属性二升序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二));//先以属性一升序,升序结果进行属性一降序,再进行属性二升序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()).thenComparing(类::属性二));//先以属性一降序,再进行属性二升序
 
//返回 对象集合以类属性一降序 属性二降序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一升序,升序结果进行属性一降序,再进行属性二降序
 
list.stream().sorted(Comparator.comparing(类::属性一,Comparator.reverseOrder()).thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一降序,再进行属性二降序
 
//返回 对象集合以类属性一升序 属性二降序 注意两种写法
 
list.stream().sorted(Comparator.comparing(类::属性一).reversed().thenComparing(类::属性二).reversed());//先以属性一升序,升序结果进行属性一降序,再进行属性二升序,结果进行属性一降序属性二降序
 
list.stream().sorted(Comparator.comparing(类::属性一).thenComparing(类::属性二,Comparator.reverseOrder()));//先以属性一升序,再进行属性二降序<br><br><br>
```

通过以上例子我们可以发现

> 1. Comparator.comparing(类::属性一).reversed();
>
> 2. Comparator.comparing(类::属性一,Comparator.reverseOrder());
>
>    两种排序是完全不一样的,一定要区分开来 1 是得到排序结果后再排序,2是直接进行排序,很多人会混淆导致理解出错,2更好理解,建议使用2

**实际例子:
现有一个类test 有两个属性:state 状态 time 时间,需要状态顺序且时间倒序**

```java
class test {
    //状态
    private int state;
    //时间
    private Date time;
 
    public test(int state, Date time) {
        this.state = state;
        this.time = time;
    }
 
    public int getState() {
        return state;
    }
 
    public void setState(int state) {
        this.state = state;
    }
 
    public Date getTime() {
        return time;
    }
 
    public void setTime(Date time) {
        this.time = time;
    }
 
    @Override
    public String toString() {
        return "test{" +
                "state=" + state +
                ", time=" + DateUtils.formatDateYMD(time) +
                '}';
    }
}
class testRun {
    public static void main(String[] args) {
        List<test> testList = new ArrayList<>();
        Date d = DateUtils.now();
        for (int i = 1; i <= 3; i++) {
            test t = new test(i, DateUtils.addDays(d, i));
            testList.add(t);
        }
        for (int i = 1; i <= 3; i++) {
            test t = new test(i, DateUtils.addMonths(d, i));
            testList.add(t);
        }
 
        testList.forEach(o -> {
            System.out.println(o.toString());
        });
        List<test> sort = testList.stream().sorted(Comparator.comparing(test::getState).thenComparing(test::getTime,Comparator.reverseOrder())).collect(toList());
        System.out.println("------------------------------------");
        sort.forEach(o -> {
            System.out.println(o.toString());
        });
 
 
    }
}
```

运行结果:

```java
排序前:
test{state=1, time=2019-07-24}
test{state=2, time=2019-07-25}
test{state=3, time=2019-07-26}
test{state=1, time=2019-08-23}
test{state=2, time=2019-09-23}
test{state=3, time=2019-10-23}
------------------------------------
排序后:
test{state=1, time=2019-08-23}
test{state=1, time=2019-07-24}
test{state=2, time=2019-09-23}
test{state=2, time=2019-07-25}
test{state=3, time=2019-10-23}
test{state=3, time=2019-07-26}
 
Process finished with exit code 0
```



### 数组去重

```java
 public  String[] dealContains(String[] arrays) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < arrays.length; i++) {
            String array = arrays[i];
            if (!arrayList.contains(array)) {
                arrayList.add(array);
            }
        }
        String[] b = (String[]) arrayList.toArray(new String[arrayList.size()]);
        return b;
    }
```



### Base64编码和解码

**在Java 中， 使用Base64进行编码和解码的方式常见的有三种：**

- [推荐] 从Java 8 开始， Base64 就成为Java类库的标准，位于java.util 的包下。对应类是Base64。
- Java 7及之前的版本，JDK默认没有支持Base64, 需要导入JRE 目录的lib 下的文件 rt.jar， 通过sun.misc.BASE64Encoder 和sun.misc.BASE64Decoder 进行加解密。（完整的路径%JAVA_HOME%\jre\lib\rt.jar）。 在JDK８中依旧可以使用这个方法。
- Apache Commons Codec， 这是Apache 提供的常见编码器和解码器的实现，包括： Base64、Hex、Phonetic 和 URL。

#### Java 8 内置Base 64 编码器、解码器

Java 8 中通过java.util.Base64 提供的方法进行Base 64的编码和解码。

- 这里以“123” 的编码为例， 编码后的字符串是 “MTIz”。

```java
	@Test
	public void jdk8() {
		// 编码
		String encodedStr = java.util.Base64.getEncoder().encodeToString("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");

		// 解码
		byte[] decodeBytes = java.util.Base64.getDecoder().decode("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");
	}
```

#### Java 7使用rt.jar　中的相关类进行编码和解码

rt.jar　中用于处理的类是 sun.misc.BASE64Encoder 和sun.misc.BASE64Decoder 。通过创建这两个类的实例后，调用encode() 和 decodeBuffer() 方法。 示例代码如下：

```java
	@Test
	public void jdk7() throws IOException {
		String encodedStr =  new sun.misc.BASE64Encoder().encode("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");
		
		byte[] decodeBytes =  new sun.misc.BASE64Decoder().decodeBuffer("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");

	}
```

#### [Apache](https://so.csdn.net/so/search?q=Apache&spm=1001.2101.3001.7020) Commons Codec 实现Base64 的编码和解码

Apache Commons Codec 的官方地址是：
https://commons.apache.org/proper/commons-codec/。
Apache Commons Codec 用来处理Base64的类是 org.apache.commons.codec.binary.Base64。

```java
	@Test
	public void apacheCodec() {
		String encodedStr = org.apache.commons.codec.binary.Base64.encodeBase64String("123".getBytes());
		Assertions.assertEquals(encodedStr, "MTIz");
		
		byte[] decodeBytes =   org.apache.commons.codec.binary.Base64.decodeBase64("MTIz");
		Assertions.assertEquals(new String(decodeBytes), "123");	
	}
```





### 生成文件的md5校验值

```java
import java.io.File; 
import java.io.FileInputStream; 
import java.io.IOException; 
import java.io.InputStream; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 
import lombok.extern.slf4j.Slf4j; 
/** 
 * author yvioo 
 */ 
@Slf4j 
public class MD5Util { 
     /** 
      * 生成文件的md5校验值 
      *  
      * @param path 文件路径 
      * @return 文件md5校验值 
      * @throws IOException 
     * @throws NoSuchAlgorithmException  
      */ 
    public static String getFileMD5String(String path){ 
        InputStream fis = null; 
        MessageDigest messagedigest = null; 
        try { 
            messagedigest = MessageDigest.getInstance("md5"); 
            File file = new File(path); 
            if(!file.exists() || !file.isFile()) { 
                log.error("不存在或不是一个文件"); 
                return ""; 
            } 
             
            fis = new FileInputStream(file); 
            byte[] buffer = new byte[1024]; 
            int numRead = 0; 
            while ((numRead = fis.read(buffer)) > 0) { 
               messagedigest.update(buffer, 0, numRead); 
            } 
        }catch (IOException e) { 
            e.printStackTrace(); 
        }catch (NoSuchAlgorithmException e){ 
            e.printStackTrace(); 
        }finally { 
            try { 
                if(fis != null) { 
                    fis.close(); 
                } 
            } catch (IOException e) { 
                e.printStackTrace(); 
            } 
        } 
        return bufferToHex(messagedigest.digest()); 
    } 
    private static String bufferToHex(byte bytes[]) { 
        return bufferToHex(bytes, 0, bytes.length); 
    } 
    private static String bufferToHex(byte bytes[], int m, int n) { 
        StringBuffer stringbuffer = new StringBuffer(2 * n); 
        int k = m + n; 
        for (int l = m; l < k; l++) { 
         appendHexPair(bytes[l], stringbuffer); 
        } 
        return stringbuffer.toString(); 
    } 
    /** 
     * 默认的密码字符串组合，用来将字节转换成 16 进制表示的字符,apache校验下载的文件的正确性用的就是默认的这个组合 
     */ 
    protected static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }; 
    private static void appendHexPair(byte bt, StringBuffer stringbuffer) { 
     char c0 = hexDigits[(bt & 0xf0) >> 4];// 取字节中高 4 位的数字转换, >>> 为逻辑右移，将符号位一起右移,此处未发现两种符号有何不同  
     char c1 = hexDigits[bt & 0xf];// 取字节中低 4 位的数字转换  
     stringbuffer.append(c0); 
     stringbuffer.append(c1); 
    } 
}
```



### 文件和Base64、二进制（byte[]）互转

#### 导入包

```java
//导入spring工具包，如果使用spring框架（我想大概率会使用）直接使用FileCopyUtils.java
import org.springframework.util.FileCopyUtils;
//导入IO包
import java.io.*;
//导入base64包
import java.util.Base64;
```

#### 方法干货

在IO操作中关于关流这块，尽量使用JDK7的特性，将涉及到关流的对象放到try(...)里面，就可以不用写finally块了。

```java

/**
     * 文件转二进制
     * @param filePath
     * @return
     */
    public static byte[] file2Bytes(String filePath){
        File file = new File(filePath);
        if (!file.exists())return new byte[0];
        try(InputStream inputStream = new FileInputStream(file);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ) {
            byte[] buf = new byte[1024];
            int len;
            if ((len=bufferedInputStream.read(buf))>=0){
                outputStream.write(buf,0,len);
            }
            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }


	/**
	 * 将文件转换成二进制数组
	 * @param filePath 文件路基
	 * @return 二进制数组
	 */
	public static  byte[] fileToByte(String filePath){
		File file = new File(filePath);
		try (InputStream in = new FileInputStream(file)){
			if (!file.exists()){
				return new byte[0];
			}
			return FileCopyUtils.copyToByteArray(file);
		}catch (Exception ignored){
			throw new RuntimeException("文件转换二进制数组出错!",ignored);
		}
	}

 
    /**
     * 将二进制流转换为文件
     * @param bytes
     * @return
     */
    public static String bytes2File(byte[] bytes,String fileName){
        File file = new File(fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try (FileOutputStream outputStream = new FileOutputStream(fileName);){
            outputStream.write(bytes);
            return fileName;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
 
    /**
     * 将二进制流转换为文件
     * @param bytes
     * @param fileName
     */
    public static void bytes2FileSpring(byte[] bytes,String fileName){
        try {
            FileCopyUtils.copy(bytes,new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    /**
     * 将文件转base64
     * @param filePath 文件路径
     */
    public static String fileToBase64(String filePath){
        try( InputStream fileInputStream = new FileInputStream(new File(filePath));
             BufferedInputStream buf = new BufferedInputStream(fileInputStream);
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();){
            int len;
            byte [] bytes = new byte[1024];
            while ((len=buf.read(bytes))!=-1){
                byteArrayOutputStream.write(bytes,0,len);
            }
            return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 将base64字符串转文件
     * @param base64Str base64字符串
     */
    public static void base64ToFile(String base64Str,String fileName){
        //解码
        byte [] bytes = Base64.getDecoder().decode(base64Str);
        try (FileOutputStream outputStream = new FileOutputStream(fileName);){
            outputStream.write(bytes);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
```

#### base64码表

base64码表，我觉得作为一个程序猿，尤其是多年经验的程序猿，建议背会，像背诵当年的化学元素周期表一样，况且只有64个：

| Base64   |              |          |              |          |              |          |              |
| -------- | ------------ | -------- | ------------ | -------- | ------------ | -------- | ------------ |
| **索引** | **对应字符** | **索引** | **对应字符** | **索引** | **对应字符** | **索引** | **对应字符** |
| 0        | **A**        | 17       | **R**        | 34       | **i**        | 51       | **z**        |
| 1        | **B**        | 18       | **S**        | 35       | **j**        | 52       | **0**        |
| 2        | **C**        | 19       | **T**        | 36       | **k**        | 53       | **1**        |
| 3        | **D**        | 20       | **U**        | 37       | **l**        | 54       | **2**        |
| 4        | **E**        | 21       | **V**        | 38       | **m**        | 55       | **3**        |
| 5        | **F**        | 22       | **W**        | 39       | **n**        | 56       | **4**        |
| 6        | **G**        | 23       | **X**        | 40       | **o**        | 57       | **5**        |
| 7        | **H**        | 24       | **Y**        | 41       | **p**        | 58       | **6**        |
| 8        | **I**        | 25       | **Z**        | 42       | **q**        | 59       | **7**        |
| 9        | **J**        | 26       | **a**        | 43       | **r**        | 60       | **8**        |
| 10       | **K**        | 27       | **b**        | 44       | **s**        | 61       | **9**        |
| 11       | **L**        | 28       | **c**        | 45       | **t**        | 62       | **+**        |
| 12       | **M**        | 29       | **d**        | 46       | **u**        | 63       | **/**        |
| 13       | **N**        | 30       | **e**        | 47       | **v**        |          |              |
| 14       | **O**        | 31       | **f**        | 48       | **w**        |          |              |
| 15       | **P**        | 32       | **g**        | 49       | **x**        |          |              |
| 16       | **Q**        | 33       | **h**        | 50       | **y**        |          |              |



## 问题

### List<Map> 集合 使用stream流 Comparator.reversed()颠倒排序时，使用lambda报错



#### 目的：

> 对list中元素的属性做操作之后进行颠倒排序，使用[lambda](https://so.csdn.net/so/search?q=lambda&spm=1001.2101.3001.7020)表达式报错



#### 提示：

> Cannot resolve method 'getContributionProportion' in 'Object' 



List<Employee> list = employeeService.listByIds(idList);

```java
list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed());
不使用lambda表达式时，直接使用方法引用，不会报错，但是没有达到需求
list.sort(Comparator.comparing(Employee::getContributionProportion).reversed());
```

使用lambda[表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)，不进行颠倒排序，也不会报错，但是也没有达到需求

```javascript
list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())));
```

正确的写法：

```coffeescript
list.sort(Comparator.comparing((Employee o) -> new BigDecimal(o.getContributionProportion())).reversed());
```

   

> 如果不使用方法引用，要传递一些其他的参数，使用lambda表达式时，这个时候，可以在lambda中显示提供参数类型。
>
> ​    当然，如果不是这样的写法，分开来处理，也是可以多种方式实现业务需求的，现在只是针对这种方式讲一下是为什么会报错。



**先简单了解一下lambda表达式：**

>  Lambda 表达式中的参数类型都是由编译器推断得出的。 Lambda 表达式中无需指定类型，程序依然可以编译，这是因为 javac 根据程序的上下文，在后台推断出了参数的类型。 Lambda 表达式的类型依赖于上下文环境，是由编译器推断出来的。这就是所谓的 “类型推断”。Lambda表达式会有类型检查和类型推断这两个过程，保证了Lambda使用的正确性。
>
> **类型检查：**主要就是检查Lambda表达式是否能正确匹配函数式接口的方法。
>
> **类型推断：**当Lambda表达式没有显式声明参数类型，JVM编译器会通过目标类型推断入参类型。
>
> Lambda的上下文：使用Lambda表达式来传递的方法的参数，或接受Lambda表达式的值的局部变量    
>
> 目标类型：是指Lambda表达式所在上下文环境的类型。比如，将 Lambda 表达式赋值给一个局部变量，或传递给一个方法作为参数，局部变量或方法参数的类型就是 Lambda 表达式的目标类型。

**第一种分析：**

>  sort()的参数是Comparator<? super E> c，这里在对list进行排序时，此时的E是根据前面list推断出是Employee类型，那参数必须也是这个类型，list.sort()就需要传的参数是Comparator<? super Employee> c，也就是说Comparator.comparing()需要返回的类型为Comparator<Employee>，这意味着Comparator.comparing()需要Function采用一个Employee传入参数。
>
> ​    2、4指定了类型，也就不需要作类型推断了，肯定能编译通过。
>
> ​    3去掉了reversed()为什么可以编译通过？因为Comparator.comparing()没有指定具体的泛型参数，在方法调用前面使用<>指定，或给方法传入具体类型的入参，编译器也可以推断出来，否则会默认从sort()入参类型里推断，这里就默认从sort()的入参类型推断为Employee。
>
> ​    1为什么编译失败？reversed()的返回类型依赖前一个方法的返回类型推断，Comparator.comparing()又依赖它的入参Function<? super T, ? extends U> keyExtractor推断，但是它的入参没有明确的类型，那么它只能直接推断成最顶级的类型，即Object类型。这里为什么没有默认从sort()入参类型里推断呢？注意，这里的比较器是多层的，comparing()和reversed()，最终我们需要的比较器是reversed()返回的，因为编译器的类型推断机制不够强大，无法同时采取两个步骤，推断不出来类型，也就是无法从上下文中推断出类型。个人的理解，lambda表达式要推断类型的时候，如果是一层操作，可以根据上下文推断出来，如果是多层操作则推断不出来，变成多个上下文，而reversed()是后操作的，是离lambda表达式最近的上下文，两层上下文，参数是未知的，就变成object，lambda推断的就是object。

  

**第二种分析：**

> ist.sort(Comparator.comparing(o -> new [BigDecimal](https://so.csdn.net/so/search?q=BigDecimal&spm=1001.2101.3001.7020)(o.getContributionProportion())));
>
> list.sort(Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed());
>
> ​    再来对比一下这两种写法，o -> new BigDecimal(o.getContributionProportion())看成一个整体。
>
> 第一种写法：推断类型的时候，根据目标类型推断，也就是根据Comparator.comparing()的参数类型推断，因为comparing()的参数类型和返回类型是一个类型，这里只有一个comparing()操作，且sort()需要Employee类型，即comparing()需要返回Employee类型，综合，推断o类型是Employee；
>
> 第二种写法：整体看，sort()需要Employee类型，也就是原本需要Comparator.comparing(o -> new BigDecimal(o.getContributionProportion())).reversed()返回一个Employee类型，由于这里有两个操作，最终返回Employee类型，所以编译器推断的时候并不确定comparing()的参数传的就是Employee类型，所以推断成它的父类Object，因此就推断了o类型是Object类型，而sort()不需要Object类型，所以报错。





# Centos

## 一、环境安装



### CentOS 7

#### 简介

##### VMware

   VMware是一款虚拟机软件就是通过软件模拟的具有完整硬件系统功能的、运行在一个完全隔离环境中的完整计算机系统。

  与物理机一样，虚拟机是运行操作系统和应用程序的软件计算机。虚拟机包含一组规范和配置文件，并由主机的物理资源提供支持。每个虚拟机都具有一些虚拟设备，这些设备可提供与物理硬件相同的功能，但是可移植性更强、更安全且更易于管理。

官网：https://www.vmware.com/cn/products/workstation-pro/workstation-pro-evaluation.html

##### CentOS

  CentOS是免费的、开源的、可以重新分发的开源操作系统。全名为“社区企业操作系统（Community Enterprise Operating System）”，提供长期免费升级和更新服务，自由使用。国内最大的服务器操作系统，现在基本所有的互联网公司后台服务器都采用CentOS

官网：[Download![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/icon-default.png)https://www.centos.org/download/](https://www.centos.org/download/)

------

#### 安装步骤



##### 一、安装前的准备

1.VMware的安装本文不再介绍，正常安装即可

2.下载CentOS 7 的镜像文件

##### 二、下载镜像文件

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/qdqwd2813129327193.png)

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/dsadsadiohsadoh32-16337404812941.png)

##### 三、开始安装

1、创建新的虚拟机
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1dd289313893461fa6e462929797ab2c.png)
2、选择自定义
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/112796089c4b460699fa2aa603e41460.png)
3、硬盘兼容性-- **默认**
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/6a427cbdc7954f9990f269f04960e326-16337405882682.png)
4、稍后安装操作系统（需要在虚拟机安装完成之后，删除不需要的硬件，所以稍后安装操作系统)
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/cd046a15d55740f09467df5a02d30aac.png)
5、选择客户端操作系统：

1. 客户机操作系统–Linux
2. 版本–centos 64位

（注意：版本一定要对应镜像文件版本，其中centos是32位，centos 64位则就是64位，windows系统应安装64位版本）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/26bd85e0ad19444bab04358e6a4000b4.png)
6、**命名虚拟机**（简略表示出该虚拟机的类型、版本。例如：centos7 ）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/b63f8d7c4f764c83ad099ab1894db16e.png)
7、处理器配置（CPU）–总处理器核心数一般为 4![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/62c3c4347e6a40fc8eba26db69b61d07.png)

虚拟机总核心数不能超过主机核心数。若超出则会警告提醒。
![90e6a28e55dc402dae3be53775615c7e](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/90e6a28e55dc402dae3be53775615c7e.png)

8、此虚拟机内存 => **一般2G** 

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/829c4c6f5a6e42518b1f22318d701d0f.png)



9、网络类型–桥接网络（可以使虚拟机与主机使用同一网络）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/b61f0d1ee3e743199668b267fda6672b.png)
注释：

- VMnet1网口对应的是仅主机模式
- VMnet8网口对应的是NAT模式
- VMnet0网口对应的是桥接模式

查看以上对应是在VMware workstation中的编辑-虚拟网络编辑器

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/a353f4983ef34044b4059d770519f709.png)

10、选择I/O控制器类型（相对于硬盘）-- **默认**
从硬盘到内存是I（input），从内存在硬盘是O（output）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/0a9fffcd16cf4a99927d71b115cfad23.png)
11、选择磁盘类型-- **默认** （硬盘接口，家庭个人常用SATA类型，服务器常用SCSI类型）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/8ddcce346c544c1b980764ee91447c63.png)
12、选择磁盘–创建新的虚拟磁盘（其他两个不常用）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1b9717fdc8ba4d9592c2c87d1d88a757.png)
13、指定磁盘容量–100G（是假的虚拟的不占主机内存）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/425a1d11afdd421f930f633d8ebc2c19.png)
14、指定磁盘文件（.vmdk）文件
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/87046a2ae51e4864b59464d619764c4b.png)
15、完成
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/647bb97956084a9994b579c09d85bd6c.png)
**删除不需要的硬件** – 编辑虚拟机设置–删-USB控制器、声卡、打印机（可以使虚拟器启动的快一点）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/321d4e6ae2934eca97dc9d2972c4b650.png)
也可以手动添加硬件，比如，一个网口不够，再添加一个。**（网络连接仍然选择桥接模式)**
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/23ede02d93084fe486016fd84a3f7369.png)
此时虚拟机中的硬件已经搭建完成

16、继续添加映像文件，选择设备中的CD/DVD（IDE），在连接处选择–使用ISO映像文件–确定
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/78644e458c4245fcad43aa125a0451ff.png)

17、开启虚拟机，选择第一项 Install CentOS 7，等待一段时间；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/95478d83f8c949b5a0e15d9f6f711f62.png)

18、WELCOME TO CENTOS 7.设置语言–推荐使用English–点击Continue

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/d3683405b4f648b0a115aafb350c4593.png)

19、INSTALLATION SUMMARY 安装总览（这里可以完成centos 7 版本Linux的全部设置）

1. 首先，设置时区–DATE & TIME，找到Asia–Shanghai并点击–Done
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/51364f8948064c359653ae74e95473b1.png)
2. KEYBOARD 键盘就默认是English（US）
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/605a9879f9844a44bd739548576386f1.png)
3. LANGUAGE SUPPORT语言支持
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/701bf432071649958748040cc0ee2a5d.png)
   可以是默认的English 也可以自行添加Chinese简体中文的支持
4. INSTALLATION SOURCE 安装资源，默认选择–Local media 本地媒体文件
5. SOFTWARE SELECTION软件安装选择，字符界面安装–Minimal install 或者 Basic Web Server
   ![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/ef04f5b886e344208ff0dda36c445112.png)
   **图形界面安装–Server with GUI 或者 GNOME Desktop**
   字符界面与图形界面安装过程相同，只在这一步有区分。点击–Done进入下一步

20、INSTALLATION DESTINATION 安装位置—即进行系统分区，选中我们在创建虚拟机时候的100G虚拟硬盘
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/67cec099b8be497c8be19b6891324afe.png)

21、这是我们已完成所有设置，-- Begin Installation
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/a2503f20b9ef451daa05871183eb500a.png)
22、这时需要设置管理员Root Password（务必记住密码！）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/6b1354c37da64a90aa8e6793f7141d3b.png)
接下来可以创建用户（此处可以不进行创建，安装完成后进入root也可以重新创建）
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/8b2f11e9d3144ffb92af0bc73991d9ef.png)
23、等待一段时间，centos 7安装完成 – 点击reboot重启使用
![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/049eefca4b2e4d7291f480bb56e3c3ef.png)
安装完成后如图所示：

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/9ed31b60fe5f41b59bd75162faf82b76.png)

##### **四、Centos7网络设置**



###### 1、以系统管理员打开VMWare

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220640411-1638143989-163270706427928.png)

 

 

###### 2、选择虚拟网络编辑器菜单

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220656139-932999627-163270706147427.png)

 

 

###### 3、选择VMnet8这一行

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828221055935-751649340-163270705868626.png)

 

 

###### 4、还原VMnet8的默认设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828221043544-1092016821-163270704264925.png)

 

 

###### 5、修改VMnet8的参数

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828221019608-1050958148-163270704008324.png)

 

 

**虚拟机子网IP地址段和子网掩码由您自己来定，如果你不熟练，就按上图中的内容来设置也没有问题。**

###### 6、NAT设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220949109-29323353-163270703788623.png)

 

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828221000625-194874741-163270703575622.png)

 

 

 

###### 7、保存设置

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220930748-1453195985-163270703416421.png)

 

 

###### 8、确认虚拟机为NAT模式

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220911716-260754383-163270703231520.png)

 

 

###### 9、启动虚拟机CentOS7

###### 10、设置CentOS7的不静态IP地址

修改虚拟机网卡配置文件，如/etc/sysconfig/network-scripts/ifcfg-ens33，注意，文件名不一定是ifcfg-ens33，根据您的实际情况决定。

1）修改BOOTPROTO参数，把地址协议改为静态IP方式。

```cpp
BOOTPROTO=static  # dhcp-动态分配，static-静态分配（重要）。
```

2）修改ONBOOT参数，把开机启动选项ONBOOT设置为yes。

```cpp
ONBOOT=yes  # 是否开机引导。
```

3）设置DSN服务器的IP，添加以下内容。

```cpp
DNS1=114.114.114.114  # 第1个DSN服务器的IP地址。
DNS2=1.2.4.8  # 第2个DSN服务器的IP地址。
```

4）设置CentOS7的IP地址、子网掩码和网关参数，添加以下内容。

```cpp
IPADDR=192.168.226.128  # IP地址（重要）。
NETMARSK=255.255.255.0  # 子网掩码（重要）。
GATEWAY=192.168.226.2   # 网关（重要）。
```

###### 11、重启CentOS7的网络服务

```cpp
systemctl restart network
```

###### 12、测试效果

ping一下百度。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828220840068-2104581774-163270702933619.png)

ok。

###### 13、注意事项

如果您对网络知识不熟悉，或对虚拟机不熟悉，建议按本文章依葫芦画瓢，照抄参数。

 

二、动态IP上网（所有操作不涉及到IP地址，单纯能连外网）

1、以系统管理员打开VMWare

2、选择虚拟网络编辑器菜单

3、选择VMnet8这一行，勾选三处vi

*![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828221541766-1435435146-163270702588718.png)*

 

在VMnet8中设置子网号192.168.xx.0,子网掩码为255.255.255.0，然后点开DHCP配置，按照默认的即可

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828222608262-1675109985-163270702269117.png)

 

 注意一定要在下面“使用本地DHCP服务器将IP地址分配给虚拟机”打钩，否则就是静态NAT了。

4、编辑网卡

输入cd /etc/sysconfig/network-scripts ,然后用打开其中的ifcfg-ens33(我是这个，有些人是32，自己打开这个文件夹ls一下)

执行：vim ifcfg-ens33

然后把其中ONBOOT改为yes，再Esc  :wq退出即可（如果不用root来修改这个文件，保存的时候会出现问题![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828223624526-727798003-163270701856016.png)



5、虚拟机设置，选择网络适配器为NAT(在虚拟机关闭的时候可以在虚拟机设置中打开)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1346858-20210828222254631-1521578394-163270701584915.png)

 

 

6、重启网卡生效

```sh
sudo systemctl restart network #重启网卡

sudo systemctl enable network #开机启动网卡
```



##### 五、关闭防火墙的命令!

###### 1:查看防火状态

```java
systemctl status firewalld

service  iptables status
```

###### 2:暂时关闭防火墙

```java
systemctl stop firewalld

service  iptables stop
```

###### 3:永久关闭防火墙

```java
systemctl disable firewalld

chkconfig iptables off
```

###### 4:重启防火墙

```java
systemctl enable firewalld

service iptables restart  
```

###### 5:永久关闭后重启

```java
chkconfig iptables on
```

#### 问题

##### 		一、无图形界面

​						（在安装CentOS7时，如果选择 “最小化” 安装那么系统就只有命令行界面，但是没有图形化界面）
​							![image-20210926100317797](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20210926100317797.png)

###### 解决措施：

一、先装X windows，-y表示参数同意所有软件安装操，当出现 Complete！说明这里安装成功了。

```
 yum groupinstall "X Window System" -y
```

二、yum grouplist 检查一下我们已经安装的软件以及可以安装的软件，下面安装的名字要和下面的对应起来，否则会出现No packages in any requested group available to install or update 的错误。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/338023-20210826113925504-564694671.png)

 

 

安装GNOME桌面环境，提示complete!，说明成功。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/338023-20210826113958410-1351466191.png)

```
 yum groupinstall "GNOME Desktop" "Graphical Administration Tools" -y
```

 **安装报错 transaction check error**

file /boot/efi/EFI/centos from install of fwupdate-efi-12-5.el7.centos.x86_64 conflicts with file from package grub2-common-1:2.02-0.65.el7.centos.2.noarch

用  只升级所有包，不升级软件和系统内核。

```
yum upgrade -y
```

再进行安装即可

三、通过命令 startx 进入图形界面，第一次进入会比较慢，耐心等待就可以了。

四、更新系统的默认运行级别

经过上面的操作，系统启动默认还是命令行页面的，需要我们使用**Ctrl+Alt+F2**进行切换。如果想要使系统启动即为图形化窗口，需要执行下面的命令

```
systemctl set-default graphical.target  #设置成图形模式
或者
ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target #默认运行级别5
```

如果要换回默认开机命令模式使用 systemctl set-default multi-user.target #设置成命令模式

五、重启

##### 二、system restart network 报错

```
Job for network.service failed because the control process exited with error code
```

在CentOS系统上，目前有NetworkManager和network两种网络管理工具。如果两种都配置会引起冲突。由于一般我们都是使用 network 配置静态ip，可能是关机（某种缘故）导致NetWorkManager自动配置，发生了冲突，所以把它禁用掉就好了。

> 临时关闭
> systemctl stop NetworkManager
> 永久关闭
> systemctl disable NetworkManager
> 重启
> systemctl restart network

### CentOS 8



#### 一、下载ContOS 8

ContOS官网：https://www.centos.org/download/
![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/64bcdfb82379494dbc5d6e03c8aff163.png)
官网显示只有版本7，但是需要下载CentOS 8，所以在历史版本中下载：http://mirror.nsc.liu.se/centos-store/8.5.2111/isos/x86_64/
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/d1570b6e9d864844bbddad0f1c8a886b.png)

#### 二、创建虚拟机

##### 1.打开VMWare，选择创建新的虚拟机

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/6a2c15ac163c4e4bad5aac4131175dad.png)

##### 2.选择类型配置

选择典型配置即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/bc3dbebb838d423f992bc8f21d0df6c5.png)

##### 3.选择稍后安装操作系统

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/096fac7fafb54d05adb6725dd3d6ec13.png)

##### 4.选择操作系统

选择Linux系统，CentOS 8 64位
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/60bb69ede60c4933902770fb6081cc9b.png)

##### 5.命名虚拟机与选择安装位置

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/e58e1d7c471b41689951b79162d59b90.png)

##### 6.指定磁盘容量

使用建议的容量大小，这里设置将虚拟磁盘存储为单个文件
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2e23109c0bdd40c0b586796bc940ef3b.png)

##### 7.确认创建虚拟机的信息

可以不用自定义硬件，使用默认的即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/49e8f4a5ca524d15b84ed77b915ab383.png)

#### 三、安装设置CentOS 8

##### 1.安装CentOS 8

选择刚刚创建的虚拟机，点击编辑虚拟机设置
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/d3f4a24af8464cb6b244cdf7a94aba53.png)

##### 2.选择iso文件

选择CD/DVD(IDE)→使用ISO映像文件，选择下载的iso文件→确定
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/182a9d7c84244e28806a9db6f061013c.png)

##### 3、设置虚拟机

###### 1.选择并打开虚拟机

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/8a139fd84f324410ba6e3d0f3fd6a8c9.png)

###### 2.安装系统

上键选择Install CentOS Linux 8，如果按上键没有反应，使用鼠标左键进入虚拟机中，将鼠标退出虚拟机Ctrl+Alt
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/5e76a09349ef4d8e95d2c74908ef38a4.png)

###### 3.配置虚拟机

###### 3.1、选择语言

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/c7da315389cd4f46a2a1aa6f9391f03e.png)

###### 3.2、进入配置界面，红色的是必须设置

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2c9c3ca9d3d943c2bfbee402d74e6b4b.png)

###### 3.3、配置网络和主机名

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/449444d43f4e429683097b76214c2e51.png)
将以太网按钮打开，可以看到连接成功的信息，并设置一个主机名
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/6d25e6c1e4394e02b2fc9ae89881b258.png)

###### 3.4、设置时间

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/76c7b6c69c1e4a7ba217cf2117464f4f.png)
地区选择亚洲，城市选择上海，打开网络时间即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/df9dd318933c4868bc131409a786c233.png)

###### 3.5、设置安装目的地

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/74e8b91a5c5e458b9529f70e73782083.png)
点击自定义→完成进入手动分区界面
点击左下角"+“号，选择挂载点：/boot，期望容量400MB
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/78adb10a48484b858fee73bf8c22dec8.png)
再次点击”+“号，选择挂载点：swap，期望容量：2GB
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/e4cb72825dcc43f6afca9f20696ecc0c.png)
再次点击”+"号，选择挂载点：/，剩余容量自动分配
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/0f2e8272654d49c7b98fa7048c136d58.png)
配置后→完成→接受更改
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/4879a5f302a84e8990f6f612a27b86f2.png)

###### 3.6、设置安装源与软件选择

选择自动检测到的安装介质，就是本地介质即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/1ac86d1fca23499eae448f7a27ab7b52.png)
软件选择：选择最小安装即可，我这里选择带GUI的服务器（自己视情况而定）
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/63f8eef20483453180b893814c8787a4.png)

###### 3.7、设置根密码与创建用户

设置根密码
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/9c66368923f64f6fa3ac28a42d592b15.png)
创建用户
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/49a1059f2a6647bdac530813d7cdd3be.png)
设置完成后，点击安装，等待安装完成即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/68990c24d4234a57849be107d4904841.png)

#### 四、登录虚拟机



因为安装的是带GUI的服务器，就像WINDOWS系统一样有图形化的操作界面，登录进去后，也有几个简单的设置，视自己情况而定即可
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/9a7920219899496cb4f7162165efd815.png)
登录之后的图形化界面
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2a849c9283e147098337f6deaf765cb3.png)
至此安装虚拟机全部完成

#### 五、网络配置

##### 1. 物理机下的配置

1. 查看主机IP地址，win+R打开运行，输入[cmd](https://so.csdn.net/so/search?q=cmd&spm=1001.2101.3001.7020)并按下回车，在打开的窗口中输入`ipconfig`，查看所有连接。
   注意这一步不要找错IP地址！！！找的是**以太网适配器 VMware Network Adapter VMnet8:下的IPv4地址！！！**
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624182016620.png)
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624182458116.png)
2. 右击WiFi图标，点击 **打开“网络和Internet”设置**，在打开的窗口中点击**更改适配器选项**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210624182738732.png)

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624182912209.png)

1. 右键点击**VMware Network Adapter VMnet8**，选择**属性**
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624183322541.png)
2. 双击**Internet协议版本4（TCP/IPv4）**

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624183509717.png)

1. 都选择自动获得，点击确定，再点击确定

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624183648352.png)

##### 2. 虚拟机下的配置

1. 单击左侧**我的计算机**，打开界面
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624184000168.png)
2. 右键点击设置
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624184121966.png)
3. 选择左侧网络适配器，右侧NAT模式，点击确定
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624184244876.png)
4. **编辑**中打开**虚拟网络编辑器**

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624184336287.png)

1. 点击右下角更改设置，如果有弹出来一个窗口，点击“是”即可

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021062418455769.png)

1. 看图看图看图，注意在此处的步骤！！！下面细细说。

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624184913627.png)

① 选择**VMnet8 NAT模式**

②两个方框**都勾上**

③子网IP和子网掩码对应在物理机中获得的IPv4地址，你要根据你的物理机中IPv4地址和子网掩码得到相应的子网，这样我们就可以保证物理机和虚拟机在同一个网段上了

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624190000249.png)

④ 点击DHCP设置，在其中设置起始IP地址和结束IP地址，即是第③步得到的网段中的全部可用IP地址，从1~254。之后点击确定

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624190309262.png)

⑤点击NAT设置，设置网关，即你的**IP子网地址.2**，这里我的子网是192.168.208.0，即网关为192.168.208.2，点击确定。

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624190552550.png)

**这五个小步骤全部完成后点击“应用”，再点击“确定”**

1. 查看虚拟机的IP地址，右键点击**Open in Terminal**，打开终端，输入ifconfig -a
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624191056719.png)

可以看到，我的虚拟机的IP地址为192.168.208.3

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624191516520.png)

到此我们整个网络配置即完成了，下面让我们来测试一下

##### 物理机、虚拟机、[外网](https://so.csdn.net/so/search?q=外网&spm=1001.2101.3001.7020)相互ping测试

根据上面物理机下的配置中的第1步可以得到我的**物理机的IP地址为192.168.208.1**
根据上面虚拟机下的配置中的第7步可以得到我的**虚拟机的IP地址为192.168.208.3**

大家也可以根据我的步骤得到自己电脑的物理机和虚拟机的IP地址

###### 1. 物理机ping虚拟机

win+R输入cmd并按下回车，输入`ping 192.168.208.3`，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624192211241.png)

###### 2. 物理机ping 百度

还在刚才那个窗口输入`ping www.baidu.com`,可以看到已ping通
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624192338745.png)

###### 3. 虚拟机ping物理机

在虚拟机的终端（上面已讲怎么打开终端）中输入**ping 192.168.208.1**，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/202106241937304.png)

###### 4. 虚拟机ping百度

在终端输入**ping www.baidu.com**，可以看到已ping通

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210624194340197.png)



#### 六、防火墙命令

> 一、防火墙服务
> 1、启动、关闭、重启防火墙服务。
>    systemctl start  firewalld.service
>    systemctl stop  firewalld.service
>    systemctl restart  firewalld.service
> 2、显示防火墙的状态。
>    systemctl status firewalld.service
> 3、开机启动防火墙。
>    systemctl enable firewalld.service
> 4、开机时禁用防火墙。
>    systemctl disable firewalld.service
> 5、查看防火墙是否开机启动。
>    systemctl  is-enabled  firewalld.service
> 6、查看防火墙是否开机启动。
>    systemctl  is-enabled  firewalld.service
> 7、查看已启动的服务列表。
>    systemctl list-unit-files|grep enabled
> 8、查看启动失败的服务列表。
>    systemctl  --failed
> 9、启动、停止、重启httpd服务。
>    systemctl  start   httpd
>    systemctl  stop   httpd
>    systemctl  restart  httpd
>
> 二、防火墙配置
> 1、查看版本。
>    firewall-cmd --version
> 2、查看帮助。
>    firewall-cmd  --help
> 3、显示防火墙状态。
>    firewall-cmd --state
> 4、查看所有打开的端口。
>    firewall-cmd --zone=public --list-ports
> 5、查看区域信息
>    firewall-cmd --get-active-zones
> 6、查看指定接口所属区域。
>    firewall-cmd --get-zone-of-interface=eth0
> 7、拒绝所有包、取消拒绝状态、查看是否拒绝
>    firewall-cmd --panic-on
>    firewall-cmd --panic-off
>    firewall-cmd --query-panic
> 8、开启3306端口，–permanent永久生效，没有此参数重启后失效。
>    firewall-cmd --zone=public --add-port=3306/tcp --permanent  
> 9、重新载入，更新防火墙规则。
>    firewall-cmd --reload
> 10、查看3306端口是否开放。
>    firewall-cmd --zone=public --query-port=3306/tcp 
> 11、删除3306端口配置。
>    firewall-cmd --zone=public --remove-port=3306/tcp --permanent 

#### 问题

##### 配置页面显示不全

> 在安装的时候选在第一项按 Tab 键，输入 vga=791，注意前面先有一个空格

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/1161611-20180707114511856-801554255.png)

在附上一张分辨率的参数表

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/1161611-20180707114653656-572369501.png)



##### 设置基础软件仓库时出错

> https://mirrors.aliyun.com/centos/8-stream/BaseOS/x86_64/os/

## 二、软件安装



### 使用以下命令来设置稳定的仓库。



#### 使用官方源地址（比较慢）

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**download.docker.com**/**linux**/**centos**/**docker-ce.repo
```

可以选择国内的一些源地址：

#### 阿里云

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**download.docker.com**/**linux**/**centos**/**docker-ce.repo
```

#### 清华大学源	

```sh
$ **sudo** yum-config-manager \
  --add-repo \
  https:**//**mirrors.tuna.tsinghua.edu.cn**/**docke
```

### node.js

#### 		1)安装步骤：

##### 		centos 7 yum

```
yum install -y epel-release  
/usr/bin/yum install -y nodejs
```

##### 		centos 6安装

- 由于yum源版本过低，cnpm安装失败，使用nvm管理node包的时候

```
yum remove nodejs -y  
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.29.0/install.sh | bash  
```

- 安装成功后:一定要重新启动shell

```
command -v nvm  
```

- 查看nvm可安装版本

```
nvm ls-remote  
```

- 安装nodejs

```
nvm install v6.12.3  
```

##### 		国内安装源

- cnpm

```
/usr/bin/npm install -g cnpm --registry=https://registry.npm.taobao.org
```

- 配置文件 ~/.npmrc 文件中写入源地址

```
registry =https://registry.npm.taobao.org  
```

#### 		2）问题：

##### 				nvm command not found

​						nvm是node的包版本管理工具，github地址如下：[nvm](https://github.com/creationix/nvm)

​       **安装命令**

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
1
nvm //检查nvm是否安装成功
-bash: nvm: command not found  //boom，失败了
12
```

​		解决nvm command not found问题

​		进入.nvm文件夹，新建.bash_profile：

```bash
touch .bash_profile //新建文件
open .bash_profile //打开文件
12
```

​		在里面copy如下内容：

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
12
```

​		关闭文件，然后执行这个文件：

```bash
source .bash_profile
1
```

​		执行完毕，我们再看看是否安装成功：

```bash
nvm --version
1
```

​		输出：

```bash
0.33.8
1
```

​		安装成功。

​		卸载：

```bash
$ nvm use system
$ npm uninstall -g a_module
12
```



# Linux



![linux](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/linux.jpg)

Linux 是一种自由和开放源码的类 UNIX 操作系统。

Linux 英文解释为 **Linux is not Unix**。

Linux 是在 1991 由林纳斯·托瓦兹在赫尔辛基大学上学时创立的，主要受到 Minix 和 Unix 思想的启发。

本教程，我们将为大家介绍如何使用 Linux。

Linux 其实很容易学，相信你们能很快学会。



## 一、常用命令



### 1、yum 



#### yum简介

Docker 笔记yum（ Yellow dog Updater, Modified）是一个在 Fedora 和 RedHat 以及 SUSE 中的 Shell 前端软件包管理器。

基于 RPM 包管理，能够从指定的服务器自动下载 RPM 包并且安装，可以自动处理依赖性关系，并且一次安装所有依赖的软件包，无须繁琐地一次次下载、安装。

yum 提供了查找、安装、删除某一个、一组甚至全部软件包的命令，而且命令简洁而又好记。

#### yum 语法

```
yum [options] [command] [package ...]
```

- **options：**可选，选项包括-h（帮助），-y（当安装过程提示选择全部为 "yes"），-q（不显示安装的过程）等等。
- **command：**要进行的操作。
- **package：**安装的包名。

------

#### yum常用命令

列出所有可更新的软件清单命令：

```shell
yum check-update
```

 更新所有软件命令：

```shell
yum update
```

 仅安装指定的软件命令：

```shell
yum install <package_name>
```

仅更新指定的软件命令：

```shell
yum update <package_name>
```

列出所有可安裝的软件清单命令：

```shell
yum list
```

 删除软件包命令：

```shell
yum remove <package_name>
```

 查找软件包命令：

```shel
yum search <keyword>
```

清除缓存命令:

```shell
yum clean packages: 清除缓存目录下的软件包

yum clean headers: 清除缓存目录下的 headers

yum clean oldheaders: 清除缓存目录下旧的 headers

yum clean, yum clean all (= yum clean packages; yum clean oldheaders):清除缓存目录下的软件包及旧的 headers
```

### 2、文件与目录管理

我们知道Linux的目录结构为树状结构，最顶级的目录为根目录 /。

其他目录通过挂载可以将它们添加到树中，通过解除挂载可以移除它们。

在开始本教程前我们需要先知道什么是绝对路径与相对路径。

- **绝对路径：**
  路径的写法，由根目录 **/** 写起，例如： /usr/share/doc 这个目录。

- 

  **相对路径：**
  路径的写法，不是由 **/** 写起，例如由 /usr/share/doc 要到 /usr/share/man 底下时，可以写成： **cd ../man** 这就是相对路径的写法。

------

#### 处理目录的常用命令

接下来我们就来看几个常见的处理目录的命令吧：

- ls（英文全拼：list files）: 列出目录及文件名
- cd（英文全拼：change directory）：切换目录
- pwd（英文全拼：print work directory）：显示目前的目录
- mkdir（英文全拼：make directory）：创建一个新的目录
- rmdir（英文全拼：remove directory）：删除一个空的目录
- cp（英文全拼：copy file）: 复制文件或目录
- rm（英文全拼：remove）: 删除文件或目录
- mv（英文全拼：move file）: 移动文件与目录，或修改文件与目录的名称

你可以使用 *man [命令]* 来查看各个命令的使用文档，如 ：man cp。

##### ls (列出目录)

在Linux系统当中， ls 命令可能是最常被运行的。

语法：

```
[root@www ~]# ls [-aAdfFhilnrRSt] 目录名称
[root@www ~]# ls [--color={never,auto,always}] 目录名称
[root@www ~]# ls [--full-time] 目录名称
```

选项与参数：

- -a ：全部的文件，连同隐藏文件( 开头为 . 的文件) 一起列出来(常用)
- -d ：仅列出目录本身，而不是列出目录内的文件数据(常用)
- -l ：长数据串列出，包含文件的属性与权限等等数据；(常用)

将家目录下的所有文件列出来(含属性与隐藏档)

```
[root@www ~]# ls -al ~
```

##### cd (切换目录)

cd是Change Directory的缩写，这是用来变换工作目录的命令。

语法：

```
 cd [相对路径或绝对路径]
#使用 mkdir 命令创建 runoob 目录
[root@www ~]# mkdir runoob

#使用绝对路径切换到 runoob 目录
[root@www ~]# cd /root/runoob/

#使用相对路径切换到 runoob 目录
[root@www ~]# cd ./runoob/

# 表示回到自己的家目录，亦即是 /root 这个目录
[root@www runoob]# cd ~

# 表示去到目前的上一级目录，亦即是 /root 的上一级目录的意思；
[root@www ~]# cd ..
```

接下来大家多操作几次应该就可以很好的理解 cd 命令的。

##### pwd (显示目前所在的目录)

pwd 是 **Print Working Directory** 的缩写，也就是显示目前所在目录的命令。

```
[root@www ~]# pwd [-P]
```

选项与参数：

- **-P** ：显示出确实的路径，而非使用连结 (link) 路径。

实例：单纯显示出目前的工作目录：

```
[root@www ~]# pwd
/root   <== 显示出目录啦～
```

实例显示出实际的工作目录，而非连结档本身的目录名而已。

```
[root@www ~]# cd /var/mail   <==注意，/var/mail是一个连结档
[root@www mail]# pwd
/var/mail         <==列出目前的工作目录
[root@www mail]# pwd -P
/var/spool/mail   <==怎么回事？有没有加 -P 差很多～
[root@www mail]# ls -ld /var/mail
lrwxrwxrwx 1 root root 10 Sep  4 17:54 /var/mail -> spool/mail
# 看到这里应该知道为啥了吧？因为 /var/mail 是连结档，连结到 /var/spool/mail 
# 所以，加上 pwd -P 的选项后，会不以连结档的数据显示，而是显示正确的完整路径啊！
```

##### mkdir (创建新目录)

如果想要创建新的目录的话，那么就使用mkdir (make directory)吧。

语法：

```
mkdir [-mp] 目录名称
```

选项与参数：

- -m ：配置文件的权限喔！直接配置，不需要看默认权限 (umask) 的脸色～
- -p ：帮助你直接将所需要的目录(包含上一级目录)递归创建起来！

实例：请到/tmp底下尝试创建数个新目录看看：

```
[root@www ~]# cd /tmp
[root@www tmp]# mkdir test    <==创建一名为 test 的新目录
[root@www tmp]# mkdir test1/test2/test3/test4
mkdir: cannot create directory `test1/test2/test3/test4': 
No such file or directory       <== 没办法直接创建此目录啊！
[root@www tmp]# mkdir -p test1/test2/test3/test4
```

加了这个 -p 的选项，可以自行帮你创建多层目录！

实例：创建权限为 **rwx--x--x** 的目录。

```
[root@www tmp]# mkdir -m 711 test2
[root@www tmp]# ls -l
drwxr-xr-x  3 root  root 4096 Jul 18 12:50 test
drwxr-xr-x  3 root  root 4096 Jul 18 12:53 test1
drwx--x--x  2 root  root 4096 Jul 18 12:54 test2
```

上面的权限部分，如果没有加上 -m 来强制配置属性，系统会使用默认属性。

如果我们使用 -m ，如上例我们给予 -m 711 来给予新的目录 drwx--x--x 的权限。

##### rmdir (删除空的目录)

语法：

```
 rmdir [-p] 目录名称
```

选项与参数：

- **-p ：**从该目录起，一次删除多级空目录

删除 runoob 目录

```
[root@www tmp]# rmdir runoob/
```

将 mkdir 实例中创建的目录(/tmp 底下)删除掉！

```
[root@www tmp]# ls -l   <==看看有多少目录存在？
drwxr-xr-x  3 root  root 4096 Jul 18 12:50 test
drwxr-xr-x  3 root  root 4096 Jul 18 12:53 test1
drwx--x--x  2 root  root 4096 Jul 18 12:54 test2
[root@www tmp]# rmdir test   <==可直接删除掉，没问题
[root@www tmp]# rmdir test1  <==因为尚有内容，所以无法删除！
rmdir: `test1': Directory not empty
[root@www tmp]# rmdir -p test1/test2/test3/test4
[root@www tmp]# ls -l        <==您看看，底下的输出中test与test1不见了！
drwx--x--x  2 root  root 4096 Jul 18 12:54 test2
```

利用 -p 这个选项，立刻就可以将 test1/test2/test3/test4 一次删除。

不过要注意的是，这个 rmdir 仅能删除空的目录，你可以使用 rm 命令来删除非空目录。

##### cp (复制文件或目录)

cp 即拷贝文件和目录。

语法:

```
[root@www ~]# cp [-adfilprsu] 来源档(source) 目标档(destination)
[root@www ~]# cp [options] source1 source2 source3 .... directory
```

选项与参数：

- **-a：**相当於 -pdr 的意思，至於 pdr 请参考下列说明；(常用)
- **-d：**若来源档为连结档的属性(link file)，则复制连结档属性而非文件本身；
- **-f：**为强制(force)的意思，若目标文件已经存在且无法开启，则移除后再尝试一次；
- **-i：**若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
- **-l：**进行硬式连结(hard link)的连结档创建，而非复制文件本身；
- **-p：**连同文件的属性一起复制过去，而非使用默认属性(备份常用)；
- **-r：**递归持续复制，用於目录的复制行为；(常用)
- **-s：**复制成为符号连结档 (symbolic link)，亦即『捷径』文件；
- **-u：**若 destination 比 source 旧才升级 destination ！

用 root 身份，将 root 目录下的 .bashrc 复制到 /tmp 下，并命名为 bashrc

```
[root@www ~]# cp ~/.bashrc /tmp/bashrc
[root@www ~]# cp -i ~/.bashrc /tmp/bashrc
cp: overwrite `/tmp/bashrc'? n  <==n不覆盖，y为覆盖
```

##### rm (移除文件或目录)

语法：

```
 rm [-fir] 文件或目录
```

选项与参数：

- -f ：就是 force 的意思，忽略不存在的文件，不会出现警告信息；
- -i ：互动模式，在删除前会询问使用者是否动作
- -r ：递归删除啊！最常用在目录的删除了！这是非常危险的选项！！！
- 

将刚刚在 cp 的实例中创建的 bashrc 删除掉！

```
[root@www tmp]# rm -i bashrc
rm: remove regular file `bashrc'? y
rm -rf bashrc/ 删除目录
```

如果加上 -i 的选项就会主动询问喔，避免你删除到错误的档名！

##### mv (移动文件与目录，或修改名称)

语法：

```
[root@www ~]# mv [-fiu] source destination
[root@www ~]# mv [options] source1 source2 source3 .... directory
```

选项与参数：

- -f ：force 强制的意思，如果目标文件已经存在，不会询问而直接覆盖；
- -i ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！
- -u ：若目标文件已经存在，且 source 比较新，才会升级 (update)

复制一文件，创建一目录，将文件移动到目录中

```
[root@www ~]# cd /tmp
[root@www tmp]# cp ~/.bashrc bashrc
[root@www tmp]# mkdir mvtest
[root@www tmp]# mv bashrc mvtest
```

将某个文件移动到某个目录去，就是这样做！

将刚刚的目录名称更名为 mvtest2

```
[root@www tmp]# mv mvtest mvtest2
```

------

#### Linux 文件内容查看

Linux系统中使用以下命令来查看文件的内容：

- cat 由第一行开始显示文件内容
- tac 从最后一行开始显示，可以看出 tac 是 cat 的倒着写！
- nl  显示的时候，顺道输出行号！
- more 一页一页的显示文件内容
- less 与 more 类似，但是比 more 更好的是，他可以往前翻页！
- head 只看头几行
- tail 只看尾巴几行

你可以使用 *man [命令]*来查看各个命令的使用文档，如 ：man cp。

##### cat

由第一行开始显示文件内容

语法：

```
cat [-AbEnTv]
```

选项与参数：

- -A ：相当於 -vET 的整合选项，可列出一些特殊字符而不是空白而已；
- -b ：列出行号，仅针对非空白行做行号显示，空白行不标行号！
- -E ：将结尾的断行字节 $ 显示出来；
- -n ：列印出行号，连同空白行也会有行号，与 -b 的选项不同；
- -T ：将 [tab] 按键以 ^I 显示出来；
- -v ：列出一些看不出来的特殊字符

检看 /etc/issue 这个文件的内容：

```
[root@www ~]# cat /etc/issue
CentOS release 6.4 (Final)
Kernel \r on an \m
```

##### tac

tac与cat命令刚好相反，文件内容从最后一行开始显示，可以看出 tac 是 cat 的倒着写！如：

```
[root@www ~]# tac /etc/issue

Kernel \r on an \m
CentOS release 6.4 (Final)
```

##### nl

显示行号

语法：

```
nl [-bnw] 文件
```

选项与参数：

- -b ：指定行号指定的方式，主要有两种：
  -b a ：表示不论是否为空行，也同样列出行号(类似 cat -n)；
  -b t ：如果有空行，空的那一行不要列出行号(默认值)；
- -n ：列出行号表示的方法，主要有三种：
  -n ln ：行号在荧幕的最左方显示；
  -n rn ：行号在自己栏位的最右方显示，且不加 0 ；
  -n rz ：行号在自己栏位的最右方显示，且加 0 ；
- -w ：行号栏位的占用的位数。

实例一：用 nl 列出 /etc/issue 的内容

```
[root@www ~]# nl /etc/issue
     1  CentOS release 6.4 (Final)
     2  Kernel \r on an \m
```

##### more

一页一页翻动

```
[root@www ~]# more /etc/man_db.config 
#
# Generated automatically from man.conf.in by the
# configure script.
#
# man.conf from man-1.6d
....(中间省略)....
--More--(28%)  <== 重点在这一行喔！你的光标也会在这里等待你的命令
```

在 more 这个程序的运行过程中，你有几个按键可以按的：

- 空白键 (space)：代表向下翻一页；
- Enter     ：代表向下翻『一行』；
- /字串     ：代表在这个显示的内容当中，向下搜寻『字串』这个关键字；
- :f      ：立刻显示出档名以及目前显示的行数；
- q       ：代表立刻离开 more ，不再显示该文件内容。
- b 或 [ctrl]-b ：代表往回翻页，不过这动作只对文件有用，对管线无用。

##### less

一页一页翻动，以下实例输出/etc/man.config文件的内容：

```
[root@www ~]# less /etc/man.config
#
# Generated automatically from man.conf.in by the
# configure script.
#
# man.conf from man-1.6d
....(中间省略)....
:   <== 这里可以等待你输入命令！
```

less运行时可以输入的命令有：

- 空白键  ：向下翻动一页；
- [pagedown]：向下翻动一页；
- [pageup] ：向上翻动一页；
- /字串   ：向下搜寻『字串』的功能；
- ?字串   ：向上搜寻『字串』的功能；
- n     ：重复前一个搜寻 (与 / 或 ? 有关！)
- N     ：反向的重复前一个搜寻 (与 / 或 ? 有关！)
- q     ：离开 less 这个程序；

##### head

取出文件前面几行

语法：

```
head [-n number] 文件 
```

选项与参数：

- -n ：后面接数字，代表显示几行的意思

```
[root@www ~]# head /etc/man.config
```

默认的情况中，显示前面 10 行！若要显示前 20 行，就得要这样：

```
[root@www ~]# head -n 20 /etc/man.config
```

##### tail

取出文件后面几行

语法：

```
tail [-n number] 文件 
```

选项与参数：

- -n ：后面接数字，代表显示几行的意思
- -f ：表示持续侦测后面所接的档名，要等到按下[ctrl]-c才会结束tail的侦测

```
[root@www ~]# tail /etc/man.config
# 默认的情况中，显示最后的十行！若要显示最后的 20 行，就得要这样：
[root@www ~]# tail -n 20 /etc/man.config
```





### 3、ps



Linux ps （英文全拼：process status）命令用于显示当前进程的状态，类似于 windows 的任务管理器。

#### 语法

```
ps [options] [--help]
```

**参数**：

- ps 的参数非常多, 在此仅列出几个常用的参数并大略介绍含义

- -A 列出所有的进程

- -w 显示加宽可以显示较多的资讯

- -au 显示较详细的资讯

- -aux 显示所有包含其他使用者的行程

- au(x) 输出格式 :

  ```
  USER PID %CPU %MEM VSZ RSS TTY STAT START TIME COMMAND
  ```

  - USER: 行程拥有者
  - PID: pid
  - %CPU: 占用的 CPU 使用率
  - %MEM: 占用的记忆体使用率
  - VSZ: 占用的虚拟记忆体大小
  - RSS: 占用的记忆体大小
  - TTY: 终端的次要装置号码 (minor device number of tty)
  - STAT: 该行程的状态:
    - D: 无法中断的休眠状态 (通常 IO 的进程)
    - R: 正在执行中
    - S: 静止状态
    - T: 暂停执行
    - Z: 不存在但暂时无法消除
    - W: 没有足够的记忆体分页可分配
    - <: 高优先序的行程
    - N: 低优先序的行程
    - L: 有记忆体分页分配并锁在记忆体内 (实时系统或捱A I/O)
  - START: 行程开始时间
  - TIME: 执行的时间
  - COMMAND:所执行的指令

#### 实例

查找指定进程格式：

```
ps -ef | grep 进程关键字
```





### 4、 文件基本属性



Linux 系统是一种典型的多用户系统，不同的用户处于不同的地位，拥有不同的权限。

为了保护系统的安全性，Linux 系统对不同的用户访问同一文件（包括目录文件）的权限做了不同的规定。

在 Linux 中我们通常使用以下两个命令来修改文件或目录的所属用户与权限：

- chown (change ownerp) ： 修改所属用户与组。
- chmod (change mode) ： 修改用户的权限。

下图中通过 chown 来授权用户，通过 chmod 为用户设置可以开门的权限。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1_151733904241.png)

在 Linux 中我们可以使用 **ll** 或者 **ls –l** 命令来显示一个文件的属性以及文件所属的用户和组，如：

```
[root@www /]# ls -l
total 64
dr-xr-xr-x   2 root root 4096 Dec 14  2012 bin
dr-xr-xr-x   4 root root 4096 Apr 19  2012 boot
……
```

实例中，**bin** 文件的第一个属性用 **d** 表示。**d** 在 Linux 中代表该文件是一个目录文件。

在 Linux 中第一个字符代表这个文件是目录、文件或链接文件等等。

- 当为 **d** 则是目录
- 当为 **-** 则是文件；
- 若是 **l** 则表示为链接文档(link file)；
- 若是 **b** 则表示为装置文件里面的可供储存的接口设备(可随机存取装置)；
- 若是 **c** 则表示为装置文件里面的串行端口设备，例如键盘、鼠标(一次性读取装置)。

接下来的字符中，以三个为一组，且均为 **rwx** 的三个参数的组合。其中， **r** 代表可读(read)、 **w** 代表可写(write)、 **x** 代表可执行(execute)。 要注意的是，这三个权限的位置不会改变，如果没有权限，就会出现减号 **-** 而已。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/file-llls22.jpg)

每个文件的属性由左边第一部分的 10 个字符来确定（如下图）。

![363003_1227493859FdXT](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/363003_1227493859FdXT.png)

从左至右用 **0-9** 这些数字来表示。

第 **0** 位确定文件类型，第 **1-3** 位确定属主（该文件的所有者）拥有该文件的权限。

第4-6位确定属组（所有者的同组用户）拥有该文件的权限，第7-9位确定其他用户拥有该文件的权限。



其中，第 **1、4、7** 位表示读权限，如果用 **r** 字符表示，则有读权限，如果用 **-** 字符表示，则没有读权限；

第 **2、5、8** 位表示写权限，如果用 **w** 字符表示，则有写权限，如果用 **-** 字符表示没有写权限；第 **3、6、9** 位表示可执行权限，如果用 **x** 字符表示，则有执行权限，如果用 **-** 字符表示，则没有执行权限。



#### 文件属主和属组



```
[root@www /]# ls -l
total 64
drwxr-xr-x 2 root  root  4096 Feb 15 14:46 cron
drwxr-xr-x 3 mysql mysql 4096 Apr 21  2014 mysql
……
```

对于文件来说，它都有一个特定的所有者，也就是对该文件具有所有权的用户。

同时，在Linux系统中，用户是按组分类的，一个用户属于一个或多个组。

文件所有者以外的用户又可以分为文件所有者的同组用户和其他用户。

因此，Linux系统按文件所有者、文件所有者同组用户和其他用户来规定了不同的文件访问权限。

在以上实例中，mysql 文件是一个目录文件，属主和属组都为 mysql，属主有可读、可写、可执行的权限；与属主同组的其他用户有可读和可执行的权限；其他用户也有可读和可执行的权限。

对于 root 用户来说，一般情况下，文件的权限对其不起作用。

------

#### 更改文件属性

##### 1、chgrp：更改文件属组

语法：

```
chgrp [-R] 属组名 文件名
```

参数选项

- -R：递归更改文件属组，就是在更改某个目录文件的属组时，如果加上-R的参数，那么该目录下的所有文件的属组都会更改。

##### 2、chown：更改文件属主，也可以同时更改文件属组

语法：

```
chown [–R] 属主名 文件名
chown [-R] 属主名：属组名 文件名
```

进入 /root 目录（~）将install.log的拥有者改为bin这个账号：

```
[root@www ~] cd ~
[root@www ~]# chown bin install.log
[root@www ~]# ls -l
-rw-r--r--  1 bin  users 68495 Jun 25 08:53 install.log
```

将install.log的拥有者与群组改回为root：

```
[root@www ~]# chown root:root install.log
[root@www ~]# ls -l
-rw-r--r--  1 root root 68495 Jun 25 08:53 install.log
```

##### 3、chmod：更改文件9个属性

Linux文件属性有两种设置方法，一种是数字，一种是符号。

Linux 文件的基本权限就有九个，分别是 **owner/group/others(拥有者/组/其他)** 三种身份各有自己的 **read/write/execute** 权限。

先复习一下刚刚上面提到的数据：文件的权限字符为： **-rwxrwxrwx** ， 这九个权限是三个三个一组的！其中，我们可以使用数字来代表各个权限，各权限的分数对照表如下：

- r:4
- w:2
- x:1

每种身份(owner/group/others)各自的三个权限(r/w/x)分数是需要累加的，例如当权限为： **-rwxrwx---** 分数则是：

- owner = rwx = 4+2+1 = 7
- group = rwx = 4+2+1 = 7
- others= --- = 0+0+0 = 0

所以等一下我们设定权限的变更时，该文件的权限数字就是 **770**。变更权限的指令 chmod 的语法是这样的：

```
 chmod [-R] xyz 文件或目录
```

选项与参数：

- xyz : 就是刚刚提到的数字类型的权限属性，为 rwx 属性数值的相加。
- -R : 进行递归(recursive)的持续变更，亦即连同次目录下的所有文件都会变更

举例来说，如果要将 .bashrc 这个文件所有的权限都设定启用，那么命令如下：

```
[root@www ~]# ls -al .bashrc
-rw-r--r--  1 root root 395 Jul  4 11:45 .bashrc
[root@www ~]# chmod 777 .bashrc
[root@www ~]# ls -al .bashrc
-rwxrwxrwx  1 root root 395 Jul  4 11:45 .bashrc
```

那如果要将权限变成 *-rwxr-xr--* 呢？那么权限的分数就成为 [4+2+1][4+0+1][4+0+0]=754。

##### 符号类型改变文件权限

还有一个改变权限的方法，从之前的介绍中我们可以发现，基本上就九个权限分别是：

- user：用户
- group：组
- others：其他

那么我们就可以使用 **u, g, o** 来代表三种身份的权限。

此外， **a** 则代表 **all**，即全部的身份。读写的权限可以写成 **r, w, x**，也就是可以使用下表的方式来看：



| chmod | u g o a | +(加入) -(除去) =(设定) | r w x | 文件或目录 |
| ----- | ------- | ----------------------- | ----- | ---------- |
|       |         |                         |       |            |

如果我们需要将文件权限设置为 **-rwxr-xr--** ，可以使用 **chmod u=rwx,g=rx,o=r 文件名** 来设定:

```
#  touch test1    // 创建 test1 文件
# ls -al test1    // 查看 test1 默认权限
-rw-r--r-- 1 root root 0 Nov 15 10:32 test1
# chmod u=rwx,g=rx,o=r  test1    // 修改 test1 权限
# ls -al test1
-rwxr-xr-- 1 root root 0 Nov 15 10:32 test1
```

而如果是要将权限去掉而不改变其他已存在的权限呢？例如要拿掉全部人的可执行权限，则：

```
#  chmod  a-x test1
# ls -al test1
-rw-r--r-- 1 root root 0 Nov 15 10:32 test1
```



### 5、用户和用户组管理

Linux系统是一个多用户多任务的分时操作系统，任何一个要使用系统资源的用户，都必须首先向系统管理员申请一个账号，然后以这个账号的身份进入系统。

用户的账号一方面可以帮助系统管理员对使用系统的用户进行跟踪，并控制他们对系统资源的访问；另一方面也可以帮助用户组织文件，并为用户提供安全性保护。

每个用户账号都拥有一个唯一的用户名和各自的口令。

用户在登录时键入正确的用户名和口令后，就能够进入系统和自己的主目录。

实现用户账号的管理，要完成的工作主要有如下几个方面：

- 用户账号的添加、删除与修改。

- 用户口令的管理。

- 用户组的管理。

  



#### 一、Linux系统用户账号的管理

用户账号的管理工作主要涉及到用户账号的添加、修改和删除。

添加用户账号就是在系统中创建一个新账号，然后为新账号分配用户号、用户组、主目录和登录Shell等资源。刚添加的账号是被锁定的，无法使用。

##### 1、添加新的用户账号使用useradd命令，其语法如下：

```
useradd 选项 用户名
```

参数说明：

- 选项:

  - -c comment 指定一段注释性描述。
  - -d 目录 指定用户主目录，如果此目录不存在，则同时使用-m选项，可以创建主目录。
  - -g 用户组 指定用户所属的用户组。
  - -G 用户组，用户组 指定用户所属的附加组。
  - -s Shell文件 指定用户的登录Shell。
  - -u 用户号 指定用户的用户号，如果同时有-o选项，则可以重复使用其他用户的标识号。

- 用户名:

  指定新账号的登录名。

###### 实例1

```
# useradd –d  /home/sam -m sam
```

此命令创建了一个用户sam，其中-d和-m选项用来为登录名sam产生一个主目录 /home/sam（/home为默认的用户主目录所在的父目录）。

###### 实例2

```
# useradd -s /bin/sh -g group –G adm,root gem
```

此命令新建了一个用户gem，该用户的登录Shell是 `/bin/sh`，它属于group用户组，同时又属于adm和root用户组，其中group用户组是其主组。

这里可能新建组：`#groupadd group及groupadd adm`

增加用户账号就是在/etc/passwd文件中为新用户增加一条记录，同时更新其他系统文件如/etc/shadow, /etc/group等。

Linux提供了集成的系统管理工具userconf，它可以用来对用户账号进行统一管理。

##### 2、删除帐号

如果一个用户的账号不再使用，可以从系统中删除。删除用户账号就是要将/etc/passwd等系统文件中的该用户记录删除，必要时还删除用户的主目录。

删除一个已有的用户账号使用`userdel`命令，其格式如下：

```
userdel 选项 用户名
```

常用的选项是 **-r**，它的作用是把用户的主目录一起删除。

例如：

```
# userdel -r sam
```

此命令删除用户sam在系统文件中（主要是/etc/passwd, /etc/shadow, /etc/group等）的记录，同时删除用户的主目录。

##### 3、修改帐号

修改用户账号就是根据实际情况更改用户的有关属性，如用户号、主目录、用户组、登录Shell等。

修改已有用户的信息使用`usermod`命令，其格式如下：

```
usermod 选项 用户名
```

常用的选项包括`-c, -d, -m, -g, -G, -s, -u以及-o等`，这些选项的意义与`useradd`命令中的选项一样，可以为用户指定新的资源值。

另外，有些系统可以使用选项：-l 新用户名

这个选项指定一个新的账号，即将原来的用户名改为新的用户名。

例如：

```
# usermod -s /bin/ksh -d /home/z –g developer sam
```

此命令将用户sam的登录Shell修改为ksh，主目录改为/home/z，用户组改为developer。

##### 4、用户口令的管理

用户管理的一项重要内容是用户口令的管理。用户账号刚创建时没有口令，但是被系统锁定，无法使用，必须为其指定口令后才可以使用，即使是指定空口令。

指定和修改用户口令的Shell命令是`passwd`。超级用户可以为自己和其他用户指定口令，普通用户只能用它修改自己的口令。命令的格式为：

```
passwd 选项 用户名
```

可使用的选项：

- -l 锁定口令，即禁用账号。
- -u 口令解锁。
- -d 使账号无口令。
- -f 强迫用户下次登录时修改口令。

如果默认用户名，则修改当前用户的口令。

例如，假设当前用户是sam，则下面的命令修改该用户自己的口令：

```
$ passwd 
Old password:****** 
New password:******* 
Re-enter new password:*******
```

如果是超级用户，可以用下列形式指定任何用户的口令：

```
# passwd sam 
New password:******* 
Re-enter new password:*******
```

普通用户修改自己的口令时，passwd命令会先询问原口令，验证后再要求用户输入两遍新口令，如果两次输入的口令一致，则将这个口令指定给用户；而超级用户为用户指定口令时，就不需要知道原口令。

为了系统安全起见，用户应该选择比较复杂的口令，例如最好使用8位长的口令，口令中包含有大写、小写字母和数字，并且应该与姓名、生日等不相同。

为用户指定空口令时，执行下列形式的命令：

```
# passwd -d sam
```

此命令将用户 sam 的口令删除，这样用户 sam 下一次登录时，系统就不再允许该用户登录了。

passwd 命令还可以用 -l(lock) 选项锁定某一用户，使其不能登录，例如：

```
# passwd -l sam
```

------

#### 二、Linux系统用户组的管理

每个用户都有一个用户组，系统可以对一个用户组中的所有用户进行集中管理。不同Linux 系统对用户组的规定有所不同，如Linux下的用户属于与它同名的用户组，这个用户组在创建用户时同时创建。

用户组的管理涉及用户组的添加、删除和修改。组的增加、删除和修改实际上就是对/etc/group文件的更新。

##### 1、增加一个新的用户组使用groupadd命令。其格式如下：

```
groupadd 选项 用户组
```

可以使用的选项有：

- -g GID 指定新用户组的组标识号（GID）。
- -o 一般与-g选项同时使用，表示新用户组的GID可以与系统已有用户组的GID相同。

###### 实例1：

```
# groupadd group1
```

此命令向系统中增加了一个新组group1，新组的组标识号是在当前已有的最大组标识号的基础上加1。

###### 实例2：

```
# groupadd -g 101 group2
```

此命令向系统中增加了一个新组group2，同时指定新组的组标识号是101。

##### 2、如果要删除一个已有的用户组，使用groupdel命令，其格式如下：

```
groupdel 用户组
```

###### 例如：

```
# groupdel group1
```

此命令从系统中删除组group1。

##### 3、修改用户组的属性使用groupmod命令。其语法如下：

```
groupmod 选项 用户组
```

常用的选项有：

- -g GID 为用户组指定新的组标识号。
- -o 与-g选项同时使用，用户组的新GID可以与系统已有用户组的GID相同。
- -n新用户组 将用户组的名字改为新名字

###### 实例1：

```
# groupmod -g 102 group2
```

此命令将组group2的组标识号修改为102。

###### 实例2：

```
# groupmod –g 10000 -n group3 group2
```

此命令将组group2的标识号改为10000，组名修改为group3。

##### 4、如果一个用户同时属于多个用户组，那么用户可以在用户组之间切换，以便具有其他用户组的权限。

用户可以在登录后，使用命令newgrp切换到其他用户组，这个命令的参数就是目的用户组。例如：

```
$ newgrp root
```

这条命令将当前用户切换到root用户组，前提条件是root用户组确实是该用户的主组或附加组。类似于用户账号的管理，用户组的管理也可以通过集成的系统管理工具来完成。

------

#### 三、与用户账号有关的系统文件

完成用户管理的工作有许多种方法，但是每一种方法实际上都是对有关的系统文件进行修改。

与用户和用户组相关的信息都存放在一些系统文件中，这些文件包括/etc/passwd, /etc/shadow, /etc/group等。

下面分别介绍这些文件的内容。

##### 1、/etc/passwd文件是用户管理工作涉及的最重要的一个文件。

Linux系统中的每个用户都在/etc/passwd文件中有一个对应的记录行，它记录了这个用户的一些基本属性。

这个文件对所有用户都是可读的。它的内容类似下面的例子：

```
＃ cat /etc/passwd

root:x:0:0:Superuser:/:
daemon:x:1:1:System daemons:/etc:
bin:x:2:2:Owner of system commands:/bin:
sys:x:3:3:Owner of system files:/usr/sys:
adm:x:4:4:System accounting:/usr/adm:
uucp:x:5:5:UUCP administrator:/usr/lib/uucp:
auth:x:7:21:Authentication administrator:/tcb/files/auth:
cron:x:9:16:Cron daemon:/usr/spool/cron:
listen:x:37:4:Network daemon:/usr/net/nls:
lp:x:71:18:Printer administrator:/usr/spool/lp:
sam:x:200:50:Sam san:/home/sam:/bin/sh
```

从上面的例子我们可以看到，/etc/passwd中一行记录对应着一个用户，每行记录又被冒号(:)分隔为7个字段，其格式和具体含义如下：

```
用户名:口令:用户标识号:组标识号:注释性描述:主目录:登录Shell
```

###### 1）"用户名"是代表用户账号的字符串。

通常长度不超过8个字符，并且由大小写字母和/或数字组成。登录名中不能有冒号(:)，因为冒号在这里是分隔符。

为了兼容起见，登录名中最好不要包含点字符(.)，并且不使用连字符(-)和加号(+)打头。

###### 2）“口令”一些系统中，存放着加密后的用户口令字。

虽然这个字段存放的只是用户口令的加密串，不是明文，但是由于/etc/passwd文件对所有用户都可读，所以这仍是一个安全隐患。因此，现在许多Linux 系统（如SVR4）都使用了shadow技术，把真正的加密后的用户口令字存放到/etc/shadow文件中，而在/etc/passwd文件的口令字段中只存放一个特殊的字符，例如“x”或者“*”。

###### 3）“用户标识号”是一个整数，系统内部用它来标识用户。

一般情况下它与用户名是一一对应的。如果几个用户名对应的用户标识号是一样的，系统内部将把它们视为同一个用户，但是它们可以有不同的口令、不同的主目录以及不同的登录Shell等。

通常用户标识号的取值范围是0～65 535。0是超级用户root的标识号，1～99由系统保留，作为管理账号，普通用户的标识号从100开始。在Linux系统中，这个界限是500。

###### 4）“组标识号”字段记录的是用户所属的用户组。

它对应着/etc/group文件中的一条记录。

###### 5)“注释性描述”字段记录着用户的一些个人情况。

例如用户的真实姓名、电话、地址等，这个字段并没有什么实际的用途。在不同的Linux 系统中，这个字段的格式并没有统一。在许多Linux系统中，这个字段存放的是一段任意的注释性描述文字，用做finger命令的输出。

###### 6)“主目录”，也就是用户的起始工作目录。

它是用户在登录到系统之后所处的目录。在大多数系统中，各用户的主目录都被组织在同一个特定的目录下，而用户主目录的名称就是该用户的登录名。各用户对自己的主目录有读、写、执行（搜索）权限，其他用户对此目录的访问权限则根据具体情况设置。

###### 7)用户登录后，要启动一个进程，负责将用户的操作传给内核，这个进程是用户登录到系统后运行的命令解释器或某个特定的程序，即Shell。

Shell是用户与Linux系统之间的接口。Linux的Shell有许多种，每种都有不同的特点。常用的有sh(Bourne Shell), csh(C Shell), ksh(Korn Shell), tcsh(TENEX/TOPS-20 type C Shell), bash(Bourne Again Shell)等。

系统管理员可以根据系统情况和用户习惯为用户指定某个Shell。如果不指定Shell，那么系统使用sh为默认的登录Shell，即这个字段的值为/bin/sh。

用户的登录Shell也可以指定为某个特定的程序（此程序不是一个命令解释器）。

利用这一特点，我们可以限制用户只能运行指定的应用程序，在该应用程序运行结束后，用户就自动退出了系统。有些Linux 系统要求只有那些在系统中登记了的程序才能出现在这个字段中。

###### 8)系统中有一类用户称为伪用户（pseudo users）。

这些用户在/etc/passwd文件中也占有一条记录，但是不能登录，因为它们的登录Shell为空。它们的存在主要是方便系统管理，满足相应的系统进程对文件属主的要求。

常见的伪用户如下所示：

```
伪 用 户 含 义 
bin 拥有可执行的用户命令文件 
sys 拥有系统文件 
adm 拥有帐户文件 
uucp UUCP使用 
lp lp或lpd子系统使用 
nobody NFS使用
```





##### 2、拥有帐户文件



###### **1、除了上面列出的伪用户外，还有许多标准的伪用户，例如：audit, cron, mail, usenet等，它们也都各自为相关的进程和文件所需要。**

由于/etc/passwd文件是所有用户都可读的，如果用户的密码太简单或规律比较明显的话，一台普通的计算机就能够很容易地将它破解，因此对安全性要求较高的Linux系统都把加密后的口令字分离出来，单独存放在一个文件中，这个文件是/etc/shadow文件。 有超级用户才拥有该文件读权限，这就保证了用户密码的安全性。

###### **2、/etc/shadow中的记录行与/etc/passwd中的一一对应，它由pwconv命令根据/etc/passwd中的数据自动产生**

它的文件格式与/etc/passwd类似，由若干个字段组成，字段之间用":"隔开。这些字段是：

```
登录名:加密口令:最后一次修改时间:最小时间间隔:最大时间间隔:警告时间:不活动时间:失效时间:标志
```

1. "登录名"是与/etc/passwd文件中的登录名相一致的用户账号
2. "口令"字段存放的是加密后的用户口令字，长度为13个字符。如果为空，则对应用户没有口令，登录时不需要口令；如果含有不属于集合 { ./0-9A-Za-z }中的字符，则对应的用户不能登录。
3. "最后一次修改时间"表示的是从某个时刻起，到用户最后一次修改口令时的天数。时间起点对不同的系统可能不一样。例如在SCO Linux 中，这个时间起点是1970年1月1日。
4. "最小时间间隔"指的是两次修改口令之间所需的最小天数。
5. "最大时间间隔"指的是口令保持有效的最大天数。
6. "警告时间"字段表示的是从系统开始警告用户到用户密码正式失效之间的天数。
7. "不活动时间"表示的是用户没有登录活动但账号仍能保持有效的最大天数。
8. "失效时间"字段给出的是一个绝对的天数，如果使用了这个字段，那么就给出相应账号的生存期。期满后，该账号就不再是一个合法的账号，也就不能再用来登录了。

下面是/etc/shadow的一个例子：

```
＃ cat /etc/shadow

root:Dnakfw28zf38w:8764:0:168:7:::
daemon:*::0:0::::
bin:*::0:0::::
sys:*::0:0::::
adm:*::0:0::::
uucp:*::0:0::::
nuucp:*::0:0::::
auth:*::0:0::::
cron:*::0:0::::
listen:*::0:0::::
lp:*::0:0::::
sam:EkdiSECLWPdSa:9740:0:0::::
```

##### 3、用户组的所有信息都存放在/etc/group文件中。



将用户分组是Linux 系统中对用户进行管理及控制访问权限的一种手段。

每个用户都属于某个用户组；一个组中可以有多个用户，一个用户也可以属于不同的组。

当一个用户同时是多个组中的成员时，在/etc/passwd文件中记录的是用户所属的主组，也就是登录时所属的默认组，而其他组称为附加组。

用户要访问属于附加组的文件时，必须首先使用newgrp命令使自己成为所要访问的组中的成员。

用户组的所有信息都存放在/etc/group文件中。此文件的格式也类似于/etc/passwd文件，由冒号(:)隔开若干个字段，这些字段有：

```
组名:口令:组标识号:组内用户列表
```

1. "组名"是用户组的名称，由字母或数字构成。与/etc/passwd中的登录名一样，组名不应重复。
2. "口令"字段存放的是用户组加密后的口令字。一般Linux 系统的用户组都没有口令，即这个字段一般为空，或者是*。
3. "组标识号"与用户标识号类似，也是一个整数，被系统内部用来标识组。
4. "组内用户列表"是属于这个组的所有用户的列表，不同用户之间用逗号(,)分隔。这个用户组可能是用户的主组，也可能是附加组。

/etc/group文件的一个例子如下：

```
root::0:root
bin::2:root,bin
sys::3:root,uucp
adm::4:root,adm
daemon::5:root,daemon
lp::7:root,lp
users::20:root,sam
```

#### 四、添加批量用户

添加和删除用户对每位Linux系统管理员都是轻而易举的事，比较棘手的是如果要添加几十个、上百个甚至上千个用户时，我们不太可能还使用useradd一个一个地添加，必然要找一种简便的创建大量用户的方法。Linux系统提供了创建大量用户的工具，可以让您立即创建大量用户，方法如下：

##### （1）先编辑一个文本用户文件。

每一列按照`/etc/passwd`密码文件的格式书写，要注意每个用户的用户名、UID、宿主目录都不可以相同，其中密码栏可以留做空白或输入x号。一个范例文件user.txt内容如下：

```
user001::600:100:user:/home/user001:/bin/bash
user002::601:100:user:/home/user002:/bin/bash
user003::602:100:user:/home/user003:/bin/bash
user004::603:100:user:/home/user004:/bin/bash
user005::604:100:user:/home/user005:/bin/bash
user006::605:100:user:/home/user006:/bin/bash
```

##### （2）以root身份执行命令 `/usr/sbin/newusers`，从刚创建的用户文件`user.txt`中导入数据，创建用户：

```
# newusers < user.txt
```

然后可以执行命令 `vipw` 或 `vi /etc/passwd` 检查 `/etc/passwd` 文件是否已经出现这些用户的数据，并且用户的宿主目录是否已经创建。

##### （3）执行命令/usr/sbin/pwunconv。

将 `/etc/shadow` 产生的 `shadow` 密码解码，然后回写到 `/etc/passwd` 中，并将`/etc/shadow`的`shadow`密码栏删掉。这是为了方便下一步的密码转换工作，即先取消 `shadow password` 功能。

```
# pwunconv
```

##### （4）编辑每个用户的密码对照文件。

格式为：

```
用户名:密码
```

实例文件 `passwd.txt` 内容如下：

```
user001:123456
user002:123456
user003:123456
user004:123456
user005:123456
user006:123456
```

##### （5）以 root 身份执行命令 `/usr/sbin/chpasswd`。

创建用户密码，`chpasswd` 会将经过 `/usr/bin/passwd` 命令编码过的密码写入 `/etc/passwd` 的密码栏。

```
# chpasswd < passwd.txt
```

##### （6）确定密码经编码写入/etc/passwd的密码栏后。

执行命令 `/usr/sbin/pwconv` 将密码编码为 `shadow password`，并将结果写入 `/etc/shadow`。

```
# pwconv
```

这样就完成了大量用户的创建了，之后您可以到/home下检查这些用户宿主目录的权限设置是否都正确，并登录验证用户密码是否正确。



### 6、 磁盘管理



Linux 磁盘管理好坏直接关系到整个系统的性能问题。

Linux 磁盘管理常用三个命令为 **df**、**du** 和 **fdisk**。

- **df**（英文全称：disk full）：列出文件系统的整体磁盘使用量
- **du**（英文全称：disk used）：检查磁盘空间使用量
- **fdisk**：用于磁盘分区



#### df

df命令参数功能：检查文件系统的磁盘空间占用情况。可以利用该命令来获取硬盘被占用了多少空间，目前还剩下多少空间等信息。

语法：

```
df [-ahikHTm] [目录或文件名]
```

选项与参数：

- -a ：列出所有的文件系统，包括系统特有的 /proc 等文件系统；
- -k ：以 KBytes 的容量显示各文件系统；
- -m ：以 MBytes 的容量显示各文件系统；
- -h ：以人们较易阅读的 GBytes, MBytes, KBytes 等格式自行显示；
- -H ：以 M=1000K 取代 M=1024K 的进位方式；
- -T ：显示文件系统类型, 连同该 partition 的 filesystem 名称 (例如 ext3) 也列出；
- -i ：不用硬盘容量，而以 inode 的数量来显示

##### 实例 1

将系统内所有的文件系统列出来！

```
[root@www ~]# df
Filesystem      1K-blocks      Used Available Use% Mounted on
/dev/hdc2         9920624   3823112   5585444  41% /
/dev/hdc3         4956316    141376   4559108   4% /home
/dev/hdc1          101086     11126     84741  12% /boot
tmpfs              371332         0    371332   0% /dev/shm
```

在 Linux 底下如果 df 没有加任何选项，那么默认会将系统内所有的 (不含特殊内存内的文件系统与 swap) 都以 1 Kbytes 的容量来列出来！

##### 实例 2

将容量结果以易读的容量格式显示出来

```
[root@www ~]# df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/hdc2             9.5G  3.7G  5.4G  41% /
/dev/hdc3             4.8G  139M  4.4G   4% /home
/dev/hdc1              99M   11M   83M  12% /boot
tmpfs                 363M     0  363M   0% /dev/shm
```

##### 实例 3

将系统内的所有特殊文件格式及名称都列出来

```
[root@www ~]# df -aT
Filesystem    Type 1K-blocks    Used Available Use% Mounted on
/dev/hdc2     ext3   9920624 3823112   5585444  41% /
proc          proc         0       0         0   -  /proc
sysfs        sysfs         0       0         0   -  /sys
devpts      devpts         0       0         0   -  /dev/pts
/dev/hdc3     ext3   4956316  141376   4559108   4% /home
/dev/hdc1     ext3    101086   11126     84741  12% /boot
tmpfs        tmpfs    371332       0    371332   0% /dev/shm
none   binfmt_misc         0       0         0   -  /proc/sys/fs/binfmt_misc
sunrpc  rpc_pipefs         0       0         0   -  /var/lib/nfs/rpc_pipefs
```

##### 实例 4

将 /etc 底下的可用的磁盘容量以易读的容量格式显示

```
[root@www ~]# df -h /etc
Filesystem            Size  Used Avail Use% Mounted on
/dev/hdc2             9.5G  3.7G  5.4G  41% /
```



#### du

Linux du 命令也是查看使用空间的，但是与 df 命令不同的是 Linux du 命令是对文件和目录磁盘使用的空间的查看，还是和df命令有一些区别的，这里介绍 Linux du 命令。

语法：

```
du [-ahskm] 文件或目录名称
```

选项与参数：

- -a ：列出所有的文件与目录容量，因为默认仅统计目录底下的文件量而已。
- -h ：以人们较易读的容量格式 (G/M) 显示；
- -s ：列出总量而已，而不列出每个各别的目录占用容量；
- -S ：不包括子目录下的总计，与 -s 有点差别。
- -k ：以 KBytes 列出容量显示；
- -m ：以 MBytes 列出容量显示；

##### 实例 1

只列出当前目录下的所有文件夹容量（包括隐藏文件夹）:

```
[root@www ~]# du
8       ./test4     <==每个目录都会列出来
8       ./test2
....中间省略....
12      ./.gconfd   <==包括隐藏文件的目录
220     .           <==这个目录(.)所占用的总量
```

直接输入 du 没有加任何选项时，则 du 会分析当前所在目录里的子目录所占用的硬盘空间。

##### 实例 2

将文件的容量也列出来

```
[root@www ~]# du -a
12      ./install.log.syslog   <==有文件的列表了
8       ./.bash_logout
8       ./test4
8       ./test2
....中间省略....
12      ./.gconfd
220     .
```

##### 实例 3

检查根目录底下每个目录所占用的容量

```
[root@www ~]# du -sm /*
7       /bin
6       /boot
.....中间省略....
0       /proc
.....中间省略....
1       /tmp
3859    /usr     <==系统初期最大就是他了啦！
77      /var
```

通配符 * 来代表每个目录。

与 df 不一样的是，du 这个命令其实会直接到文件系统内去搜寻所有的文件数据。



#### fdisk



fdisk 是 Linux 的磁盘分区表操作工具。

语法：

```
fdisk [-l] 装置名称
```

选项与参数：

- -l ：输出后面接的装置所有的分区内容。若仅有 fdisk -l 时， 则系统将会把整个系统内能够搜寻到的装置的分区均列出来。

##### 实例 1

列出所有分区信息

```
[root@AY120919111755c246621 tmp]# fdisk -l

Disk /dev/xvda: 21.5 GB, 21474836480 bytes
255 heads, 63 sectors/track, 2610 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x00000000

    Device Boot      Start         End      Blocks   Id  System
/dev/xvda1   *           1        2550    20480000   83  Linux
/dev/xvda2            2550        2611      490496   82  Linux swap / Solaris

Disk /dev/xvdb: 21.5 GB, 21474836480 bytes
255 heads, 63 sectors/track, 2610 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x56f40944

    Device Boot      Start         End      Blocks   Id  System
/dev/xvdb2               1        2610    20964793+  83  Linux
```

##### 实例 2

找出你系统中的根目录所在磁盘，并查阅该硬盘内的相关信息

```
[root@www ~]# df /            <==注意：重点在找出磁盘文件名而已
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/hdc2              9920624   3823168   5585388  41% /

[root@www ~]# fdisk /dev/hdc  <==仔细看，不要加上数字喔！
The number of cylinders for this disk is set to 5005.
There is nothing wrong with that, but this is larger than 1024,
and could in certain setups cause problems with:
1) software that runs at boot time (e.g., old versions of LILO)
2) booting and partitioning software from other OSs
   (e.g., DOS FDISK, OS/2 FDISK)

Command (m for help):     <==等待你的输入！
```

输入 m 后，就会看到底下这些命令介绍

```
Command (m for help): m   <== 输入 m 后，就会看到底下这些命令介绍
Command action
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition            <==删除一个partition
   l   list known partition types
   m   print this menu
   n   add a new partition           <==新增一个partition
   o   create a new empty DOS partition table
   p   print the partition table     <==在屏幕上显示分割表
   q   quit without saving changes   <==不储存离开fdisk程序
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit  <==将刚刚的动作写入分割表
   x   extra functionality (experts only)
```

离开 fdisk 时按下 `q`，那么所有的动作都不会生效！相反的， 按下`w`就是动作生效的意思。

```
Command (m for help): p  <== 这里可以输出目前磁盘的状态

Disk /dev/hdc: 41.1 GB, 41174138880 bytes        <==这个磁盘的文件名与容量
255 heads, 63 sectors/track, 5005 cylinders      <==磁头、扇区与磁柱大小
Units = cylinders of 16065 * 512 = 8225280 bytes <==每个磁柱的大小

   Device Boot      Start         End      Blocks   Id  System
/dev/hdc1   *           1          13      104391   83  Linux
/dev/hdc2              14        1288    10241437+  83  Linux
/dev/hdc3            1289        1925     5116702+  83  Linux
/dev/hdc4            1926        5005    24740100    5  Extended
/dev/hdc5            1926        2052     1020096   82  Linux swap / Solaris
# 装置文件名 启动区否 开始磁柱    结束磁柱  1K大小容量 磁盘分区槽内的系统

Command (m for help): q
```

想要不储存离开吗？按下 q 就对了！不要随便按 w 啊！

使用 `p` 可以列出目前这颗磁盘的分割表信息，这个信息的上半部在显示整体磁盘的状态。



#### 磁盘格式化

磁盘分割完毕后自然就是要进行文件系统的格式化，格式化的命令非常的简单，使用 `mkfs`（make filesystem） 命令。

语法：

```
mkfs [-t 文件系统格式] 装置文件名
```

选项与参数：

- -t ：可以接文件系统格式，例如 ext3, ext2, vfat 等(系统有支持才会生效)

##### 实例 1

查看 mkfs 支持的文件格式

```
[root@www ~]# mkfs[tab][tab]
mkfs         mkfs.cramfs  mkfs.ext2    mkfs.ext3    mkfs.msdos   mkfs.vfat
```

按下两个[tab]，会发现 mkfs 支持的文件格式如上所示。

##### 实例 2

将分区 /dev/hdc6（可指定你自己的分区） 格式化为 ext3 文件系统：

```
[root@www ~]# mkfs -t ext3 /dev/hdc6
mke2fs 1.39 (29-May-2006)
Filesystem label=                <==这里指的是分割槽的名称(label)
OS type: Linux
Block size=4096 (log=2)          <==block 的大小配置为 4K 
Fragment size=4096 (log=2)
251392 inodes, 502023 blocks     <==由此配置决定的inode/block数量
25101 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=515899392
16 block groups
32768 blocks per group, 32768 fragments per group
15712 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Writing inode tables: done
Creating journal (8192 blocks): done <==有日志记录
Writing superblocks and filesystem accounting information: done

This filesystem will be automatically checked every 34 mounts or
180 days, whichever comes first.  Use tune2fs -c or -i to override.
# 这样就创建起来我们所需要的 Ext3 文件系统了！简单明了！
```



#### 磁盘检验



fsck（file system check）用来检查和维护不一致的文件系统。

若系统掉电或磁盘发生问题，可利用fsck命令对文件系统进行检查。

语法：

```
fsck [-t 文件系统] [-ACay] 装置名称
```

选项与参数：

- -t : 给定档案系统的型式，若在 /etc/fstab 中已有定义或 kernel 本身已支援的则不需加上此参数
- -s : 依序一个一个地执行 fsck 的指令来检查
- -A : 对/etc/fstab 中所有列出来的 分区（partition）做检查
- -C : 显示完整的检查进度
- -d : 打印出 e2fsck 的 debug 结果
- -p : 同时有 -A 条件时，同时有多个 fsck 的检查一起执行
- -R : 同时有 -A 条件时，省略 / 不检查
- -V : 详细显示模式
- -a : 如果检查有错则自动修复
- -r : 如果检查有错则由使用者回答是否修复
- -y : 选项指定检测每个文件是自动输入yes，在不确定那些是不正常的时候，可以执行 # fsck -y 全部检查修复。

##### 实例 1

查看系统有多少文件系统支持的 fsck 命令：

```
[root@www ~]# fsck[tab][tab]
fsck         fsck.cramfs  fsck.ext2    fsck.ext3    fsck.msdos   fsck.vfat
```

##### 实例 2

强制检测 /dev/hdc6 分区:

```
[root@www ~]# fsck -C -f -t ext3 /dev/hdc6 
fsck 1.39 (29-May-2006)
e2fsck 1.39 (29-May-2006)
Pass 1: Checking inodes, blocks, and sizes
Pass 2: Checking directory structure
Pass 3: Checking directory connectivity
Pass 4: Checking reference counts
Pass 5: Checking group summary information
vbird_logical: 11/251968 files (9.1% non-contiguous), 36926/1004046 blocks
```

如果没有加上 -f 的选项，则由于这个文件系统不曾出现问题，检查的经过非常快速！若加上 -f 强制检查，才会一项一项的显示过程。



#### 磁盘挂载与卸除



Linux 的磁盘挂载使用 `mount` 命令，卸载使用 `umount` 命令。

磁盘挂载语法：

```
mount [-t 文件系统] [-L Label名] [-o 额外选项] [-n]  装置文件名  挂载点
```

##### 实例 1

用默认的方式，将刚刚创建的 /dev/hdc6 挂载到 /mnt/hdc6 上面！

```
[root@www ~]# mkdir /mnt/hdc6
[root@www ~]# mount /dev/hdc6 /mnt/hdc6
[root@www ~]# df
Filesystem           1K-blocks      Used Available Use% Mounted on
.....中间省略.....
/dev/hdc6              1976312     42072   1833836   3% /mnt/hdc6
```

磁盘卸载命令 `umount` 语法：

```
umount [-fn] 装置文件名或挂载点
```

选项与参数：

- -f ：强制卸除！可用在类似网络文件系统 (NFS) 无法读取到的情况下；
- -n ：不升级 /etc/mtab 情况下卸除。

卸载/dev/hdc6

```
[root@www ~]# umount /dev/hdc6     
```







### 7、vi/vim

所有的 Unix Like 系统都会内建 vi 文书编辑器，其他的文书编辑器则不一定会存在。

但是目前我们使用比较多的是 vim 编辑器。

vim 具有程序编辑的能力，可以主动的以字体颜色辨别语法的正确性，方便程序设计。

相关文章：[史上最全Vim快捷键键位图 — 入门到进阶](https://www.runoob.com/w3cnote/all-vim-cheatsheat.html)



#### 什么是 vim？



Vim是从 vi 发展出来的一个文本编辑器。代码补完、编译及错误跳转等方便编程的功能特别丰富，在程序员中被广泛使用。

简单的来说， vi 是老式的字处理器，不过功能已经很齐全了，但是还是有可以进步的地方。 vim 则可以说是程序开发者的一项很好用的工具。

连 vim 的官方网站 ([http://www.vim.org](http://www.vim.org/)) 自己也说 vim 是一个程序开发工具而不是文字处理软件。

vim 键盘图：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/vi-vim-cheat-sheet-sch.gif)



#### vi/vim 的使用

基本上 vi/vim 共分为三种模式，分别是**命令模式（Command mode）**，**输入模式（Insert mode）**和**底线命令模式（Last line mode）**。 这三种模式的作用分别是：

##### 命令模式：

用户刚刚启动 vi/vim，便进入了命令模式。

此状态下敲击键盘动作会被Vim识别为命令，而非输入字符。比如我们此时按下i，并不会输入一个字符，i被当作了一个命令。

以下是常用的几个命令：

- **i** 切换到输入模式，以输入字符。
- **x** 删除当前光标所在处的字符。
- **:** 切换到底线命令模式，以在最底一行输入命令。

若想要编辑文本：启动Vim，进入了命令模式，按下i，切换到输入模式。

命令模式只有一些最基本的命令，因此仍要依靠底线命令模式输入更多命令。

##### 输入模式

在命令模式下按下i就进入了输入模式。

在输入模式中，可以使用以下按键：

- **字符按键以及Shift组合**，输入字符
- **ENTER**，回车键，换行
- **BACK SPACE**，退格键，删除光标前一个字符
- **DEL**，删除键，删除光标后一个字符
- **方向键**，在文本中移动光标
- **HOME**/**END**，移动光标到行首/行尾
- **Page Up**/**Page Down**，上/下翻页
- **Insert**，切换光标为输入/替换模式，光标将变成竖线/下划线
- **ESC**，退出输入模式，切换到命令模式

##### 底线命令模式

在命令模式下按下:（英文冒号）就进入了底线命令模式。

底线命令模式可以输入单个或多个字符的命令，可用的命令非常多。

在底线命令模式中，基本的命令有（已经省略了冒号）：

- q 退出程序
- w 保存文件

按ESC键可随时退出底线命令模式。

简单的说，我们可以将这三个模式想成底下的图标来表示：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/vim-vi-workmodel.png)



#### vi/vim 使用实例



##### 使用 vi/vim 进入一般模式

如果你想要使用 vi 来建立一个名为 runoob.txt 的文件时，你可以这样做：

```
$ vim runoob.txt
```

直接输入 **vi 文件名** 就能够进入 vi 的一般模式了。请注意，记得 vi 后面一定要加文件名，不管该文件存在与否！

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/078207F0-B204-4464-AAEF-982F45EDDAE9.jpg)

##### 按下 i 进入输入模式(也称为编辑模式)，开始编辑文字

在一般模式之中，只要按下 i, o, a 等字符就可以进入输入模式了！

在编辑模式当中，你可以发现在左下角状态栏中会出现 –INSERT- 的字样，那就是可以输入任意字符的提示。

这个时候，键盘上除了 **Esc** 这个按键之外，其他的按键都可以视作为一般的输入按钮了，所以你可以进行任何的编辑。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1C928383-471E-4AF1-A61E-9E2CCBD5A913.jpg)

##### 按下 ESC 按钮回到一般模式

好了，假设我已经按照上面的样式给他编辑完毕了，那么应该要如何退出呢？是的！没错！就是给他按下 **Esc** 这个按钮即可！马上你就会发现画面左下角的 – INSERT – 不见了！

##### 在一般模式中按下 **:wq** 储存后离开 vi

OK，我们要存档了，存盘并离开的指令很简单，输入 **:wq** 即可保存离开！

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/B2FB5146-327C-4019-AC96-DD7A8EE7460C.jpg)

OK! 这样我们就成功创建了一个 runoob.txt 的文件。



#### vi/vim 按键说明

除了上面简易范例的 i, Esc, :wq 之外，其实 vim 还有非常多的按键可以使用。

##### 第一部分：一般模式可用的光标移动、复制粘贴、搜索替换等

| 移动光标的方法                                               |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| h 或 向左箭头键(←)                                           | 光标向左移动一个字符                                         |
| j 或 向下箭头键(↓)                                           | 光标向下移动一个字符                                         |
| k 或 向上箭头键(↑)                                           | 光标向上移动一个字符                                         |
| l 或 向右箭头键(→)                                           | 光标向右移动一个字符                                         |
| 如果你将右手放在键盘上的话，你会发现 hjkl 是排列在一起的，因此可以使用这四个按钮来移动光标。 如果想要进行多次移动的话，例如向下移动 30 行，可以使用 "30j" 或 "30↓" 的组合按键， 亦即加上想要进行的次数(数字)后，按下动作即可！ |                                                              |
| [Ctrl] + [f]                                                 | 屏幕『向下』移动一页，相当于 [Page Down]按键 (常用)          |
| [Ctrl] + [b]                                                 | 屏幕『向上』移动一页，相当于 [Page Up] 按键 (常用)           |
| [Ctrl] + [d]                                                 | 屏幕『向下』移动半页                                         |
| [Ctrl] + [u]                                                 | 屏幕『向上』移动半页                                         |
| +                                                            | 光标移动到非空格符的下一行                                   |
| -                                                            | 光标移动到非空格符的上一行                                   |
| n<space>                                                     | 那个 n 表示『数字』，例如 20 。按下数字后再按空格键，光标会向右移动这一行的 n 个字符。例如 20<space> 则光标会向后面移动 20 个字符距离。 |
| 0 或功能键[Home]                                             | 这是数字『 0 』：移动到这一行的最前面字符处 (常用)           |
| $ 或功能键[End]                                              | 移动到这一行的最后面字符处(常用)                             |
| H                                                            | 光标移动到这个屏幕的最上方那一行的第一个字符                 |
| M                                                            | 光标移动到这个屏幕的中央那一行的第一个字符                   |
| L                                                            | 光标移动到这个屏幕的最下方那一行的第一个字符                 |
| G                                                            | 移动到这个档案的最后一行(常用)                               |
| nG                                                           | n 为数字。移动到这个档案的第 n 行。例如 20G 则会移动到这个档案的第 20 行(可配合 :set nu) |
| gg                                                           | 移动到这个档案的第一行，相当于 1G 啊！ (常用)                |
| n<Enter>                                                     | n 为数字。光标向下移动 n 行(常用)                            |
| 搜索替换                                                     |                                                              |
| /word                                                        | 向光标之下寻找一个名称为 word 的字符串。例如要在档案内搜寻 vbird 这个字符串，就输入 /vbird 即可！ (常用) |
| ?word                                                        | 向光标之上寻找一个字符串名称为 word 的字符串。               |
| n                                                            | 这个 n 是英文按键。代表重复前一个搜寻的动作。举例来说， 如果刚刚我们执行 /vbird 去向下搜寻 vbird 这个字符串，则按下 n 后，会向下继续搜寻下一个名称为 vbird 的字符串。如果是执行 ?vbird 的话，那么按下 n 则会向上继续搜寻名称为 vbird 的字符串！ |
| N                                                            | 这个 N 是英文按键。与 n 刚好相反，为『反向』进行前一个搜寻动作。 例如 /vbird 后，按下 N 则表示『向上』搜寻 vbird 。 |
| 使用 /word 配合 n 及 N 是非常有帮助的！可以让你重复的找到一些你搜寻的关键词！ |                                                              |
| :n1,n2s/word1/word2/g                                        | n1 与 n2 为数字。在第 n1 与 n2 行之间寻找 word1 这个字符串，并将该字符串取代为 word2 ！举例来说，在 100 到 200 行之间搜寻 vbird 并取代为 VBIRD 则： 『:100,200s/vbird/VBIRD/g』。(常用) |
| **:1,$s/word1/word2/g** 或 **:%s/word1/word2/g**             | 从第一行到最后一行寻找 word1 字符串，并将该字符串取代为 word2 ！(常用) |
| **:1,$s/word1/word2/gc** 或 **:%s/word1/word2/gc**           | 从第一行到最后一行寻找 word1 字符串，并将该字符串取代为 word2 ！且在取代前显示提示字符给用户确认 (confirm) 是否需要取代！(常用) |
| 删除、复制与贴上                                             |                                                              |
| x, X                                                         | 在一行字当中，x 为向后删除一个字符 (相当于 [del] 按键)， X 为向前删除一个字符(相当于 [backspace] 亦即是退格键) (常用) |
| nx                                                           | n 为数字，连续向后删除 n 个字符。举例来说，我要连续删除 10 个字符， 『10x』。 |
| dd                                                           | 删除游标所在的那一整行(常用)                                 |
| ndd                                                          | n 为数字。删除光标所在的向下 n 行，例如 20dd 则是删除 20 行 (常用) |
| d1G                                                          | 删除光标所在到第一行的所有数据                               |
| dG                                                           | 删除光标所在到最后一行的所有数据                             |
| d$                                                           | 删除游标所在处，到该行的最后一个字符                         |
| d0                                                           | 那个是数字的 0 ，删除游标所在处，到该行的最前面一个字符      |
| yy                                                           | 复制游标所在的那一行(常用)                                   |
| nyy                                                          | n 为数字。复制光标所在的向下 n 行，例如 20yy 则是复制 20 行(常用) |
| y1G                                                          | 复制游标所在行到第一行的所有数据                             |
| yG                                                           | 复制游标所在行到最后一行的所有数据                           |
| y0                                                           | 复制光标所在的那个字符到该行行首的所有数据                   |
| y$                                                           | 复制光标所在的那个字符到该行行尾的所有数据                   |
| p, P                                                         | p 为将已复制的数据在光标下一行贴上，P 则为贴在游标上一行！ 举例来说，我目前光标在第 20 行，且已经复制了 10 行数据。则按下 p 后， 那 10 行数据会贴在原本的 20 行之后，亦即由 21 行开始贴。但如果是按下 P 呢？ 那么原本的第 20 行会被推到变成 30 行。 (常用) |
| J                                                            | 将光标所在行与下一行的数据结合成同一行                       |
| c                                                            | 重复删除多个数据，例如向下删除 10 行，[ 10cj ]               |
| u                                                            | 复原前一个动作。(常用)                                       |
| [Ctrl]+r                                                     | 重做上一个动作。(常用)                                       |
| 这个 u 与 [Ctrl]+r 是很常用的指令！一个是复原，另一个则是重做一次～ 利用这两个功能按键，你的编辑，嘿嘿！很快乐的啦！ |                                                              |
| .                                                            | 不要怀疑！这就是小数点！意思是重复前一个动作的意思。 如果你想要重复删除、重复贴上等等动作，按下小数点『.』就好了！ (常用) |

##### 第二部分：一般模式切换到编辑模式的可用的按钮说明

| 进入输入或取代的编辑模式                                     |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| i, I                                                         | 进入输入模式(Insert mode)： i 为『从目前光标所在处输入』， I 为『在目前所在行的第一个非空格符处开始输入』。 (常用) |
| a, A                                                         | 进入输入模式(Insert mode)： a 为『从目前光标所在的下一个字符处开始输入』， A 为『从光标所在行的最后一个字符处开始输入』。(常用) |
| o, O                                                         | 进入输入模式(Insert mode)： 这是英文字母 o 的大小写。o 为在目前光标所在的下一行处输入新的一行； O 为在目前光标所在的上一行处输入新的一行！(常用) |
| r, R                                                         | 进入取代模式(Replace mode)： r 只会取代光标所在的那一个字符一次；R会一直取代光标所在的文字，直到按下 ESC 为止；(常用) |
| 上面这些按键中，在 vi 画面的左下角处会出现『--INSERT--』或『--REPLACE--』的字样。 由名称就知道该动作了吧！！特别注意的是，我们上面也提过了，你想要在档案里面输入字符时， 一定要在左下角处看到 INSERT 或 REPLACE 才能输入喔！ |                                                              |
| [Esc]                                                        | 退出编辑模式，回到一般模式中(常用)                           |

##### 第三部分：一般模式切换到指令行模式的可用的按钮说明

| 指令行的储存、离开等指令                                     |                                                              |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| :w                                                           | 将编辑的数据写入硬盘档案中(常用)                             |
| :w!                                                          | 若文件属性为『只读』时，强制写入该档案。不过，到底能不能写入， 还是跟你对该档案的档案权限有关啊！ |
| :q                                                           | 离开 vi (常用)                                               |
| :q!                                                          | 若曾修改过档案，又不想储存，使用 ! 为强制离开不储存档案。    |
| 注意一下啊，那个惊叹号 (!) 在 vi 当中，常常具有『强制』的意思～ |                                                              |
| :wq                                                          | 储存后离开，若为 :wq! 则为强制储存后离开 (常用)              |
| ZZ                                                           | 这是大写的 Z 喔！如果修改过，保存当前文件，然后退出！效果等同于(保存并退出) |
| ZQ                                                           | 不保存，强制退出。效果等同于 **:q!**。                       |
| :w [filename]                                                | 将编辑的数据储存成另一个档案（类似另存新档）                 |
| :r [filename]                                                | 在编辑的数据中，读入另一个档案的数据。亦即将 『filename』 这个档案内容加到游标所在行后面 |
| :n1,n2 w [filename]                                          | 将 n1 到 n2 的内容储存成 filename 这个档案。                 |
| :! command                                                   | 暂时离开 vi 到指令行模式下执行 command 的显示结果！例如 『:! ls /home』即可在 vi 当中察看 /home 底下以 ls 输出的档案信息！ |
| vim 环境的变更                                               |                                                              |
| :set nu                                                      | 显示行号，设定之后，会在每一行的前缀显示该行的行号           |
| :set nonu                                                    | 与 set nu 相反，为取消行号！                                 |

特别注意，在 vi/vim 中，数字是很有意义的！数字通常代表重复做几次的意思！ 也有可能是代表去到第几个什么什么的意思。

举例来说，要删除 50 行，则是用 『50dd』 对吧！ 数字加在动作之前，如我要向下移动 20 行呢？那就是『20j』或者是『20↓』即可。



### 7、apt 命令



**Centos apt 改为 yum**

apt（Advanced Packaging Tool）是一个在 Debian 和 Ubuntu 中的 Shell 前端软件包管理器。

apt 命令提供了查找、安装、升级、删除某一个、一组甚至全部软件包的命令，而且命令简洁而又好记。

apt 命令执行需要超级管理员权限(root)。

#### apt 语法

```
  apt [options] [command] [package ...]
```

- **options：**可选，选项包括 -h（帮助），-y（当安装过程提示选择全部为"yes"），-q（不显示安装的过程）等等。
- **command：**要进行的操作。
- **package**：安装的包名。



#### apt 常用命令



- 列出所有可更新的软件清单命令：**sudo apt update**

- 升级软件包：**sudo apt upgrade**

  列出可更新的软件包及版本信息：**apt list --upgradeable**

  升级软件包，升级前先删除需要更新软件包：**sudo apt full-upgrade**

- 安装指定的软件命令：**sudo apt install <package_name>**

  安装多个软件包：**sudo apt install <package_1> <package_2> <package_3>**

- 更新指定的软件命令：**sudo apt update <package_name>**

- 显示软件包具体信息,例如：版本号，安装大小，依赖关系等等：**sudo apt show <package_name>**

- 删除软件包命令：**sudo apt remove <package_name>**

- 清理不再使用的依赖和库文件: **sudo apt autoremove**

- 移除软件包及配置文件: **sudo apt purge <package_name>**

- 查找软件包命令： **sudo apt search <keyword>**

- 列出所有已安装的包：**apt list --installed**

- 列出所有已安装的包的版本信息：**apt list --all-versions**

##### 实例

查看一些可更新的包：

```
sudo apt update
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples.png)

升级安装包：

```
sudo apt upgrade
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-1.png)

在以上交互式输入字母 **Y** 即可开始升级。

可以将以下两个命令组合起来，一键升级：

```
sudo apt update && sudo apt upgrade -y
```

安装 mplayer 包：

```
sudo apt install mplayer
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-3.png)

如过不太记得完整的包名，我们可以只输入前半部分的包名，然后按下 **Tab** 键，会列出相关的包名：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-2.png)

以上实例我们输入来 **reds**，然后按下 **Tab** 键，输出来四个相关的包。

如果我们想安装一个软件包，但如果软件包已经存在，则不要升级它，可以使用 **–no-upgrade** 选项:

```
sudo apt install <package_name> --no-upgrade
```

安装 mplayer 如果存在则不要升级：

```
sudo apt install mplayer --no-upgrade
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-4.png)

如果只想升级，不要安装可以使用 **--only-upgrade** 参数：

```
sudo apt install <package_name> --only-upgrade
```

只升级 mplayer，如果不存在就不要安装它：

```
sudo apt install mplayer --only-upgrade
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-5.png)

如果需要设置指定版本，语法格式如下：

```
sudo apt install <package_name>=<version_number>
```

**package_name** 为包名，**version_number** 为版本号。

移除包可以使用 remove 命令：

```
sudo apt remove mplayer
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-6-e1499720021872.png)

查找名为 libimobile 的相关包：

```
apt search libimobile
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-8.png)

查看 pinta 包的相关信息：

```
apt show pinta
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-7.png)

列出可更新的软件包：

```
apt list --upgradeable
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-9.png)

清理不再使用的依赖和库文件：

```
sudo apt autoremove
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/apt-commands-examples-10.png)

在以上交互式输入字母 **Y** 即可开始清理。





## 二、问题



### 1、E45: 'readonly' option is set (add ! to override)

```shell
:wq!  强制保存并退出
```















# Docker

## Docker介绍

### 什么是 Docker？

说实话关于 Docker 是什么并不太好说，下面我通过四点向你说明 Docker 到底是个什么东西。

- Docker 是世界领先的软件容器平台，基于 **Go 语言** 进行开发实现。
- Docker 能够自动执行重复性任务，例如搭建和配置开发环境，从而解放开发人员。
- 用户可以方便地创建和使用容器，把自己的应用放入容器。容器还可以进行版本管理、复制、分享、修改，就像管理普通的代码一样。
- Docker 可以**对进程进行封装隔离，属于操作系统层面的虚拟化技术。** 由于隔离的进程独立于宿主和其它的隔离的进程，因此也称其为容器。

官网地址：https://www.docker.com/ 。

![认识容器](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/container.png)

### 为什么要用 Docker?

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app），更重要的是容器性能开销极低。

传统的开发流程中，我们的项目通常需要使用 MySQL、Redis、FastDFS 等等环境，这些环境都是需要我们手动去进行下载并配置的，安装配置流程极其复杂，而且不同系统下的操作也不一样。

Docker 的出现完美地解决了这一问题，我们可以在容器中安装 MySQL、Redis 等软件环境，使得应用和环境架构分开，它的优势在于：

1. 一致的运行环境，能够更轻松地迁移
2. 对进程进行封装隔离，容器与容器之间互不影响，更高效地利用系统资源
3. 可以通过镜像复制多个一致的容器

另外，[《Docker 从入门到实践》open in new window](https://yeasy.gitbook.io/docker_practice/introduction/why) 这本开源书籍中也已经给出了使用 Docker 的原因。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/20210412220015698.png)

## Docker 的安装

### Windows

接下来对 Docker 进行安装，以 Windows 系统为例，访问 Docker 的官网：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-4e3146984adaee0067bdc5e9b1d757bb479.png)

然后点击`Get Started`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-96adfbfebe3e59097c8ba25e55f68ba7908.png)

在此处点击`Download for Windows`即可进行下载。

如果你的电脑是`Windows 10 64位专业版`的操作系统，则在安装 Docker 之前需要开启一下`Hyper-V`，开启方式如下。打开控制面板，选择程序：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-73ce678240826de0f49225250a970b4d205.png)

点击`启用或关闭Windows功能`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-9c7a96c332e56b9506325a1f1fdb608a659.png)

勾选上`Hyper-V`，点击确定即可：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-aad4a58c5e917f7185908d6320d7fb06861.png)

完成更改后需要重启一下计算机。

开启了`Hyper-V`后，我们就可以对 Docker 进行安装了，打开安装程序后，等待片刻点击`Ok`即可：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-62ac3c9184bdc21387755294613ff5054c6.png)

安装完成后，我们仍然需要重启计算机，重启后，若提示如下内容：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-3585c7d6a4632134ed925493a7d43e14a43.png)

它的意思是询问我们是否使用 WSL2，这是基于 Windows 的一个 Linux 子系统，这里我们取消即可，它就会使用我们之前勾选的`Hyper-V`虚拟机。

因为是图形界面的操作，这里就不介绍 Docker Desktop 的具体用法了。

### Mac

直接使用 Homebrew 安装即可



```bash
brew install --cask docker
```

### Linux

下面来看看 Linux 中如何安装 Docker，这里以 CentOS7 为例。

在测试或开发环境中，Docker 官方为了简化安装流程，提供了一套便捷的安装脚本，执行这个脚本后就会自动地将一切准备工作做好，并且把 Docker 的稳定版本安装在系统中。



```bash
curl -fsSL get.docker.com -o get-docker.sh
```



```bash
sh get-docker.sh --mirror Aliyun
```



安装完成后直接启动服务：



```bash
systemctl start docker
```

推荐设置开机自启，执行指令：



```bash
systemctl enable docker
```

## Docker 中的几个概念

在正式学习 Docker 之前，我们需要了解 Docker 中的几个核心概念：

### 镜像

镜像就是一个只读的模板，镜像可以用来创建 Docker 容器，一个镜像可以创建多个容器

### 容器

容器是用镜像创建的运行实例，Docker 利用容器独立运行一个或一组应用。它可以被启动、开始、停止、删除，每个容器都是相互隔离的、保证安全的平台。 可以把容器看作是一个简易的 Linux 环境和运行在其中的应用程序。容器的定义和镜像几乎一模一样，也是一堆层的统一视角，唯一区别在于容器的最上面那一层是可读可写的

### 仓库

仓库是集中存放镜像文件的场所。仓库和仓库注册服务器是有区别的，仓库注册服务器上往往存放着多个仓库，每个仓库中又包含了多个镜像，每个镜像有不同的标签。 仓库分为公开仓库和私有仓库两种形式，最大的公开仓库是 DockerHub，存放了数量庞大的镜像供用户下载，国内的公开仓库有阿里云、网易云等

### 总结

通俗点说，一个镜像就代表一个软件；而基于某个镜像运行就是生成一个程序实例，这个程序实例就是容器；而仓库是用来存储 Docker 中所有镜像的。

其中仓库又分为远程仓库和本地仓库，和 Maven 类似，倘若每次都从远程下载依赖，则会大大降低效率，为此，Maven 的策略是第一次访问依赖时，将其下载到本地仓库，第二次、第三次使用时直接用本地仓库的依赖即可，Docker 的远程仓库和本地仓库的作用也是类似的。

## Docker 初体验

下面我们来对 Docker 进行一个初步的使用，这里以下载一个 MySQL 的镜像为例`(在CentOS7下进行)`。

和 GitHub 一样，Docker 也提供了一个 DockerHub 用于查询各种镜像的地址和安装教程，为此，我们先访问 DockerHub：[https://hub.docker.com/open in new window](https://hub.docker.com/)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-37d083cc92fe36aad829e975646b9d27fa0.png)

在左上角的搜索框中输入`MySQL`并回车：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-ced37002391a059754def9b3a6c2aa4e342.png)

可以看到相关 MySQL 的镜像非常多，若右上角有`OFFICIAL IMAGE`标识，则说明是官方镜像，所以我们点击第一个 MySQL 镜像：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-48ba3fdc99c93a96e18b929195ca8e93c6c.png)

右边提供了下载 MySQL 镜像的指令为`docker pull MySQL`，但该指令始终会下载 MySQL 镜像的最新版本。

若是想下载指定版本的镜像，则点击下面的`View Available Tags`：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-ed601649275c6cfe65bbe422b463c263a64.png)

这里就可以看到各种版本的镜像，右边有下载的指令，所以若是想下载 5.7.32 版本的 MySQL 镜像，则执行：



```bash
docker pull MySQL:5.7.32
```



然而下载镜像的过程是非常慢的，所以我们需要配置一下镜像源加速下载，访问`阿里云`官网：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-0a46effd262d3db1b613a0db597efa31f34.png)

点击控制台：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-60f198e0106be6b43044969d2900272504f.png)

然后点击左上角的菜单，在弹窗的窗口中，将鼠标悬停在产品与服务上，并在右侧搜索容器镜像服务，最后点击容器镜像服务：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-2f6706a979b405dab01bc44a29bb6b26fc4.png)

点击左侧的镜像加速器，并依次执行右侧的配置指令即可。



```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://679xpnpz.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



## Docker 镜像指令

Docker 需要频繁地操作相关的镜像，所以我们先来了解一下 Docker 中的镜像指令。

若想查看 Docker 中当前拥有哪些镜像，则可以使用 `docker images` 命令。



```bash
[root@izrcf5u3j3q8xaz ~]# docker images
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
MySQL         5.7.32    f07dfa83b528   11 days ago     448MB
tomcat        latest    feba8d001e3f   2 weeks ago     649MB
nginx         latest    ae2feff98a0c   2 weeks ago     133MB
hello-world   latest    bf756fb1ae65   12 months ago   13.3kB
```



其中`REPOSITORY`为镜像名，`TAG`为版本标志，`IMAGE ID`为镜像 id(唯一的)，`CREATED`为创建时间，注意这个时间并不是我们将镜像下载到 Docker 中的时间，而是镜像创建者创建的时间，`SIZE`为镜像大小。

该指令能够查询指定镜像名：



```bash
docker image MySQL
```



若如此做，则会查询出 Docker 中的所有 MySQL 镜像：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images MySQL
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
MySQL        5.6       0ebb5600241d   11 days ago     302MB
MySQL        5.7.32    f07dfa83b528   11 days ago     448MB
MySQL        5.5       d404d78aa797   20 months ago   205MB
```



该指令还能够携带`-q`参数：`docker images -q` ， `-q`表示仅显示镜像的 id：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images -q
0ebb5600241d
f07dfa83b528
feba8d001e3f
d404d78aa797
```



若是要下载镜像，则使用：



```bash
docker pull MySQL:5.7
```

`docker pull`是固定的，后面写上需要下载的镜像名及版本标志；若是不写版本标志，而是直接执行`docker pull MySQL`，则会下载镜像的最新版本。

一般在下载镜像前我们需要搜索一下镜像有哪些版本才能对指定版本进行下载，使用指令：



```bash
docker search MySQL
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-559083ae80e7501e86e95fbbad25b6d571a.png)

不过该指令只能查看 MySQL 相关的镜像信息，而不能知道有哪些版本，若想知道版本，则只能这样查询：



```bash
docker search MySQL:5.5
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-68394e25f652964bb042571151c5e0fd2e9.png)

若是查询的版本不存在，则结果为空：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-abfdd51b9ad2ced3711268369f52b077b12.png)

删除镜像使用指令：



```bash
docker image rm MySQL:5.5
```

若是不指定版本，则默认删除的也是最新版本。

还可以通过指定镜像 id 进行删除：



```bash
docker image rm bf756fb1ae65
```

然而此时报错了：



```bash
[root@izrcf5u3j3q8xaz ~]# docker image rm bf756fb1ae65
Error response from daemon: conflict: unable to delete bf756fb1ae65 (must be forced) - image is being used by stopped container d5b6c177c151
```

这是因为要删除的`hello-world`镜像正在运行中，所以无法删除镜像，此时需要强制执行删除：

```bash
docker image rm -f bf756fb1ae65
```



该指令会将镜像和通过该镜像执行的容器全部删除，谨慎使用。

Docker 还提供了删除镜像的简化版本：`docker rmi 镜像名:版本标志` 。

此时我们即可借助`rmi`和`-q`进行一些联合操作，比如现在想删除所有的 MySQL 镜像，那么你需要查询出 MySQL 镜像的 id，并根据这些 id 一个一个地执行`docker rmi`进行删除，但是现在，我们可以这样：



```bash
docker rmi -f $(docker images MySQL -q)
```

首先通过`docker images MySQL -q`查询出 MySQL 的所有镜像 id，`-q`表示仅查询 id，并将这些 id 作为参数传递给`docker rmi -f`指令，这样所有的 MySQL 镜像就都被删除了。

## Docker 容器指令

掌握了镜像的相关指令之后，我们需要了解一下容器的指令，容器是基于镜像的。

若需要通过镜像运行一个容器，则使用：



```bash
docker run tomcat:8.0-jre8
```

当然了，运行的前提是你拥有这个镜像，所以先下载镜像：



```bash
docker pull tomcat:8.0-jre8
```

下载完成后就可以运行了，运行后查看一下当前运行的容器：`docker ps` 。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-bd48e20ef07b7c91ad16f92821a3dbca5b5.png)

其中`CONTAINER_ID`为容器的 id，`IMAGE`为镜像名，`COMMAND`为容器内执行的命令，`CREATED`为容器的创建时间，`STATUS`为容器的状态，`PORTS`为容器内服务监听的端口，`NAMES`为容器的名称。

通过该方式运行的 tomcat 是不能直接被外部访问的，因为容器具有隔离性，若是想直接通过 8080 端口访问容器内部的 tomcat，则需要对宿主机端口与容器内的端口进行映射：



```bash
docker run -p 8080:8080 tomcat:8.0-jre8
```

解释一下这两个端口的作用(`8080:8080`)，第一个 8080 为宿主机端口，第二个 8080 为容器内的端口，外部访问 8080 端口就会通过映射访问容器内的 8080 端口。

此时外部就可以访问 Tomcat 了：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-16d9ff4d29094681f51424ea8d0ee4fd73e.png)

若是这样进行映射：



```bash
docker run -p 8088:8080 tomcat:8.0-jre8
```

则外部需访问 8088 端口才能访问 tomcat，需要注意的是，每次运行的容器都是相互独立的，所以同时运行多个 tomcat 容器并不会产生端口的冲突。

容器还能够以后台的方式运行，这样就不会占用终端：



```bash
docker run -d -p 8080:8080 tomcat:8.0-jre8
```

启动容器时默认会给容器一个名称，但这个名称其实是可以设置的，使用指令：



```bash
docker run -d -p 8080:8080 --name tomcat01 tomcat:8.0-jre8
```

此时的容器名称即为 tomcat01，容器名称必须是唯一的。

再来引申一下`docker ps`中的几个指令参数，比如`-a`：



```bash
docker ps -a
```



该参数会将运行和非运行的容器全部列举出来：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-16d9ff4d29094681f51424ea8d0ee4fd73e.png)

`-q`参数将只查询正在运行的容器 id：`docker ps -q` 。



```bash
[root@izrcf5u3j3q8xaz ~]# docker ps -q
f3aac8ee94a3
074bf575249b
1d557472a708
4421848ba294
```

若是组合使用，则查询运行和非运行的所有容器 id：`docker ps -qa` 。



```bash
[root@izrcf5u3j3q8xaz ~]# docker ps -aq
f3aac8ee94a3
7f7b0e80c841
074bf575249b
a1e830bddc4c
1d557472a708
4421848ba294
b0440c0a219a
c2f5d78c5d1a
5831d1bab2a6
d5b6c177c151
```

接下来是容器的停止、重启指令，因为非常简单，就不过多介绍了。



```bash
docker start c2f5d78c5d1a
```

通过该指令能够将已经停止运行的容器运行起来，可以通过容器的 id 启动，也可以通过容器的名称启动。



```bash
docker restart c2f5d78c5d1a
```

该指令能够重启指定的容器。



```bash
docker stop c2f5d78c5d1a
```

该指令能够停止指定的容器。



```bash
docker kill c2f5d78c5d1a
```

该指令能够直接杀死指定的容器。

以上指令都能够通过容器的 id 和容器名称两种方式配合使用。

------

当容器被停止之后，容器虽然不再运行了，但仍然是存在的，若是想删除它，则使用指令：



```bash
docker rm d5b6c177c151
```

需要注意的是容器的 id 无需全部写出来，只需唯一标识即可。

若是想删除正在运行的容器，则需要添加`-f`参数强制删除：



```bash
docker rm -f d5b6c177c151
```

若是想删除所有容器，则可以使用组合指令：



```bash
docker rm -f $(docker ps -qa)
```

先通过`docker ps -qa`查询出所有容器的 id，然后通过`docker rm -f`进行删除。

------

当容器以后台的方式运行时，我们无法知晓容器的运行状态，若此时需要查看容器的运行日志，则使用指令：



```bash
docker logs 289cc00dc5ed
```

这样的式显示的日志并不是实时的，若是想实时显示，需要使用`-f`参数：



```bash
docker logs -f 289cc00dc5ed
```

通过`-t`参数还能够显示日志的时间戳，通常与`-f`参数联合使用：



```bash
docker logs -ft 289cc00dc5ed
```

------

查看容器内运行了哪些进程，可以使用指令：



```bash
docker top 289cc00dc5ed
```



![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-7ec71a682712e56e90490f55c32cf660fd3.png)

若是想与容器进行交互，则使用指令：



```bash
docker exec -it 289cc00dc5ed bash
```



![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-fd17796322f833685ca8ead592d38581898.png)

此时终端将会进入容器内部，执行的指令都将在容器中生效，在容器内只能执行一些比较简单的指令，如：ls、cd 等，若是想退出容器终端，重新回到 CentOS 中，则执行`exit`即可。

现在我们已经能够进入容器终端执行相关操作了，那么该如何向 tomcat 容器中部署一个项目呢？



```bash
docker cp ./test.html 289cc00dc5ed:/usr/local/tomcat/webapps
```



通过`docker cp`指令能够将文件从 CentOS 复制到容器中，`./test.html`为 CentOS 中的资源路径，`289cc00dc5ed`为容器 id，`/usr/local/tomcat/webapps`为容器的资源路径，此时`test.html`文件将会被复制到该路径下。



```bash
[root@izrcf5u3j3q8xaz ~]# docker exec -it 289cc00dc5ed bash
root@289cc00dc5ed:/usr/local/tomcat# cd webapps
root@289cc00dc5ed:/usr/local/tomcat/webapps# ls
test.html
root@289cc00dc5ed:/usr/local/tomcat/webapps#
```



若是想将容器内的文件复制到 CentOS 中，则反过来写即可：



```bash
docker cp 289cc00dc5ed:/usr/local/tomcat/webapps/test.html ./
```



所以现在若是想要部署项目，则先将项目上传到 CentOS，然后将项目从 CentOS 复制到容器内，此时启动容器即可。

------

虽然使用 Docker 启动软件环境非常简单，但同时也面临着一个问题，我们无法知晓容器内部具体的细节，比如监听的端口、绑定的 ip 地址等等，好在这些 Docker 都帮我们想到了，只需使用指令：



```bash
docker inspect 923c969b0d91
```



![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-fca74d4350cdfebfc2b06101e1cab411619.png)

## Docker 数据卷

学习了容器的相关指令之后，我们来了解一下 Docker 中的数据卷，它能够实现宿主机与容器之间的文件共享，它的好处在于我们对宿主机的文件进行修改将直接影响容器，而无需再将宿主机的文件再复制到容器中。

现在若是想将宿主机中`/opt/apps`目录与容器中`webapps`目录做一个数据卷，则应该这样编写指令：



```bash
docker run -d -p 8080:8080 --name tomcat01 -v /opt/apps:/usr/local/tomcat/webapps tomcat:8.0-jre8
```



然而此时访问 tomcat 会发现无法访问：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-8fa1b23f6ea2567b5938370e7d7f636533f.png)

这就说明我们的数据卷设置成功了，Docker 会将容器内的`webapps`目录与`/opt/apps`目录进行同步，而此时`/opt/apps`目录是空的，导致`webapps`目录也会变成空目录，所以就访问不到了。

此时我们只需向`/opt/apps`目录下添加文件，就会使得`webapps`目录也会拥有相同的文件，达到文件共享，测试一下：



```bash
[root@centos-7 opt]# cd apps/
[root@centos-7 apps]# vim test.html
[root@centos-7 apps]# ls
test.html
[root@centos-7 apps]# cat test.html
<h1>This is a test html!</h1>
```



在`/opt/apps`目录下创建了一个 `test.html` 文件，那么容器内的`webapps`目录是否会有该文件呢？进入容器的终端：



```bash
[root@centos-7 apps]# docker exec -it tomcat01 bash
root@115155c08687:/usr/local/tomcat# cd webapps/
root@115155c08687:/usr/local/tomcat/webapps# ls
test.html
```



容器内确实已经有了该文件，那接下来我们编写一个简单的 Web 应用：



```java
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println("Hello World!");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
```



这是一个非常简单的 Servlet，我们将其打包上传到`/opt/apps`中，那么容器内肯定就会同步到该文件，此时进行访问：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/up-712716a8c8c444ba3a77ade8ff27e7c6cf5.png)

这种方式设置的数据卷称为自定义数据卷，因为数据卷的目录是由我们自己设置的，Docker 还为我们提供了另外一种设置数据卷的方式：



```bash
docker run -d -p 8080:8080 --name tomcat01 -v aa:/usr/local/tomcat/webapps tomcat:8.0-jre8
```

1

此时的`aa`并不是数据卷的目录，而是数据卷的别名，Docker 会为我们自动创建一个名为`aa`的数据卷，并且会将容器内`webapps`目录下的所有内容复制到数据卷中，该数据卷的位置在`/var/lib/docker/volumes`目录下：



```bash
[root@centos-7 volumes]# pwd
/var/lib/docker/volumes
[root@centos-7 volumes]# cd aa/
[root@centos-7 aa]# ls
_data
[root@centos-7 aa]# cd _data/
[root@centos-7 _data]# ls
docs  examples  host-manager  manager  ROOT
```

1
2
3
4
5
6
7
8

此时我们只需修改该目录的内容就能能够影响到容器。

------

最后再介绍几个容器和镜像相关的指令：



```bash
docker commit -m "描述信息" -a "镜像作者" tomcat01 my_tomcat:1.0
```

1

该指令能够将容器打包成一个镜像，此时查询镜像：



```bash
[root@centos-7 _data]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my_tomcat           1.0                 79ab047fade5        2 seconds ago       463MB
tomcat              8                   a041be4a5ba5        2 weeks ago         533MB
MySQL               latest              db2b37ec6181        2 months ago        545MB
```

1
2
3
4
5

若是想将镜像备份出来，则可以使用指令：



```bash
docker save my_tomcat:1.0 -o my-tomcat-1.0.tar
```

1



```bash
[root@centos-7 ~]# docker save my_tomcat:1.0 -o my-tomcat-1.0.tar
[root@centos-7 ~]# ls
anaconda-ks.cfg  initial-setup-ks.cfg  公共  视频  文档  音乐
get-docker.sh    my-tomcat-1.0.tar     模板  图片  下载  桌面
```

1
2
3
4

若是拥有`.tar`格式的镜像，该如何将其加载到 Docker 中呢？执行指令：



```bash
docker load -i my-tomcat-1.0.tar
```

1



```bash
root@centos-7 ~]# docker load -i my-tomcat-1.0.tar
b28ef0b6fef8: Loading layer [==================================================>]  105.5MB/105.5MB
0b703c74a09c: Loading layer [==================================================>]  23.99MB/23.99MB
......
Loaded image: my_tomcat:1.0
[root@centos-7 ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
my_tomcat           1.0                 79ab047fade5        7 minutes ago       463MB
```

### **使用官方安装脚本自动安装**

安装命令如下：

```
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

也可以使用国内 daocloud 一键安装命令：

```
curl -sSL https://get.daocloud.io/docker | sh
```

------

### **手动安装**

#### **卸载旧版本**

较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

```
$ sudo yum remove docker \
         docker-client \
         docker-client-latest \
         docker-common \
         docker-latest \
         docker-latest-logrotate \
         docker-logrotate \
         docker-engine
```

#### **安装 Docker Engine-Community**

#### **使用 Docker 仓库进行安装**

在新主机上首次安装 Docker Engine-Community 之前，需要设置 Docker 仓库。之后，您可以从仓库安装和更新 Docker。

**设置仓库**

安装所需的软件包。yum-utils 提供了 yum-config-manager ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。

$ **sudo** **yum install** -y yum-utils \
 device-mapper-persistent-data \
 lvm2

使用以下命令来设置稳定的仓库。

#### **使用官方源地址（比较慢）**

```
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

可以选择国内的一些源地址：

#### **阿里云**

```
$ sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

#### **清华大学源**

```
$ sudo yum-config-manager \
    --add-repo \
    https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
```

#### **安装 Docker Engine-Community**

安装最新版本的 Docker Engine-Community 和 containerd，或者转到下一步安装特定版本：

```
$ sudo yum install docker-ce docker-ce-cli containerd.io
```

如果提示您接受 GPG 密钥，请选是。

> **有多个 Docker 仓库吗？**
>
> 如果启用了多个 Docker 仓库，则在未在 yum install 或 yum update 命令中指定版本的情况下，进行的安装或更新将始终安装最高版本，这可能不适合您的稳定性需求。

Docker 安装完默认未启动。并且已经创建好 docker 用户组，但该用户组下没有用户。

**要安装特定版本的 Docker Engine-Community，请在存储库中列出可用版本，然后选择并安装：**

1、列出并排序您存储库中可用的版本。此示例按版本号（从高到低）对结果进行排序。

$ **yum list** docker-ce --showduplicates **|** **sort** -r

docker-ce.x86_64  3:18.09.1-3.el7           docker-ce-stable
docker-ce.x86_64  3:18.09.0-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.1.ce-3.el7           docker-ce-stable
docker-ce.x86_64  18.06.0.ce-3.el7           docker-ce-stable

2、通过其完整的软件包名称安装特定版本，该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。例如：docker-ce-18.09.1。

```
$ sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
```

启动 Docker。

```
$ sudo systemctl start docker
```

通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

```
$ sudo docker run hello-world
```

#### **卸载 docker**

#### **启动并加入开机启动**

```
$ sudo systemctl start docker
$ sudo systemctl enable docker
```

**删除安装包：**

```
yum remove docker-ce
```

**删除镜像、容器、配置文件等内容：**

```
rm -rf /var/lib/docker
```

#### 镜像加速

```
科大镜像：https://docker.mirrors.ustc.edu.cn/
网易：https://hub-mirror.c.163.com/
阿里云：https://<你的ID>.mirror.aliyuncs.com
七牛云加速器：https://reg-mirror.qiniu.com
```



对于使用 systemd 的系统，请在 /etc/docker/daemon.json 中写入如下内容（如果文件不存在请新建该文件）：

```json
{"registry-mirrors":["https://reg-mirror.qiniu.com/"]}
```

之后重新启动服务：

```shell
systemctl daemon-reload
systemctl restart docker
```



## **Docker 基本命令**

### 1、启动或停止docker命令

```shell
sudo service docker start 启动docker centos6.x的命令sudo service docker restart 重启docker centos6.x的命令sudo service docker stop 关闭docker centos6.x的命令sudo systemctl start docker 启动docker centos7.x 命令sudo systemctl restart  docker 重启docker centos7.x 命令sudo systemctl stop docker 关闭docker centos7.x 命令
```

### 2、查看docker版本

```
docker -v
```

### 3、查看docker下载的镜像

```
docker images
```

![9a0eed828034a8c16a8d796922353ca3.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/9a0eed828034a8c16a8d796922353ca3.png)

### 4、設置docker开机自启动

```
systemctl enable docker
```

### 5、查看容器启动日志

```shell
①、docker logs -f -t --tail 10 smartbus #实时查看docker容器名为smartbus的最后10行日志
②、docker logs -f -t --since="2020-08-06" --tail=100 smartbus #查看指定时间后的日志，只显示最后100行
③、docker logs --since 30m smartbus #查看最近30分钟的日志
④、docker logs -t --since="2020-08-06T13:13:13" smartbus #查看某时间之后的日志
⑤、docker logs -t --since="2020-08-01T13:13:13" --until "2020-08-06 13:13:13" smartbus #查看某时间段的日志⑥、docker logs -f -t --since="2020-08-01" smartbus | grep error >>logs_error.txt #将错误日志写入文件
```

### 6、进入容器

```shell
docker exec -it [容器id] /bin/bash
```

从docker容器进入镜像容器后台例如进行mysql容器,例如：docker exec -it b30062adc08c /bin/bash 这样子就可以操作mysql容器

### 7、查看docker正在运行的容器

```
docker ps
```

![9e89323794a8b5109c3ec8d9284ee76d.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/9e89323794a8b5109c3ec8d9284ee76d.png)

### 8、停止容器

```
docker stop 
```

### 9、重启容器

```
docker restart 
```

### 10、拉取远程镜像

```
docker pull 镜像名<:tags># 从远程仓库抽取镜像,<:tags>是指镜像的版本,不加就是下载最新的镜像 例如：docker pull tomcat:7
```

### 11、创建容器，并且启动容器

```
docker run 镜像名<:tags> #如果不需要启动容器的话，直接用docker create 镜像名<:tags>
```

### 12、删除指定容器

```
docker rm  容器id#如果这个容器还在运行的情况下，加上-f这个参数代表强制删除
```

### 13、删除指定版本的镜像

```
docker rmi  镜像名:#加-f就是强制删除，即使这个镜像有对应的容器
```

### 14、查看所有的镜像和容器存储在宿主机的哪个位置 默认是存储在这个地址 cd /var/lib/docker

![0a3a5abbef058d5d4259a30922fe4693.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/0a3a5abbef058d5d4259a30922fe4693.png)

### 15、删除docker所有容器

```
docker rm $(docker ps -aq)
```

![c7fd62c92e74d54f23d8d7b2b25e1d46.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/c7fd62c92e74d54f23d8d7b2b25e1d46.png)



Docker 需要频繁地操作相关的镜像，所以我们先来了解一下 Docker 中的镜像指令。

若想查看 Docker 中当前拥有哪些镜像，则可以使用 `docker images` 命令。



```bash
[root@izrcf5u3j3q8xaz ~]# docker images
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
MySQL         5.7.32    f07dfa83b528   11 days ago     448MB
tomcat        latest    feba8d001e3f   2 weeks ago     649MB
nginx         latest    ae2feff98a0c   2 weeks ago     133MB
hello-world   latest    bf756fb1ae65   12 months ago   13.3kB
```

其中`REPOSITORY`为镜像名，`TAG`为版本标志，`IMAGE ID`为镜像 id(唯一的)，`CREATED`为创建时间，注意这个时间并不是我们将镜像下载到 Docker 中的时间，而是镜像创建者创建的时间，`SIZE`为镜像大小。

该指令能够查询指定镜像名：



```bash
docker image MySQL
```

若如此做，则会查询出 Docker 中的所有 MySQL 镜像：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images MySQL
REPOSITORY   TAG       IMAGE ID       CREATED         SIZE
MySQL        5.6       0ebb5600241d   11 days ago     302MB
MySQL        5.7.32    f07dfa83b528   11 days ago     448MB
MySQL        5.5       d404d78aa797   20 months ago   205MB
```

该指令还能够携带`-q`参数：`docker images -q` ， `-q`表示仅显示镜像的 id：



```bash
[root@izrcf5u3j3q8xaz ~]# docker images -q
0ebb5600241d
f07dfa83b528
feba8d001e3f
d404d78aa797
```

若是要下载镜像，则使用：



```bash
docker pull MySQL:5.7
```

`docker pull`是固定的，后面写上需要下载的镜像名及版本标志；若是不写版本标志，而是直接执行`docker pull MySQL`，则会下载镜像的最新版本。

一般在下载镜像前我们需要搜索一下镜像有哪些版本才能对指定版本进行下载，使用指令：



```bash
docker search MySQL
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-559083ae80e7501e86e95fbbad25b6d571a.png)

不过该指令只能查看 MySQL 相关的镜像信息，而不能知道有哪些版本，若想知道版本，则只能这样查询：



```bash
docker search MySQL:5.5
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-68394e25f652964bb042571151c5e0fd2e9.png)

若是查询的版本不存在，则结果为空：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/up-abfdd51b9ad2ced3711268369f52b077b12.png)

删除镜像使用指令：



```bash
docker image rm MySQL:5.5
```

若是不指定版本，则默认删除的也是最新版本。

还可以通过指定镜像 id 进行删除：



```bash
docker image rm bf756fb1ae65
```

然而此时报错了：



```bash
[root@izrcf5u3j3q8xaz ~]# docker image rm bf756fb1ae65
Error response from daemon: conflict: unable to delete bf756fb1ae65 (must be forced) - image is being used by stopped container d5b6c177c151
```

这是因为要删除的`hello-world`镜像正在运行中，所以无法删除镜像，此时需要强制执行删除：



```bash
docker image rm -f bf756fb1ae65
```

该指令将镜像和通过该镜像执行的容器全部删除，谨慎使用。

Docker 还提供了删除镜像的简化版本：`docker rmi 镜像名:版本标志` 。

此时我们即可借助`rmi`和`-q`进行一些联合操作，比如现在想删除所有的 MySQL 镜像，那么你需要查询出 MySQL 镜像的 id，并根据这些 id 一个一个地执行`docker rmi`进行删除，但是现在，我们可以这样：



```bash
docker rmi -f $(docker images MySQL -q)
```

首先通过`docker images MySQL -q`查询出 MySQL 的所有镜像 id，`-q`表示仅查询 id，并将这些 id 作为参数传递给`docker rmi -f`指令，这样所有的 MySQL 镜像就都被删除了。



### 16、拷贝文件



##### 1、从容器里面拷文件到宿主机？

   答：在宿主机里面执行以下命令   **docker cp 容器名：要拷贝的文件在容器里面的路径    要拷贝到宿主机的相应路径** 

   示例： 假设容器名为testtomcat,要从容器里面拷贝的文件路为：/usr/local/tomcat/webapps/test/js/test.js, 现在要将test.js从容器里面拷到宿主机的/opt路径下面，在宿主机上面执行命令

```
docker cp testtomcat：/usr/local/tomcat/webapps/test/js/test.js /opt
```

 

##### 2、从宿主机拷文件到容器里面		

   答：在宿主机里面执行如下命令 **docker cp 要拷贝的文件路径 容器名：要拷贝到容器里面对应的路径**

​    示例：假设容器名为testtomcat,现在要将宿主机/opt/test.js文件拷贝到容器里面的/usr/local/tomcat/webapps/test/js路径下面，在宿主机上面执行如下命令   

```
docker cp /opt/test.js testtomcat：/usr/local/tomcat/webapps/test/js
```

### 重启docker服务



```sh
#systemcsystemctl ⽅式
#守护进程重启
sudo systemctl restart docker
#关闭docker
sudo systemctl stop docker
#service ⽅式
#重启docker服务
sudo service docker restart
#关闭docker
```

sudo service docker stop
--------------------------------------------------------
作者：铁头采蓝知识集
链接：https://wenku.baidu.com/view/0b6377901937f111f18583d049649b6648d70969.html
来源：百度文库
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

## Docker 安装 Oracle



### 1、拉取镜像

> docker pull registry.cn-hangzhou.aliyuncs.com/helowin/oracle_11g

镜像详情：https://dev.aliyun.com/detail.html?spm=5176.1972343.2.8.E6Cbr1&repoId=1969

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325141.jpg)

由于镜像我已经拉取，所以此处显示已存在，查看镜像信息

> docker iamges

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325142.jpg)

### 2、创建并容器信息

> docker run -d -p 1521:1521 --name oracle_11g registry.aliyuncs.com/helowin/oracle_11g

由于此处我的容器已经创建（命令如想，容器名称 oracle_11g）此处我直接启动即可。

> docker start oracle_11g

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325143.jpg)

### 3、进入控制台设置用户信息

> docker exec -it oracle_11g bash

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325144.jpg)

登录sqlplus，此处发现sqlplus命令不可用，所以需要进行相关配置，操作步骤如下：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325145.jpg)

（1）、切换到root用户模式下

> su root

输入密码helowin

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325146.jpg)

(2)、编辑profile文件配置ORACLE环境变量

vi /etc/profile 并在文件最后添加如下命令

> export ORACLE_HOME=/home/oracle/app/oracle/product/11.2.0/dbhome_2
>
> export ORACLE_SID=helowin
>
> export PATH=$ORACLE_HOME/bin:$PATH

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325147.jpg)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325148.jpg)

推出并保存。

（3）、软件连接

> ln -s $ORACLE_HOME/bin/sqlplus /usr/bin

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325149.jpg)

因为我已经创建过所以包标志已存在。

（4）、切换到oracle 用户

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325150.jpg)

登录sqlplus并修改sys、system用户密码

> sqlplus /nolog
>
> conn /as sysdba

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325153.jpg)

接着执行下面命令

> alter user system identified by oracle;
>
> alter user sys identified by oracle;
>
> ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325154.jpg)

### 4、登录验证

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325155.jpg)

**登录成功**

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/2021091810325156.jpg)

#### 5、提交修改

​	docker commit 容器名称或ID 新的镜像名称:版本









## Docker 安装 MySQL



MySQL 是世界上最受欢迎的开源数据库。凭借其可靠性、易用性和性能，MySQL 已成为 Web 应用程序的数据库优先选择。

### 1、查看可用的 MySQL 版本

访问 MySQL 镜像库地址：https://hub.docker.com/_/mysql?tab=tags 。

可以通过 Sort by 查看其他版本的 MySQL，默认是最新版本 **mysql:latest** 。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql1.png)

你也可以在下拉列表中找到其他你想要的版本：

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql2.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql2.png)

此外，我们还可以用 **docker search mysql** 命令来查看可用版本：

```
$ docker search mysql
NAME                     DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
mysql                    MySQL is a widely used, open-source relati...   2529      [OK]       
mysql/mysql-server       Optimized MySQL Server Docker images. Crea...   161                  [OK]
centurylink/mysql        Image containing mysql. Optimized to be li...   45                   [OK]
sameersbn/mysql                                                          36                   [OK]
google/mysql             MySQL server for Google Compute Engine          16                   [OK]
appcontainers/mysql      Centos/Debian Based Customizable MySQL Con...   8                    [OK]
marvambass/mysql         MySQL Server based on Ubuntu 14.04              6                    [OK]
drupaldocker/mysql       MySQL for Drupal                                2                    [OK]
azukiapp/mysql           Docker image to run MySQL by Azuki - http:...   2                    [OK]
...
```

### 2、拉取 MySQL 镜像

这里我们拉取官方的最新版本的镜像：

```
$ docker pull mysql:latest
```

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql3.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql3.png)

### 3、查看本地镜像

使用以下命令来查看是否已安装了 mysql：

```
$ docker images
```

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql6.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql6.png)

在上图中可以看到我们已经安装了最新版本（latest）的 mysql 镜像。

### 4、运行容器

安装完成后，我们可以使用以下命令来运行 mysql 容器：

```
$ docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
```

参数说明：

- **-p 3306:3306** ：映射容器服务的 3306 端口到宿主机的 3306 端口，外部主机可以直接通过 **宿主机ip:3306** 访问到 MySQL 的服务。
- **MYSQL_ROOT_PASSWORD=123456**：设置 MySQL 服务 root 用户的密码。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql4.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql4.png)

### 5、安装成功

通过 **docker ps** 命令查看是否安装成功：

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql5.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql5.png)

本机可以通过 root 和密码 123456 访问 MySQL 服务。

[![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/docker-mysql7.png)](https://www.runoob.com/wp-content/uploads/2016/06/docker-mysql7.png)

### **问题**

#### 一、无法连接到 docker hub

```
docker: error pulling image configuration: Get https://registry-1.docker.io/v2/library/mysql/: TLS handshake timeout.
```

##### **解决：**

```
systemctl stop docker

echo "DOCKER_OPTS=\"\$DOCKER_OPTS --registry-mirror=http://f2d6cb40.m.daocloud.io\"" | sudo tee -a /etc/default/docker

service docker restart
```

#### **二、docker加载新的镜像后repository和tag名称都为none**

​	**解决：**

```
docker tag [image id] [name]:[版本]
```

#### **三、删除镜像报错**

```
Error response from daemon: conflict: unable to delete
```

**解决：**

当通过该镜像创建的容器未被销毁时，镜像是无法被删除的。为了验证这一点，我们来做个试验。首先，我们通过 `docker pull alpine` 命令，拉取一个最新的 `alpine` 镜像, 然后启动镜像，让其输出 `hello, docker!`:

![Docker run alpine](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271716522566.jpeg)

接下来，我们来删除这个镜像试试：

![Docker 删除镜像](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271728646226.jpeg)

可以看到提示信息，无法删除该镜像，因为有容器正在引用他！同时，这段信息还告诉我们，除非通过添加 `-f` 子命令，也就是强制删除，才能移除掉该镜像！

```bash
docker rmi -f docker.io/alpine
```

但是，我们一般不推荐这样暴力的做法，正确的做法应该是：

1. 先删除引用这个镜像的容器；
2. 再删除这个镜像；

也就是，根据上图中提示的，引用该镜像的容器 ID (`9d59e2278553`), 执行删除命令：

```bash
docker rm 9d59e2278553
```

然后，再执行删除镜像的命令：

```bash
docker rmi 5cb3aa00f899
```

![Docker 删除镜像](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/155271780037954.jpeg)

这个时候，就能正常删除了！

#### 四、主机无法连接虚拟机库

```
ERROR 1130: Host X.X.X.X is not allowed to connect to this MySQL serve
```

​		默认服务器直能支持本机用户登录，如果需要用工具链接需要给账号权限才行，具体操作如下

​       创建一个用户，xxxxx是用户名 % 代表任意地址都行123456 是密码

```sql
CREATE USER 'XXXXX'@'%' IDENTIFIED BY '123456';
```

​        授权用户，让他具有所有数据库的操作权限

```sql
grant all privileges on *.* to 'XXXXX'@'%';
```

​		授权用户，让他对所有的数据库具有SUPER 的权限

```sql
grant SUPER on *.* to 'XXXXX'@'%';
```

​		刷新权限

```sql
flush privileges;
```

​        这样在链接就好了，

​		如果root用户还不行

​		![image-20210927084510250](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20210927084510250.png)

将Host改为%，代表所有IP可连接

改完刷新权限

#### **五、MySQL 5.7root用户密码修改**

在MySQL 5.7 password字段已从mysql.user表中删除，新的字段名是“authenticalion_string”。

选择数据库

```sql
use mysql;
```

更新root的密码：

```sql
update user set authentication_string=password('新密码') where user='root' and Host='localhost';
```

刷新权限：

```sql
flush privileges;
```



## CentOS 7.0防火墙



CentOS 7.0默认使用的是firewall作为防火墙



查看防火墙状态

```
firewall-cmd --state
```

停止firewall

```
systemctl stop firewalld.service
```

禁止firewall开机启动

```
systemctl disable firewalld.service 
```



## 问题

### docker基础容器中bash: vi: command not found

​	**这是因为vim没有安装。**

```shell
apt-get update
apt-get install vim
```



### CentOS 8 安装 docker 文件冲突

- 卸载旧版本的docker或者docker-engine，最后你下载完的docker可以在/var/lib/docker中找到，然后这个文件夹中包含了images、container、volumes和networks，docker引擎的包叫做docker-ce。

```bash
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

- 继续别跳步骤，通用方法安装repository，输入下面两条命令。

```bash
sudo yum install -y yum-utils

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

- 如果你想安装nightly或者test repositories，输入前两条命令即可，第三条命令是禁用命令，同时，想要了解nightly、test repository的请戳这个链接看一下[Install Docker Engine | Docker Documentation](https://docs.docker.com/engine/install/)：

```bash
sudo yum-config-manager --enable docker-ce-nightly

sudo yum-config-manager --enable docker-ce-test

sudo yum-config-manager --disable docker-ce-nightly
```

- 在安装docker engine时，我遇到了一个问题就是有多个文件冲突，这时候网上很多说需要先安装lvm2，再继续安装docker engine：

```bash
yum install lvm2 -y
```

- 但是我在安装之后，并没有什么卵用，可以看到错误是和podman文件有关，然后查了一下podman是linux自带的容器，与docker冲突了，这样我们就知道问题出在哪了，只需要移除这个即可：

```bash
rpm -q podman  # 看一下自己的podman版本

yum remove podman  # 移除podman
```

- 正式安装docker，这里还会出现一些安装不上的东西，但这些东西并不影响正常的docker运行，我们在安装命令后加上 --allowerasing，如下图所示：

```bash
//第一种方式，直接安装最新版的docker engine
sudo yum install docker-ce docker-ce-cli containerd.io --allowerasing

//第二种安装指定版本，首先要看一下目前docker engine都有哪些版本，输入下列命令：

yum list docker-ce --showduplicates | sort -r

//然后你就取中间那一列的某一行，自己选一下，比如说3:20.10.0-3.el8，去掉“3：”和“-3.el8”只要“20.10.0”，接着输入命令：

sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io --allowerasing

//注意这里的VERSION_STRING要替换成你选的那个版本号，例如上述“20.10.0”，输入的时候不要加双引号，同时去掉尖括号
```

 

# Oracle

## 常用语句

### 查询锁表

```sql
select t2.username,
       t2.sid,
       t2.serial#,
       t3.object_name,
       t2.OSUSER,
       t2.MACHINE,
       t2.PROGRAM,
       t2.LOGON_TIME,
       t2.COMMAND,
       t2.LOCKWAIT,
       t2.SADDR,
       t2.PADDR,
       t2.TADDR,
       t2.SQL_ADDRESS,
       t1.LOCKED_MODE
  from v$locked_object t1, v$session t2, dba_objects t3
 where t1.session_id = t2.sid
   and t1.object_id = t3.object_id
 order by t2.logon_time;
```

### 解决锁表

```sql
alter system kill session 'sid,seial#';
```

### 创建表空间

```sql
create tablespace TEST_DATA 
logging 
datafile 'E:\oracle\oradata\orcl\TEST_DATA.dbf' 
size 50m 
autoextend on 
next 50m maxsize UNLIMITED 
extent management local; 
```

### 创建临时表空间

```sql
create temporary tablespace TEST_TEMP
tempfile 'E:\oracle\oradata\orcl\TEST_TEMP.dbf' 
size 50m 
autoextend on 
next 50m maxsize UNLIMITED 
extent management local;
```

### 创建用户

```sql
create user test_username identified by 123456 
default tablespace TEST_DATA 
temporary tablespace TEST_TEMP; 
```

### 分配权限

```sql
grant connect,resource,dba to test_username; 
```

### 创建同义词

#### **一、定义**

同义词顾名思义，是数据库方案对象的一个别名。这里的数据库方案对象指表、视图、序列、存储过程、包等。

> 相当于alias(别名),比如把user1.table1在user2中建一个同义词table1
> create synonym table1 for user1.table1;
> 这样当你在user2中查select * from table1时就相当于查select * from user1.table1;

#### **二、同义词的好处**

>1、不占内存空间，节省大量的数据库空间
>2、简化了数据库对象的访问
>3、提高了数据库对象访问的安全性
>4、扩展的数据库的使用范围，能够在不同的数据库用户之间实现无缝交互;同义词可以创建在不同一个数据库服务器上，通过网络实现连接

#### **三、创建同义词语法**

```
create public synonym table_a for user.table_a;
```

#### **四、同义词的删除语法**

因为同义词也是对象 ，删除语法统表一样

```
drop public synonym table_a;
```

#### **五、扩展**

如果要访问不同数据库下或者不同用户下的表table_a，当然也可以使用同义词，但需要先创建一个Database Link(数据库连接)来扩展访问，然后在使用如下语句创建数据库同义词：

```
create synonym table_a for table_a @DB_Link;
```

#### **六、实例演示**

##### **1、同库不同用户演示**

如图所示 在ctdev用户下有 表employees
![image-20220617100001741](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100001741.png)
但是在ctcheck用户没有这张表，在ctcheck用户下如图不能访问表employees；

![image-20220617100014988](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100014988.png)

创建同义词

```
create public synonym employees for ctdev.employees;
```

创建好后就可以在ctcheck用户下访问ctdev用户的表employees
![image-20220617100051460](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100051460.png)

##### **2、跨库同义词演示**

在10.248.100.81库下用户cwuser中并不能访问employees ，如下图所示。
![image-20220617100113422](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100113422.png)
已知10.248.100.81库下用户cwuser用户下已有访问 库10.1.2.1用户ctdev的dblink名称为to_ctdev。如下图所示

![image-20220617100126443](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100126443.png)

在cwuser用户下创建访问ctdev下employees 表的同义词

```
create synonym employees for employees@TO_CTDEV
```

即可在cwuser下访问ctdev下的表employees ，如下图所示

![image-20220617100152524](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220617100152524.png)



### 查询当前用户下某些字段在那个表

```sql
select table_name,column_name from user_tab_columns where column_name like '%column_name %';  
```

### 回滚误操作

```sql
select * from 表名 as of timestamp to_timestamp('2017-7-24 17:35:50','yyyy-mm-dd hh24:mi:ss');
```

### 修改用户名

```sql
 UPDATE USER$ SET NAME='JXZJXT1231SIT' WHERE user#=ZXZJXT1231SIT;
     COMMIT;
```

### Expdp Impdp 数据泵导入导出

**导出** 

```sql
sql>conn username/password@orcl as sysdba;
sql>create directory dmp_dir as 'd:\temp';
sql>select * from dba_directories;

sql>grant read,write on directory dmp_dir to username;

sql>grant exp_full_database to username;
sql>grant imp_full_database to username;

sql>alter user username quota unlimited on user_tablespace ; 

c:\expdp username/password@ip/orcl DIRECTORY=dmp_dir SCHEMAS=schema_name(username) dumpfile=XXX.dmp logfile=XXX.log 

-- content=metadata_only : 表结构
-- include=TABLE,VIEW,Procedure,sequence (表、视图、存储过程、序列)
c:\expdp username/password@ip/orcl DIRECTORY=dmp_dir SCHEMAS=schema_name(username) dumpfile=XXX.dmp logfile=XXX.log CONTENT=METADATA_ONLY INCLUDE=TABLE,VIEW,Procedure,sequence
```

高版本兼容低版本

```sql
expdp system/oracle@ip:1521/dbname directory=dmp11g logfile=xxx.log dumpfile=xxx_%U.dmp parallel=8 schemas=user1,user2 compression=all version=11.2.0.0.0 cluster=n

impdp system/oracle directory=dmp11g logfile=xxx.09011.log dumpfile=xxx_%U.dmp parallel=8 schemas=user1,user2 cluster=n
```



新建逻辑目录，Oracle不会自动创建实际的物理目录“D:\temp”（务必先手动创建此目录然后再执行oracle 导出目录定义），仅仅是进行定义逻辑路径dmp_dir；  

```bash
sql>conn username/password@orcl as sysdba;
sql>create directory dmp_dir as 'd:\temp';
sql>select * from dba_directories;
```

oracle 定义路径前必须创建物理地址，不然会出现如下错误。  

```sql
    ORA-39002: invalid operation
    ORA-39070: Unable to open the log file.
    ORA-29283: invalid file operation
    ORA-06512: at "SYS.UTL_FILE", line 536
    ORA-29283: invalid file operation
```

确认目录存在，当出现如下错误，看看是否有授权 

```sql
ORA-39002: invalid operation
ORA-39070: Unable to open the log file.
ORA-39087: directory name DUMP_DIR is invalid
```

解决方法：

```sql
grant read,write on directory dump_dir to username;
```

导出时出现如下错误，表示没有给用户授权（grant exp_full_database to username;）

```sql
    ORA-31631: privileges are required

    ORA-39161: Full database jobs require privileges
```

导入时出现如下错误，表示没有给用户授权（grant imp_full_database to username;）

```sql
    ORA-31631: privileges are required

    ORA-39122: Unprivileged users may not perform REMAP_SCHEMA remappings.
```

表空间已满提示错误：

```sql
    ORA-31626: job does not exist
    ORA-31633: unable to create master table "DRM_PFM.SYS_EXPORT_FULL_06"
    ORA-06512: at "SYS.DBMS_SYS_ERROR", line 95
    ORA-06512: at "SYS.KUPV$FT", line 1020
    ORA-01658: unable to create INITIAL extent for segment in tablespace TS_PFM
```

查看表空间情况：

```sql
SELECT a.tablespace_name "表空间名",
       total / 1024 / 1024 "表空间大小单位[M]",
       free / 1024 / 1024 "表空间剩余大小单位[M]",
       (total - free) / 1024 / 1024 "表空间使用大小单位[M]",
       Round((total - free) / total, 4) * 100 "使用率   [%]"
  FROM (SELECT tablespace_name, Sum(bytes) free
          FROM DBA_FREE_SPACE
         GROUP BY tablespace_name) a,
       (SELECT tablespace_name, Sum(bytes) total
          FROM DBA_DATA_FILES
         GROUP BY tablespace_name) b
 WHERE a.tablespace_name = b.tablespace_name;
```

解决方法：

```
-- 增加表空间容量
ALTER DATABASE DATAFILE 'D:\DEVELOP\DATABASE\TABLESPACE\TS_PFM.DBF' RESIZE 200M;

-- 修改表空间自动扩展
ALTER DATABASE DATAFILE 'D:\DEVELOP\DATABASE\TABLESPACE\TS_PFM.DBF' AUTOEXTEND ON NEXT 100M MAXSIZE 1G;
```

​    如果表空间设置了自动扩展，查询到的表空间容量快满了，手动执行表空间容量增加；我的就是设置了表空间自动扩展的，但是expdp 导出的时候还是报错，通过手动增加容量解决问题。  

文件已存在提示错误，删除之前因为出现错误创建的文件重新执行即可。

```sql
    ORA-39001: invalid argument value
    ORA-39000: bad dump file specification
    ORA-31641: unable to create dump file "d:\temp\oracle\drm_pfm0711.dmp"
    ORA-27038: created file already exists
    OSD-04010: <create> option specified, file already exists
```

根据用户导出数据提示

```sql
ORA-31626: job does not exist
ORA-31633: unable to create master table "DRM_PFM.SYS_EXPORT_SCHEMA_05"
ORA-06512: at "SYS.DBMS_SYS_ERROR", line 95
ORA-06512: at "SYS.KUPV$FT", line 1020
ORA-01950: no privileges on tablespace 'TS_PFM'
```

解决办法

```sql
SQL> alter user jnzjxt quota unlimited on tablespace_name ; 
```

 

**导入**

```sql
Impdp jnzjxt/jnzjxt@orcl directory=dmp_dir dumpfile=jnzjxt_expdp.dmp logfile=jnzjxt_impdp.log schemas=jnzjxt table_exists_action=append
```



角色确实不存在忽略即可，如果还是导入不了看看是否是表空间不足。

```sql
IMP-00058: ORACLE error 1653 encountered
ORA-01653: unable to extend table user.testTable by 8 in tablespace TS_test
IMP-00028: partial import of previous table rolled back: 10 rows rolled back
IMP-00017: following statement failed with ORACLE error 1917: 
           "GRANT SELECT ON "testTable" TO "testUser""
IMP-00003: ORACLE error 1917 encountered
ORA-01917: user or role 'testUser' does not exist
```

 

```sql
ORA-39006: internal error
ORA-39213: Metadata processing is notavailable
SQL> execute  sys.dbms_metadata_util.load_stylesheets;
```

###  表、字段加注释

```sql
COMMENT ON TABLE ZQ_STRESS_SCENARIO IS '债券压力情景设计表';

COMMENT ON COLUMN ZQ_STRESS_SCENARIO.ID IS '记录ID';
```

### ID自增

```sql
------创建序列

CREATE SEQUENCE ZQ_STRESS_SEQ
 
MINVALUE 1 --最小值
 
NOMAXVALUE --不设置最大值
 
START WITH 1 --从1开始计数
 
INCREMENT BY 1 --每次加1个
 
NOCYCLE --一直累加，不循环
 
NOCACHE; --不建缓冲区


-----创建触发器
CREATE OR REPLACE TRIGGER ZQ_STRESS_IS_TRIGGER
BEFORE INSERT ON ZQ_STRESS_SCENARIO FOR EACH ROW
BEGIN
SELECT ZQ_STRESS_SEQ.NEXTVAL INTO :NEW.ID FROM DUAL;
END;
```

### 扩字段

```sql
alter table 表名 modify 列名 数据类型
alter table 表名 ADD 列名 数据类型 #新增字段
```

### 查看表空间的名称及大小 

```sql
SELECT t.tablespace_name, round(SUM(bytes / (1024 * 1024)), 0) ts_size 
FROM dba_tablespaces t, dba_data_files d 
WHERE t.tablespace_name = d.tablespace_name 
GROUP BY t.tablespace_name; 
```

### 创建索引、查询索引

##### 1、创建索引

```sql
create index 索引名 on 表名(列名);
```

##### 2、删除索引

```sql
drop index 索引名;
```

 

##### 3、创建组合索引

```sql
    create index 索引名 on 表名(列名1,,列名2);
```

 

```sql
*查看目标表中已添加的索引
*
*/
--在数据库中查找表名
select` `* ``from` `user_tables ``where` `table_name ``like` `'tablename%'``;
```

 

```sql
--查看该表的所有索引
select` `* ``from` `all_indexes ``where` `table_name = ``'tablename'``;
```

 

```sql
--查看该表的所有索引列
select``* ``from` `all_ind_columns ``where` `table_name = ``'tablename'``;
```

### 数值处理函数

```sql
1.绝对值：abs()

   select abs(-2) value from dual;

2.取整函数（大）：ceil（）

   select ceil(-2.001) value from dual;(-2)

3.取整函数（小）：floor（）

   select floor(-2.001) value from dual;(-3)

4.取整函数（截取）：trunc（）

   select trunc(-2.001) value from dual; (-2)

5.四舍五入：round（）

   select round(1.234564) value from dual;(1.2346)

6.取平方：Power（m,n）

   select power(4,2) value from dual;(16)

7.取平方根:SQRT()

   select sqrt(16) value from dual;(4)

8.取随机数:dbms_random(minvalue,maxvalue)

   select sys.dbms.random.value(0,1) value from dual;

9.取符号：Sign()

   select sign(-3) value from dual;(-)

10,取集合的最大值:greatest(value)

   select greatest(-1,3,5,7,9) value from dual;(9)

11.取集合的最小值:least(value)

   select least(-1,3,5,7,9) value from dual;(-1)

12.处理Null值：nvl(空值，代替值)

   select  nvl(null,10) value from dual;(10)

13.求字符序号:ascii()

    select ascii(a) value from dual;

14.求序号字符:chr()

    select chr(97) value from dual;

15.链接：concat()

    select concat("11","22") value from dual;(1122)

16.获取系统时间:sysdate()

    select sysdate value from dual;

17.求日期

    select trunc(sysdate) from dual;

18.求时间

    select  to_char(sysdate,"hh24:mm:ss") from dual;

19.首字母大写：InitCAP()

    select INITCAP(abc def ghi) value from dual;(Abc Def Ghi)
```

### 查看版本号

```sql
select * from v$version;
```

### 查询表注释

```sql
select * from user_tab_comments
```

### 修改数据库字符集

> 1.先查看客户端和服务端的编码集
> 客户端：
>
>  SELECT * FROM V$NLS_PARAMETERS;
> 1
> 服务端
>
> SELECT * FROM NLS_DATABASE_PARAMETERS;
> 1
> \2. 修改客户端编码集
> oralce中常用字符集：
>
> AMERICAN_AMERICA.ZHS16GBK
>
> SIMPLIFIED CHINESE_CHINA.ZHS16GBK
>
> SIMPLIFIED CHINESE_CHINA.AL32UTF8
>
> 修改客户端编码集有两步：
>
> 打开注册表，找到路径：\HKEY_LOCAL_MACHINE\SOFTWARE\Oracle\HOME，下的NLS_LANG修改为需要指定的字符集即可
> 在系统变量中新建：NLS_LANG = 编码集，如AMERICAN_AMERICA.ZHS16GBK
> \3. 修改服务端编码集（不推荐）
> 以管理员模式启动cmd
> sqlplus / as sysdba
> 依次执行
>
> SQL> shutdown immediate
> SQL> startup mount
> SQL> alter system enable restricted session;
> SQL> alter system set job_queue_processes=0;
> SQL> alter system set aq_tm_processes=0;
> SQL> alter database open;
>
> SQL> alter database character set internal_use AMERICAN_AMERICA.ZHS16GBK; --强制转换，不加internal_use 会报错
>
> SQL> shutdown immediate
> SQL> startup
>
> SQL> select userenv('language') from dual;
>  
>
> 
> 修改完成
>
> 总结： 把客户端编码集改为GBK，服务端改为UTF8即可解决中文的乱码问题。

### instr字符查找函数

#### 语法

**instr( string1, string2, start_position,nth_appearance )**

#### 参数

● string1：源字符串，要在此字符串中查找。

●string2：要在string1中查找的字符串 。

●start_position：代表string1 的哪个位置开始查找。此参数可选，如果省略默认为1. 字符串索引从1开始。如果此参数为正，从左到右开始检索，如果此参数为负，从右到左检索，返回要查找的字符串在源字符串中的开始索引。

●nth_appearance：代表要查找第几次出现的string2. 此参数可选，如果省略，默认为 1.如果为负数系统会报错。 [2] 

#### 注意

位置索引号从1开始。

如果String2在String1中没有找到，instr函数返回0。　

#### 示例

```
SELECT instr('syranmo','s') FROM dual; -- 返回 1

SELECT instr('syranmo','ra') FROM dual; -- 返回 3

SELECT instr('syran mo','at',1,2) FROM dual; -- 返回 0
```



### 去重

```sql
create table test(
    id int primary key not null,
    name varchar(10) not null,
    age int not null
);


insert into test values (1,'张三'，20);
insert into test values (2,'张三'，20);
insert into test values (3,'李四'，20);
insert into test values (4,'李四'，30);
insert into test values (5,'王五'，40);
insert into test values (6,'王五'，40);
commit;
```

1.distinct

```sql
select distinct name,age from test
```

2.group by 

```sql
select name,age from test
group by name,age;
```

3.rowid（伪列去重）

```sql
select id,name,age from test t1
where t1.rowid in (select min(rowid) from test t2 where t1.name=t2.name and t1.age=t2.age);
```

4.窗口函数row_number () over()

```sql
select t.id ,t.name,t.age from 
(select row_number() over(partition by name,age order by age) rank,test.* from test)t
where t.rank = 1;
```



### Over() Partition By

group by是分组函数，partition by是分区函数（像sum()等是[聚合函数](https://so.csdn.net/so/search?q=聚合函数&spm=1001.2101.3001.7020)），注意区分。

#### 1、over函数的写法：

```
over（partition by cno order by degree ）
1
```

先对cno 中相同的进行分区，在cno 中相同的情况下对degree 进行排序

#### 2、分区函数Partition By与rank()的用法“对比”分区函数Partition By与row_number()的用法

例：查询每名课程的第一名的成绩

###### （1）使用rank()

```
SELECT	* 
FROM	(select sno,cno,degree,
      	rank()over(partition by cno order by degree desc) mm 
      	from score) 
where mm = 1;
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190323180421725.png)

###### （2）使用row_number()

```
SELECT * 
FROM   (select sno,cno,degree,
       row_number()over(partition by cno order by degree desc) mm 
       from score) 
where mm = 1;
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190323180844735.png)

###### （3）rank()与row_number()的区别

由以上的例子得出，在求第一名成绩的时候，不能用row_number()，因为如果同班有两个并列第一，row_number()只返回一个结果。

#### 3、分区函数Partition By与rank()的用法“对比”分区函数Partition By与dense_rank()的用法

例：查询课程号为‘3-245’的成绩与排名

###### （1） 使用rank()

```
SELECT * 
FROM   (select sno,cno,degree,
       rank()over(partition by cno order by degree desc) mm 
       from score) 
where cno = '3-245'
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190325162112385.png)

###### （2） 使用dense_rank()

```
SELECT * 
FROM   (select sno,cno,degree,
       dense_rank()over(partition by cno order by degree desc) mm 
       from score) 
where cno = '3-245'
12345
```

得到结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190325162956912.png)

###### （3）rank()与dense_rank()的区别

由以上的例子得出，rank()和dense_rank()都可以将并列第一名的都查找出来；但rank()是跳跃排序，有两个第一名时接下来是第三名；而dense_rank()是非跳跃排序，有两个第一名时接下来是第二名。



### Decode

#### 1、decode的含义

```
decode(条件,值1,返回值1,值2,返回值2,…值n,返回值n,缺省值)
```

这个是decode的[表达式](https://so.csdn.net/so/search?q=表达式&spm=1001.2101.3001.7020)，具体的含义解释为：

```java
IF 条件=值1 THEN
　　　　RETURN(翻译值1)
ELSIF 条件=值2 THEN
　　　　RETURN(翻译值2)
　　　　......
ELSIF 条件=值n THEN
　　　　RETURN(翻译值n)
ELSE
　　　　RETURN(缺省值)
END IF
12345678910
```

#### 2.decode的用法



##### 2.1 翻译值

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143638977.png)
需求：查询出的数据，1表示男生，2表示女生

```sql
select t.id,
       t.name,
       t.age,
       decode(t.sex, '1', '男生', '2', '女生', '其他') as sex
  from STUDENT2 t
12345
```

结果：![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143719566.png)

##### 2.2 decode比较大小

说明：sign(value)函数会根据value的值为0，正数，负数，分别返回0，1,-1

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143759114.png)
需求：年龄在20以上的显示20以上，20以下的显示20以下，20的显示正好20

```sql
select t.id,
       t.name,
       t.age,
       decode(sign(t.age - 20),
              1,
              '20以上',
              -1,
              '20以下',
              0,
              '正好20',
              '未知') as sex
  from STUDENT2 t
123456789101112
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143835462.png)

##### 2.3 decode分段

数据暂无
需求：工资大于5000为高薪，工资介于3000到5000为中等，工资小于3000为底薪

```sql
select name,
       sal,
       decode(sign(sal - 5000),
              1,
              '高薪',
              0,
              '高薪',
              -1,
              decode(sign(sal - 3000), 1, '中等', 0, '中等', -1, '低薪')) as salname
  from person;
12345678910
```

##### 2.4 搜索字符串

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617143936574.png)
需求：找到含有三的姓名

```sql
select t.id,
       decode(instr(t.name, '三'), 0, '姓名不含有三', '姓名含有三') as name,
       t.age,
       t.sex
  from STUDENT2 t
12345
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144009575.png)

##### 2.5 判断是否为空

数据截图：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144032102.png)
需求：性别为空显示“暂无数据”，不为空原样输出

```sql
select t.id,
       t.name,
       t.age,
       decode(t.sex,NULL,'暂无数据',t.sex) as sex
  from STUDENT2 t
12345
```

结果：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20210617144108973.png)



### Case when

> Case具有两种格式。简单Case函数和Case搜索函数。
>
> **Case when 在Oracle 中的用法：**
>
> （a）以case开头，以end 结尾；
>
> （b）分之中when后跟条件，then 显示结果；
>
> （c）else 为除此之外的默认情况，类似于高级语言程序中的 switc case 的default可以不加；
>
> （d）end后面跟别名；
>
> **Case 有两种表达式：**
>
> （A）简单case表达式试用表达式确定返回值；
>
> （B）搜索case表达式，使用条件确定返回值；



第一种 格式 : 简单Case函数 :

格式说明

```
case` `列名``when` `条件值1 ``then` `选项1``when` `条件值2 ``then` `选项2.......``else` `默认值 ``end
```

eg:

```
select``case` `　　job_level``when` `'1'` `then` `'1111'``when``　 ``'2'` `then` `'1111'``when``　 ``'3'` `then` `'1111'``else` `'eee'` `end``from` `dbo.employee
```

第二种 格式 :Case搜索函数

格式说明

```
case``when` `列名= 条件值1 ``then` `选项1``when` `列名=条件值2 ``then` `选项2.......``else` `默认值 ``end
```

eg:

```
update` `employee``set` `e_wage =``case``when` `job_level = ``'1'` `then` `e_wage*1.97``when` `job_level = ``'2'` `then` `e_wage*1.07``when` `job_level = ``'3'` `then` `e_wage*1.06``else` `e_wage*1.05``end
```

### replace 字符串替换

```sql
replace(要更新的字段,'要替换的字符串'，'替换后字符串')
```

```sql
#替换手机号中间的4位为*号	
replace(phone,substr(phone,4,4),'****')
substr(username,1,3)||'****'||substr(username,-4,4)
```



### length() 

> 返回字符串的长度



### 字符串截取

```sql
//返回截取的字符串
substr(字符串,截取开始位置,截取长度) 
```



### 替换数组存入数据库后多出的","

```sql
CASE
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',',LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1) > 0
        AND instr(point.FZ_ADOPT_CONTROL_MEASURES, ',,') > 0
        THEN REPLACE(SUBSTR(point.FZ_ADOPT_CONTROL_MEASURES, 0, LENGTH 
            (point.FZ_ADOPT_CONTROL_MEASURES) - 1), ',,',',')
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',',LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1) > 0
        THEN SUBSTR(point.FZ_ADOPT_CONTROL_MEASURES, 0, LENGTH(point.FZ_ADOPT_CONTROL_MEASURES) - 
            1)
        WHEN instr(point.FZ_ADOPT_CONTROL_MEASURES, ',,') > 0
        THEN REPLACE(point.FZ_ADOPT_CONTROL_MEASURES, ',,',',')
    END FZ_ADOPT_CONTROL_MEASURES
```



### 修改字段类型为clob

```sql
–增加大字段项
alter table t add a_copy CLOB;
–将需要改成大字段的项内容copy到大字段中
update t set a_copy= a;
–删除原有字段
alter table t DROP COLUMN a;
–将大字段名改成原字段名
alter table t RENAME COLUMN a_copy TO a;
最后commit
```



### connect by prior

CONNECT BY PRIOR 这个子句主要是用于B树结构类型的数据递归查询,通俗点讲就是当表中存在ID和父类ID时可以通过子节点或者父节点查到相应的数据。

实际例子：以299499为父结点，遍历其子结点



```
SELECT FG.CODE,FG.NAME,LEVEL,FG.PARENT_ID FROM CMS.FND_GROUP FG 
START WITH FG.PARENT_ID = '299499'
CONNECT BY PRIOR FG.ID = FG.PARENT_ID
ORDER BY LEVEL DESC
(WHERE FG.ID )
```

start with 子句：遍历起始条件，

connect by 子句：连接条件。关键词prior，跟父类字段连接就向父节点遍历，parentid、id两列谁放在“=”前都无所谓，关键是prior跟谁在一起。上面就是向子类遍历。

order by 子句：排序，根据级别降序。



### update 多表关联更新

先看例子

```sql
select t.*, t.rowid from T1 t;
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708091836624.png)

```sql
select t.*, t.rowid from T2 t;
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708091956979.png)

**错误示范：**

```sql
update t1 set t1.money = (select t2.money from t2 where t2.name = t1.name);
```

**结果：**
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200708093109444.png)
因更新条件不够，可以看到name不相等的money为空了，所以要排除name不相等的情况，只更name相等的

#### 方法一：update

```sql
update t1 
   set t1.money = (select t2.money 
				     from t2 
				    where t2.name = t1.name
				   )
where exists (select 1 from t2 where t2.name = t1.name);
```

#### 方法二：内联视图更新

```sql
update (
        select t1.money money1,t2.money money2 from t1,t2 where t1.name = t2.name
       ) t
   set t.money1 = t.money2;
```

**注意：** 括号里通过关联两表建立一个视图，set中设置好更新的字段。这个解决方法比写法较直观且执行速度快。但表t2的主键一定要在where条件中，并且是以“=”来关联被更新表，否则报错误：ORA-01779: 无法修改与非键值保存表对应的列。

#### 方法三：merge into更新

```sql
    merge into t1
           using (select t2.name,t2.money from t2) t
              on (t.name = t1.name)
    when matched then 
    update  set t1.money = t.money;
```

#### 方法四：快速游标更新法

（参考：https://www.cnblogs.com/jingbf-BI/p/4909612.html）
begin for cr in (查询语句) loop –-循环 --更新语句（根据查询出来的结果集合） endloop; --结束循环 end; oracle支持快速游标，不需要定义直接把游标写到for循环中，这样就方便了我们批量更新数据。再加上oracle的rowid物理字段（oracle默认给每个表都有rowid这个字段，并且是唯一索引），可以快速定位到要更新的记录上。
例：

```sql
begin
for cr in (select a.rowid,b.join_state 
             from t_join_situation a,t_people_info b
            where a.people_number=b.people_number and
                  a.year='2011'and 
                  a.city_number='M00000'and 
                  a.town_number='M51000'
           )
loop
  update t_join_situation set join_state=cr.join_state where
  rowid = cr.rowid;
end loop;
end;
```

结论 方案 建议 标准update语法 单表更新或较简单的语句采用使用此方案更优。 inline view更新法 两表关联且被更新表通过关联表主键关联的，采用此方案更优。 merge更新法 两表关联且被更新表不是通过关联表主键关联的，采用此方案更优。 快速游标更新法 多表关联且逻辑复杂的，采用此方案更优。



### to_date

**TO_DATE格式(以时间:2007-11-02 13:45:25为例)**

**Year**:

> yy    two digits  两位年        显示值:07
>
> yyy   three digits 三位年        显示值:007
>
> yyyy  four digits 四位年        显示值:2007

**Month**:

> mm   number     两位月          显示值:11
>
> mon   abbreviated  字符集表示      显示值:11月,若是英文版,显示nov
>
> month spelled out  字符集表示      显示值:11月,若是英文版,显示november

**Day**:

> dd   number     当月第几天       显示值:02
>
> ddd  number     当年第几天       显示值:02
>
> dy   abbreviated  当周第几天简写   显示值:星期五,若是英文版,显示fri
>
> day  spelled out   当周第几天全写   显示值:星期五,若是英文版,显示friday 

**Hour**:

> hh    two digits   12小时进制    显示值:01
>
> hh24  two digits   24小时进制    显示值:13

**Minute**:

> mi  two digits   60进制      显示值:45

**Second**:

> ss  two digits   60进制      显示值:25

**其它**

> Q    digit      季度          显示值:4
>
> WW  digit      当年第几周     显示值:44
>
> W    digit      当月第几周     显示值:1

24小时格式下时间范围为： 0:00:00 - 23:59:59....

12小时格式下时间范围为： 1:00:00 - 12:59:59....



#### 1. 日期和字符转换函数用法（to_date,to_char）

```sql
select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') as nowTime from dual;   //日期转化为字符串  
select to_char(sysdate,'yyyy')  as nowYear   from dual;   //获取时间的年  
select to_char(sysdate,'mm')    as nowMonth  from dual;   //获取时间的月  
select to_char(sysdate,'dd')    as nowDay    from dual;   //获取时间的日  
select to_char(sysdate,'hh24')  as nowHour   from dual;   //获取时间的时  
select to_char(sysdate,'mi')    as nowMinute from dual;   //获取时间的分  
select to_char(sysdate,'ss')    as nowSecond from dual;   //获取时间的秒
```



#### 2. 字符串和时间互转

```sql
select to_date('2004-05-07 13:23:44','yyyy-mm-dd hh24:mi:ss') from dual
select to_char( to_date(222,'J'),'Jsp') from dual //显示Two Hundred Twenty-Two    
```



#### 3.求某天是星期几

```sql
select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day') from dual;     //星期一     
select to_char(to_date('2002-08-26','yyyy-mm-dd'),'day',
'NLS_DATE_LANGUAGE = American') from dual;   // monday   
//设置日期语言     
ALTER SESSION SET NLS_DATE_LANGUAGE='AMERICAN';     
//也可以这样     
TO_DATE ('2002-08-26', 'YYYY-mm-dd', 'NLS_DATE_LANGUAGE = American')    
```



#### 4. 两个日期间的天数

```sql
select floor(sysdate - to_date('20020405','yyyymmdd')) from dual;
```



#### 5. 时间为null的用法

```sql
select id, active_date from table1     
UNION    
select 1, TO_DATE(null) from dual;  //注意要用TO_DATE(null)
```



#### 6.月份差

```sql
a_date between to_date('20011201','yyyymmdd') and to_date('20011231','yyyymmdd')     
//那么12月31号中午12点之后和12月1号的12点之前是不包含在这个范围之内的。     
//所以，当时间需要精确的时候，觉得to_char还是必要的
```



#### 7. 日期格式冲突问题

输入的格式要看你安装的ORACLE字符集的类型, 比如: US7ASCII, date格式的类型就是: '01-Jan-01'

```sql
alter system set NLS_DATE_LANGUAGE = American     
alter session set NLS_DATE_LANGUAGE = American     
//或者在to_date中写     
select to_char(to_date('2002-08-26','yyyy-mm-dd'),
   'day','NLS_DATE_LANGUAGE = American') from dual;     
//注意我这只是举了NLS_DATE_LANGUAGE，当然还有很多，可查看     
select * from nls_session_parameters     
select * from V$NLS_PARAMETERS    
```



#### 8.查询特殊条件天数

```sql
select count(*)     
from ( select rownum-1 rnum     
   from all_objects     
   where rownum <= to_date('2002-02-28','yyyy-mm-dd') - to_date('2002-     
   02-01','yyyy-mm-dd')+1     
  )     
where to_char( to_date('2002-02-01','yyyy-mm-dd')+rnum-1, 'D' )     
    not in ( '1', '7' )     
//查找2002-02-28至2002-02-01间除星期一和七的天数     
//在前后分别调用DBMS_UTILITY.GET_TIME, 让后将结果相减(得到的是1/100秒, 而不是毫秒).    
```



#### 9. 查找月份

```sql
select months_between(to_date('01-31-1999','MM-DD-YYYY'),
to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;     
//结果为：1     
select months_between(to_date('02-01-1999','MM-DD-YYYY'),
to_date('12-31-1998','MM-DD-YYYY')) "MONTHS" FROM DUAL;     
//结果为：1.03225806451613
```



#### 10. Next_day的用法

```sql
Next_day(date, day)     
Monday-Sunday, for format code DAY     
Mon-Sun, for format code DY     
1-7, for format code D    
```



#### 11.获得小时数

```sql
//extract()找出日期或间隔值的字段值
SELECT EXTRACT(HOUR FROM TIMESTAMP '2001-02-16 2:38:40') from offer     
select sysdate ,to_char(sysdate,'hh') from dual;     
 
SYSDATE               TO_CHAR(SYSDATE,'HH')     
-------------------- ---------------------     
2003-10-13 19:35:21   07     
select sysdate ,to_char(sysdate,'hh24') from dual;     
 
SYSDATE               TO_CHAR(SYSDATE,'HH24')     
-------------------- -----------------------     
2003-10-13 19:35:21   19    
```



#### 12.年月日的处理

```sql
SELECT
  older_date,
  newer_date,
  years,
  months,
  ABS (
    TRUNC (
      newer_date - ADD_MONTHS (older_date, years * 12 + months)
    )
  ) days
FROM
  (
    SELECT
      TRUNC (
        MONTHS_BETWEEN (newer_date, older_date) / 12
      ) YEARS,
      MOD (
        TRUNC (
          MONTHS_BETWEEN (newer_date, older_date)
        ),
        12
      ) MONTHS,
      newer_date,
      older_date
    FROM
      (
        SELECT
          hiredate older_date,
          ADD_MONTHS (hiredate, ROWNUM) + ROWNUM newer_date
        FROM
          emp
      )
  )   
```



#### 13.处理月份天数不定的办法

```sql
select to_char(add_months(last_day(sysdate) +1, -2), 'yyyymmdd'),last_day(sysdate) from dual    
```



#### 14.找出今年的天数

```sql
select add_months(trunc(sysdate,'year'), 12) - trunc(sysdate,'year') from dual    
   //闰年的处理方法     
  to_char( last_day( to_date('02'    | | :year,'mmyyyy') ), 'dd' )     
   //如果是28就不是闰年 

```



#### 15.yyyy与rrrr的区别

```sql
   YYYY99  TO_C     
   ------- ----     
   yyyy 99 0099     
   rrrr 99 1999     
   yyyy 01 0001     
   rrrr 01 2001   
```



#### 16.不同时区的处理

```sql
 select to_char( NEW_TIME( sysdate, 'GMT','EST'), 'dd/mm/yyyy hh:mi:ss') ,
   sysdate   from dual;    
```



#### 17. 5秒钟一个间隔

```sql
   Select TO_DATE(FLOOR(TO_CHAR(sysdate,'SSSSS')/300) * 300,'SSSSS') ,
   TO_CHAR(sysdate,'SSSSS')   from dual    
   //2002-11-1 9:55:00 35786     
   //SSSSS表示5位秒数    
```



#### 18.一年的第几天

```sql
    select TO_CHAR(SYSDATE,'DDD'),sysdate from dual   
   //310  2002-11-6 10:03:51    
```



#### 19.计算小时,分,秒,毫秒

```sql
     SELECT
      Days,
      A,
      TRUNC (A * 24) Hours,
      TRUNC (A * 24 * 60 - 60 * TRUNC(A * 24)) Minutes,
      TRUNC (
        A * 24 * 60 * 60 - 60 * TRUNC (A * 24 * 60)
      ) Seconds,
      TRUNC (
        A * 24 * 60 * 60 * 100 - 100 * TRUNC (A * 24 * 60 * 60)
      ) mSeconds
    FROM
      (
        SELECT
          TRUNC (SYSDATE) Days,
          SYSDATE - TRUNC (SYSDATE) A
        FROM
          dual
      ) SELECT
        *
      FROM
        tabname
      ORDER BY
        DECODE (MODE, 'FIFO', 1 ,- 1) * TO_CHAR (rq, 'yyyymmddhh24miss')
      
   //   floor((date2-date1) /365) 作为年     
   //  floor((date2-date1, 365) /30) 作为月     
   //  d(mod(date2-date1, 365), 30)作为日.
 
```



#### 20.next_day函数

```sql
   //返回下个星期的日期,day为1-7或星期日-星期六,1表示星期日
   next_day(sysdate,6)是从当前开始下一个星期五。后面的数字是从星期日开始算起。     
   // 1  2  3  4  5  6  7     
   //日 一 二 三 四 五 六   
   select (sysdate-to_date('2003-12-03 12:55:45','yyyy-mm-dd hh24:mi:ss'))*24*60*60 from dual
   //日期 返回的是天 然后 转换为ss
```



#### 21,round[舍入到最接近的日期](day:舍入到最接近的星期日)

```sql
   select sysdate S1,
   round(sysdate) S2 ,
   round(sysdate,'year') YEAR,
   round(sysdate,'month') MONTH ,
   round(sysdate,'day') DAY from dual
```



#### 22,trunc[截断到最接近的日期,单位为天],返回的是日期类型

```sql
   select sysdate S1,                    
     trunc(sysdate) S2,                 //返回当前日期,无时分秒
     trunc(sysdate,'year') YEAR,        //返回当前年的1月1日,无时分秒
     trunc(sysdate,'month') MONTH ,     //返回当前月的1日,无时分秒
     trunc(sysdate,'day') DAY           //返回当前星期的星期天,无时分秒
   from dual
```



#### 23,返回日期列表中最晚日期

```sql
   select greatest('01-1月-04','04-1月-04','10-2月-04') from dual
```



#### 24.计算时间差

```sql
     注:oracle时间差是以天数为单位,所以换算成年月,日
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))/365) as spanYears from dual        //时间差-年
      select ceil(moths_between(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))) as spanMonths from dual           //时间差-月
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))) as spanDays from dual             //时间差-天
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24) as spanHours from dual         //时间差-时
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24*60) as spanMinutes from dual    //时间差-分
      select floor(to_number(sysdate-to_date('2007-11-02 15:55:03',
      'yyyy-mm-dd hh24:mi:ss'))*24*60*60) as spanSeconds from dual //时间差-秒
```



#### 25.更新时间

```sql
 //oracle时间加减是以天数为单位,设改变量为n,所以换算成年月,日
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
    to_char(sysdate+n*365,'yyyy-mm-dd hh24:mi:ss') as newTime from dual        //改变时间-年
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     add_months(sysdate,n) as newTime from dual                                 //改变时间-月
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n,'yyyy-mm-dd hh24:mi:ss') as newTime from dual            //改变时间-日
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24,'yyyy-mm-dd hh24:mi:ss') as newTime from dual         //改变时间-时
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual      //改变时间-分
 select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss'),
     to_char(sysdate+n/24/60/60,'yyyy-mm-dd hh24:mi:ss') as newTime from dual   //改变时间-秒
```



#### 26.查找月的第一天,最后一天

```sql
     SELECT Trunc(Trunc(SYSDATE, 'MONTH') - 1, 'MONTH') First_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') - 1 / 86400 Last_Day_Last_Month,
       Trunc(SYSDATE, 'MONTH') First_Day_Cur_Month,
       LAST_DAY(Trunc(SYSDATE, 'MONTH')) + 1 - 1 / 86400 Last_Day_Cur_Month
   FROM dual;
```



### wm_concat

```sql
select id，wm_concat(name) from tab1 group by id;
```

![下载](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202208171132486.png)

说明：wm_concat 用于纵转成一横非常有用，然后每个数据之间自动打上逗号，
但wm_concat 参数有一个限制，参数字符串不能超过30k字节，如果超过30k，就需要手工写一个函数了

## 附件

### 数据类型

![image-20220609200341416](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220609200341416.png)



### 日期时间

| 字母 | 日期或时间元素   | 表示              | 示例                                  |
| :--- | :--------------- | :---------------- | :------------------------------------ |
| G    | Era标志符        | Text              | AD                                    |
| y    | 年               | Year              | 1996; 96                              |
| M    | 年中的月份       | Month             | July; Jul;07                          |
| w    | 年中的周数       | Number            | 27                                    |
| W    | 月份中的周数     | Number            | 2                                     |
| D    | 年中的天数       | Number            | 189                                   |
| d    | 月份中的天数     | Number            | 10                                    |
| F    | 月份中的星期     | Number            | 2                                     |
| E    | 星期中的天数     | Text              | Tuesday; Tue                          |
| a    | Am/pm 标记       | Text              | PM                                    |
| H    | 一天中的小时数   | （0-23）          | Number 0                              |
| k    | 一天中的小时数   | （1-24）          | Number 24                             |
| K    | am/pm 中的小时数 | （0-11）          | Number 0                              |
| h    | am/pm 中的小时数 | （1-12）          | Number 12                             |
| m    | 小时中的分钟数   | Number            | 30                                    |
| s    | 分钟中的秒数     | Number            | 55                                    |
| S    | 毫秒数           | Number            | 978                                   |
| z    | 时区             | General time zone | Pacific Standard Time; PST; GMT-08:00 |
| Z    | 时区             | RFC 822 time zone | -0800                                 |



## 问题

### ORA-00001

```
违反唯一约束条件
```

### Impdp 导入报错



```
高版本向低版本导入必须加版本号
dmp文件放到创建的directory=dmp_dir 路径下
```



### ORA-01940 无法删除当前已连接的用户



首先查询一下数据中有没有用户在使用

```sh
select username,sid,serial#,paddr from v$session where username='ECITY';
USERNAME                                    SID    SERIAL# PADDR
------------------------------ ---------- -------------------------------------------------
ECITY                                       634        7   00000000C028D198
SQL> select PROGRAM from v$process where addr='00000000C028D198';
PROGRAM
----------------------------------------------------------------------------------------------------------
Oracle@oradb01 (DW00)
```

其次杀掉系统中的这个进程

```sh
SQL> alter system kill session '634,7';
System altered.
```

然后执行删除操作，即可完成

```sh
SQL> select saddr,sid,serial#,paddr,username,status from v$session where username is not null;
SQL> drop user ecity CASCADE;
User dropped.
```

问题解决，记得KILL进程前，先看看是啥进程，哪台机连过来的，能否KILL等等。避免杀掉其他进程



### ORA-28547

> 连接工具⾃带的oci.dll并不⽀持oracle版本，需要去官⽹下载⽀持的版本。
>
> 1. 先⽤你的IDEA或者别⼈的连接到oracle数据库（为了查询版本）
> 1.1 查询版本SQL：select * from v$version;
> 1.2 IDEA连接Oracle数据库（会的跳到下⼀步）
> 2. 去oracle下载对应的oci.dll⽂件
> 6. 重启连接工具并连接Oracl



### 分页有重复数据

> **在Oracle数据库中进行分页查询时，如果排序条件所对应的字段值有大量重复或为空时，则可能会出现分页数据重复的情况。如果要避免出现分页数据重复的情况，则一定要在排序时加上唯一值字段为排序条件。**









# Extjs

## 一、字段注释

### 1、 region

页面布局：主要使用一个控件panel，感觉就相当于HTML中的div。位置通过region来设置。

有一点需要注意的是，使用region来布局，位置一定要设成north→center→south或者west→center→east。如果只有两块就west→center而不能westr→east。 

```
region: 'west',//自动排位置
```

### 2、显示边界

```js
style:'border:1px solid #3399ff;',
```

### 3、表格自动产生序号

```js
new Ext.grid.RowNumberer({width:30}),
```

### 4、设置表单项不可用

```js
disabled:true,
```

### 5、嵌套表头

```js
var zqsd_item = [
    { header: '期限',align : 'left',width : 100,sortable : true,dataIndex: 'email' },
    {
        text:"滚动情景",
        columns:[
            { header: '轻度',align : 'left',width : 100,sortable : true,dataIndex: 'email'},
            { header: '中度',align : 'left',width : 100,sortable : true,dataIndex: 'email'},
            { header: '重度',align : 'left',width : 100,sortable : true,dataIndex: 'email'}
        ]
    }
]
```

### 6、自动高度

```js
autoHeight : 'true',
```

### 7、表格分组

```js
// 1) store 里声明分组字段
groupField: 'name',
// 2) store 通过不同的 name 区分

// 3）Panel 定义添加 features 属性
    var gmjjGrid =new Ext.grid.Panel({
        id:functionName+"gmjjGrid",
        autoHeight : 'true',
        region:'center',
        layout: 'fit',
        bufferedRenderer : false,//防止grid刷新出现空白
        border: true,
        columns: zqsd_item,
        features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: "{name}",
            startCollapsed:false,// 分组后，所有组是展开还是收缩
            hideGroupedHeader: true,
            enableGroupingMenu: false
        }],
        store:simpsonsStore
    });
```

### 8、使用新的store/columns重新生成grid

```js
reconfigure( Ext.data.Store store, Object[] columns )
//使用一个新的store/columns配置项进行重新配置生成grid。 如果你不想改变store或者columns，他们任意一项均可以省略。
```



### 9、动态设置store的值

```js
zqStore.removeAll();
zqStore.getProxy().data  = jsonObj.dblist;
zqStore.reload();
```



### 10、reset（）

**重置表单项的当前值为最初加载的值并清除验证信息**



### 11、setDisabled（）



```js
setDisabled( Boolean disabled )
设置使用当前action的所有组件的禁用状态。 enable 和 disable 的快捷方式。

Parameters
disabled : Boolean
True 将禁用组件, false 将启用它
```



### 12、日期选择框

```js
//字段定义
xtype : "datefield",
value:sys.datetime
//取值
var scene_date = Ext.getCmp(functionName+'sceneDate').getRawValue();
```



# Node.js





![nodejs](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/nodejs.jpg)

简单的说 Node.js 就是运行在服务端的 JavaScript。

Node.js 是一个基于Chrome JavaScript 运行时建立的一个平台。

Node.js是一个事件驱动I/O服务端JavaScript环境，基于Google的V8引擎，V8引擎执行Javascript的速度非常快，性能非常好。





## NPM 使用介绍



NPM是随同NodeJS一起安装的包管理工具，能解决NodeJS代码部署上的很多问题，常见的使用场景有以下几种：

- 允许用户从NPM服务器下载别人编写的第三方包到本地使用。
- 允许用户从NPM服务器下载并安装别人编写的命令行程序到本地使用。
- 允许用户将自己编写的包或命令行程序上传到NPM服务器供别人使用。

由于新版的nodejs已经集成了npm，所以之前npm也一并安装好了。同样可以通过输入 **"npm -v"** 来测试是否成功安装。命令如下，出现版本提示表示安装成功:

```
$ npm -v
2.3.0
```

如果你安装的是旧版本的 npm，可以很容易得通过 npm 命令来升级，命令如下：

```
$ sudo npm install npm -g
/usr/local/bin/npm -> /usr/local/lib/node_modules/npm/bin/npm-cli.js
npm@2.14.2 /usr/local/lib/node_modules/npm
```

如果是 Window 系统使用以下命令即可：

```
npm install npm -g
```

> 使用淘宝镜像的命令：
>
> ```
> npm install -g cnpm --registry=https://registry.npm.taobao.org
> ```

------

### 使用 npm 命令安装模块

npm 安装 Node.js 模块语法格式如下：

```
$ npm install <Module Name>
```

以下实例，我们使用 npm 命令安装常用的 Node.js web框架模块 **express**:

```
$ npm install express
```

安装好之后，express 包就放在了工程目录下的 node_modules 目录中，因此在代码中只需要通过 **require('express')** 的方式就好，无需指定第三方包路径。

```
var express = require('express');
```

------

### 全局安装与本地安装

npm 的包安装分为本地安装（local）、全局安装（global）两种，从敲的命令行来看，差别只是有没有-g而已，比如

```
npm install express          # 本地安装
npm install express -g   # 全局安装
```

如果出现以下错误：

```
npm err! Error: connect ECONNREFUSED 127.0.0.1:8087 
```

解决办法为：

```
$ npm config set proxy null
```

#### 本地安装

- \1. 将安装包放在 ./node_modules 下（运行 npm 命令时所在的目录），如果没有 node_modules 目录，会在当前执行 npm 命令的目录下生成 node_modules 目录。
- \2. 可以通过 require() 来引入本地安装的包。

#### 全局安装

- \1. 将安装包放在 /usr/local 下或者你 node 的安装目录。
- \2. 可以直接在命令行里使用。

如果你希望具备两者功能，则需要在两个地方安装它或使用 **npm link**。

接下来我们使用全局方式安装 express

```
$ npm install express -g
```

安装过程输出如下内容，第一行输出了模块的版本号及安装位置。



## Nodejs 服务后台常驻



### 一、利用 forever



forever是一个nodejs守护进程，完全由命令行操控。forever会监控nodejs服务，并在服务挂掉后进行重启。

1、安装 forever

```shell
npm install forever -g
```

2、启动服务

```shell
service forever start
```

3、使用 forever 启动 js 文件

```shell
forever start index.js
```

4、停止 js 文件

```shell
forever stop index.js
```

5、启动js文件并输出日志文件

```shell
forever start -l forever.log -o out.log -e err.log index.js
```

6、重启js文件

```shell
forever restart index.js
```

7、查看正在运行的进程

```shell
forever list
```



### 二、PM2



pm2是一个进程管理工具,可以用它来管理你的node进程,并查看node进程的状态,当然也支持性能监控,进程守护,负载均衡等功能

```shell
npm install -g pm2
pm2 start app.js        // 启动
pm2 start app.js -i max //启动 使用所有CPU核心的集群
pm2 stop app.js         // 停止
pm2 stop all            // 停止所有
pm2 restart app.js      // 重启
pm2 restart all         // 重启所有
pm2 delete  app.js      // 关闭
```

### 三、nohub



nodejs 自带node.js自带服务nohub，不需要安装别的包。
缺点：存在无法查询日志等问题,关闭终端后服务也就关闭了， 经测试是这样的。

```shell
nohup node ***.js &
```





## 问题







# Spring Boot

## **一、**Spring Boot 入门

### 1、Spring Boot 简介

> 简化Spring应用开发的一个框架；
>
> 整个Spring技术栈的一个大整合；
>
> J2EE开发的一站式解决方案；

### 2、微服务

2014，martin fowler

微服务：架构风格（服务微化）

一个应用应该是一组小型服务；可以通过HTTP的方式进行互通；

单体应用：ALL IN ONE

微服务：每一个功能元素最终都是一个可独立替换和独立升级的软件单元；

[详细参照微服务文档](https://martinfowler.com/articles/microservices.html#MicroservicesAndSoa)



### 3、环境准备

http://www.gulixueyuan.com/ 谷粒学院

环境约束

> –jdk1.8：Spring Boot 推荐jdk1.7及以上；java version "1.8.0_112"
>
> –maven3.x：maven 3.3以上版本；Apache Maven 3.3.9
>
> –IntelliJIDEA2017：IntelliJ IDEA 2017.2.2 x64、STS
>
> –SpringBoot 1.5.9.RELEASE：1.5.9；

统一环境；



#### 1、MAVEN设置；

给maven 的settings.xml配置文件的profiles标签添加

```xml
<profile>
  <id>jdk-1.8</id>
  <activation>
    <activeByDefault>true</activeByDefault>
    <jdk>1.8</jdk>
  </activation>
  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
  </properties>
</profile>
```

#### 2、IDEA设置

整合maven进来；

![idea设置](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129151045.png)



![images/](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129151112.png)

### 4、Spring Boot HelloWorld

一个功能：

浏览器发送hello请求，服务器接受请求并处理，响应Hello World字符串；



#### 1、创建一个maven工程；（jar）

#### 2、导入spring boot相关的依赖

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
    </parent>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
```

#### 3、编写一个主程序；启动Spring Boot应用

```java
/**
 *  @SpringBootApplication 来标注一个主程序类，说明这是一个Spring Boot应用
 */
@SpringBootApplication
public class HelloWorldMainApplication {

    public static void main(String[] args) {

        // Spring应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class,args);
    }
}
```

#### 4、编写相关的Controller、Service

```java
@Controller
public class HelloController {

    @ResponseBody
    @RequestMapping("/hello")
    public String hello(){
        return "Hello World!";
    }
}

```



#### 5、运行主程序测试

#### 6、简化部署

```xml
 <!-- 这个插件，可以将应用打包成一个可执行的jar包；-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
```

将这个应用打成jar包，直接使用java -jar的命令进行执行；

### 5、Hello World探究

#### 1、POM文件

##### 1、父项目

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>1.5.9.RELEASE</version>
</parent>

他的父项目是
<parent>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-dependencies</artifactId>
  <version>1.5.9.RELEASE</version>
  <relativePath>../../spring-boot-dependencies</relativePath>
</parent>
他来真正管理Spring Boot应用里面的所有依赖版本；

```

Spring Boot的版本仲裁中心；

以后我们导入依赖默认是不需要写版本；（没有在dependencies里面管理的依赖自然需要声明版本号）

##### 2、启动器

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

**spring-boot-starter**-==web==：

​	spring-boot-starter：spring-boot场景启动器；帮我们导入了web模块正常运行所依赖的组件；



Spring Boot将所有的功能场景都抽取出来，做成一个个的starters（启动器），只需要在项目里面引入这些starter相关场景的所有依赖都会导入进来。要用什么功能就导入什么场景的启动器



#### 2、主程序类，主入口类

```java
/**
 *  @SpringBootApplication 来标注一个主程序类，说明这是一个Spring Boot应用
 */
@SpringBootApplication
public class HelloWorldMainApplication {

    public static void main(String[] args) {

        // Spring应用启动起来
        SpringApplication.run(HelloWorldMainApplication.class,args);
    }
}

```

@**SpringBootApplication**:    Spring Boot应用标注在某个类上说明这个类是SpringBoot的主配置类，SpringBoot就应该运行这个类的main方法来启动SpringBoot应用；



```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(excludeFilters = {
      @Filter(type = FilterType.CUSTOM, classes = TypeExcludeFilter.class),
      @Filter(type = FilterType.CUSTOM, classes = AutoConfigurationExcludeFilter.class) })
public @interface SpringBootApplication {
```

@**SpringBootConfiguration**:Spring Boot的配置类；

​		标注在某个类上，表示这是一个Spring Boot的配置类；

​		@**Configuration**:配置类上来标注这个注解；

​			配置类 -----  配置文件；配置类也是容器中的一个组件；@Component



@**EnableAutoConfiguration**：开启自动配置功能；

​		以前我们需要配置的东西，Spring Boot帮我们自动配置；@**EnableAutoConfiguration**告诉SpringBoot开启自动配置功能；这样自动配置才能生效；

```java
@AutoConfigurationPackage
@Import(EnableAutoConfigurationImportSelector.class)
public @interface EnableAutoConfiguration {
```

​      	@**AutoConfigurationPackage**：自动配置包

​		@**Import**(AutoConfigurationPackages.Registrar.class)：

​		Spring的底层注解@Import，给容器中导入一个组件；导入的组件由AutoConfigurationPackages.Registrar.class；

==将主配置类（@SpringBootApplication标注的类）的所在包及下面所有子包里面的所有组件扫描到Spring容器；==

​	@**Import**(EnableAutoConfigurationImportSelector.class)；

​		给容器中导入组件？

​		**EnableAutoConfigurationImportSelector**：导入哪些组件的选择器；

​		将所有需要导入的组件以全类名的方式返回；这些组件就会被添加到容器中；

​		会给容器中导入非常多的自动配置类（xxxAutoConfiguration）；就是给容器中导入这个场景需要的所有组件，并配置好这些组件；		![自动配置类](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180129224104.png)

有了自动配置类，免去了我们手动编写配置注入功能组件等的工作；

​		SpringFactoriesLoader.loadFactoryNames(EnableAutoConfiguration.class,classLoader)；



==Spring Boot在启动的时候从类路径下的META-INF/spring.factories中获取EnableAutoConfiguration指定的值，将这些值作为自动配置类导入到容器中，自动配置类就生效，帮我们进行自动配置工作；==以前我们需要自己配置的东西，自动配置类都帮我们；

J2EE的整体整合解决方案和自动配置都在spring-boot-autoconfigure-1.5.9.RELEASE.jar；



​		

==Spring注解版（谷粒学院）==



### 6、使用Spring Initializer快速创建Spring Boot项目

#### 1、IDEA：使用 Spring Initializer快速创建项目

IDE都支持使用Spring的项目创建向导快速创建一个Spring Boot项目；

选择我们需要的模块；向导会联网创建Spring Boot项目；

默认生成的Spring Boot项目；

- 主程序已经生成好了，我们只需要我们自己的逻辑
- resources文件夹中目录结构
  - static：保存所有的静态资源； js css  images；
  - templates：保存所有的模板页面；（Spring Boot默认jar包使用嵌入式的Tomcat，默认不支持JSP页面）；可以使用模板引擎（freemarker、thymeleaf）；
  - application.properties：Spring Boot应用的配置文件；可以修改一些默认设置；

#### 2、STS使用 Spring Starter Project快速创建项目



-------------



## 二、配置文件

### 1、配置文件

SpringBoot使用一个全局的配置文件，配置文件名是固定的；

•application.properties

•application.yml



配置文件的作用：修改SpringBoot自动配置的默认值；SpringBoot在底层都给我们自动配置好；



YAML（YAML Ain't Markup Language）

​	YAML  A Markup Language：是一个标记语言

​	YAML   isn't Markup Language：不是一个标记语言；

标记语言：

​	以前的配置文件；大多都使用的是  **xxxx.xml**文件；

​	YAML：**以数据为中心**，比json、xml等更适合做配置文件；

​	YAML：配置例子

```yaml
server:
  port: 8081
```

​	XML：

```xml
<server>
	<port>8081</port>
</server>
```



### 2、YAML语法：

#### 1、基本语法

k:(空格)v：表示一对键值对（空格必须有）；

以**空格**的缩进来控制层级关系；只要是左对齐的一列数据，都是同一个层级的

```yaml
server:
    port: 8081
    path: /hello
```

属性和值也是大小写敏感；



#### 2、值的写法

##### 字面量：普通的值（数字，字符串，布尔）

​	k: v：字面直接来写；

​		字符串默认不用加上单引号或者双引号；

​		""：双引号；不会转义字符串里面的特殊字符；特殊字符会作为本身想表示的意思

​				name:   "zhangsan \n lisi"：输出；zhangsan 换行  lisi

​		''：单引号；会转义特殊字符，特殊字符最终只是一个普通的字符串数据

​				name:   ‘zhangsan \n lisi’：输出；zhangsan \n  lisi



##### 对象、Map（属性和值）（键值对）：

​	k: v：在下一行来写对象的属性和值的关系；注意缩进

​		对象还是k: v的方式

```yaml
friends:
		lastName: zhangsan
		age: 20
```

行内写法：

```yaml
friends: {lastName: zhangsan,age: 18}
```



##### 数组（List、Set）：

用- 值表示数组中的一个元素

```yaml
pets:
 - cat
 - dog
 - pig
```

行内写法

```yaml
pets: [cat,dog,pig]
```



### 3、配置文件值注入

配置文件

```yaml
person:    lastName: hello    age: 18    boss: false    birth: 2017/12/12    maps: {k1: v1,k2: 12}    lists:      - lisi      - zhaoliu    dog:      name: 小狗      age: 12
```

javaBean：

```java
/**
 * 将配置文件中配置的每一个属性的值，映射到这个组件中
 * @ConfigurationProperties：告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定；
 *      prefix = "person"：配置文件中哪个下面的所有属性进行一一映射
 *
 * 只有这个组件是容器中的组件，才能容器提供的@ConfigurationProperties功能；
 *
 */
@Component
@ConfigurationProperties(prefix = "person")
public class Person {

    private String lastName;
    private Integer age;
    private Boolean boss;
    private Date birth;

    private Map<String,Object> maps;
    private List<Object> lists;
    private Dog dog;

```



我们可以导入配置文件处理器，以后编写配置就有提示了

```xml
<!--导入配置文件处理器，配置文件进行绑定就会有提示-->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>
```

#### 1、properties配置文件在idea中默认utf-8可能会乱码

调整

![idea配置乱码](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180130161620.png)

#### 2、@Value获取值和@ConfigurationProperties获取值比较

|                      | @ConfigurationProperties | @Value     |
| -------------------- | ------------------------ | ---------- |
| 功能                 | 批量注入配置文件中的属性 | 一个个指定 |
| 松散绑定（松散语法） | 支持                     | 不支持     |
| SpEL                 | 不支持                   | 支持       |
| JSR303数据校验       | 支持                     | 不支持     |
| 复杂类型封装         | 支持                     | 不支持     |

配置文件yml还是properties他们都能获取到值；

如果说，我们只是在某个业务逻辑中需要获取一下配置文件中的某项值，使用@Value；

如果说，我们专门编写了一个javaBean来和配置文件进行映射，我们就直接使用@ConfigurationProperties；



#### 3、配置文件注入值数据校验

```java
@Component@ConfigurationProperties(prefix = "person")@Validatedpublic class Person {    /**     * <bean class="Person">     *      <property name="lastName" value="字面量/${key}从环境变量、配置文件中获取值/#{SpEL}"></property>     * <bean/>     */   //lastName必须是邮箱格式    @Email    //@Value("${person.last-name}")    private String lastName;    //@Value("#{11*2}")    private Integer age;    //@Value("true")    private Boolean boss;    private Date birth;    private Map<String,Object> maps;    private List<Object> lists;    private Dog dog;
```



#### 4、@PropertySource&@ImportResource&@Bean

@**PropertySource**：加载指定的配置文件；

```java
/**
 * 将配置文件中配置的每一个属性的值，映射到这个组件中
 * @ConfigurationProperties：告诉SpringBoot将本类中的所有属性和配置文件中相关的配置进行绑定；
 *      prefix = "person"：配置文件中哪个下面的所有属性进行一一映射
 *
 * 只有这个组件是容器中的组件，才能容器提供的@ConfigurationProperties功能；
 *  @ConfigurationProperties(prefix = "person")默认从全局配置文件中获取值；
 *
 */
@PropertySource(value = {"classpath:person.properties"})
@Component
@ConfigurationProperties(prefix = "person")
//@Validated
public class Person {

    /**
     * <bean class="Person">
     *      <property name="lastName" value="字面量/${key}从环境变量、配置文件中获取值/#{SpEL}"></property>
     * <bean/>
     */

   //lastName必须是邮箱格式
   // @Email
    //@Value("${person.last-name}")
    private String lastName;
    //@Value("#{11*2}")
    private Integer age;
    //@Value("true")
    private Boolean boss;

```



@**ImportResource**：导入Spring的配置文件，让配置文件里面的内容生效；

Spring Boot里面没有Spring的配置文件，我们自己编写的配置文件，也不能自动识别；

想让Spring的配置文件生效，加载进来；@**ImportResource**标注在一个配置类上

```java
@ImportResource(locations = {"classpath:beans.xml"})
导入Spring的配置文件让其生效
```



不来编写Spring的配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">


    <bean id="helloService" class="com.atguigu.springboot.service.HelloService"></bean>
</beans>
```

SpringBoot推荐给容器中添加组件的方式；推荐使用全注解的方式

1、配置类**@Configuration**------>Spring配置文件

2、使用**@Bean**给容器中添加组件

```java
/** * @Configuration：指明当前类是一个配置类；就是来替代之前的Spring配置文件 * * 在配置文件中用<bean><bean/>标签添加组件 * */@Configurationpublic class MyAppConfig {    //将方法的返回值添加到容器中；容器中这个组件默认的id就是方法名    @Bean    public HelloService helloService02(){        System.out.println("配置类@Bean给容器中添加组件了...");        return new HelloService();    }}
```

##4、配置文件占位符

##### 1、随机数

```java
${random.value}、${random.int}、${random.long}${random.int(10)}、${random.int[1024,65536]}
```



##### 2、占位符获取之前配置的值，如果没有可以是用:指定默认值

```properties
person.last-name=张三${random.uuid}
person.age=${random.int}
person.birth=2017/12/15
person.boss=false
person.maps.k1=v1
person.maps.k2=14
person.lists=a,b,c
person.dog.name=${person.hello:hello}_dog
person.dog.age=15
```



### 5、Profile

#### 1、多Profile文件

我们在主配置文件编写的时候，文件名可以是   application-{profile}.properties/yml

默认使用application.properties的配置；



#### 2、yml支持多文档块方式

```yml
server:
  port: 8081
spring:
  profiles:
    active: prod

---
server:
  port: 8083
spring:
  profiles: dev


---

server:
  port: 8084
spring:
  profiles: prod  #指定属于哪个环境
```





#### 3、激活指定profile

​	1、在配置文件中指定  spring.profiles.active=dev

​	2、命令行：

​		java -jar spring-boot-02-config-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev；

​		可以直接在测试的时候，配置传入命令行参数

​	3、虚拟机参数；

​		-Dspring.profiles.active=dev



### 6、配置文件加载位置

springboot 启动会扫描以下位置的application.properties或者application.yml文件作为Spring boot的默认配置文件

–file:./config/

–file:./

–classpath:/config/

–classpath:/

优先级由高到底，高优先级的配置会覆盖低优先级的配置；

SpringBoot会从这四个位置全部加载主配置文件；**互补配置**；



==我们还可以通过spring.config.location来改变默认的配置文件位置==

**项目打包好以后，我们可以使用命令行参数的形式，启动项目的时候来指定配置文件的新位置；指定配置文件和默认加载的这些配置文件共同起作用形成互补配置；**

java -jar spring-boot-02-config-02-0.0.1-SNAPSHOT.jar --spring.config.location=G:/application.properties

### 7、外部配置加载顺序

**==SpringBoot也可以从以下位置加载配置； 优先级从高到低；高优先级的配置覆盖低优先级的配置，所有的配置会形成互补配置==**

**1.命令行参数**

所有的配置都可以在命令行上进行指定

java -jar spring-boot-02-config-02-0.0.1-SNAPSHOT.jar --server.port=8087  --server.context-path=/abc

多个配置用空格分开； --配置项=值



2.来自java:comp/env的JNDI属性

3.Java系统属性（System.getProperties()）

4.操作系统环境变量

5.RandomValuePropertySource配置的random.*属性值



==**由jar包外向jar包内进行寻找；**==

==**优先加载带profile**==

**6.jar包外部的application-{profile}.properties或application.yml(带spring.profile)配置文件**

**7.jar包内部的application-{profile}.properties或application.yml(带spring.profile)配置文件**



==**再来加载不带profile**==

**8.jar包外部的application.properties或application.yml(不带spring.profile)配置文件**

**9.jar包内部的application.properties或application.yml(不带spring.profile)配置文件**



10.@Configuration注解类上的@PropertySource

11.通过SpringApplication.setDefaultProperties指定的默认属性

所有支持的配置加载来源；

[参考官方文档](https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#boot-features-external-config)

### 8、自动配置原理

配置文件到底能写什么？怎么写？自动配置原理；

[配置文件能配置的属性参照](https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/htmlsingle/#common-application-properties)



#### 1、**自动配置原理：**

1）、SpringBoot启动的时候加载主配置类，开启了自动配置功能 ==@EnableAutoConfiguration==

**2）、@EnableAutoConfiguration 作用：**

 - 利用EnableAutoConfigurationImportSelector给容器中导入一些组件？

- 可以查看selectImports()方法的内容；

- List<String> configurations = getCandidateConfigurations(annotationMetadata,      attributes);获取候选的配置

  - ```java
    SpringFactoriesLoader.loadFactoryNames()
    扫描所有jar包类路径下  META-INF/spring.factories
    把扫描到的这些文件的内容包装成properties对象
    从properties中获取到EnableAutoConfiguration.class类（类名）对应的值，然后把他们添加在容器中
    
    ```

    

**==将 类路径下  META-INF/spring.factories 里面配置的所有EnableAutoConfiguration的值加入到了容器中；==**

```properties
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration,\
org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration,\
org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration,\
org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration,\
org.springframework.boot.autoconfigure.cloud.CloudAutoConfiguration,\
org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration,\
org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration,\
org.springframework.boot.autoconfigure.couchbase.CouchbaseAutoConfiguration,\
org.springframework.boot.autoconfigure.dao.PersistenceExceptionTranslationAutoConfiguration,\
org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.cassandra.CassandraRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.couchbase.CouchbaseDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.couchbase.CouchbaseRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.ldap.LdapDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.ldap.LdapRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.neo4j.Neo4jDataAutoConfiguration,\
org.springframework.boot.autoconfigure.data.neo4j.Neo4jRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.solr.SolrRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration,\
org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration,\
org.springframework.boot.autoconfigure.elasticsearch.jest.JestAutoConfiguration,\
org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration,\
org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration,\
org.springframework.boot.autoconfigure.h2.H2ConsoleAutoConfiguration,\
org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration,\
org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration,\
org.springframework.boot.autoconfigure.hazelcast.HazelcastJpaDependencyAutoConfiguration,\
org.springframework.boot.autoconfigure.info.ProjectInfoAutoConfiguration,\
org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration,\
org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.JndiDataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.XADataSourceAutoConfiguration,\
org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration,\
org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.JndiConnectionFactoryAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.activemq.ActiveMQAutoConfiguration,\
org.springframework.boot.autoconfigure.jms.artemis.ArtemisAutoConfiguration,\
org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration,\
org.springframework.boot.autoconfigure.groovy.template.GroovyTemplateAutoConfiguration,\
org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration,\
org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration,\
org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration,\
org.springframework.boot.autoconfigure.ldap.embedded.EmbeddedLdapAutoConfiguration,\
org.springframework.boot.autoconfigure.ldap.LdapAutoConfiguration,\
org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration,\
org.springframework.boot.autoconfigure.mail.MailSenderAutoConfiguration,\
org.springframework.boot.autoconfigure.mail.MailSenderValidatorAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.DeviceResolverAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.DeviceDelegatingViewResolverAutoConfiguration,\
org.springframework.boot.autoconfigure.mobile.SitePreferenceAutoConfiguration,\
org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration,\
org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration,\
org.springframework.boot.autoconfigure.mustache.MustacheAutoConfiguration,\
org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration,\
org.springframework.boot.autoconfigure.reactor.ReactorAutoConfiguration,\
org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration,\
org.springframework.boot.autoconfigure.security.SecurityFilterAutoConfiguration,\
org.springframework.boot.autoconfigure.security.FallbackWebSecurityAutoConfiguration,\
org.springframework.boot.autoconfigure.security.oauth2.OAuth2AutoConfiguration,\
org.springframework.boot.autoconfigure.sendgrid.SendGridAutoConfiguration,\
org.springframework.boot.autoconfigure.session.SessionAutoConfiguration,\
org.springframework.boot.autoconfigure.social.SocialWebAutoConfiguration,\
org.springframework.boot.autoconfigure.social.FacebookAutoConfiguration,\
org.springframework.boot.autoconfigure.social.LinkedInAutoConfiguration,\
org.springframework.boot.autoconfigure.social.TwitterAutoConfiguration,\
org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration,\
org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration,\
org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration,\
org.springframework.boot.autoconfigure.transaction.jta.JtaAutoConfiguration,\
org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration,\
org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration,\
org.springframework.boot.autoconfigure.web.EmbeddedServletContainerAutoConfiguration,\
org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.web.HttpEncodingAutoConfiguration,\
org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration,\
org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration,\
org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration,\
org.springframework.boot.autoconfigure.web.WebClientAutoConfiguration,\
org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration,\
org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration,\
org.springframework.boot.autoconfigure.websocket.WebSocketMessagingAutoConfiguration,\
org.springframework.boot.autoconfigure.webservices.WebServicesAutoConfiguration
```

每一个这样的  xxxAutoConfiguration类都是容器中的一个组件，都加入到容器中；用他们来做自动配置；

3）、每一个自动配置类进行自动配置功能；

4）、以**HttpEncodingAutoConfiguration（Http编码自动配置）**为例解释自动配置原理；

```java
@Configuration   //表示这是一个配置类，以前编写的配置文件一样，也可以给容器中添加组件@EnableConfigurationProperties(HttpEncodingProperties.class)  //启动指定类的ConfigurationProperties功能；将配置文件中对应的值和HttpEncodingProperties绑定起来；并把HttpEncodingProperties加入到ioc容器中@ConditionalOnWebApplication //Spring底层@Conditional注解（Spring注解版），根据不同的条件，如果满足指定的条件，整个配置类里面的配置就会生效；    判断当前应用是否是web应用，如果是，当前配置类生效@ConditionalOnClass(CharacterEncodingFilter.class)  //判断当前项目有没有这个类CharacterEncodingFilter；SpringMVC中进行乱码解决的过滤器；@ConditionalOnProperty(prefix = "spring.http.encoding", value = "enabled", matchIfMissing = true)  //判断配置文件中是否存在某个配置  spring.http.encoding.enabled；如果不存在，判断也是成立的//即使我们配置文件中不配置pring.http.encoding.enabled=true，也是默认生效的；public class HttpEncodingAutoConfiguration {    	//他已经和SpringBoot的配置文件映射了  	private final HttpEncodingProperties properties;     //只有一个有参构造器的情况下，参数的值就会从容器中拿  	public HttpEncodingAutoConfiguration(HttpEncodingProperties properties) {		this.properties = properties;	}      @Bean   //给容器中添加一个组件，这个组件的某些值需要从properties中获取	@ConditionalOnMissingBean(CharacterEncodingFilter.class) //判断容器没有这个组件？	public CharacterEncodingFilter characterEncodingFilter() {		CharacterEncodingFilter filter = new OrderedCharacterEncodingFilter();		filter.setEncoding(this.properties.getCharset().name());		filter.setForceRequestEncoding(this.properties.shouldForce(Type.REQUEST));		filter.setForceResponseEncoding(this.properties.shouldForce(Type.RESPONSE));		return filter;	}
```

根据当前不同的条件判断，决定这个配置类是否生效？

一但这个配置类生效；这个配置类就会给容器中添加各种组件；这些组件的属性是从对应的properties类中获取的，这些类里面的每一个属性又是和配置文件绑定的；







5）、所有在配置文件中能配置的属性都是在xxxxProperties类中封装者‘；配置文件能配置什么就可以参照某个功能对应的这个属性类

```java
@ConfigurationProperties(prefix = "spring.http.encoding")  //从配置文件中获取指定的值和bean的属性进行绑定
public class HttpEncodingProperties {

   public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
```





**精髓：**

​	**1）、SpringBoot启动会加载大量的自动配置类**

​	**2）、我们看我们需要的功能有没有SpringBoot默认写好的自动配置类；**

​	**3）、我们再来看这个自动配置类中到底配置了哪些组件；（只要我们要用的组件有，我们就不需要再来配置了）**

​	**4）、给容器中自动配置类添加组件的时候，会从properties类中获取某些属性。我们就可以在配置文件中指定这些属性的值；**



xxxxAutoConfigurartion：自动配置类；

给容器中添加组件

xxxxProperties:封装配置文件中相关属性；



#### 2、细节



##### 1、@Conditional派生注解（Spring注解版原生的@Conditional作用）

作用：必须是@Conditional指定的条件成立，才给容器中添加组件，配置配里面的所有内容才生效；

| @Conditional扩展注解            | 作用（判断是否满足当前指定条件）                 |
| ------------------------------- | ------------------------------------------------ |
| @ConditionalOnJava              | 系统的java版本是否符合要求                       |
| @ConditionalOnBean              | 容器中存在指定Bean；                             |
| @ConditionalOnMissingBean       | 容器中不存在指定Bean；                           |
| @ConditionalOnExpression        | 满足SpEL表达式指定                               |
| @ConditionalOnClass             | 系统中有指定的类                                 |
| @ConditionalOnMissingClass      | 系统中没有指定的类                               |
| @ConditionalOnSingleCandidate   | 容器中只有一个指定的Bean，或者这个Bean是首选Bean |
| @ConditionalOnProperty          | 系统中指定的属性是否有指定的值                   |
| @ConditionalOnResource          | 类路径下是否存在指定资源文件                     |
| @ConditionalOnWebApplication    | 当前是web环境                                    |
| @ConditionalOnNotWebApplication | 当前不是web环境                                  |
| @ConditionalOnJndi              | JNDI存在指定项                                   |

**自动配置类必须在一定的条件下才能生效；**

我们怎么知道哪些自动配置类生效；

**==我们可以通过启用  debug=true属性；来让控制台打印自动配置报告==**，这样我们就可以很方便的知道哪些自动配置类生效；

```java
=========================
AUTO-CONFIGURATION REPORT
=========================


Positive matches:（自动配置类启用的）
-----------------

   DispatcherServletAutoConfiguration matched:
      - @ConditionalOnClass found required class 'org.springframework.web.servlet.DispatcherServlet'; @ConditionalOnMissingClass did not find unwanted class (OnClassCondition)
      - @ConditionalOnWebApplication (required) found StandardServletEnvironment (OnWebApplicationCondition)
        
    
Negative matches:（没有启动，没有匹配成功的自动配置类）
-----------------

   ActiveMQAutoConfiguration:
      Did not match:
         - @ConditionalOnClass did not find required classes 'javax.jms.ConnectionFactory', 'org.apache.activemq.ActiveMQConnectionFactory' (OnClassCondition)

   AopAutoConfiguration:
      Did not match:
         - @ConditionalOnClass did not find required classes 'org.aspectj.lang.annotation.Aspect', 'org.aspectj.lang.reflect.Advice' (OnClassCondition)
        
```





## 三、日志

### 1、日志框架

 小张；开发一个大型系统；

​		1、System.out.println("")；将关键数据打印在控制台；去掉？写在一个文件？

​		2、框架来记录系统的一些运行时信息；日志框架 ；  zhanglogging.jar；

​		3、高大上的几个功能？异步模式？自动归档？xxxx？  zhanglogging-good.jar？

​		4、将以前框架卸下来？换上新的框架，重新修改之前相关的API；zhanglogging-prefect.jar；

​		5、JDBC---数据库驱动；

​			写了一个统一的接口层；日志门面（日志的一个抽象层）；logging-abstract.jar；

​			给项目中导入具体的日志实现就行了；我们之前的日志框架都是实现的抽象层；



**市面上的日志框架；**

JUL、JCL、Jboss-logging、logback、log4j、log4j2、slf4j....

| 日志门面  （日志的抽象层）                                   | 日志实现                                             |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| ~~JCL（Jakarta  Commons Logging）~~    SLF4j（Simple  Logging Facade for Java）    **~~jboss-logging~~** | Log4j  JUL（java.util.logging）  Log4j2  **Logback** |

左边选一个门面（抽象层）、右边来选一个实现；

日志门面：  SLF4J；

日志实现：Logback；



SpringBoot：底层是Spring框架，Spring框架默认是用JCL；‘

​	**==SpringBoot选用 SLF4j和logback；==**



### 2、SLF4j使用

#### 1、如何在系统中使用SLF4j   https://www.slf4j.org

以后开发的时候，日志记录方法的调用，不应该来直接调用日志的实现类，而是调用日志抽象层里面的方法；

给系统里面导入slf4j的jar和  logback的实现jar

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    logger.info("Hello World");
  }
}
```

图示；

![images/concrete-bindings.png](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/concrete-bindings.png)

每一个日志的实现框架都有自己的配置文件。使用slf4j以后，**配置文件还是做成日志实现框架自己本身的配置文件；**

#### 2、遗留问题

a（slf4j+logback）: Spring（commons-logging）、Hibernate（jboss-logging）、MyBatis、xxxx

统一日志记录，即使是别的框架和我一起统一使用slf4j进行输出？

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/legacy.png)

**如何让系统中所有的日志都统一到slf4j；**

==1、将系统中其他日志框架先排除出去；==

==2、用中间包来替换原有的日志框架；==

==3、我们导入slf4j其他的实现==



### 3、SpringBoot日志关系

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
```



SpringBoot使用它来做日志功能；

```xml
	<dependency>			<groupId>org.springframework.boot</groupId>			<artifactId>spring-boot-starter-logging</artifactId>		</dependency>
```

底层依赖关系

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180131220946.png)

总结：

​	1）、SpringBoot底层也是使用slf4j+logback的方式进行日志记录

​	2）、SpringBoot也把其他的日志都替换成了slf4j；

​	3）、中间替换包？

```java
@SuppressWarnings("rawtypes")public abstract class LogFactory {    static String UNSUPPORTED_OPERATION_IN_JCL_OVER_SLF4J = "http://www.slf4j.org/codes.html#unsupported_operation_in_jcl_over_slf4j";    static LogFactory logFactory = new SLF4JLogFactory();
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180131221411.png)



​	4）、如果我们要引入其他框架？一定要把这个框架的默认日志依赖移除掉？

​			Spring框架用的是commons-logging；

```xml
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<exclusions>
				<exclusion>
					<groupId>commons-logging</groupId>
					<artifactId>commons-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
```

**==SpringBoot能自动适配所有的日志，而且底层使用slf4j+logback的方式记录日志，引入其他框架的时候，只需要把这个框架依赖的日志框架排除掉即可；==**

### 4、日志使用；

#### 1、默认配置

SpringBoot默认帮我们配置好了日志；

```java
	//记录器
	Logger logger = LoggerFactory.getLogger(getClass());
	@Test
	public void contextLoads() {
		//System.out.println();

		//日志的级别；
		//由低到高   trace<debug<info<warn<error
		//可以调整输出的日志级别；日志就只会在这个级别以以后的高级别生效
		logger.trace("这是trace日志...");
		logger.debug("这是debug日志...");
		//SpringBoot默认给我们使用的是info级别的，没有指定级别的就用SpringBoot默认规定的级别；root级别
		logger.info("这是info日志...");
		logger.warn("这是warn日志...");
		logger.error("这是error日志...");


	}
```



        日志输出格式：
    		%d表示日期时间，
    		%thread表示线程名，
    		%-5level：级别从左显示5个字符宽度
    		%logger{50} 表示logger名字最长50个字符，否则按照句点分割。 
    		%msg：日志消息，
    		%n是换行符
        -->
        %d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{50} - %msg%n

SpringBoot修改日志的默认配置

```properties
logging.level.com.atguigu=trace


#logging.path=
# 不指定路径在当前项目下生成springboot.log日志
# 可以指定完整的路径；
#logging.file=G:/springboot.log

# 在当前磁盘的根路径下创建spring文件夹和里面的log文件夹；使用 spring.log 作为默认文件
logging.path=/spring/log

#  在控制台输出的日志的格式
logging.pattern.console=%d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n
# 指定文件中日志输出的格式
logging.pattern.file=%d{yyyy-MM-dd} === [%thread] === %-5level === %logger{50} ==== %msg%n
```

| logging.file | logging.path | Example  | Description                        |
| ------------ | ------------ | -------- | ---------------------------------- |
| (none)       | (none)       |          | 只在控制台输出                     |
| 指定文件名   | (none)       | my.log   | 输出日志到my.log文件               |
| (none)       | 指定目录     | /var/log | 输出到指定目录的 spring.log 文件中 |

#### 2、指定配置

给类路径下放上每个日志框架自己的配置文件即可；SpringBoot就不使用他默认配置的了

| Logging System          | Customization                                                |
| ----------------------- | ------------------------------------------------------------ |
| Logback                 | `logback-spring.xml`, `logback-spring.groovy`, `logback.xml` or `logback.groovy` |
| Log4j2                  | `log4j2-spring.xml` or `log4j2.xml`                          |
| JDK (Java Util Logging) | `logging.properties`                                         |

logback.xml：直接就被日志框架识别了；

**logback-spring.xml**：日志框架就不直接加载日志的配置项，由SpringBoot解析日志配置，可以使用SpringBoot的高级Profile功能

```xml
<springProfile name="staging">
    <!-- configuration to be enabled when the "staging" profile is active -->
  	可以指定某段配置只在某个环境下生效
</springProfile>

```

如：

```xml
<appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
        <!--
        日志输出格式：
			%d表示日期时间，
			%thread表示线程名，
			%-5level：级别从左显示5个字符宽度
			%logger{50} 表示logger名字最长50个字符，否则按照句点分割。 
			%msg：日志消息，
			%n是换行符
        -->
        <layout class="ch.qos.logback.classic.PatternLayout">
            <springProfile name="dev">
                <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} ----> [%thread] ---> %-5level %logger{50} - %msg%n</pattern>
            </springProfile>
            <springProfile name="!dev">
                <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} ==== [%thread] ==== %-5level %logger{50} - %msg%n</pattern>
            </springProfile>
        </layout>
    </appender>
```



如果使用logback.xml作为日志配置文件，还要使用profile功能，会有以下错误

 `no applicable action for [springProfile]`

### 5、切换日志框架

可以按照slf4j的日志适配图，进行相关的切换；

slf4j+log4j的方式；

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-web</artifactId>
  <exclusions>
    <exclusion>
      <artifactId>logback-classic</artifactId>
      <groupId>ch.qos.logback</groupId>
    </exclusion>
    <exclusion>
      <artifactId>log4j-over-slf4j</artifactId>
      <groupId>org.slf4j</groupId>
    </exclusion>
  </exclusions>
</dependency>

<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-log4j12</artifactId>
</dependency>

```





切换为log4j2

```xml
   <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <exclusion>
                    <artifactId>spring-boot-starter-logging</artifactId>
                    <groupId>org.springframework.boot</groupId>
                </exclusion>
            </exclusions>
        </dependency>

<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-log4j2</artifactId>
</dependency>
```

-----------------

## 四、Web开发

### 1、简介



使用SpringBoot；

**1）、创建SpringBoot应用，选中我们需要的模块；**

**2）、SpringBoot已经默认将这些场景配置好了，只需要在配置文件中指定少量配置就可以运行起来**

**3）、自己编写业务代码；**



**自动配置原理？**

这个场景SpringBoot帮我们配置了什么？能不能修改？能修改哪些配置？能不能扩展？xxx

```
xxxxAutoConfiguration：帮我们给容器中自动配置组件；
xxxxProperties:配置类来封装配置文件的内容；

```



### 2、SpringBoot对静态资源的映射规则；

```java
@ConfigurationProperties(prefix = "spring.resources", ignoreUnknownFields = false)
public class ResourceProperties implements ResourceLoaderAware {
  //可以设置和静态资源有关的参数，缓存时间等
```



```java
	WebMvcAuotConfiguration：
		@Override
		public void addResourceHandlers(ResourceHandlerRegistry registry) {
			if (!this.resourceProperties.isAddMappings()) {
				logger.debug("Default resource handling disabled");
				return;
			}
			Integer cachePeriod = this.resourceProperties.getCachePeriod();
			if (!registry.hasMappingForPattern("/webjars/**")) {
				customizeResourceHandlerRegistration(
						registry.addResourceHandler("/webjars/**")
								.addResourceLocations(
										"classpath:/META-INF/resources/webjars/")
						.setCachePeriod(cachePeriod));
			}
			String staticPathPattern = this.mvcProperties.getStaticPathPattern();
          	//静态资源文件夹映射
			if (!registry.hasMappingForPattern(staticPathPattern)) {
				customizeResourceHandlerRegistration(
						registry.addResourceHandler(staticPathPattern)
								.addResourceLocations(
										this.resourceProperties.getStaticLocations())
						.setCachePeriod(cachePeriod));
			}
		}

        //配置欢迎页映射
		@Bean
		public WelcomePageHandlerMapping welcomePageHandlerMapping(
				ResourceProperties resourceProperties) {
			return new WelcomePageHandlerMapping(resourceProperties.getWelcomePage(),
					this.mvcProperties.getStaticPathPattern());
		}

       //配置喜欢的图标
		@Configuration
		@ConditionalOnProperty(value = "spring.mvc.favicon.enabled", matchIfMissing = true)
		public static class FaviconConfiguration {

			private final ResourceProperties resourceProperties;

			public FaviconConfiguration(ResourceProperties resourceProperties) {
				this.resourceProperties = resourceProperties;
			}

			@Bean
			public SimpleUrlHandlerMapping faviconHandlerMapping() {
				SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
				mapping.setOrder(Ordered.HIGHEST_PRECEDENCE + 1);
              	//所有  **/favicon.ico 
				mapping.setUrlMap(Collections.singletonMap("**/favicon.ico",
						faviconRequestHandler()));
				return mapping;
			}

			@Bean
			public ResourceHttpRequestHandler faviconRequestHandler() {
				ResourceHttpRequestHandler requestHandler = new ResourceHttpRequestHandler();
				requestHandler
						.setLocations(this.resourceProperties.getFaviconLocations());
				return requestHandler;
			}

		}

```



==1）、所有 /webjars/** ，都去 classpath:/META-INF/resources/webjars/ 找资源；==

​	webjars：以jar包的方式引入静态资源；

http://www.webjars.org/

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180203181751.png)

localhost:8080/webjars/jquery/3.3.1/jquery.js

```xml
<!--引入jquery-webjar-->在访问的时候只需要写webjars下面资源的名称即可
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>jquery</artifactId>
			<version>3.3.1</version>
		</dependency>
```



==2）、"/**" 访问当前项目的任何资源，都去（静态资源的文件夹）找映射==

```
"classpath:/META-INF/resources/", 
"classpath:/resources/",
"classpath:/static/", 
"classpath:/public/" 
"/"：当前项目的根路径
```

localhost:8080/abc ===  去静态资源文件夹里面找abc

==3）、欢迎页； 静态资源文件夹下的所有index.html页面；被"/**"映射；==

​	localhost:8080/   找index页面

==4）、所有的 **/favicon.ico  都是在静态资源文件下找；==



### 3、模板引擎

JSP、Velocity、Freemarker、Thymeleaf

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/template-engine.png)



SpringBoot推荐的Thymeleaf；

语法更简单，功能更强大；



#### 1、引入thymeleaf；

```xml
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
          	2.1.6
		</dependency>
切换thymeleaf版本
<properties>
		<thymeleaf.version>3.0.9.RELEASE</thymeleaf.version>
		<!-- 布局功能的支持程序  thymeleaf3主程序  layout2以上版本 -->
		<!-- thymeleaf2   layout1-->
		<thymeleaf-layout-dialect.version>2.2.2</thymeleaf-layout-dialect.version>
  </properties>
```



#### 2、Thymeleaf使用

```java
@ConfigurationProperties(prefix = "spring.thymeleaf")
public class ThymeleafProperties {

	private static final Charset DEFAULT_ENCODING = Charset.forName("UTF-8");

	private static final MimeType DEFAULT_CONTENT_TYPE = MimeType.valueOf("text/html");

	public static final String DEFAULT_PREFIX = "classpath:/templates/";

	public static final String DEFAULT_SUFFIX = ".html";
  	//
```

只要我们把HTML页面放在classpath:/templates/，thymeleaf就能自动渲染；

使用：

1、导入thymeleaf的名称空间

```xml
<html lang="en" xmlns:th="http://www.thymeleaf.org">
```

2、使用thymeleaf语法；

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <h1>成功！</h1>
    <!--th:text 将div里面的文本内容设置为 -->
    <div th:text="${hello}">这是显示欢迎信息</div>
</body>
</html>
```

#### 3、语法规则

1）、th:text；改变当前元素里面的文本内容；

​	th：任意html属性；来替换原生属性的值

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2018-02-04_123955.png)



2）、表达式？

```properties
Simple expressions:（表达式语法）
    Variable Expressions: ${...}：获取变量值；OGNL；
    		1）、获取对象的属性、调用方法
    		2）、使用内置的基本对象：
    			#ctx : the context object.
    			#vars: the context variables.
                #locale : the context locale.
                #request : (only in Web Contexts) the HttpServletRequest object.
                #response : (only in Web Contexts) the HttpServletResponse object.
                #session : (only in Web Contexts) the HttpSession object.
                #servletContext : (only in Web Contexts) the ServletContext object.
                
                ${session.foo}
            3）、内置的一些工具对象：
#execInfo : information about the template being processed.
#messages : methods for obtaining externalized messages inside variables expressions, in the same way as they would be obtained using #{…} syntax.
#uris : methods for escaping parts of URLs/URIs
#conversions : methods for executing the configured conversion service (if any).
#dates : methods for java.util.Date objects: formatting, component extraction, etc.
#calendars : analogous to #dates , but for java.util.Calendar objects.
#numbers : methods for formatting numeric objects.
#strings : methods for String objects: contains, startsWith, prepending/appending, etc.
#objects : methods for objects in general.
#bools : methods for boolean evaluation.
#arrays : methods for arrays.
#lists : methods for lists.
#sets : methods for sets.
#maps : methods for maps.
#aggregates : methods for creating aggregates on arrays or collections.
#ids : methods for dealing with id attributes that might be repeated (for example, as a result of an iteration).

    Selection Variable Expressions: *{...}：选择表达式：和${}在功能上是一样；
    	补充：配合 th:object="${session.user}：
   <div th:object="${session.user}">
    <p>Name: <span th:text="*{firstName}">Sebastian</span>.</p>
    <p>Surname: <span th:text="*{lastName}">Pepper</span>.</p>
    <p>Nationality: <span th:text="*{nationality}">Saturn</span>.</p>
    </div>
    
    Message Expressions: #{...}：获取国际化内容
    Link URL Expressions: @{...}：定义URL；
    		@{/order/process(execId=${execId},execType='FAST')}
    Fragment Expressions: ~{...}：片段引用表达式
    		<div th:insert="~{commons :: main}">...</div>
    		
Literals（字面量）
      Text literals: 'one text' , 'Another one!' ,…
      Number literals: 0 , 34 , 3.0 , 12.3 ,…
      Boolean literals: true , false
      Null literal: null
      Literal tokens: one , sometext , main ,…
Text operations:（文本操作）
    String concatenation: +
    Literal substitutions: |The name is ${name}|
Arithmetic operations:（数学运算）
    Binary operators: + , - , * , / , %
    Minus sign (unary operator): -
Boolean operations:（布尔运算）
    Binary operators: and , or
    Boolean negation (unary operator): ! , not
Comparisons and equality:（比较运算）
    Comparators: > , < , >= , <= ( gt , lt , ge , le )
    Equality operators: == , != ( eq , ne )
Conditional operators:条件运算（三元运算符）
    If-then: (if) ? (then)
    If-then-else: (if) ? (then) : (else)
    Default: (value) ?: (defaultvalue)
Special tokens:
    No-Operation: _ 
```

### 4、SpringMVC自动配置

https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications

#### 1. Spring MVC auto-configuration

Spring Boot 自动配置好了SpringMVC

以下是SpringBoot对SpringMVC的默认配置:**==（WebMvcAutoConfiguration）==**

- Inclusion of `ContentNegotiatingViewResolver` and `BeanNameViewResolver` beans.

  - 自动配置了ViewResolver（视图解析器：根据方法的返回值得到视图对象（View），视图对象决定如何渲染（转发？重定向？））
  - ContentNegotiatingViewResolver：组合所有的视图解析器的；
  - ==如何定制：我们可以自己给容器中添加一个视图解析器；自动的将其组合进来；==

- Support for serving static resources, including support for WebJars (see below).静态资源文件夹路径,webjars

- Static `index.html` support. 静态首页访问

- Custom `Favicon` support (see below).  favicon.ico

  

- 自动注册了 of `Converter`, `GenericConverter`, `Formatter` beans.

  - Converter：转换器；  public String hello(User user)：类型转换使用Converter
  - `Formatter`  格式化器；  2017.12.17===Date；

```java
		@Bean
		@ConditionalOnProperty(prefix = "spring.mvc", name = "date-format")//在文件中配置日期格式化的规则
		public Formatter<Date> dateFormatter() {
			return new DateFormatter(this.mvcProperties.getDateFormat());//日期格式化组件
		}
```

​	==自己添加的格式化器转换器，我们只需要放在容器中即可==

- Support for `HttpMessageConverters` (see below).

  - HttpMessageConverter：SpringMVC用来转换Http请求和响应的；User---Json；

  - `HttpMessageConverters` 是从容器中确定；获取所有的HttpMessageConverter；

    ==自己给容器中添加HttpMessageConverter，只需要将自己的组件注册容器中（@Bean,@Component）==

    

- Automatic registration of `MessageCodesResolver` (see below).定义错误代码生成规则

- Automatic use of a `ConfigurableWebBindingInitializer` bean (see below).

  ==我们可以配置一个ConfigurableWebBindingInitializer来替换默认的；（添加到容器）==

  ```
  初始化WebDataBinder；
  请求数据=====JavaBean；
  ```

**org.springframework.boot.autoconfigure.web：web的所有自动场景；**

If you want to keep Spring Boot MVC features, and you just want to add additional [MVC configuration](https://docs.spring.io/spring/docs/4.3.14.RELEASE/spring-framework-reference/htmlsingle#mvc) (interceptors, formatters, view controllers etc.) you can add your own `@Configuration` class of type `WebMvcConfigurerAdapter`, but **without** `@EnableWebMvc`. If you wish to provide custom instances of `RequestMappingHandlerMapping`, `RequestMappingHandlerAdapter` or `ExceptionHandlerExceptionResolver` you can declare a `WebMvcRegistrationsAdapter` instance providing such components.

If you want to take complete control of Spring MVC, you can add your own `@Configuration` annotated with `@EnableWebMvc`.

#### 2、扩展SpringMVC

```xml
    <mvc:view-controller path="/hello" view-name="success"/>
    <mvc:interceptors>
        <mvc:interceptor>
            <mvc:mapping path="/hello"/>
            <bean></bean>
        </mvc:interceptor>
    </mvc:interceptors>
```

**==编写一个配置类（@Configuration），是WebMvcConfigurerAdapter类型；不能标注@EnableWebMvc==**;

既保留了所有的自动配置，也能用我们扩展的配置；

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
@Configuration
//spring 5 采用 WebMvcConfigurer  WebMvcConfigurerAdapter已弃用
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }
}
```

**spring 5 采用 WebMvcConfigurer  WebMvcConfigurerAdapter以弃用**

```java
@Configuration
public class MyConfig implements WebMvcConfigurer {
}
```



原理：

​	1）、WebMvcAutoConfiguration是SpringMVC的自动配置类

​	2）、在做其他自动配置时会导入；@Import(**EnableWebMvcConfiguration**.class)

```java
    @Configuration
	public static class EnableWebMvcConfiguration extends DelegatingWebMvcConfiguration {
      private final WebMvcConfigurerComposite configurers = new WebMvcConfigurerComposite();

	 //从容器中获取所有的WebMvcConfigurer
      @Autowired(required = false)
      public void setConfigurers(List<WebMvcConfigurer> configurers) {
          if (!CollectionUtils.isEmpty(configurers)) {
              this.configurers.addWebMvcConfigurers(configurers);
            	//一个参考实现；将所有的WebMvcConfigurer相关配置都来一起调用；  
            	@Override
             // public void addViewControllers(ViewControllerRegistry registry) {
              //    for (WebMvcConfigurer delegate : this.delegates) {
               //       delegate.addViewControllers(registry);
               //   }
              }
          }
	}
```

​	3）、容器中所有的WebMvcConfigurer都会一起起作用；

​	4）、我们的配置类也会被调用；

​	效果：SpringMVC的自动配置和我们的扩展配置都会起作用；

#### 3、全面接管SpringMVC；

SpringBoot对SpringMVC的自动配置不需要了，所有都是我们自己配置；所有的SpringMVC的自动配置都失效了

**我们需要在配置类中添加@EnableWebMvc即可；**

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
@EnableWebMvc
@Configuration
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }
}
```

原理：

为什么@EnableWebMvc自动配置就失效了；

1）@EnableWebMvc的核心

```java
@Import(DelegatingWebMvcConfiguration.class)
public @interface EnableWebMvc {
```

2）、

```java
@Configuration
public class DelegatingWebMvcConfiguration extends WebMvcConfigurationSupport {
```

3）、

```java
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({ Servlet.class, DispatcherServlet.class,
		WebMvcConfigurerAdapter.class })
//容器中没有这个组件的时候，这个自动配置类才生效
@ConditionalOnMissingBean(WebMvcConfigurationSupport.class)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE + 10)
@AutoConfigureAfter({ DispatcherServletAutoConfiguration.class,
		ValidationAutoConfiguration.class })
public class WebMvcAutoConfiguration {
```

4）、@EnableWebMvc将WebMvcConfigurationSupport组件导入进来；

5）、导入的WebMvcConfigurationSupport只是SpringMVC最基本的功能；



### 5、如何修改SpringBoot的默认配置

模式：

​	1）、SpringBoot在自动配置很多组件的时候，先看容器中有没有用户自己配置的（@Bean、@Component）如果有就用用户配置的，如果没有，才自动配置；如果有些组件可以有多个（ViewResolver）将用户配置的和自己默认的组合起来；

​	2）、在SpringBoot中会有非常多的xxxConfigurer帮助我们进行扩展配置

​	3）、在SpringBoot中会有很多的xxxCustomizer帮助我们进行定制配置

### 6、RestfulCRUD

#### 1）、默认访问首页

```java
//使用WebMvcConfigurerAdapter可以来扩展SpringMVC的功能
//@EnableWebMvc   不要接管SpringMVC
@Configuration
public class MyMvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
       // super.addViewControllers(registry);
        //浏览器发送 /atguigu 请求来到 success
        registry.addViewController("/atguigu").setViewName("success");
    }

    //所有的WebMvcConfigurerAdapter组件都会一起起作用
    @Bean //将组件注册在容器
    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(){
        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("login");
                registry.addViewController("/index.html").setViewName("login");
            }
        };
        return adapter;
    }
}

```

#### 2）、国际化

**1）、编写国际化配置文件；**

2）、使用ResourceBundleMessageSource管理国际化资源文件

3）、在页面使用fmt:message取出国际化内容



步骤：

1）、编写国际化配置文件，抽取页面需要显示的国际化消息

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180211130721.png)



2）、SpringBoot自动配置好了管理国际化资源文件的组件；

```java
@ConfigurationProperties(prefix = "spring.messages")public class MessageSourceAutoConfiguration {        /**	 * Comma-separated list of basenames (essentially a fully-qualified classpath	 * location), each following the ResourceBundle convention with relaxed support for	 * slash based locations. If it doesn't contain a package qualifier (such as	 * "org.mypackage"), it will be resolved from the classpath root.	 */	private String basename = "messages";      //我们的配置文件可以直接放在类路径下叫messages.properties；        @Bean	public MessageSource messageSource() {		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();		if (StringUtils.hasText(this.basename)) {            //设置国际化资源文件的基础名（去掉语言国家代码的）			messageSource.setBasenames(StringUtils.commaDelimitedListToStringArray(					StringUtils.trimAllWhitespace(this.basename)));		}		if (this.encoding != null) {			messageSource.setDefaultEncoding(this.encoding.name());		}		messageSource.setFallbackToSystemLocale(this.fallbackToSystemLocale);		messageSource.setCacheSeconds(this.cacheSeconds);		messageSource.setAlwaysUseMessageFormat(this.alwaysUseMessageFormat);		return messageSource;	}
```



3）、去页面获取国际化的值；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180211134506.png)



```html
<!DOCTYPE html>
<html lang="en"  xmlns:th="http://www.thymeleaf.org">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Signin Template for Bootstrap</title>
		<!-- Bootstrap core CSS -->
		<link href="asserts/css/bootstrap.min.css" th:href="@{/webjars/bootstrap/4.0.0/css/bootstrap.css}" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="asserts/css/signin.css" th:href="@{/asserts/css/signin.css}" rel="stylesheet">
	</head>

	<body class="text-center">
		<form class="form-signin" action="dashboard.html">
			<img class="mb-4" th:src="@{/asserts/img/bootstrap-solid.svg}" src="asserts/img/bootstrap-solid.svg" alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal" th:text="#{login.tip}">Please sign in</h1>
			<label class="sr-only" th:text="#{login.username}">Username</label>
			<input type="text" class="form-control" placeholder="Username" th:placeholder="#{login.username}" required="" autofocus="">
			<label class="sr-only" th:text="#{login.password}">Password</label>
			<input type="password" class="form-control" placeholder="Password" th:placeholder="#{login.password}" required="">
			<div class="checkbox mb-3">
				<label>
          		<input type="checkbox" value="remember-me"/> [[#{login.remember}]]
        </label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit" th:text="#{login.btn}">Sign in</button>
			<p class="mt-5 mb-3 text-muted">© 2017-2018</p>
			<a class="btn btn-sm">中文</a>
			<a class="btn btn-sm">English</a>
		</form>

	</body>

</html>
```

效果：根据浏览器语言设置的信息切换了国际化；



原理：

​	国际化Locale（区域信息对象）；LocaleResolver（获取区域信息对象）；

```java
		@Bean		@ConditionalOnMissingBean		@ConditionalOnProperty(prefix = "spring.mvc", name = "locale")		public LocaleResolver localeResolver() {			if (this.mvcProperties					.getLocaleResolver() == WebMvcProperties.LocaleResolver.FIXED) {				return new FixedLocaleResolver(this.mvcProperties.getLocale());			}			AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();			localeResolver.setDefaultLocale(this.mvcProperties.getLocale());			return localeResolver;		}默认的就是根据请求头带来的区域信息获取Locale进行国际化
```

4）、点击链接切换国际化

```java
/**
 * 可以在连接上携带区域信息
 */
public class MyLocaleResolver implements LocaleResolver {
    
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String l = request.getParameter("l");
        Locale locale = Locale.getDefault();
        if(!StringUtils.isEmpty(l)){
            String[] split = l.split("_");
            locale = new Locale(split[0],split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}


 @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}


```

#### 3）、登陆

开发期间模板引擎页面修改以后，要实时生效

1）、禁用模板引擎的缓存

```
# 禁用缓存
spring.thymeleaf.cache=false 
```

2）、页面修改完成以后ctrl+f9：重新编译；



登陆错误消息的显示

```html
<p style="color: red" th:text="${msg}" th:if="${not #strings.isEmpty(msg)}"></p>
```



#### 4）、拦截器进行登陆检查

拦截器

```java
/** * 登陆检查， */public class LoginHandlerInterceptor implements HandlerInterceptor {    //目标方法执行之前    @Override    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {        Object user = request.getSession().getAttribute("loginUser");        if(user == null){            //未登陆，返回登陆页面            request.setAttribute("msg","没有权限请先登陆");            request.getRequestDispatcher("/index.html").forward(request,response);            return false;        }else{            //已登陆，放行请求            return true;        }    }    @Override    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {    }    @Override    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {    }}
```



注册拦截器

```java
  //所有的WebMvcConfigurerAdapter组件都会一起起作用    @Bean //将组件注册在容器    public WebMvcConfigurerAdapter webMvcConfigurerAdapter(){        WebMvcConfigurerAdapter adapter = new WebMvcConfigurerAdapter() {            @Override            public void addViewControllers(ViewControllerRegistry registry) {                registry.addViewController("/").setViewName("login");                registry.addViewController("/index.html").setViewName("login");                registry.addViewController("/main.html").setViewName("dashboard");            }            //注册拦截器            @Override            public void addInterceptors(InterceptorRegistry registry) {                //super.addInterceptors(registry);                //静态资源；  *.css , *.js                //SpringBoot已经做好了静态资源映射                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")                        .excludePathPatterns("/index.html","/","/user/login");            }        };        return adapter;    }
```

#### 5）、CRUD-员工列表

实验要求：

1）、RestfulCRUD：CRUD满足Rest风格；

URI：  /资源名称/资源标识       HTTP请求方式区分对资源CRUD操作

|      | 普通CRUD（uri来区分操作） | RestfulCRUD       |
| ---- | ------------------------- | ----------------- |
| 查询 | getEmp                    | emp---GET         |
| 添加 | addEmp?xxx                | emp---POST        |
| 修改 | updateEmp?id=xxx&xxx=xx   | emp/{id}---PUT    |
| 删除 | deleteEmp?id=1            | emp/{id}---DELETE |

2）、实验的请求架构;

| 实验功能                             | 请求URI | 请求方式 |
| ------------------------------------ | ------- | -------- |
| 查询所有员工                         | emps    | GET      |
| 查询某个员工(来到修改页面)           | emp/1   | GET      |
| 来到添加页面                         | emp     | GET      |
| 添加员工                             | emp     | POST     |
| 来到修改页面（查出员工进行信息回显） | emp/1   | GET      |
| 修改员工                             | emp     | PUT      |
| 删除员工                             | emp/1   | DELETE   |

3）、员工列表：

##### thymeleaf公共页面元素抽取

```html
1、抽取公共片段<div th:fragment="copy">&copy; 2011 The Good Thymes Virtual Grocery</div>2、引入公共片段<div th:insert="~{footer :: copy}"></div>~{templatename::selector}：模板名::选择器~{templatename::fragmentname}:模板名::片段名3、默认效果：insert的公共片段在div标签中如果使用th:insert等属性进行引入，可以不用写~{}：行内写法可以加上：[[~{}]];[(~{})]；
```



三种引入公共片段的th属性：

**th:insert**：将公共片段整个插入到声明引入的元素中

**th:replace**：将声明引入的元素替换为公共片段

**th:include**：将被引入的片段的内容包含进这个标签中



```html
<footer th:fragment="copy">&copy; 2011 The Good Thymes Virtual Grocery</footer>引入方式<div th:insert="footer :: copy"></div><div th:replace="footer :: copy"></div><div th:include="footer :: copy"></div>效果<div>    <footer>    &copy; 2011 The Good Thymes Virtual Grocery    </footer></div><footer>&copy; 2011 The Good Thymes Virtual Grocery</footer><div>&copy; 2011 The Good Thymes Virtual Grocery</div>
```



引入片段的时候传入参数： 

```html
<nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">    <div class="sidebar-sticky">        <ul class="nav flex-column">            <li class="nav-item">                <a class="nav-link active"                   th:class="${activeUri=='main.html'?'nav-link active':'nav-link'}"                   href="#" th:href="@{/main.html}">                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>                        <polyline points="9 22 9 12 15 12 15 22"></polyline>                    </svg>                    Dashboard <span class="sr-only">(current)</span>                </a>            </li><!--引入侧边栏;传入参数--><div th:replace="commons/bar::#sidebar(activeUri='emps')"></div>
```

#### 6）、CRUD-员工添加

添加页面

```html
<form>
    <div class="form-group">
        <label>LastName</label>
        <input type="text" class="form-control" placeholder="zhangsan">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" placeholder="zhangsan@atguigu.com">
    </div>
    <div class="form-group">
        <label>Gender</label><br/>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender"  value="1">
            <label class="form-check-label">男</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender"  value="0">
            <label class="form-check-label">女</label>
        </div>
    </div>
    <div class="form-group">
        <label>department</label>
        <select class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>
    <div class="form-group">
        <label>Birth</label>
        <input type="text" class="form-control" placeholder="zhangsan">
    </div>
    <button type="submit" class="btn btn-primary">添加</button>
</form>
```

提交的数据格式不对：生日：日期；

2017-12-12；2017/12/12；2017.12.12；

日期的格式化；SpringMVC将页面提交的值需要转换为指定的类型;

2017-12-12---Date； 类型转换，格式化;

默认日期是按照/的方式；

#### 7）、CRUD-员工修改

修改添加二合一表单

```html
<!--需要区分是员工修改还是添加；-->
<form th:action="@{/emp}" method="post">
    <!--发送put请求修改员工数据-->
    <!--
1、SpringMVC中配置HiddenHttpMethodFilter;（SpringBoot自动配置好的）
2、页面创建一个post表单
3、创建一个input项，name="_method";值就是我们指定的请求方式
-->
    <input type="hidden" name="_method" value="put" th:if="${emp!=null}"/>
    <input type="hidden" name="id" th:if="${emp!=null}" th:value="${emp.id}">
    <div class="form-group">
        <label>LastName</label>
        <input name="lastName" type="text" class="form-control" placeholder="zhangsan" th:value="${emp!=null}?${emp.lastName}">
    </div>
    <div class="form-group">
        <label>Email</label>
        <input name="email" type="email" class="form-control" placeholder="zhangsan@atguigu.com" th:value="${emp!=null}?${emp.email}">
    </div>
    <div class="form-group">
        <label>Gender</label><br/>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" value="1" th:checked="${emp!=null}?${emp.gender==1}">
            <label class="form-check-label">男</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="gender" value="0" th:checked="${emp!=null}?${emp.gender==0}">
            <label class="form-check-label">女</label>
        </div>
    </div>
    <div class="form-group">
        <label>department</label>
        <!--提交的是部门的id-->
        <select class="form-control" name="department.id">
            <option th:selected="${emp!=null}?${dept.id == emp.department.id}" th:value="${dept.id}" th:each="dept:${depts}" th:text="${dept.departmentName}">1</option>
        </select>
    </div>
    <div class="form-group">
        <label>Birth</label>
        <input name="birth" type="text" class="form-control" placeholder="zhangsan" th:value="${emp!=null}?${#dates.format(emp.birth, 'yyyy-MM-dd HH:mm')}">
    </div>
    <button type="submit" class="btn btn-primary" th:text="${emp!=null}?'修改':'添加'">添加</button>
</form>
```

#### 8）、CRUD-员工删除

```html
<tr th:each="emp:${emps}">    <td th:text="${emp.id}"></td>    <td>[[${emp.lastName}]]</td>    <td th:text="${emp.email}"></td>    <td th:text="${emp.gender}==0?'女':'男'"></td>    <td th:text="${emp.department.departmentName}"></td>    <td th:text="${#dates.format(emp.birth, 'yyyy-MM-dd HH:mm')}"></td>    <td>        <a class="btn btn-sm btn-primary" th:href="@{/emp/}+${emp.id}">编辑</a>        <button th:attr="del_uri=@{/emp/}+${emp.id}" class="btn btn-sm btn-danger deleteBtn">删除</button>    </td></tr><script>    $(".deleteBtn").click(function(){        //删除当前员工的        $("#deleteEmpForm").attr("action",$(this).attr("del_uri")).submit();        return false;    });</script>
```



### 7、错误处理机制

#### 1）、SpringBoot默认的错误处理机制

默认效果：

​		1）、浏览器，返回一个默认的错误页面

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226173408.png)

  浏览器发送请求的请求头：

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226180347.png)

​		2）、如果是其他客户端，默认响应一个json数据

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226173527.png)

​		![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180226180504.png)

原理：

​	可以参照ErrorMvcAutoConfiguration；错误处理的自动配置；

  	给容器中添加了以下组件

​	1、DefaultErrorAttributes：

```java
帮我们在页面共享信息；@Override	public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes,			boolean includeStackTrace) {		Map<String, Object> errorAttributes = new LinkedHashMap<String, Object>();		errorAttributes.put("timestamp", new Date());		addStatus(errorAttributes, requestAttributes);		addErrorDetails(errorAttributes, requestAttributes, includeStackTrace);		addPath(errorAttributes, requestAttributes);		return errorAttributes;	}
```



​	2、BasicErrorController：处理默认/error请求

```java
@Controller@RequestMapping("${server.error.path:${error.path:/error}}")public class BasicErrorController extends AbstractErrorController {        @RequestMapping(produces = "text/html")//产生html类型的数据；浏览器发送的请求来到这个方法处理	public ModelAndView errorHtml(HttpServletRequest request,			HttpServletResponse response) {		HttpStatus status = getStatus(request);		Map<String, Object> model = Collections.unmodifiableMap(getErrorAttributes(				request, isIncludeStackTrace(request, MediaType.TEXT_HTML)));		response.setStatus(status.value());                //去哪个页面作为错误页面；包含页面地址和页面内容		ModelAndView modelAndView = resolveErrorView(request, response, status, model);		return (modelAndView == null ? new ModelAndView("error", model) : modelAndView);	}	@RequestMapping	@ResponseBody    //产生json数据，其他客户端来到这个方法处理；	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {		Map<String, Object> body = getErrorAttributes(request,				isIncludeStackTrace(request, MediaType.ALL));		HttpStatus status = getStatus(request);		return new ResponseEntity<Map<String, Object>>(body, status);	}
```



​	3、ErrorPageCustomizer：

```java
	@Value("${error.path:/error}")	private String path = "/error";  系统出现错误以后来到error请求进行处理；（web.xml注册的错误页面规则）
```



​	4、DefaultErrorViewResolver：

```java
@Override	public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status,			Map<String, Object> model) {		ModelAndView modelAndView = resolve(String.valueOf(status), model);		if (modelAndView == null && SERIES_VIEWS.containsKey(status.series())) {			modelAndView = resolve(SERIES_VIEWS.get(status.series()), model);		}		return modelAndView;	}	private ModelAndView resolve(String viewName, Map<String, Object> model) {        //默认SpringBoot可以去找到一个页面？  error/404		String errorViewName = "error/" + viewName;                //模板引擎可以解析这个页面地址就用模板引擎解析		TemplateAvailabilityProvider provider = this.templateAvailabilityProviders				.getProvider(errorViewName, this.applicationContext);		if (provider != null) {            //模板引擎可用的情况下返回到errorViewName指定的视图地址			return new ModelAndView(errorViewName, model);		}        //模板引擎不可用，就在静态资源文件夹下找errorViewName对应的页面   error/404.html		return resolveResource(errorViewName, model);	}
```



​	步骤：

​		一但系统出现4xx或者5xx之类的错误；ErrorPageCustomizer就会生效（定制错误的响应规则）；就会来到/error请求；就会被**BasicErrorController**处理；

​		1）响应页面；去哪个页面是由**DefaultErrorViewResolver**解析得到的；

```java
protected ModelAndView resolveErrorView(HttpServletRequest request,      HttpServletResponse response, HttpStatus status, Map<String, Object> model) {    //所有的ErrorViewResolver得到ModelAndView   for (ErrorViewResolver resolver : this.errorViewResolvers) {      ModelAndView modelAndView = resolver.resolveErrorView(request, status, model);      if (modelAndView != null) {         return modelAndView;      }   }   return null;}
```

#### 2）、如果定制错误响应：

##### 	**1）、如何定制错误的页面；**

​			**1）、有模板引擎的情况下；error/状态码;** 【将错误页面命名为  错误状态码.html 放在模板引擎文件夹里面的 error文件夹下】，发生此状态码的错误就会来到  对应的页面；

​			我们可以使用4xx和5xx作为错误页面的文件名来匹配这种类型的所有错误，精确优先（优先寻找精确的状态码.html）；		

​			页面能获取的信息；

​				timestamp：时间戳

​				status：状态码

​				error：错误提示

​				exception：异常对象

​				message：异常消息

​				errors：JSR303数据校验的错误都在这里

​			2）、没有模板引擎（模板引擎找不到这个错误页面），静态资源文件夹下找；

​			3）、以上都没有错误页面，就是默认来到SpringBoot默认的错误提示页面；



##### 	2）、如何定制错误的json数据；

​		1）、自定义异常处理&返回定制json数据；

```java
@ControllerAdvice
public class MyExceptionHandler {    @ResponseBody    @ExceptionHandler(UserNotExistException.class)    public Map<String,Object> handleException(Exception e){        Map<String,Object> map = new HashMap<>();        map.put("code","user.notexist");        map.put("message",e.getMessage());        return map;    }}//没有自适应效果...
```



​		2）、转发到/error进行自适应响应效果处理

```java
 @ExceptionHandler(UserNotExistException.class)    public String handleException(Exception e, HttpServletRequest request){        Map<String,Object> map = new HashMap<>();        //传入我们自己的错误状态码  4xx 5xx，否则就不会进入定制错误页面的解析流程        /**         * Integer statusCode = (Integer) request         .getAttribute("javax.servlet.error.status_code");         */        request.setAttribute("javax.servlet.error.status_code",500);        map.put("code","user.notexist");        map.put("message",e.getMessage());        //转发到/error        return "forward:/error";    }
```

##### 	3）、将我们的定制数据携带出去；

出现错误以后，会来到/error请求，会被BasicErrorController处理，响应出去可以获取的数据是由getErrorAttributes得到的（是AbstractErrorController（ErrorController）规定的方法）；

​	1、完全来编写一个ErrorController的实现类【或者是编写AbstractErrorController的子类】，放在容器中；

​	2、页面上能用的数据，或者是json返回能用的数据都是通过errorAttributes.getErrorAttributes得到；

​			容器中DefaultErrorAttributes.getErrorAttributes()；默认进行数据处理的；

自定义ErrorAttributes

```java
//给容器中加入我们自己定义的ErrorAttributes@Componentpublic class MyErrorAttributes extends DefaultErrorAttributes {    @Override    public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {        Map<String, Object> map = super.getErrorAttributes(requestAttributes, includeStackTrace);        map.put("company","atguigu");        return map;    }}
```

最终的效果：响应是自适应的，可以通过定制ErrorAttributes改变需要返回的内容，

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180228135513.png)



### 8、配置嵌入式Servlet容器

SpringBoot默认使用Tomcat作为嵌入式的Servlet容器；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180301142915.png)



问题？

#### 1）、如何定制和修改Servlet容器的相关配置；

1、修改和server有关的配置（ServerProperties【也是EmbeddedServletContainerCustomizer】）；

```properties
server.port=8081server.context-path=/crudserver.tomcat.uri-encoding=UTF-8//通用的Servlet容器设置server.xxx//Tomcat的设置server.tomcat.xxx
```

2、编写一个**EmbeddedServletContainerCustomizer**：嵌入式的Servlet容器的定制器；来修改Servlet容器的配置

```java
@Bean  //一定要将这个定制器加入到容器中
public EmbeddedServletContainerCustomizer embeddedServletContainerCustomizer(){    return new EmbeddedServletContainerCustomizer() {        //定制嵌入式的Servlet容器相关的规则        @Override        
    public void customize(ConfigurableEmbeddedServletContainer container) {            container.setPort(8083);        }    };}
```

**EmbeddedServletContainerCustomizer** 被**WebServerFactoryCustomizer**替换

```java
@Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer(){
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {
                factory.setPort(8081);
            }
        };
    }
```



#### 2）、注册Servlet三大组件【Servlet、Filter、Listener】

由于SpringBoot默认是以jar包的方式启动嵌入式的Servlet容器来启动SpringBoot的web应用，没有web.xml文件。

注册三大组件用以下方式

ServletRegistrationBean

```java
//注册三大组件@Beanpublic ServletRegistrationBean myServlet(){    ServletRegistrationBean registrationBean = new ServletRegistrationBean(new MyServlet(),"/myServlet");    return registrationBean;}
```

FilterRegistrationBean

```java
@Bean
public FilterRegistrationBean myFilter(){    FilterRegistrationBean registrationBean = new FilterRegistrationBean();    registrationBean.setFilter(new MyFilter());    registrationBean.setUrlPatterns(Arrays.asList("/hello","/myServlet"));    return registrationBean;}
```

ServletListenerRegistrationBean

```java
@Bean
public ServletListenerRegistrationBean myListener(){    ServletListenerRegistrationBean<MyListener> registrationBean = new ServletListenerRegistrationBean<>(new MyListener());    return registrationBean;}
```



SpringBoot帮我们自动SpringMVC的时候，自动的注册SpringMVC的前端控制器；DIspatcherServlet；

DispatcherServletAutoConfiguration中：

```java
@Bean(name = DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME)@ConditionalOnBean(value = DispatcherServlet.class, name = DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)public ServletRegistrationBean dispatcherServletRegistration(      DispatcherServlet dispatcherServlet) {   ServletRegistrationBean registration = new ServletRegistrationBean(         dispatcherServlet, this.serverProperties.getServletMapping());    //默认拦截： /  所有请求；包静态资源，但是不拦截jsp请求；   /*会拦截jsp    //可以通过server.servletPath来修改SpringMVC前端控制器默认拦截的请求路径       registration.setName(DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);   registration.setLoadOnStartup(         this.webMvcProperties.getServlet().getLoadOnStartup());   if (this.multipartConfig != null) {      registration.setMultipartConfig(this.multipartConfig);   }   return registration;}
```

2）、SpringBoot能不能支持其他的Servlet容器；

#### 3）、替换为其他嵌入式Servlet容器

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302114401.png)

默认支持：

Tomcat（默认使用）

```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   引入web模块默认就是使用嵌入式的Tomcat作为Servlet容器；
</dependency>
```

Jetty

```xml
<!-- 引入web模块 -->
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   <exclusions>
      <exclusion>
         <artifactId>spring-boot-starter-tomcat</artifactId>
         <groupId>org.springframework.boot</groupId>
      </exclusion>
   </exclusions>
</dependency>

<!--引入其他的Servlet容器-->
<dependency>
   <artifactId>spring-boot-starter-jetty</artifactId>
   <groupId>org.springframework.boot</groupId>
</dependency>
```

Undertow

```xml
<!-- 引入web模块 -->
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-web</artifactId>
   <exclusions>
      <exclusion>
         <artifactId>spring-boot-starter-tomcat</artifactId>
         <groupId>org.springframework.boot</groupId>
      </exclusion>
   </exclusions>
</dependency>

<!--引入其他的Servlet容器-->
<dependency>
   <artifactId>spring-boot-starter-undertow</artifactId>
   <groupId>org.springframework.boot</groupId>
</dependency>
```

#### 4）、嵌入式Servlet容器自动配置原理；



EmbeddedServletContainerAutoConfiguration：嵌入式的Servlet容器自动配置？

```java
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)@Configuration@ConditionalOnWebApplication@Import(BeanPostProcessorsRegistrar.class)//导入BeanPostProcessorsRegistrar：Spring注解版；给容器中导入一些组件//导入了EmbeddedServletContainerCustomizerBeanPostProcessor：//后置处理器：bean初始化前后（创建完对象，还没赋值赋值）执行初始化工作public class EmbeddedServletContainerAutoConfiguration {        @Configuration	@ConditionalOnClass({ Servlet.class, Tomcat.class })//判断当前是否引入了Tomcat依赖；	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)//判断当前容器没有用户自己定义EmbeddedServletContainerFactory：嵌入式的Servlet容器工厂；作用：创建嵌入式的Servlet容器	public static class EmbeddedTomcat {		@Bean		public TomcatEmbeddedServletContainerFactory tomcatEmbeddedServletContainerFactory() {			return new TomcatEmbeddedServletContainerFactory();		}	}        /**	 * Nested configuration if Jetty is being used.	 */	@Configuration	@ConditionalOnClass({ Servlet.class, Server.class, Loader.class,			WebAppContext.class })	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)	public static class EmbeddedJetty {		@Bean		public JettyEmbeddedServletContainerFactory jettyEmbeddedServletContainerFactory() {			return new JettyEmbeddedServletContainerFactory();		}	}	/**	 * Nested configuration if Undertow is being used.	 */	@Configuration	@ConditionalOnClass({ Servlet.class, Undertow.class, SslClientAuthMode.class })	@ConditionalOnMissingBean(value = EmbeddedServletContainerFactory.class, search = SearchStrategy.CURRENT)	public static class EmbeddedUndertow {		@Bean		public UndertowEmbeddedServletContainerFactory undertowEmbeddedServletContainerFactory() {			return new UndertowEmbeddedServletContainerFactory();		}	}
```

1）、EmbeddedServletContainerFactory（嵌入式Servlet容器工厂）

```java
public interface EmbeddedServletContainerFactory {   //获取嵌入式的Servlet容器   EmbeddedServletContainer getEmbeddedServletContainer(         ServletContextInitializer... initializers);}
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302144835.png)

2）、EmbeddedServletContainer：（嵌入式的Servlet容器）

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302144910.png)



3）、以**TomcatEmbeddedServletContainerFactory**为例

```java
@Overridepublic EmbeddedServletContainer getEmbeddedServletContainer(      ServletContextInitializer... initializers) {    //创建一个Tomcat   Tomcat tomcat = new Tomcat();        //配置Tomcat的基本环节   File baseDir = (this.baseDirectory != null ? this.baseDirectory         : createTempDir("tomcat"));   tomcat.setBaseDir(baseDir.getAbsolutePath());   Connector connector = new Connector(this.protocol);   tomcat.getService().addConnector(connector);   customizeConnector(connector);   tomcat.setConnector(connector);   tomcat.getHost().setAutoDeploy(false);   configureEngine(tomcat.getEngine());   for (Connector additionalConnector : this.additionalTomcatConnectors) {      tomcat.getService().addConnector(additionalConnector);   }   prepareContext(tomcat.getHost(), initializers);        //将配置好的Tomcat传入进去，返回一个EmbeddedServletContainer；并且启动Tomcat服务器   return getTomcatEmbeddedServletContainer(tomcat);}
```

4）、我们对嵌入式容器的配置修改是怎么生效？

```
ServerProperties、EmbeddedServletContainerCustomizer
```



**EmbeddedServletContainerCustomizer**：定制器帮我们修改了Servlet容器的配置？

怎么修改的原理？

5）、容器中导入了**EmbeddedServletContainerCustomizerBeanPostProcessor**

```java
//初始化之前@Overridepublic Object postProcessBeforeInitialization(Object bean, String beanName)      throws BeansException {    //如果当前初始化的是一个ConfigurableEmbeddedServletContainer类型的组件   if (bean instanceof ConfigurableEmbeddedServletContainer) {       //      postProcessBeforeInitialization((ConfigurableEmbeddedServletContainer) bean);   }   return bean;}private void postProcessBeforeInitialization(			ConfigurableEmbeddedServletContainer bean) {    //获取所有的定制器，调用每一个定制器的customize方法来给Servlet容器进行属性赋值；    for (EmbeddedServletContainerCustomizer customizer : getCustomizers()) {        customizer.customize(bean);    }}private Collection<EmbeddedServletContainerCustomizer> getCustomizers() {    if (this.customizers == null) {        // Look up does not include the parent context        this.customizers = new ArrayList<EmbeddedServletContainerCustomizer>(            this.beanFactory            //从容器中获取所有这葛类型的组件：EmbeddedServletContainerCustomizer            //定制Servlet容器，给容器中可以添加一个EmbeddedServletContainerCustomizer类型的组件            .getBeansOfType(EmbeddedServletContainerCustomizer.class,                            false, false)            .values());        Collections.sort(this.customizers, AnnotationAwareOrderComparator.INSTANCE);        this.customizers = Collections.unmodifiableList(this.customizers);    }    return this.customizers;}ServerProperties也是定制器
```

步骤：

1）、SpringBoot根据导入的依赖情况，给容器中添加相应的EmbeddedServletContainerFactory【TomcatEmbeddedServletContainerFactory】

2）、容器中某个组件要创建对象就会惊动后置处理器；EmbeddedServletContainerCustomizerBeanPostProcessor；

只要是嵌入式的Servlet容器工厂，后置处理器就工作；

3）、后置处理器，从容器中获取所有的**EmbeddedServletContainerCustomizer**，调用定制器的定制方法



###5）、嵌入式Servlet容器启动原理；

什么时候创建嵌入式的Servlet容器工厂？什么时候获取嵌入式的Servlet容器并启动Tomcat；

获取嵌入式的Servlet容器工厂：

1）、SpringBoot应用启动运行run方法

2）、refreshContext(context);SpringBoot刷新IOC容器【创建IOC容器对象，并初始化容器，创建容器中的每一个组件】；如果是web应用创建**AnnotationConfigEmbeddedWebApplicationContext**，否则：**AnnotationConfigApplicationContext**

3）、refresh(context);**刷新刚才创建好的ioc容器；**

```java
public void refresh() throws BeansException, IllegalStateException {
   synchronized (this.startupShutdownMonitor) {
      // Prepare this context for refreshing.
      prepareRefresh();

      // Tell the subclass to refresh the internal bean factory.
      ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

      // Prepare the bean factory for use in this context.
      prepareBeanFactory(beanFactory);

      try {
         // Allows post-processing of the bean factory in context subclasses.
         postProcessBeanFactory(beanFactory);

         // Invoke factory processors registered as beans in the context.
         invokeBeanFactoryPostProcessors(beanFactory);

         // Register bean processors that intercept bean creation.
         registerBeanPostProcessors(beanFactory);

         // Initialize message source for this context.
         initMessageSource();

         // Initialize event multicaster for this context.
         initApplicationEventMulticaster();

         // Initialize other special beans in specific context subclasses.
         onRefresh();

         // Check for listener beans and register them.
         registerListeners();

         // Instantiate all remaining (non-lazy-init) singletons.
         finishBeanFactoryInitialization(beanFactory);

         // Last step: publish corresponding event.
         finishRefresh();
      }

      catch (BeansException ex) {
         if (logger.isWarnEnabled()) {
            logger.warn("Exception encountered during context initialization - " +
                  "cancelling refresh attempt: " + ex);
         }

         // Destroy already created singletons to avoid dangling resources.
         destroyBeans();

         // Reset 'active' flag.
         cancelRefresh(ex);

         // Propagate exception to caller.
         throw ex;
      }

      finally {
         // Reset common introspection caches in Spring's core, since we
         // might not ever need metadata for singleton beans anymore...
         resetCommonCaches();
      }
   }
}
```

4）、  onRefresh(); web的ioc容器重写了onRefresh方法

5）、webioc容器会创建嵌入式的Servlet容器；**createEmbeddedServletContainer**();

**6）、获取嵌入式的Servlet容器工厂：**

EmbeddedServletContainerFactory containerFactory = getEmbeddedServletContainerFactory();

​	从ioc容器中获取EmbeddedServletContainerFactory 组件；**TomcatEmbeddedServletContainerFactory**创建对象，后置处理器一看是这个对象，就获取所有的定制器来先定制Servlet容器的相关配置；

7）、**使用容器工厂获取嵌入式的Servlet容器**：this.embeddedServletContainer = containerFactory      .getEmbeddedServletContainer(getSelfInitializer());

8）、嵌入式的Servlet容器创建对象并启动Servlet容器；

**先启动嵌入式的Servlet容器，再将ioc容器中剩下没有创建出的对象获取出来；**

**==IOC容器启动创建嵌入式的Servlet容器==**



### 9、使用外置的Servlet容器

嵌入式Servlet容器：应用打成可执行的jar

​		优点：简单、便携；

​		缺点：默认不支持JSP、优化定制比较复杂（使用定制器【ServerProperties、自定义EmbeddedServletContainerCustomizer】，自己编写嵌入式Servlet容器的创建工厂【EmbeddedServletContainerFactory】）；



外置的Servlet容器：外面安装Tomcat---应用war包的方式打包；

#### 步骤

1）、必须创建一个war项目；（利用idea创建好目录结构）

2）、将嵌入式的Tomcat指定为provided；

```xml
<dependency>   <groupId>org.springframework.boot</groupId>   <artifactId>spring-boot-starter-tomcat</artifactId>   <scope>provided</scope></dependency>
```

3）、必须编写一个**SpringBootServletInitializer**的子类，并调用configure方法

```java
public class ServletInitializer extends SpringBootServletInitializer {

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
       //传入SpringBoot应用的主程序
      return application.sources(SpringBoot04WebJspApplication.class);
   }

}
```

4）、启动服务器就可以使用；

#### 原理

jar包：执行SpringBoot主类的main方法，启动ioc容器，创建嵌入式的Servlet容器；

war包：启动服务器，**服务器启动SpringBoot应用**【SpringBootServletInitializer】，启动ioc容器；



servlet3.0（Spring注解版）：

8.2.4 Shared libraries / runtimes pluggability：

规则：

​	1）、服务器启动（web应用启动）会创建当前web应用里面每一个jar包里面ServletContainerInitializer实例：

​	2）、ServletContainerInitializer的实现放在jar包的META-INF/services文件夹下，有一个名为javax.servlet.ServletContainerInitializer的文件，内容就是ServletContainerInitializer的实现类的全类名

​	3）、还可以使用@HandlesTypes，在应用启动的时候加载我们感兴趣的类；



流程：

1）、启动Tomcat

2）、org\springframework\spring-web\4.3.14.RELEASE\spring-web-4.3.14.RELEASE.jar!\META-INF\services\javax.servlet.ServletContainerInitializer：

Spring的web模块里面有这个文件：**org.springframework.web.SpringServletContainerInitializer**

3）、SpringServletContainerInitializer将@HandlesTypes(WebApplicationInitializer.class)标注的所有这个类型的类都传入到onStartup方法的Set<Class<?>>；为这些WebApplicationInitializer类型的类创建实例；

4）、每一个WebApplicationInitializer都调用自己的onStartup；

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180302221835.png)

5）、相当于我们的SpringBootServletInitializer的类会被创建对象，并执行onStartup方法

6）、SpringBootServletInitializer实例执行onStartup的时候会createRootApplicationContext；创建容器

```java
protected WebApplicationContext createRootApplicationContext(
      ServletContext servletContext) {
    //1、创建SpringApplicationBuilder
   SpringApplicationBuilder builder = createSpringApplicationBuilder();
   StandardServletEnvironment environment = new StandardServletEnvironment();
   environment.initPropertySources(servletContext, null);
   builder.environment(environment);
   builder.main(getClass());
   ApplicationContext parent = getExistingRootWebApplicationContext(servletContext);
   if (parent != null) {
      this.logger.info("Root context already created (using as parent).");
      servletContext.setAttribute(
            WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, null);
      builder.initializers(new ParentContextApplicationContextInitializer(parent));
   }
   builder.initializers(
         new ServletContextApplicationContextInitializer(servletContext));
   builder.contextClass(AnnotationConfigEmbeddedWebApplicationContext.class);
    
    //调用configure方法，子类重写了这个方法，将SpringBoot的主程序类传入了进来
   builder = configure(builder);
    
    //使用builder创建一个Spring应用
   SpringApplication application = builder.build();
   if (application.getSources().isEmpty() && AnnotationUtils
         .findAnnotation(getClass(), Configuration.class) != null) {
      application.getSources().add(getClass());
   }
   Assert.state(!application.getSources().isEmpty(),
         "No SpringApplication sources have been defined. Either override the "
               + "configure method or add an @Configuration annotation");
   // Ensure error pages are registered
   if (this.registerErrorPageFilter) {
      application.getSources().add(ErrorPageFilterConfiguration.class);
   }
    //启动Spring应用
   return run(application);
}
```

7）、Spring的应用就启动并且创建IOC容器

```java
public ConfigurableApplicationContext run(String... args) {
   StopWatch stopWatch = new StopWatch();
   stopWatch.start();
   ConfigurableApplicationContext context = null;
   FailureAnalyzers analyzers = null;
   configureHeadlessProperty();
   SpringApplicationRunListeners listeners = getRunListeners(args);
   listeners.starting();
   try {
      ApplicationArguments applicationArguments = new DefaultApplicationArguments(
            args);
      ConfigurableEnvironment environment = prepareEnvironment(listeners,
            applicationArguments);
      Banner printedBanner = printBanner(environment);
      context = createApplicationContext();
      analyzers = new FailureAnalyzers(context);
      prepareContext(context, environment, listeners, applicationArguments,
            printedBanner);
       
       //刷新IOC容器
      refreshContext(context);
      afterRefresh(context, applicationArguments);
      listeners.finished(context, null);
      stopWatch.stop();
      if (this.logStartupInfo) {
         new StartupInfoLogger(this.mainApplicationClass)
               .logStarted(getApplicationLog(), stopWatch);
      }
      return context;
   }
   catch (Throwable ex) {
      handleRunFailure(context, listeners, analyzers, ex);
      throw new IllegalStateException(ex);
   }
}
```

**==启动Servlet容器，再启动SpringBoot应用==**



## 五、SpringBoot与数据访问

### 1、JDBC

```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<scope>runtime</scope>
		</dependency>
```



```yaml
spring:
  datasource:
    username: root
    password: 123456
    url: jdbc:mysql://192.168.15.22:3306/jdbc
    driver-class-name: com.mysql.jdbc.Driver
```

效果：

​	默认是用org.apache.tomcat.jdbc.pool.DataSource作为数据源；

​	数据源的相关配置都在DataSourceProperties里面；

自动配置原理：

org.springframework.boot.autoconfigure.jdbc：

1、参考DataSourceConfiguration，根据配置创建数据源，默认使用Tomcat连接池；可以使用spring.datasource.type指定自定义的数据源类型；

2、SpringBoot默认可以支持；

```
org.apache.tomcat.jdbc.pool.DataSource、HikariDataSource、BasicDataSource、
```

3、自定义数据源类型

```java
/**
 * Generic DataSource configuration.
 */
@ConditionalOnMissingBean(DataSource.class)
@ConditionalOnProperty(name = "spring.datasource.type")
static class Generic {

   @Bean
   public DataSource dataSource(DataSourceProperties properties) {
       //使用DataSourceBuilder创建数据源，利用反射创建响应type的数据源，并且绑定相关属性
      return properties.initializeDataSourceBuilder().build();
   }

}
```

4、**DataSourceInitializer：ApplicationListener**；

​	作用：

​		1）、runSchemaScripts();运行建表语句；

​		2）、runDataScripts();运行插入数据的sql语句；

默认只需要将文件命名为：

```properties
schema-*.sql、data-*.sql
默认规则：schema.sql，schema-all.sql；
可以使用   
	schema:
      - classpath:department.sql
      指定位置
      
      

      
```

```yaml
2.x需要添加配置
sql:
    init:
      mode: always
      
 2.5+
   sql:
    init:
      mode: always
      schema-locations:
        - classpath:employee.sql
        - classpath:department.sql
```



5、操作数据库：自动配置了JdbcTemplate操作数据库

### 2、整合Druid数据源

```java
导入druid数据源
@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druid(){
       return  new DruidDataSource();
    }

    //配置Druid的监控
    //1、配置一个管理后台的Servlet
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        Map<String,String> initParams = new HashMap<>();

        initParams.put("loginUsername","admin");
        initParams.put("loginPassword","123456");
        initParams.put("allow","");//默认就是允许所有访问
        initParams.put("deny","192.168.15.21");

        bean.setInitParameters(initParams);
        return bean;
    }


    //2、配置一个web监控的filter
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());

        Map<String,String> initParams = new HashMap<>();
        initParams.put("exclusions","*.js,*.css,/druid/*");

        bean.setInitParameters(initParams);

        bean.setUrlPatterns(Arrays.asList("/*"));

        return  bean;
    }
}

```

yaml 配置

```properties
spring:
  datasource:
    #1.JDBC
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/springboot?useUnicode=true&characterEncoding=utf8
    username: root
    password: 123
    druid:
      #2.连接池配置
      #初始化连接池的连接数量 大小，最小，最大
      initial-size: 5
      min-idle: 5
      max-active: 20
      #配置获取连接等待超时的时间
      max-wait: 60000
       #配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
      time-between-eviction-runs-millis: 60000
      # 配置一个连接在池中最小生存的时间，单位是毫秒
      min-evictable-idle-time-millis: 30000
      validation-query: SELECT 1 FROM DUAL
      test-while-idle: true
      test-on-borrow: true
      test-on-return: false
      # 是否缓存preparedStatement，也就是PSCache  官方建议MySQL下建议关闭   个人建议如果想用SQL防火墙 建议打开
      pool-prepared-statements: true
      max-pool-prepared-statement-per-connection-size: 20
      # 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
      filter:
        stat:
          merge-sql: true
          slow-sql-millis: 5000
      #3.基础监控配置
      web-stat-filter:
        enabled: true
        url-pattern: /*
        #设置不统计哪些URL
        exclusions: "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*"
        session-stat-enable: true
        session-stat-max-count: 100
      stat-view-servlet:
        enabled: true
        url-pattern: /druid/*
        reset-enable: true
        #设置监控页面的登录名和密码
        login-username: admin
        login-password: admin
        allow: 127.0.0.1
        #deny: 192.168.1.100
```



### 3、整合MyBatis

```xml
		<dependency>
			<groupId>org.mybatis.spring.boot</groupId>
			<artifactId>mybatis-spring-boot-starter</artifactId>
			<version>1.3.1</version>
		</dependency>
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180305194443.png)

步骤：

​	1）、配置数据源相关属性（见上一节Druid）

​	2）、给数据库建表

​	3）、创建JavaBean

#### 	4）、注解版

```java
//指定这是一个操作数据库的mapper
@Mapper
public interface DepartmentMapper {

    @Select("select * from department where id=#{id}")
    public Department getDeptById(Integer id);

    @Delete("delete from department where id=#{id}")
    public int deleteDeptById(Integer id);

    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Insert("insert into department(departmentName) values(#{departmentName})")
    public int insertDept(Department department);

    @Update("update department set departmentName=#{departmentName} where id=#{id}")
    public int updateDept(Department department);
}
```

问题：

自定义MyBatis的配置规则；给容器中添加一个ConfigurationCustomizer；

```java
@org.springframework.context.annotation.Configuration
public class MyBatisConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer(){
        return new ConfigurationCustomizer(){

            @Override
            public void customize(Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }
}
```



```java
使用MapperScan批量扫描所有的Mapper接口；
@MapperScan(value = "com.atguigu.springboot.mapper")
@SpringBootApplication
public class SpringBoot06DataMybatisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot06DataMybatisApplication.class, args);
	}
}
```

#### 5）、配置文件版

```yaml
mybatis:
  config-location: classpath:mybatis/mybatis-config.xml 指定全局配置文件的位置
  mapper-locations: classpath:mybatis/mapper/*.xml  指定sql映射文件的位置
```

开启驼峰映射

```xml
<configuration>  
    <settings>  
        <setting name="mapUnderscoreToCamelCase" value="true" />  
    </settings>  
</configuration>
```



更多使用参照

http://www.mybatis.org/spring-boot-starter/mybatis-spring-boot-autoconfigure/



### 4、整合SpringData JPA

#### 1）、SpringData简介

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306105412.png)

#### 2）、整合SpringData JPA

JPA:ORM（Object Relational Mapping）；

1）、编写一个实体类（bean）和数据表进行映射，并且配置好映射关系；

```java
//使用JPA注解配置映射关系
@Entity //告诉JPA这是一个实体类（和数据表映射的类）
@Table(name = "tbl_user") //@Table来指定和哪个数据表对应;如果省略默认表名就是user；
public class User {

    @Id //这是一个主键
    @GeneratedValue(strategy = GenerationType.IDENTITY)//自增主键
    private Integer id;

    @Column(name = "last_name",length = 50) //这是和数据表对应的一个列
    private String lastName;
    @Column //省略默认列名就是属性名
    private String email;
```

2）、编写一个Dao接口来操作实体类对应的数据表（Repository）

```java
//继承JpaRepository来完成对数据库的操作
public interface UserRepository extends JpaRepository<User,Integer> {
}

```

3）、基本的配置JpaProperties

```yaml
spring:  
 jpa:
    hibernate:
#     更新或者创建数据表结构
      ddl-auto: update
#    控制台显示SQL
    show-sql: true
```

#### 3)问题

##### No serializer found for class org.hibernate.proxy.pojo.bytebuddy.ByteBuddyIn

###### **@JsonIgnoreProperties(vh4alue={"hibernateLazyInitializer"})** 属性

在此标记不生成json对象的属性

因为jsonplugin用的是java的内审机制.hibernate会给被管理的pojo加入一个hibernateLazyInitializer属性,jsonplugin会把hibernateLazyInitializer也拿出来操作,并读取里面一个不能被反射操作的属性就产生了这个异常.  



用annotation来排除hibernateLazyInitializer 这个属性

在你的pojo类声明加上：

```java
@JsonIgnoreProperties(value={"hibernateLazyInitializer"})  
@JsonIgnoreProperties就是标注 哪个属性 不用转化为json的
```



## 六、启动配置原理



几个重要的事件回调机制

配置在META-INF/spring.factories

**ApplicationContextInitializer**

**SpringApplicationRunListener**



只需要放在ioc容器中

**ApplicationRunner**

**CommandLineRunner**



启动流程：

### **1、创建SpringApplication对象**

```java
initialize(sources);
private void initialize(Object[] sources) {
    //保存主配置类
    if (sources != null && sources.length > 0) {
        this.sources.addAll(Arrays.asList(sources));
    }
    //判断当前是否一个web应用
    this.webEnvironment = deduceWebEnvironment();
    //从类路径下找到META-INF/spring.factories配置的所有ApplicationContextInitializer；然后保存起来
    setInitializers((Collection) getSpringFactoriesInstances(
        ApplicationContextInitializer.class));
    //从类路径下找到ETA-INF/spring.factories配置的所有ApplicationListener
    setListeners((Collection) getSpringFactoriesInstances(ApplicationListener.class));
    //从多个配置类中找到有main方法的主配置类
    this.mainApplicationClass = deduceMainApplicationClass();
}
```

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306145727.png)

![](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/%E6%90%9C%E7%8B%97%E6%88%AA%E5%9B%BE20180306145855.png)

### 2、运行run方法

```java
public ConfigurableApplicationContext run(String... args) {
   StopWatch stopWatch = new StopWatch();
   stopWatch.start();
   ConfigurableApplicationContext context = null;
   FailureAnalyzers analyzers = null;
   configureHeadlessProperty();
    
   //获取SpringApplicationRunListeners；从类路径下META-INF/spring.factories
   SpringApplicationRunListeners listeners = getRunListeners(args);
    //回调所有的获取SpringApplicationRunListener.starting()方法
   listeners.starting();
   try {
       //封装命令行参数
      ApplicationArguments applicationArguments = new DefaultApplicationArguments(
            args);
      //准备环境
      ConfigurableEnvironment environment = prepareEnvironment(listeners,
            applicationArguments);
       		//创建环境完成后回调SpringApplicationRunListener.environmentPrepared()；表示环境准备完成
       
      Banner printedBanner = printBanner(environment);
       
       //创建ApplicationContext；决定创建web的ioc还是普通的ioc
      context = createApplicationContext();
       
      analyzers = new FailureAnalyzers(context);
       //准备上下文环境;将environment保存到ioc中；而且applyInitializers()；
       //applyInitializers()：回调之前保存的所有的ApplicationContextInitializer的initialize方法
       //回调所有的SpringApplicationRunListener的contextPrepared()；
       //
      prepareContext(context, environment, listeners, applicationArguments,
            printedBanner);
       //prepareContext运行完成以后回调所有的SpringApplicationRunListener的contextLoaded（）；
       
       //s刷新容器；ioc容器初始化（如果是web应用还会创建嵌入式的Tomcat）；Spring注解版
       //扫描，创建，加载所有组件的地方；（配置类，组件，自动配置）
      refreshContext(context);
       //从ioc容器中获取所有的ApplicationRunner和CommandLineRunner进行回调
       //ApplicationRunner先回调，CommandLineRunner再回调
      afterRefresh(context, applicationArguments);
       //所有的SpringApplicationRunListener回调finished方法
      listeners.finished(context, null);
      stopWatch.stop();
      if (this.logStartupInfo) {
         new StartupInfoLogger(this.mainApplicationClass)
               .logStarted(getApplicationLog(), stopWatch);
      }
       //整个SpringBoot应用启动完成以后返回启动的ioc容器；
      return context;
   }
   catch (Throwable ex) {
      handleRunFailure(context, listeners, analyzers, ex);
      throw new IllegalStateException(ex);
   }
}
```

### 3、事件监听机制

配置在META-INF/spring.factories

**ApplicationContextInitializer**

```java
public class HelloApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println("ApplicationContextInitializer...initialize..."+applicationContext);
    }
}

```

**SpringApplicationRunListener**

```java
public class HelloSpringApplicationRunListener implements SpringApplicationRunListener {

    //必须有的构造器
    public HelloSpringApplicationRunListener(SpringApplication application, String[] args){

    }

    @Override
    public void starting() {
        System.out.println("SpringApplicationRunListener...starting...");
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {
        Object o = environment.getSystemProperties().get("os.name");
        System.out.println("SpringApplicationRunListener...environmentPrepared.."+o);
    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {
        System.out.println("SpringApplicationRunListener...contextPrepared...");
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
        System.out.println("SpringApplicationRunListener...contextLoaded...");
    }

    @Override
    public void finished(ConfigurableApplicationContext context, Throwable exception) {
        System.out.println("SpringApplicationRunListener...finished...");
    }
}

```

配置（META-INF/spring.factories）

```properties
org.springframework.context.ApplicationContextInitializer=\com.atguigu.springboot.listener.HelloApplicationContextInitializerorg.springframework.boot.SpringApplicationRunListener=\com.atguigu.springboot.listener.HelloSpringApplicationRunListener
```





只需要放在ioc容器中

**ApplicationRunner**

```java
@Component
public class HelloApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("ApplicationRunner...run....");
    }
}
```



**CommandLineRunner**

```java
@Component
public class HelloCommandLineRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("CommandLineRunner...run..."+ Arrays.asList(args));
    }
}
```



### 七、自定义starter

starter：

​	1、这个场景需要使用到的依赖是什么？

​	2、如何编写自动配置

```java
@Configuration  //指定这个类是一个配置类
@ConditionalOnXXX  //在指定条件成立的情况下自动配置类生效
@AutoConfigureAfter  //指定自动配置类的顺序
@Bean  //给容器中添加组件

@ConfigurationPropertie结合相关xxxProperties类来绑定相关的配置
@EnableConfigurationProperties //让xxxProperties生效加入到容器中

自动配置类要能加载
将需要启动就加载的自动配置类，配置在META-INF/spring.factories
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.SpringApplicationAdminJmxAutoConfiguration,\
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
```

​	3、模式：

启动器只用来做依赖导入；

专门来写一个自动配置模块；

启动器依赖自动配置；别人只需要引入启动器（starter）

mybatis-spring-boot-starter；自定义启动器名-spring-boot-starter



步骤：

1）、启动器模块

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.atguigu.starter</groupId>
    <artifactId>atguigu-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!--启动器-->
    <dependencies>

        <!--引入自动配置模块-->
        <dependency>
            <groupId>com.atguigu.starter</groupId>
            <artifactId>atguigu-spring-boot-starter-autoconfigurer</artifactId>
            <version>0.0.1-SNAPSHOT</version>
        </dependency>
    </dependencies>

</project>
```

2）、自动配置模块

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
   <modelVersion>4.0.0</modelVersion>

   <groupId>com.atguigu.starter</groupId>
   <artifactId>atguigu-spring-boot-starter-autoconfigurer</artifactId>
   <version>0.0.1-SNAPSHOT</version>
   <packaging>jar</packaging>

   <name>atguigu-spring-boot-starter-autoconfigurer</name>
   <description>Demo project for Spring Boot</description>

   <parent>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-parent</artifactId>
      <version>1.5.10.RELEASE</version>
      <relativePath/> <!-- lookup parent from repository -->
   </parent>

   <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
      <java.version>1.8</java.version>
   </properties>

   <dependencies>

      <!--引入spring-boot-starter；所有starter的基本配置-->
      <dependency>
         <groupId>org.springframework.boot</groupId>
         <artifactId>spring-boot-starter</artifactId>
      </dependency>

   </dependencies>



</project>

```



```java
package com.atguigu.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "atguigu.hello")
public class HelloProperties {

    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}

```

```java
package com.atguigu.starter;

public class HelloService {

    HelloProperties helloProperties;

    public HelloProperties getHelloProperties() {
        return helloProperties;
    }

    public void setHelloProperties(HelloProperties helloProperties) {
        this.helloProperties = helloProperties;
    }

    public String sayHellAtguigu(String name){
        return helloProperties.getPrefix()+"-" +name + helloProperties.getSuffix();
    }
}

```

```java
package com.atguigu.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnWebApplication //web应用才生效
@EnableConfigurationProperties(HelloProperties.class)
public class HelloServiceAutoConfiguration {

    @Autowired
    HelloProperties helloProperties;
    @Bean
    public HelloService helloService(){
        HelloService service = new HelloService();
        service.setHelloProperties(helloProperties);
        return service;
    }
}

```



## 七、SpringBoot与缓存



### 1、JSR107



**Java Caching定义了5个核心接口，分别是CachingProvider, CacheManager, Cache, Entry 和 Expiry。**

•**CachingProvider**定义了创建、配置、获取、管理和控制多个**CacheManager**。一个应用可以在运行期访问多个CachingProvider。

•**CacheManager**定义了创建、配置、获取、管理和控制多个唯一命名的**Cache**，这些Cache存在于CacheManager的上下文中。一个CacheManager仅被一个CachingProvider所拥有。

•**Cache**是一个类似Map的数据结构并临时存储以Key为索引的值。一个Cache仅被一个CacheManager所拥有。

•**Entry**是一个存储在Cache中的key-value对。

​		Expiry 每一个存储在Cache中的条目有一个定义的有效期。一旦超过这个时间，条目为过期的状态。一旦过期，条目将不可访问、更新和删除。缓存有效期可以通过ExpiryPolicy设置

![image-20220325104246314](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325104246314.png)

### 2、Spring缓存抽象



**Spring从3.1开始定义了org.springframework.cache.Cache 和 org.springframework.cache.CacheManager接口来统一不同的缓存技术；并支持使用JCache（JSR-107）注解简化我们开发；**

•Cache接口为缓存的组件规范定义，包含缓存的各种操作集合；

•Cache接口下Spring提供了各种xxxCache的实现；如RedisCache，EhCacheCache , ConcurrentMapCache等；

•每次调用需要缓存功能的方法时，Spring会检查检查指定参数的指定的目标方法是否已经被调用过；如果有就直接从缓存中获取方法调用后的结果，如果没有就调用方法并缓存结果后返回给用户。下次调用直接从缓存中获取。

•使用Spring缓存抽象时我们需要关注以下两点；

​		1、确定方法需要被缓存以及他们的缓存策略

​		2、从缓存中读取之前缓存存储的数据

### 3、几个重要概念&缓存注解



| **Cache**          | **缓存接口，定义缓存操作。实现有：****RedisCache****、****EhCacheCache****、****ConcurrentMapCache****等** |
| ------------------ | ------------------------------------------------------------ |
| **CacheManager**   | **缓存管理器，管理各种缓存（****Cache****）组件**            |
| **@Cacheable**     | **主要针对方法配置，能够根据方法的请求参数对其结果进行缓存** |
| **@CacheEvict**    | **清空缓存**                                                 |
| **@CachePut**      | **保证方法被调用，又希望结果被缓存。**                       |
| **@EnableCaching** | **开启基于注解的缓存**                                       |
| **keyGenerator**   | **缓存数据时****key****生成策略**                            |
| **serialize**      | **缓存数据时****value****序列化策略**                        |



| **@Cacheable/@CachePut/@CacheEvict** **主要的参数** |                                                              |
| --------------------------------------------------- | ------------------------------------------------------------ |
| value                                               | 缓存的名称，在  spring 配置文件中定义，必须指定至少一个      |
| key                                                 | 缓存的  key，可以为空，如果指定要按照  **SpEL 表达式**编写，如果不指定，则缺省按照方法的所有参数进行组合 |
| condition                                           | 缓存的条件，可以为空，使用  SpEL 编写，返回  true 或者 false，只有为  true 才进行缓存/清除缓存，在调用方法之前之后都能判断 |
| allEntries  (**@CacheEvict**  )                     | 是否清空所有缓存内容，缺省为  false，如果指定为 true，则方法调用后将立即清空所有缓存 |
| beforeInvocation  **(@CacheEvict)**                 | 是否在方法执行前就清空，缺省为  false，如果指定为 true，则在方法还没有执行的时候就清空缓存，缺省情况下，如果方法执行抛出异常，则不会清空缓存 |
| unless  **(@CachePut)**  **(@Cacheable)**           | 用于否决缓存的，不像condition，该表达式只在方法执行之后判断，此时可以拿到返回值result进行判断。条件为true不会缓存，fasle才缓存 |



**Cache** **SpEL** **规则**

| **名字**        | **位置**           | **描述**                                                     | **示例**             |
| --------------- | ------------------ | ------------------------------------------------------------ | -------------------- |
| methodName      | root object        | 当前被调用的方法名                                           | #root.methodName     |
| method          | root object        | 当前被调用的方法                                             | #root.method.name    |
| target          | root object        | 当前被调用的目标对象                                         | #root.target         |
| targetClass     | root object        | 当前被调用的目标对象类                                       | #root.targetClass    |
| args            | root object        | 当前被调用的方法的参数列表                                   | #root.args[0]        |
| caches          | root object        | 当前方法调用使用的缓存列表（如@Cacheable(value={"cache1",  "cache2"})），则有两个cache | #root.caches[0].name |
| *argument name* | evaluation context | 方法参数的名字. 可以直接 #参数名 ，也可以使用 #p0或#a0 的形式，0代表参数的索引； | #iban 、 #a0 、 #p0  |
| result          | evaluation context | 方法执行后的返回值（仅当方法执行之后的判断有效，如‘unless’，’cache put’的表达式 ’cache evict’的表达式beforeInvocation=false） | #result              |



### 4、缓存使用



#### 1）缓存Demo

```
@CacheConfig(cacheNames="emp"/*,cacheManager = "employeeCacheManager"*/) //抽取缓存的公共配置
@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;

    /**
     * 将方法的运行结果进行缓存；以后再要相同的数据，直接从缓存中获取，不用调用方法；
     * CacheManager管理多个Cache组件的，对缓存的真正CRUD操作在Cache组件中，每一个缓存组件有自己唯一一个名字；
     *

     *
     * 原理：
     *   1、自动配置类；CacheAutoConfiguration
     *   2、缓存的配置类
     *   org.springframework.boot.autoconfigure.cache.GenericCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.JCacheCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.EhCacheCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.HazelcastCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.InfinispanCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.CouchbaseCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.RedisCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.CaffeineCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.GuavaCacheConfiguration
     *   org.springframework.boot.autoconfigure.cache.SimpleCacheConfiguration【默认】
     *   org.springframework.boot.autoconfigure.cache.NoOpCacheConfiguration
     *   3、哪个配置类默认生效：SimpleCacheConfiguration；
     *
     *   4、给容器中注册了一个CacheManager：ConcurrentMapCacheManager
     *   5、可以获取和创建ConcurrentMapCache类型的缓存组件；他的作用将数据保存在ConcurrentMap中；
     *
     *   运行流程：
     *   @Cacheable：
     *   1、方法运行之前，先去查询Cache（缓存组件），按照cacheNames指定的名字获取；
     *      （CacheManager先获取相应的缓存），第一次获取缓存如果没有Cache组件会自动创建。
     *   2、去Cache中查找缓存的内容，使用一个key，默认就是方法的参数；
     *      key是按照某种策略生成的；默认是使用keyGenerator生成的，默认使用SimpleKeyGenerator生成key；
     *          SimpleKeyGenerator生成key的默认策略；
     *                  如果没有参数；key=new SimpleKey()；
     *                  如果有一个参数：key=参数的值
     *                  如果有多个参数：key=new SimpleKey(params)；
     *   3、没有查到缓存就调用目标方法；
     *   4、将目标方法返回的结果，放进缓存中
     *
     *   @Cacheable标注的方法执行之前先来检查缓存中有没有这个数据，默认按照参数的值作为key去查询缓存，
     *   如果没有就运行方法并将结果放入缓存；以后再来调用就可以直接使用缓存中的数据；
     *
     *   核心：
     *      1）、使用CacheManager【ConcurrentMapCacheManager】按照名字得到Cache【ConcurrentMapCache】组件
     *      2）、key使用keyGenerator生成的，默认是SimpleKeyGenerator
     *
     *
     *   几个属性：
     *      cacheNames/value：指定缓存组件的名字;将方法的返回结果放在哪个缓存中，是数组的方式，可以指定多个缓存；
     *
     *      key：缓存数据使用的key；可以用它来指定。默认是使用方法参数的值  1-方法的返回值
     *              编写SpEL； #i d;参数id的值   #a0  #p0  #root.args[0]
     *              getEmp[2]
     *
     *      keyGenerator：key的生成器；可以自己指定key的生成器的组件id
     *              key/keyGenerator：二选一使用;
     *
     *
     *      cacheManager：指定缓存管理器；或者cacheResolver指定获取解析器
     *
     *      condition：指定符合条件的情况下才缓存；
     *              ,condition = "#id>0"
     *          condition = "#a0>1"：第一个参数的值》1的时候才进行缓存
     *
     *      unless:否定缓存；当unless指定的条件为true，方法的返回值就不会被缓存；可以获取到结果进行判断
     *              unless = "#result == null"
     *              unless = "#a0==2":如果第一个参数的值是2，结果不缓存；
     *      sync：是否使用异步模式
     * @param id
     * @return
     *
     */
    @Cacheable(value = {"emp"}/*,keyGenerator = "myKeyGenerator",condition = "#a0>1",unless = "#a0==2"*/)
    public Employee getEmp(Integer id){
        System.out.println("查询"+id+"号员工");
        Employee emp = employeeMapper.getEmpById(id);
        return emp;
    }

    /**
     * @CachePut：既调用方法，又更新缓存数据；同步更新缓存
     * 修改了数据库的某个数据，同时更新缓存；
     * 运行时机：
     *  1、先调用目标方法
     *  2、将目标方法的结果缓存起来
     *
     * 测试步骤：
     *  1、查询1号员工；查到的结果会放在缓存中；
     *          key：1  value：lastName：张三
     *  2、以后查询还是之前的结果
     *  3、更新1号员工；【lastName:zhangsan；gender:0】
     *          将方法的返回值也放进缓存了；
     *          key：传入的employee对象  值：返回的employee对象；
     *  4、查询1号员工？
     *      应该是更新后的员工；
     *          key = "#employee.id":使用传入的参数的员工id；
     *          key = "#result.id"：使用返回后的id
     *             @Cacheable的key是不能用#result
     *      为什么是没更新前的？【1号员工没有在缓存中更新】
     *
     */
    @CachePut(/*value = "emp",*/key = "#result.id")
    public Employee updateEmp(Employee employee){
        System.out.println("updateEmp:"+employee);
        employeeMapper.updateEmp(employee);
        return employee;
    }

    /**
     * @CacheEvict：缓存清除
     *  key：指定要清除的数据
     *  allEntries = true：指定清除这个缓存中所有的数据
     *  beforeInvocation = false：缓存的清除是否在方法之前执行
     *      默认代表缓存清除操作是在方法执行之后执行;如果出现异常缓存就不会清除
     *
     *  beforeInvocation = true：
     *      代表清除缓存操作是在方法运行之前执行，无论方法是否出现异常，缓存都清除
     *
     *
     */
    @CacheEvict(value="emp",beforeInvocation = true/*key = "#id",*/)
    public void deleteEmp(Integer id){
        System.out.println("deleteEmp:"+id);
        //employeeMapper.deleteEmpById(id);
        int i = 10/0;
    }

    // @Caching 定义复杂的缓存规则
    @Caching(
         cacheable = {
             @Cacheable(/*value="emp",*/key = "#lastName")
         },
         put = {
             @CachePut(/*value="emp",*/key = "#result.id"),
             @CachePut(/*value="emp",*/key = "#result.email")
         }
    )
    public Employee getEmpByLastName(String lastName){
        return employeeMapper.getEmpByLastName(lastName);
    }




}

```

#### 2）整合redis

#####  引入starter

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

##### 配置redis客户端IP/端口

```yaml
spring:
  redis:
    host: 192.168.153.4
```

##### 自定义redis 的序列化机制为Json

```java
/**
 * @author aixz
 */
@Configuration
public class RedisConfig {

    private final Jackson2JsonRedisSerializer<Object> jsonSerializer = new Jackson2JsonRedisSerializer<>(Object.class);


    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, Object>  template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        // 使用JSON格式序列化对象，对缓存数据key和value进行转换

        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);

        jsonSerializer.setObjectMapper(om);

        // 设置RedisTemplate模板API的序列化方式为JSON
        template.setDefaultSerializer(jsonSerializer);
        return template;
    }

    @Bean
    public RedisCacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        // 分别创建String和JSON格式序列化对象，对缓存数据key和value进行转换
        RedisSerializer<String> strSerializer = new StringRedisSerializer();
        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);

        jsonSerializer.setObjectMapper(om);

        // 定制缓存数据序列化方式及时效
        //.entryTtl(Duration.ofDays(1)) 缓存数据有效期设为一天
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofDays(1))
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(strSerializer))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jsonSerializer))
                .disableCachingNullValues();

        return RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(config).build();
    }
}
```



## 八、SpringBoot与消息



### 1、概述



**1.大多应用中，可通过消息服务中间件来提升系统异步通信、扩展解耦能力**

**2.消息服务中两个重要概念：**

​    消息代理（message broker）和目的地（destination）

​	当消息发送者发送消息以后，将由消息代理接管，消息代理保证消息传递到指定目的地。

**3.消息队列主要有两种形式的目的地**

​		**1.队列（queue）：点对点消息通信（point-to-point）**

​		**2.主题（topic）：发布（publish）/订阅（subscribe）消息通信**



![image-20220325110317361](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325110317361.png)



![image-20220325110352331](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325110352331.png)



**4.点对点式：**

​	–消息发送者发送消息，消息代理将其放入一个队列中，消息接收者从队列中获取消息内容，消息读取后被移出队列

​	–消息只有唯一的发送者和接受者，但并不是说只能有一个接收者

**5.发布订阅式：**

​	–发送者（发布者）发送消息到主题，多个接收者（订阅者）监听（订阅）这个主题，那么就会在消息到达时同时收到消息

**6.JMS（Java Message Service）JAVA消息服务：**

​	–基于JVM消息代理的规范。ActiveMQ、HornetMQ是JMS实现

**7.AMQP（Advanced Message Queuing Protocol）**

​	–高级消息队列协议，也是一个消息代理的规范，兼容JMS

​	–RabbitMQ是AMQP的实现



|              | JMS                                                          | AMQP                                                         |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 定义         | Java  api                                                    | 网络线级协议                                                 |
| 跨语言       | 否                                                           | 是                                                           |
| 跨平台       | 否                                                           | 是                                                           |
| Model        | 提供两种消息模型：  （1）、Peer-2-Peer  （2）、Pub/sub       | 提供了五种消息模型：  （1）、direct  exchange  （2）、fanout  exchange  （3）、topic  change  （4）、headers  exchange  （5）、system  exchange  本质来讲，后四种和JMS的pub/sub模型没有太大差别，仅是在路由机制上做了更详细的划分； |
| 支持消息类型 | 多种消息类型：  TextMessage  MapMessage  BytesMessage  StreamMessage  ObjectMessage  Message  （只有消息头和属性） | byte[]  当实际应用时，有复杂的消息，可以将消息序列化后发送。 |
| 综合评价     | JMS  定义了JAVA  API层面的标准；在java体系中，多个client均可以通过JMS进行交互，不需要应用修改代码，但是其对跨平台的支持较差； | AMQP定义了wire-level层的协议标准；天然具有跨平台、跨语言特性。 |

**8.Spring支持**

​	–**spring-****jms****提供了对****JMS****的支持**

​	–**spring-rabbit****提供了对****AMQP****的支持**

​	–**需要****ConnectionFactory****的实现来连接消息代理**

​	–**提供****JmsTemplate****、****RabbitTemplate****来发送消息**

​	–**@JmsListener****（****JMS****）、****@RabbitListener****（****AMQP****）注解在方法上监听消息代理发布的消息**

​	–**@EnableJms****、****@EnableRabbit****开启支持**

**9.Spring Boot自动配置**

​	–**JmsAutoConfiguration**

​	–**RabbitAutoConfiguration**



### 2、RabbitMQ简介



**RabbitMQ简介：**

​		RabbitMQ是一个由erlang开发的AMQP(Advanved Message Queue Protocol)的开源实现。

**核心概念**

**Message**

​		消息，消息是不具名的，它由消息头和消息体组成。消息体是不透明的，而消息头则由一系列的可选属性组成，这些属性包括routing-key（路由键）、priority（相对于其他消息的优先权）、delivery-mode（指出该消息可能需要持久性存储）等。

**Publisher**

​		消息的生产者，也是一个向交换器发布消息的客户端应用程序。

**Exchange**

​		交换器，用来接收生产者发送的消息并将这些消息路由给服务器中的队列。

​		Exchange有4种类型：direct(默认)，fanout, topic, 和headers，不同类型的Exchange转发消息的策略有所区别

**Queue**

​		消息队列，用来保存消息直到发送给消费者。它是消息的容器，也是消息的终点。一个消息可投入一个或多个队列。消息一直在队列里面，等待消费者连接到这个队列将其取走。

**Binding**

​		绑定，用于消息队列和交换器之间的关联。一个绑定就是基于路由键将交换器和消息队列连接起来的路由规则，所以可以将交换器理解成一个由绑定构成的路由表。

Exchange 和Queue的绑定可以是多对多的关系。

**Connection**

​		网络连接，比如一个TCP连接。

**Channel**

​		信道，多路复用连接中的一条独立的双向数据流通道。信道是建立在真实的TCP连接内的虚拟连接，AMQP 命令都是通过信道发出去的，不管是发布消息、订阅队列还是接收消息，这些动作都是通过信道完成。因为对于操作系统来说建立和销毁 TCP 都是非常昂贵的开销，所以引入了信道的概念，以复用一条 TCP 连接。

**Consumer**

​		消息的消费者，表示一个从消息队列中取得消息的客户端应用程序。

**Virtual Host**

​		虚拟主机，表示一批交换器、消息队列和相关对象。虚拟主机是共享相同的身份认证和加密环境的独立服务器域。每个 vhost 本质上就是一个 mini 版的 RabbitMQ 服务器，拥有自己的队列、交换器、绑定和权限机制。vhost 是 AMQP 概念的基础，必须在连接时指定，RabbitMQ 默认的 vhost 是 / 。

**Broker**

​		表示消息队列服务器实体

![image-20220325111102337](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111102337.png)



### 3、RabbitMQ运行机制



**AMQP 中的消息路由**

​			•AMQP 中消息的路由过程和 Java 开发者熟悉的 JMS 存在一些差别，AMQP 中增加了 **Exchange** 和 **Binding** 的角色。生产者把消息发布到 Exchange 上，消息最终到达队列并被消费者接收，而 Binding 决定交换器的消息应该发送到那个队列。

![image-20220325111215116](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111215116.png)



**Exchange 类型**

​		•**Exchange**分发消息时根据类型的不同分发策略有区别，目前共四种类型：**direct****、****fanout****、****topic****、****headers** 。headers 匹配 AMQP 消息的 header 而不是路由键， headers 交换器和 direct 交换器完全一致，但性能差很多，目前几乎用不到了，所以直接看另外三种类型：

![image-20220325111323518](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111323518.png)

消息中的路由键（routing key）如果和 Binding 中的 binding key 一致， 交换器就将消息发到对应的队列中。路由键与队列名完全匹配，如果一个队列绑定到交换机要求路由键为“dog”，则只转发 routing key 标记为“dog”的消息，不会转发“dog.puppy”，也不会转发“dog.guard”等等。它是完全匹配、单播的模式。



![image-20220325111347553](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111347553.png)

每个发到 fanout 类型交换器的消息都会分到所有绑定的队列上去。fanout 交换器不处理路由键，只是简单的将队列绑定到交换器上，每个发送到交换器的消息都会被转发到与该交换器绑定的所有队列上。很像子网广播，每台子网内的主机都获得了一份复制的消息。fanout 类型转发消息是最快的。



![image-20220325111418489](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220325111418489.png)

topic 交换器通过模式匹配分配消息的路由键属性，将路由键和某个模式进行匹配，此时队列需要绑定到一个模式上。它将路由键和绑定键的字符串切分成单词，这些**单词之间用点隔开**。它同样也会识别两个通配符：符号“#”和符号“**”**。**#**匹配**0**个或多个单词**，****匹配一个单词。



### 4、RabbitMQ整合



**1.引入 spring-boot-starter-amqp**

**2.application.yml配置**

**3.测试RabbitMQ**

​		1)AmqpAdmin：管理组件

​		2)RabbitTemplate：消息发送处理组件

#### 缓存Demo

1、主配置文件开启注解支持

```java
@EnableRabbit
@SpringBootApplication
public class BootAmqp01Application {

    public static void main(String[] args) {
        SpringApplication.run(BootAmqp01Application.class, args);
    }

}
```

2、发布端发送消息

```java
@SpringBootTest
class BootAmqp01ApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    AmqpAdmin amqpAdmin;


    @Test
    void textAmqpAdmin(){
        //创建一个交换器
        amqpAdmin.declareExchange(new FanoutExchange("amqp.ex"));
        //创建一个队列
        amqpAdmin.declareQueue(new Queue("amqp.queue",true));
        //创建一个绑定
        amqpAdmin.declareBinding(new Binding("amqp.queue",QUEUE,"amqp.ex","amqp",null));
    }



    @Test
    void contextLoads() {
        Map<String,Object> mes =  new HashMap<>();
        mes.put("name","Jask");
        mes.put("sex","M");
        mes.put("students", Arrays.asList("zhangsan","lisi","wangwu"));

       // rabbitTemplate.convertAndSend("aixz.direct","aixz.hello",mes);

        rabbitTemplate.convertAndSend("aixz.direct","aixz.hello","hello word");
    }

    /**
     * 测试收取消息
     */
    @Test
    void testRabbit(){
        Object o = rabbitTemplate.receiveAndConvert("aixz.hello");
        assert o != null;
        System.out.println(o.getClass());
        System.out.println(o);
    }

    /**
     *测试发送对象
     */
    @Test
    void testObjectRabbit(){
        Book book = new Book("西游记","吴承恩");
        rabbitTemplate.convertAndSend("amqp.ex","amqp",book);
    }
}
```

3、订阅端使用@RabbitListener订阅并消费信息

```java
@Service()
public class BookService {

    @RabbitListener(queues="amqp.queue")
    public void receive(Book book){
        System.out.println(book);
    }
}
```

4、将Jackson2JsonMessageConverter 转换器, 显式声明 转换bean

```
@Configuration
public class JacksonConverterConfig {
    public static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
    }

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter(){
        return new Jackson2JsonMessageConverter(objectMapper);
    }
}
```



#### [rabbitmq的序列化反序列化格式](https://www.cnblogs.com/gne-hwz/p/15668947.html)



SpringBoot封装了rabbitmq中，发送对象和接收对象时，会统一将对象和消息互相转换

会用到MessageConverter转换接口

**发送消息时，**

会将Object转换成Message Message createMessage(Object object, MessageProperties messageProperties)

**接收消息时**

SimpleMessageListenerContainer容器监听消息时，会调用SimpleMessageListenerContainer.messageListener的消息转换器将Message转换成对象 Object fromMessage(Message message)



Springboot中，默认的rabbitMq的序列化类是：SimpleMessageConverter

SimpleMessageConverter

将Object对象 和 Message 互相转换的规则



```
如果是 byte[] 或者 String 类型，直接获取字节 String.getBytes(this.defaultCharset)
设置messageProperties的 contentType = text/plain

如果是其他对象，调用 SerializationUtils.serialize(object) 进行序列化
设置messageProperties的 contentType = application/x-java-serialized-object
```

这里存在一个问题

如果发送方： object类是 com.zwh.user

但是接受方没有这个路径的类时，会抛出异常，not found class

 

所以发送消息时，最好手动将消息转换成String



Jackson2JsonMessageConverter 转换器



```
序列化时，Object 转成 json字符串在设置到 Message.body()中。 调用的是 jackson的序列化方式
反序列化时，将 Message.body()内容反序列化成 Object
```

这个方式不会出现上面SimpleMessageConverter转换器的 not found class错误



如果项目中要统一的序列化格式, 需要显示声明 转换bean



```
@Bean
public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
    return new Jackson2JsonMessageConverter();
}
```

因为自动配置类**RabbitAutoConfiguration** 和 **RabbitAnnotationDrivenConfiguration**

**RabbitAutoConfiguration**

![image-20220328155837858](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220328155837858.png)

**RabbitAnnotationDrivenConfiguration**

![image-20220328160005061](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220328160005061.png)

会自动获取Spring 容器中MessageConverter实现类bean数据

并将转换器设置到 RabbitTemplate 和 SimpleRabbitListenerContainerFactory.setMessageConverter() 中



```
RabbitAutoConfiguration 配置 发送端 RabbitTemplate 
RabbitAnnotationDrivenConfiguration 配置接收端 SimpleRabbitListenerContainerFactory
```

 

注意：如果 RabbitTemplate 配置的 jackson 序列化，而 listener没有配置（默认SimpleMessageConverter），则接受消息转换成object时将会报错

```
ListenerExecutionFailedException: Listener method could not be invoked with the incoming message
Fatal message conversion error; message rejected; it will be dropped or routed to a dead letter exchange
消息转换错误，消息被拒绝
```



## 九、SpringBoot与检索

我们的应用经常需要添加检索功能，开源的 [ElasticSearch](https://www.elastic.co/) 是目前全文搜索引擎的首选。他可以快速的存储、搜索和分析海量数据。Spring Boot通过整合Spring Data ElasticSearch为我们提供了非常便捷的检索功能支持；



Elasticsearch是一个分布式搜索服务，提供Restful API，底层基于Lucene，采用多shard（分片）的方式保证数据安全，并且提供自动resharding的功能，github等大型的站点也是采用了ElasticSearch作为其搜索服务，

### 一、概念



**•以 *员工文档* 的形式存储为例：一个文档代表一个员工数据。存储数据到 ElasticSearch 的行为叫做 *索引* ，但在索引一个文档之前，需要确定将文档存储在哪里。**

•一个 ElasticSearch 集群可以 包含多个 *索引* ，相应的每个索引可以包含多个 *类型* 。 这些不同的类型存储着多个 *文档* ，每个文档又有 多个 *属性* 。

**•类似关系：**

​				**–索引-数据库**

​				**–类型-表**

​				**–文档-表中的记录**

​				**–属性-列**

### 二、安装

#### **（一）Elastic search**

\1. 拉取镜像：docker pull elasticsearch:7.4.2

\2. 创建容器：docker run -d --name es -p 9200:9200 -p 9300:9300 -e ES_JAVA_OPTS="-Xms512m -Xmx512m" -e "discovery.type=single-node" elasticsearch:7.4.2

\3. 进入es容器，在文件config/elasticsearch.yml中增加以下跨域信息：

```yaml
http.cors.enabled: true

http.cors.allow-origin: "*"

xpack.security.enabled: false
```

\4. 重启es容器

 

#### **（二）Es-head 可视化插件**

\1. 拉取镜像：docker pull elasticsearch-head:5

\2. 创建容器：docker run -d -p 9100:9100 --name es-head mobz/elasticsearch-head:5

\3. 访问http://localhost:9100，如下图所示代表连接成功

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135206014-806376828.png)

\4. 新建索引时，若报如下错误：

{"error":"Content-Type header [application/x-www-form-urlencoded] is not supported","status":406}

解决方法：进入es-head容器中-》进入_site文件夹-》修改vendor.js两处代码

①　第6686行：

contentType: "application/x-www-form-urlencoded", 

-》

contentType: "application/json;charset=UTF-8",

②　第7574行：

var inspectData = s.contentType === "application/x-www-form-urlencoded" &&

-》

var inspectData = s.contentType === "application/json;charset=UTF-8" &&

 

#### **（三）Kibana**

\1. 拉取镜像(版本与es一致)：docker pull kibana:7.4.2

\2. 创建容器：docker run -d -p 5601:5601 --name kibana kibana:7.4.2

\3. 进入kibana容器，在文件config/kibana.yml中，修改hosts地址

[http://elasticserach:9200](http://elasticserach:9200/) 改为

http://172.17.0.1:9200

\4. 重启kibana容器后访问http://localhost:5601，若访问报错：Kibana server is not ready yet，耐心等待一会，若还报错则进入容器执行以下两个命令后再次重启kibana容器：

①　curl -u elastic:changeme -XDELETE 172.17.0.1:9200/_xpack/security/privilege/kibana-.kibana/space_all

②　curl -u elastic:changeme -XDELETE 172.17.0.1:9200/_xpack/security/privilege/kibana-.kibana/space_read

\5. 汉化设置：进入kibana容器中，在config/kibana.yml文件最后增加：i18n.locale: "zh-CN"。然后重启kibana容器即可

 

#### **（四）Logstash**

\1. 拉取镜像(版本与es一致)：docker pull logstash:7.4.2

\2. 在本地创建logstash/logstash.yml，输入以下内容

**path.config**: /usr/share/logstash/conf.d/*.conf
**path.logs**: /var/log/logstash

\3. 在本地创建logstash/conf.d/logstash_dev.conf

\4. 创建容器：

docker run -d --restart=always --log-driver json-file --log-opt max-size=100m --log-opt max-file=2 -p 5044:5044 --name logstash -v ./logstash/logstash.yml:/usr/share/logstash/config/logstash.yml -v ./logstash/conf.d/:/usr/share/logstash/conf.d/ logstash:7.4.2

 

#### **（五）IK分词器**

\1. 安装

l 进入es容器plugins文件夹中，下载ik7.4.2：

wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.4.2/elasticsearch-analysis-ik-7.4.2.zip

l 解压至ik7.4.2文件夹中：

unzip elasticsearch-analysis-ik-7.4.2.zip -d ik7.4.2

l 删除下载包及修改ik7.4.2权限

rm -rf ik-7.4.2.zip

chmod -R 777 ik7.4.2/

l 重启es容器

\2. 增加分词

l 进入到ik7.4.2/config 文件夹中

l 新增一个 xxx.dic文件，写入想要的分词

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134623767-1831195315.png)

 

l 打开IKAnalyzer.cfg.xml文件，引入刚刚新增的dic文件

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134640638-2132027795.png)

 

l 重启es容器

\3. 分词使用

打开kibana-》开发工具

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134701960-1835594534.png)

从上图可以看出 “可安” 被es当成了一个词组

 

### **三、ES基本操作**

#### **（一）Rest风格说明**

一种软件架构风格，并不是一个死规定，只是提供了一组设计原则和约束条件，用于客户端与服务端交互的软件。基于此风格设计的软件可以更简介、更有层次、更易于实现缓存等机制。

基于rest的es api设计：

| **method** | **url地址**                           | **描述**               |
| ---------- | ------------------------------------- | ---------------------- |
| PUT        | 域名/索引名称/类型名称/文档id         | 创建文档（指定文档id） |
| POST       | 域名/索引名称/类型名称                | 创建文档（随机文档id） |
| POST       | 域名/索引名称/类型名称/文档id/_update | 修改文档               |
| DELETE     | 域名/索引名称/类型名称/文档id         | 删除文档               |
| GET        | 域名/索引名称/类型名称/文档id         | 查询指定id的文档       |
| POST       | 域名/索引名称/类型名称/_search        | 查询所有数据           |

 

#### **（二）基本操作**

\1. 创建

①　创建文档数据（不推荐）：

PUT /索引名称/类型名称/文档id

{请求体}

在kibana工具中执行新增索引命令

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134738776-442433999.png)

 

在es-head工具中查看到数据

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134757210-643625934.png)

 

②　创建指定类型的索引规则：

PUT /索引名称

{请求体}

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134828429-1971627203.png)

 

③　创建默认类型的文档数据（推荐！！！）

说明：如果文档字段没有被指定，那么es会默认指定字段类型。

PUT /索引名称/_doc/文档id

{请求体}

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134853283-1862474769.png)

 

\2. 获取

①　索引文档字段类型

GET /索引名称

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134915986-97794123.png)

 

②　指定的文档数据

GET /索引名称/_doc/文档id

③　所有文档数据

POST /索引名称/_doc/_search

④　简单条件查询

GET /索引名称/_doc/_search?q=字段名:字段值

⑤　扩展命令

获取es当前信息(版本、健康值等)：GET _cat/

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134935032-1031347550.png)

 

\3. 修改：

①　PUT（不推荐），需要写全所有字段

PUT /索引名称/_doc/文档id

{请求体}

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303134950034-1640205914.png)

 

②　POST(推荐！！！)

POST /索引名称/_doc/文档id/_update

{请求体}

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135012780-2023033979.png)

 

 

\4. 删除：

①　删除索引：DELETE /索引名称

②　删除文档：DELETE /索引名称/_doc/文档id

 

#### **（三）复杂查询**

文档地址：（接口太多了，就不一一举例了）https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html

 ![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135042971-1489584238.png)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2777330-20220303135055860-1008394726.png)



#### （四）整合ElasticSearch



**•引入spring-boot-starter-data-elasticsearch**

**•安装Spring Data 对应版本的ElasticSearch**

**•application.yml配置**

**•Spring Boot自动配置的**

 **ElasticsearchRepository、ElasticsearchTemplate、Jest**

**测试ElasticSearch**



### 问题

#### 1、docker安装es-head后无法查看数据



在docker中安装es-head后，使用es-head查看索引时，无法显示数据，F12查看调用请求，发现Content-Type错误了
看一下网络流可知报406 错误

```json
{
  "error" : "Content-Type header [application/x-www-form-urlencoded] is not supported",
  "status" : 406
}
1234
```

在网上查找了相关资料，解决办法如下：

进入head插件安装目录 ，编辑/usr/src/app/_site/vendor.js

修改共有两处：

> 第6886行 : contentType: "application/x-www-form-urlencoded
> 改为 contentType: “application/json;charset=UTF-8”
> 第7573行: var inspectData = s.contentType === “application/x-www-form-urlencoded” &&
> 改为 var inspectData = s.contentType === “application/json;charset=UTF-8” &&

#### 2. received plaintext http traffic on an https channel, closing connection Netty4HttpChannel

**[2022-02-16T21:08:50,085][WARN ][o.e.x.s.t.n.SecurityNetty4HttpServerTransport] [DESKTOP-VCT39JM] received plaintext http traffic on an https channel, closing connection Netty4HttpChannel{localAddress=/[0:0:0:0:0:0:0:1]:9200, remoteAddress=/[0:0:0:0:0:0:0:1]:1172}**

解决

是因为开启了 ssl 认证。
在 ES/config/elasticsearch.yml 文件中把 `xpack.security.http.ssl:enabled` 设置成 `false` 即可

```yml
# Enable encryption for HTTP API client connections, such as Kibana, Logstash, and Agents
xpack.security.http.ssl:
  enabled: false
  keystore.path: certs/http.p12
```

#### 3. elasticsearch 账号密码

windows 下直接启动 ElasticSearch ，见到 started 为成功启动，访问 htttp://localhost:9200 需要输入密码，是因为开启了密码验证模式。
找了一轮没看到有账号密码，干脆就设置免密登录就好。

解决

找到 `elasticsearch.yml` 文件， 把 `xpack.security.enabled` 属性设置为 `false` 即可。

```yml
# Enable security features
xpack.security.enabled: false
```

#### 4. 设置内存大小

ES 的内存是自己调节的。在 `config/jvm.options` 文件中直接设置就好(追加):

```yml
-Xms512m
-Xmx2048m
```

#### 5. windows Could not rename log file ‘logs/gc.log’ to ‘logs/gc.log.14’ (Permission denied).

ES 在 windows 中只允许打开一个应用程序，当你再想去创建一个 ES 应用程序的时候，就会显示 `Permission denied`，即使是使`用 cmd 管理员运行 elasticsearch.bat 文件`也是一样的错误。

- 解决： 注意查看是否已经在别的地方已经打开 ES 服务，实在不行则进行电脑重启

#### 6. org/elasticsearch/action/ActionRequest has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0

```java
public static void main(String[] args) throws IOException {
    // 创建 ES 客户端
    RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(new HttpHost(Constants.HOST, Constants.PORT, Constants.HTTP))
    );

    client.close();
}
12345678
```

通过上述代码，使用 RestHighLevelCilent 访问 ES 客户端的时候，出现以下错误：`Exception in thread "main" java.lang.UnsupportedClassVersionError:`org/elasticsearch/action/ActionRequest has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0

看到注释，我们知道是版本不兼容的问题，查找资料看到这样一个表：

```js
Major version numbers map to Java versions:

45 = Java 1.1
46 = Java 1.2
47 = Java 1.3
48 = Java 1.4
49 = Java 5
50 = Java 6
51 = Java 7
52 = Java 8
53 = Java 9
54 = Java 10
55 = Java 11
56 = Java 12
57 = Java 13
```

讲道理在这个`版本任你发，我用Java8` 的年代，RestHighLevelClient 肯定是兼容Java 8 的，那么就只有 ES 版本太高了，把 `pom.xml` 的 `ES 依赖版本`降到跟 `elasticsearch-rest-high-level-client` 一样就可以了

```xml
<properties>
    <elasticsearch.version>7.17.0</elasticsearch.version>   	 																	<elasticsearch.client.version>7.17.0</elasticsearch.client.version>
</properties>

<dependies>
    <dependency>
        <groupId>org.elasticsearch</groupId>
        <artifactId>elasticsearch</artifactId>
        <version>${elasticsearch.version}</version>
    </dependency>
    <dependency>
        <groupId>org.elasticsearch.client</groupId>
        <artifactId>elasticsearch-rest-high-level-client</artifactId>
        <version>${elasticsearch.client.version}</version>
    </dependency>
</dependies>
```

- 解决：把 `pom.xml` 的 `elasticsearch` 和 `elasticsearch-rest-high-level-client` 版本一致即可。

## 十、SpringBoot与任务

### **一、异步任务**

在Java应用中，绝大多数情况下都是通过同步的方式来实现交互处理的；但是在处理与第三方系统交互的时候，容易造成响应迟缓的情况，之前大部分都是使用多线程来完成此类任务，其实，在Spring 3.x之后，就已经内置了@Async来完美解决这个问题。



**两个注解：**

**@EnableAysnc、@Aysnc**

#### Demo

```java
@EnableAsync
@SpringBootApplication
public class Async01Application {

    public static void main(String[] args) {
        SpringApplication.run(Async01Application.class, args);
    }

}


@Async
    public void say(){
        try {
            logger.info("-----------------------开始发送邮件"+new Date().toString());
            sendMail();
            logger.info("-----------------------邮件发送结束"+new Date().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```



### 二、定时任务 



项目开发中经常需要执行一些定时任务，比如需要在每天凌晨时候，分析一次前一天的日志信息。Spring为我们提供了异步执行任务调度的方式，提供TaskExecutor 、TaskScheduler 接口。

**两个注解：**@EnableScheduling、@Scheduled

​	**cron表达式**

| **字段** | **允许值**             | **允许的特殊字符** |
| -------- | ---------------------- | ------------------ |
| 秒       | 0-59                   | , -  * /           |
| 分       | 0-59                   | , -  * /           |
| 小时     | 0-23                   | , -  * /           |
| 日期     | 1-31                   | , -  * ? / L W C   |
| 月份     | 1-12                   | , -  * /           |
| 星期     | 0-7或SUN-SAT  0,7是SUN | , -  * ? / L C #   |

| **特殊字符** | **代表含义**               |
| ------------ | -------------------------- |
| ,            | 枚举                       |
| -            | 区间                       |
| *            | 任意                       |
| /            | 步长                       |
| ?            | 日/星期冲突匹配            |
| L            | 最后                       |
| W            | 工作日                     |
| C            | 和calendar联系后计算过的值 |
| #            | 星期，4#2，第2个星期四     |



#### Demo

```java
@EnableScheduling
@SpringBootApplication
public class Async01Application {

    public static void main(String[] args) {
        SpringApplication.run(Async01Application.class, args);
    }

}


@Service
public class ScheduledService {

    /**
     * second(秒), minute（分）, hour（时）, day of month（日）, month（月）, day of week（周几）.
     * 0 * * * * MON-FRI
     *  【0 0/5 14,18 * * ?】 每天14点整，和18点整，每隔5分钟执行一次
     *  【0 15 10 ? * 1-6】 每个月的周一至周六10:15分执行一次
     *  【0 0 2 ? * 6L】每个月的最后一个周六凌晨2点执行一次
     *  【0 0 2 LW * ?】每个月的最后一个工作日凌晨2点执行一次
     *  【0 0 2-4 ? * 1#1】每个月的第一个周一凌晨2点到4点期间，每个整点都执行一次；
     */
   // @Scheduled(cron = "0 * * * * MON-SAT")
    //@Scheduled(cron = "0,1,2,3,4 * * * * MON-SAT")
   // @Scheduled(cron = "0-4 * * * * MON-SAT")
    @Scheduled(cron = "0/4 * * * * MON-SAT")  //每4秒执行一次
    public void hello(){
        System.out.println("hello ... ");
    }
}
```



### 三、邮件任务 



**•邮件发送需要引入spring-boot-starter-mail**

**•Spring Boot 自动配置MailSenderAutoConfiguration**

**•定义MailProperties内容，配置在application.yml中**

**•自动装配JavaMailSender**

**•测试邮件发送**

![image-20220331191203537](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220331191203537.png)

现在发送邮件是一个网站必备的功能，比如注册激活，或者忘记密码等等都需要发送邮件。正常我们会用JavaMail相关api来写发送邮件的相关代码，但现在springboot提供了一套更简易使用的封装。也就是我们使用SpringBoot来发送邮件的话，代码就简单许多，毕竟SpringBoot是开箱即用的 ，它提供了什么，我们了解之后觉得可行，就使用它，自己没必要再写原生的。

##### 发送邮件流程图

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312101648110.png)
现在的Java项目大多都是按照MVC架构模型来开发的，但是这里不使用Controller层，是直接在测试中发送邮件，假如测试中发送邮件成功，那么改为用户发送请求来发送邮件也会成功。发送邮件，也就是要有发送方和接收方，我们需要在系统中配置发送方的信息，而接受方的信息我们需要根据从客户端发来的请求决定。作为发送方的我们，需要被授权，被谁授权？

1. 假如我使用QQ邮箱发送邮件，那么我就需要开启QQ邮箱授权码。
2. 假如我使用网易邮箱发送邮件，那我就需要开启网易邮箱授权码。

我们接下来就一 一开启这两个邮箱的授权码

1. 开启QQ邮箱授权码
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312111820127.png)
2. 开启网易邮箱授权码
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312111125605.png)

上图的步骤都会生成授权码，我们需要记住它，可以选择保存在本地电脑上，因为我们接下来配置发送方信息需要授权码。
接下来我们就开始编写代码发送邮件，我们使用IDEA创建SpringBoot项目，下篇博客可以快速了解如何在IDEA中创建SpringBoot项目。
[SpringBoot快速入门](https://blog.csdn.net/weixin_44176169/article/details/103982951)

##### 导入依赖

`spring-boot-starter-mail`：这个启动器内部封装了发送邮件的代码，必须引入的。
`spring-boot-starter-web`：这个启动器整合了JavaWeb常用的功能，如果只是简单在测试中发送邮件，那么就不需要引入它。
`spring-boot-starter-thymeleaf`：模板引擎依赖，如果需要将数据渲染到页面上，就需要引入它。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-mail</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
123456789101112
```

##### 配置发送方信息

我们需要在SpringBoot配置文件(application.yml)中配置发送方的信息，包括以下信息：

1. 用户名：也就是我们的邮箱账号
2. 密码：我们刚刚生成的授权码
3. 邮箱主机：配置使用哪个邮箱服务器发送邮件
   还有等等其他可选配置，在下面配置代码中，大家需要填写自己的用户名、密码、from(和用户名一样)。

```
QQ邮箱配置
spring:
  mail:
    host: smtp.qq.com #发送邮件服务器
    username:  #发送邮件的邮箱地址
    password:   #客户端授权码，不是邮箱密码，这个在qq邮箱设置里面自动生成的
    properties.mail.smtp.port: 465 #端口号465或587
    from:  # 发送邮件的地址，和上面username一致
    properties.mail.smtp.starttls.enable: true
    properties.mail.smtp.starttls.required: true
    properties.mail.smtp.ssl.enable: true
    default-encoding: utf-8
1234567891011
网易邮箱配置
spring:
  mail:
    host: smtp.163.com #发送邮件服务器
    username:  #发送邮件的邮箱地址
    password: #客户端授权码，不是邮箱密码,网易的是自己设置的
    properties.mail.smtp.port: 994 #465或者994
    from:  # 发送邮件的地址，和上面username一致
    properties.mail.smtp.starttls.enable: true
    properties.mail.smtp.starttls.required: true
    properties.mail.smtp.ssl.enable: true
    default-encoding: utf-8
1234567891011
```

##### EmailService

我们需要自己封装邮件服务，这个服务只是便于Controller层调用，也就是根据客户端发送的请求来调用，封装三种邮件服务

1. 发送普通文本邮件
2. 发送HTML邮件：一般在注册激活时使用
3. 发送带附件的邮件：可以发送图片等等附件

```java
public interface EmailService {

/**
 * 发送文本邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 */
void sendSimpleMail(String to, String subject, String content);

/**
 * 发送HTML邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 */
void sendHtmlMail(String to, String subject, String content);

/**
 * 发送带附件的邮件
 * @param to 收件人
 * @param subject 主题
 * @param content 内容
 * @param filePath 附件
 */
public void sendAttachmentsMail(String to, String subject, String content, String filePath);
}
123456789101112131415161718192021222324252627
```

##### EmailServiceImpl

我们需要实现上面的邮件服务，在具体的实现里面，则是调用了SpringBoot提供的JavaMailSender(封装了发送邮件的代码)。

```java
@Service
public class EmailServiceImpl implements EmailService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender javaMailSender;
	
	//注入配置文件中配置的信息——>from
    @Value("${spring.mail.from}")
    private String from;

    @Override
    public void sendSimpleMail(String to, String subject, String content) {
        SimpleMailMessage message = new SimpleMailMessage();
        //发件人
        message.setFrom(from);
        //收件人
        message.setTo(to);
        //邮件主题
        message.setSubject(subject);
        //邮件内容
        message.setText(content);
        //发送邮件
        javaMailSender.send(message);
    }

    @Override
    public void sendHtmlMail(String to, String subject, String content) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message,true);
            messageHelper.setFrom(from);
            messageHelper.setTo(to);
            message.setSubject(subject);
            messageHelper.setText(content,true);
            javaMailSender.send(message);
            logger.info("邮件已经发送！");
        } catch (MessagingException e) {
            logger.error("发送邮件时发生异常："+e);
        }
    }

    @Override
    public void sendAttachmentsMail(String to, String subject, String content, String filePath) {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message,true);
            messageHelper.setFrom(from);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(content,true);
            //携带附件
            FileSystemResource file = new FileSystemResource(filePath);
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            messageHelper.addAttachment(fileName,file);

            javaMailSender.send(message);
            logger.info("邮件加附件发送成功！");
        } catch (MessagingException e) {
            logger.error("发送失败："+e);
        }
    }

}
12345678910111213141516171819202122232425262728293031323334353637383940414243444546474849505152535455565758596061626364656667
```

##### EmailServiceTest

我们直接在测试中发送邮件，如果测试中发送邮件没问题，那么转为用户请求发送邮件也没有问题的。

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceTest{

    @Autowired
    private EmailService emailService;

    @Test
    public void sendSimpleEmail(){
        String content = "你好，恭喜你...";
        emailService.sendSimpleMail("XXX@qq.com","祝福邮件",content);
    }

    @Test
    public void sendMimeEmail(){
        String content = "<a href='https://blog.csdn.net/'>你好，欢迎注册网站，请点击链接激活</a>";
        emailService.sendHtmlMail("XXX@163.com","激活邮件",content);
    }

    @Test
    public void sendAttachment(){
        emailService.sendAttachmentsMail("XX@qq.com","发送附件","这是Java体系图","F:/图片/爱旅行.jpg");
    }
}
123456789101112131415161718192021222324
```

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312122149108.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312122316154.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200312123000765.png)
这样我们就实现了提供Java代码来发送邮件。发送附件功能其实就是读取我们本地电脑的文件，然后添加到发送流中即可。
[发送邮件附件的源代码](https://github.com/ZhengWenQi98/SpringBoot)





## 十一、SpringBoot与安全



### 一、安全



**Spring Security是针对Spring项目的安全框架，也是Spring Boot底层安全模块默认的技术选型。他可以实现强大的web安全控制。对于安全控制，我们仅需引入spring-boot-starter-security模块，进行少量的配置，即可实现强大的安全管理。**

****
 **几个类：**

**WebSecurityConfigurerAdapter：自定义Security策略**

**AuthenticationManagerBuilder：自定义认证策略**

**@EnableWebSecurity：开启WebSecurity模式**



**•应用程序的两个主要区域是“认证”和“授权”（或者访问控制）。这两个主要区域是Spring Security 的两个目标。**

**•“认证”（Authentication），是建立一个他声明的主体的过程（一个“主体”一般是指用户，设备或一些可以在你的应用程序中执行动作的其他系统）。**

**•“授权”（Authorization）指确定一个主体是否允许在你的应用程序执行一个动作的过程。为了抵达需要授权的店，主体的身份已经有认证过程建立。**

**•这个概念是通用的而不只在Spring Security中。**



### 二、Web&安全 



##### 1.登陆/注销

​	**–HttpSecurity配置登陆、注销功能**

##### 2.Thymeleaf提供的SpringSecurity标签支持

```html
<html xmlns:th="http://www.thymeleaf.org"
	  xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
```



```js
sec:authentication=“name”获得当前用户的用户名
sec:authentication="authorities" 角色
–sec:authorize=“hasRole(‘ADMIN’)” 当前用户必须拥有ADMIN权限时才会显示标签内容
```

[表达式说明地址](https://docs.spring.io/spring-security/reference/servlet/authorization/expression-based.html#el-access-web)

##### 3.remember me

​	**–表单添加remember-me的checkbox**

​	**–配置启用remember-me功能**

##### 4.CSRF（Cross-site request forgery）跨站请求伪造

​	**HttpSecurity启用csrf功能，会为表单添加_csrf的值，提交携带来预防CSRF；**

### 三、Demo



#### 动态登陆状态

```html
<!--未登录,显示登录按钮-->
    <div sec:authorize="!isAuthenticated()">
        <a class="item" th:href="@{/toLogin}">
            <i class="address card icon"></i> 登录
        </a>
    </div>
<!--登陆后,显示用户名,注销-->
     <div sec:authorize="isAuthenticated()">
         <a class="item">
             用户名: <span sec:authentication="name"></span>
             角色: <span sec:authentication="authorities"></span>
         </a>
         <a class="item" th:href="@{/logout}">
         	<i class="sign-out icon"></i> 注销
         </a>
      </div>
```

#### 配置类

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder()).withUser("zhangsan").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1")
                .and().withUser("lisi").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1","VIP2").and()
                .withUser("wangwu").password(new BCryptPasswordEncoder().encode("123456")).roles("VIP1","VIP2","VIP3");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");
        //开启用户登录表单
        http.formLogin().loginPage("/userlogin").passwordParameter("pwd");
        //开启记住我
        http.rememberMe().rememberMeParameter("reme");
        //开启注销
        http.logout().logoutSuccessUrl("/");
    }

}
```

#### 跟据权限动态显示:

```html
<div class="column" sec:authorize="hasRole('vip1')"> 11 </div>
<!--权限为vip1,可见-->
```





### 问题

#### There is no PasswordEncoder mapped for the id “null”异常解决办法

##### 一. 问题描述

Spring security 5.0中新增了多种加密方式,也改变了默认的密码格式.

我们来看一下官方文档:

```python
The general format for a password is:

{id}encodedPassword

Such that id is an identifier used to look up which PasswordEncoder should be used and encodedPassword is the original encoded password for the selected PasswordEncoder. The id must be at the beginning of the password, start with { and end with }. If the id cannot be found, the id will be null. For example, the following might be a list of passwords encoded using different id. All of the original passwords are "password".

{bcrypt}$2a$10$dXJ3SW6G7P50lGmMkkmwe.20cQQubK3.HZWzG3YB1tlRy.fqvM/BG 

{noop}password 

{pbkdf2}5d923b44a6d129f3ddf3e3c8d29412723dcbde72445e8ef6bf3b508fbf17fa4ed4d6b99ca763d8dc 

{scrypt}$e0801$8bWJaSu2IKSn9Z9kM+TPXfOc/9bdYSrN1oD9qfVThWEwdRTnO7re7Ei+fUZRJ68k9lTyuTeUp4of4g24hHnazw==$OAOec05+bXxvuu/1qZ6NUR+xQYvYv7BeL1QxwRpY5Pc=  

{sha256}97cde38028ad898ebc02e690819fa220e88c62e0699403e94fff291cfffaf8410849f27605abcbc0
```

**这段话的意思是说,现如今Spring Security中密码的存储格式是“{id}…………”.前面的id是加密方式,id可以是bcrypt、sha256等,后面跟着的是加密后的密码.也就是说,程序拿到传过来的密码的时候,会首先查找被“{”和“}”包括起来的id,来确定后面的密码是被怎么样加密的,如果找不到就认为id是null.**这也就是为什么我们的程序会报错:There is no PasswordEncoder mapped for the id “null”.官方文档举的例子中是各种加密方式针对同一密码加密后的存储形式,原始密码都是“password”.

##### 二. 解决办法

需要修改一下configure中的代码,我们要将前端传过来的密码进行某种方式加密,Spring Security 官方推荐的是使用bcrypt加密方式.

###### **1. 在内存中存取密码的修改方式**

修改后是这样的:

```java
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  //inMemoryAuthentication 从内存中获取
  auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder()).withUser("user1").password(new BCryptPasswordEncoder().encode("123")).roles("USER");
}
```

inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())",这相当于登陆时用BCrypt加密方式对用户密码进行处理.以前的".password("123")" 变成了 ".password(new BCryptPasswordEncoder().encode("123"))",这相当于对内存中的密码进行Bcrypt编码加密.如果比对时一致,说明密码正确,才允许登陆.

###### **2. 在数据库中存取密码的修改方式**

如果你用的是在数据库中存储用户名和密码,那么一般是要在用户注册时就使用BCrypt编码将用户密码加密处理后存储在数据库中,并且修改configure()方法,加入".passwordEncoder(new BCryptPasswordEncoder())",保证用户登录时使用bcrypt对密码进行处理再与数据库中的密码比对.如下:

```javascript
//注入userDetailsService的实现类



auth.userDetailsService(userService).passwordEncoder(new BCryptPasswordEncoder());
```

 





## 十二、Spring Boot与分布式



### 一、分布式应用

**在分布式系统中，国内常用zookeeper+dubbo组合，而Spring Boot推荐使用全栈的Spring，Spring Boot+Spring Cloud。**

**分布式系统：**

![image-20220403185156186](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403185156186.png)



**单一应用架构**

当网站流量很小时，只需一个应用，将所有功能都部署在一起，以减少部署节点和成本。此时，用于简化增删改查工作量的数据访问框架(ORM)是关键。

**垂直应用架构**

当访问量逐渐增大，单一应用增加机器带来的加速度越来越小，将应用拆成互不相干的几个应用，以提升效率。此时，用于加速前端页面开发的Web框架(MVC)是关键。

**分布式服务架构**

当垂直应用越来越多，应用之间交互不可避免，将核心业务抽取出来，作为独立的服务，逐渐形成稳定的服务中心，使前端应用能更快速的响应多变的市场需求。此时，用于提高业务复用及整合的分布式服务框架(RPC)是关键。

**流动计算架构**

当服务越来越多，容量的评估，小服务资源的浪费等问题逐渐显现，此时需增加一个调度中心基于访问压力实时管理集群容量，提高集群利用率。此时，用于提高机器利用率的资源调度和治理中心(SOA)是关键。



### 二、Zookeeper和Dubbo



**ZooKeeper**

ZooKeeper 是一个分布式的，开放源码的分布式应用程序协调服务。它是一个为分布式应用提供一致性服务的软件，提供的功能包括：配置维护、域名服务、分布式同步、组服务等。

**Dubbo**

Dubbo是Alibaba开源的分布式服务框架，它最大的特点是按照分层的方式来架构，使用这种方式可以使各个层之间解耦合（或者最大限度地松耦合）。从服务模型的角度来看，Dubbo采用的是一种非常简单的模型，要么是提供方提供服务，要么是消费方消费服务，所以基于这一点可以抽象出服务提供方（Provider）和服务消费方（Consumer）两个角色。

![image-20220403185326708](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403185326708.png)



#### 1、安装zookeeper作为注册中心

​		docker 安装

#### 2、引入依赖

```xml
<properties>
        <java.version>1.8</java.version>
        <curator.version>5.2.1</curator.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>${curator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>${curator.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-x-discovery</artifactId>
            <version>${curator.version}</version>
        </dependency>

        <!-- Dubbo Spring Boot Starter -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>2.7.8</version>
        </dependency>

        <dependency>
            <groupId>org.apache.zookeeper</groupId>
            <artifactId>zookeeper</artifactId>
            <version>3.8.0</version>
        </dependency>

    </dependencies>
```



#### 3、编写服务提供者



##### 	1）配置

​	

```yaml
spring:
  application:
    name: provider_book

demo:
  service:
    version: 1.0.0

embedded:
  zookeeper:
    port: 2181

dubbo:
  application:
    name: provider_book
  registry:
    address: zookeeper://192.168.153.4:${embedded.zookeeper.port}
    client: curator
    timeout: 10000
  scan:
    base-packages: aixz.study.provider.service
```



##### 2) 编写服务接口及实现

```java
public interface BookService {
    /**
     *购买书籍接口
     * @param name
     * @return
     */
    public String getBook(String name);
}



@DubboService(version = "${demo.service.version}",interfaceClass = BookService.class)
@Service
public class BookServiceImpl implements BookService{
    @Override
    public String getBook(String name) {
        return name+"购买了《三国演艺》";
    }
}

```

#### 4、编写服务消费者

##### 1）配置

​	

```yaml
spring:
  application:
    name: consumer_readers

demo:
  service:
    version: 1.0.0

embedded:
  zookeeper:
    port: 2181


dubbo:
  application:
    name: consumer_readers
  registry:
    address: zookeeper://192.168.153.4:${embedded.zookeeper.port}
    client: curator
    register: false #是否向此注册中心注册服务，如果设为false，将只订阅，不注册
    timeout: 10000

server:
  port: 8081
```



##### 2) 编写服务消费实现

复制服务方代码到同包下

![image-20220403190429832](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403190429832.png)



```java
@RestController
public class HelloController {

    @DubboReference(version = "1.0.0")
    BookService bookServiceImpl;


    @RequestMapping("/buy/{name}")
    public String BuyBook(@PathVariable String name){
       return bookServiceImpl.getBook(name);
    }
}

```



### 三、Spring Boot和Spring Cloud



**Spring Cloud**

Spring Cloud是一个分布式的整体解决方案。Spring Cloud 为开发者提供了**在分布式系统（配置管理，服务发现，熔断，路由，微代理，控制总线，一次性****token****，全局琐，****leader****选举，分布式****session****，集群状态）中快速构建的工具**，使用Spring Cloud的开发者可以快速的启动服务或构建应用、同时能够快速和云平台资源进行对接。



**•SpringCloud分布式开发五大常用组件**

​	**•服务发现——Netflix Eureka**

​	**•客服端负载均衡——Netflix Ribbon**

​	**•断路器——Netflix Hystrix**

​	**•服务网关——Netflix Zuul**

​	**•分布式配置——Spring Cloud Config**

![image-20220403190835342](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220403190835342.png)

#### Spring Cloud 入门



[Spring Cloud 官方文档](https://spring.io/projects/spring-cloud-netflix#learn)

##### 1、引入Eureka注册中心



##### 1、创建provider

配置注册中心

```yml
server:
  port: 8001

eureka:
  client:
    serviceUrl:
      defaultZone: http://127.0.0.1:8761/eureka/

spring:
  application:
    name: provide-car

```



引入依赖

```xml
<properties>
        <java.version>1.8</java.version>
        <spring-cloud.version>2021.0.1</spring-cloud.version>
        <eureka.version>3.1.1</eureka.version>
    </properties>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
            <version>${eureka.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

开启服务发现

```java
@EnableDiscoveryClient
@SpringBootApplication
public class ProvideCarApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvideCarApplication.class, args);
    }

}
```

编写控制器

```java
@RestController
public class BuyController {

    @Autowired
    CarService carServiceImpl;

    @RequestMapping("/buy/{name}")
    public String buy(@PathVariable String name){
        return   carServiceImpl.buyCar(name);
    };
}
```

2、创建consumer

引入依赖

开启服务发现

引入Ribbon进行客户端负载均衡

```java
@EnableDiscoveryClient
@SpringBootApplication
public class ConsumerDriverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerDriverApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}

```



编写逻辑调用服务

```java
@RestController
public class BuyCarController {
    @Autowired
    RestTemplate restTemplate;

    @Autowired
    DiscoveryClient client;

    @Autowired
    LoadBalancerClient balancerClient;

    @RequestMapping("/hello")
    public String buyCar(){
        List<ServiceInstance> instances = client.getInstances("PROVIDE_CAR");
        ServiceInstance instance = instances.get(0);
        String uri = String.valueOf(instance.getUri());
        String name = "张三";
        System.out.println(uri+"/buy/"+name);
        ResponseEntity<String> entity = restTemplate.getForEntity(uri+"/buy/"+name, String.class);
        //System.out.println(entity.getStatusCode());
        return entity.getBody();
        /*ServiceInstance instance = balancerClient.choose("PROVIDE_CAR");
        String  uri = String.valueOf(instance.getUri());
        System.out.println(uri);*/
        //return restTemplate.getForObject("http://PROVIDE-CAR/buy/张三", String.class);

    }
}
```

负载均衡

```
return restTemplate.getForObject("http://PROVIDE-CAR/buy/张三", String.class);
```





#### 问题

##### 1、**Request URI does not contain a valid hostname**

问题原因：

```
在注册服务的时候，properties文件中的服务名（spring.application.name）带上了下划线（如：PRODUCT_SERVICE）
```

解决办法：

```
将下划线换成横杠即可。(PRODUCT-SERVICE)
```





## 十三、Spring Boot与开发热部署



**在开发中我们修改一个Java文件后想看到效果不得不重启应用，这导致大量时间花费，我们希望不重启应用的情况下，程序可以自动部署（热部署）。有以下四种情况，如何能实现热部署。**



### 1、模板引擎

**–在Spring Boot中开发情况下禁用模板引擎的cache**

**–页面模板改变ctrl+F9可以重新编译当前页面并生效**

### 2、Spring Loaded

**Spring官方提供的热部署程序，实现修改类文件的热部署**

**–下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）**

**–添加运行时参数；**

**-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify**

### 3、JRebel

**–收费的一个热部署软件**

**–安装插件使用即可**

### 4、Spring Boot Devtools（推荐）

**–引入依赖**

```xml
<dependency>  
       <groupId>org.springframework.boot</groupId>  
       <artifactId>spring-boot-devtools</artifactId>   
</dependency> 

```

**–IDEA使用ctrl+F9**

**–或做一些小调整**

 ***Intellij* *IEDA和Eclipse不同，Eclipse设置了自动编译之后，修改类它会自动编译，而IDEA在非RUN或DEBUG情况下才会自动编译（前提是你已经设置了Auto-Compile）。***

**•设置自动编译（settings-compiler-make project automatically）**

**•ctrl+shift+alt+/（maintenance）**

**•勾选compiler.automake.allow.when.app.running**



## 十四、Spring Boot与监控管理

[官方文档](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator)



### 1. 启用生产就绪功能

该[`spring-boot-actuator`](https://github.com/spring-projects/spring-boot/tree/v2.6.6/spring-boot-project/spring-boot-actuator)模块提供了 Spring Boot 的所有生产就绪功能。`spring-boot-starter-actuator`启用这些功能的推荐方法是添加对“Starter”的依赖。

执行器的定义

致动器是一个制造术语，指的是用于移动或控制某物的机械装置。执行器可以通过微小的变化产生大量的运动。

要将执行器添加到基于 Maven 的项目中，请添加以下“Starter”依赖项：

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
</dependencies>
```

对于 Gradle，使用以下声明：

```gradle
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
}
```

### 2. 端点

执行器端点使您可以监视应用程序并与之交互。Spring Boot 包含许多内置端点，并允许您添加自己的端点。例如，`health`端点提供基本的应用程序健康信息。

您可以[启用或禁用](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.enabling)每个单独的端点并[通过 HTTP 或 JMX 公开它们（使它们可以远程访问）](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.exposing)。当端点被启用和公开时，它被认为是可用的。内置端点仅在可用时才会自动配置。大多数应用程序选择通过 HTTP 公开，其中端点的 ID 和前缀`/actuator`映射到 URL。例如，默认情况下，`health`端点映射到`/actuator/health`.

|      | 要了解有关执行器端点及其请求和响应格式的更多信息，请参阅单独的 API 文档（[HTML](https://docs.spring.io/spring-boot/docs/2.6.6/actuator-api/htmlsingle)或[PDF](https://docs.spring.io/spring-boot/docs/2.6.6/actuator-api/pdf/spring-boot-actuator-web-api.pdf)）。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

以下与技术无关的端点可用：

| ID                 | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| `auditevents`      | 公开当前应用程序的审计事件信息。需要一个`AuditEventRepository`豆子。 |
| `beans`            | 显示应用程序中所有 Spring bean 的完整列表。                  |
| `caches`           | 公开可用的缓存。                                             |
| `conditions`       | 显示在配置和自动配置类上评估的条件以及它们匹配或不匹配的原因。 |
| `configprops`      | 显示所有`@ConfigurationProperties`.                          |
| `env`              | 公开 Spring 的`ConfigurableEnvironment`.                     |
| `flyway`           | 显示已应用的任何 Flyway 数据库迁移。需要一个或多个`Flyway`豆子。 |
| `health`           | 显示应用程序运行状况信息。                                   |
| `httptrace`        | 显示 HTTP 跟踪信息（默认情况下，最近 100 个 HTTP 请求-响应交换）。需要一个`HttpTraceRepository`豆子。 |
| `info`             | 显示任意应用程序信息。                                       |
| `integrationgraph` | 显示 Spring 集成图。需要依赖`spring-integration-core`.       |
| `loggers`          | 显示和修改应用程序中记录器的配置。                           |
| `liquibase`        | 显示已应用的任何 Liquibase 数据库迁移。需要一个或多个`Liquibase`豆子。 |
| `metrics`          | 显示当前应用程序的“指标”信息。                               |
| `mappings`         | 显示所有`@RequestMapping`路径的整理列表。                    |
| `quartz`           | 显示有关 Quartz 调度程序作业的信息。                         |
| `scheduledtasks`   | 显示应用程序中的计划任务。                                   |
| `sessions`         | 允许从 Spring Session 支持的会话存储中检索和删除用户会话。需要使用 Spring Session 的基于 servlet 的 Web 应用程序。 |
| `shutdown`         | 让应用程序正常关闭。默认禁用。                               |
| `startup`          | 显示由. [_ ](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.spring-application.startup-tracking)`ApplicationStartup`需要`SpringApplication`配置`BufferingApplicationStartup`. |
| `threaddump`       | 执行线程转储。                                               |

如果您的应用程序是 Web 应用程序（Spring MVC、Spring WebFlux 或 Jersey），您可以使用以下附加端点：

| ID           | 描述                                                         |
| :----------- | :----------------------------------------------------------- |
| `heapdump`   | 返回一个堆转储文件。在 HotSpot JVM 上，`HPROF`返回一个 -format 文件。在 OpenJ9 JVM 上，`PHD`返回一个 -format 文件。 |
| `jolokia`    | 当 Jolokia 在类路径上时，通过 HTTP 公开 JMX bean（不适用于 WebFlux）。需要依赖`jolokia-core`. |
| `logfile`    | 返回日志文件的内容（如果已设置`logging.file.name`或属性）。`logging.file.path`支持使用 HTTP`Range`标头检索部分日志文件内容。 |
| `prometheus` | 以 Prometheus 服务器可以抓取的格式公开指标。需要依赖`micrometer-registry-prometheus`. |

#### 2.1。启用端点

默认情况下，除了`shutdown`启用之外的所有端点。要配置端点的启用，请使用其`management.endpoint.<id>.enabled`属性。以下示例启用`shutdown`端点：

特性

yaml

```properties
management.endpoint.shutdown.enabled=true
```

如果您希望端点启用是选择加入而不是选择退出，请将`management.endpoints.enabled-by-default`属性设置为`false`并使用单个端点`enabled`属性来选择重新加入。以下示例启用`info`端点并禁用所有其他端点：

特性

yaml

```properties
management.endpoints.enabled-by-default=false
management.endpoint.info.enabled=true
```

|      | 禁用的端点完全从应用程序上下文中删除。如果您只想更改暴露端点的技术，请改用[`include`and`exclude`属性](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.exposing)。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

#### 2.2. 暴露端点

由于端点可能包含敏感信息，您应该仔细考虑何时公开它们。下表显示了内置端点的默认曝光：

| ID                 | JMX    | 网络 |
| :----------------- | :----- | :--- |
| `auditevents`      | 是的   | 不   |
| `beans`            | 是的   | 不   |
| `caches`           | 是的   | 不   |
| `conditions`       | 是的   | 不   |
| `configprops`      | 是的   | 不   |
| `env`              | 是的   | 不   |
| `flyway`           | 是的   | 不   |
| `health`           | 是的   | 是的 |
| `heapdump`         | 不适用 | 不   |
| `httptrace`        | 是的   | 不   |
| `info`             | 是的   | 不   |
| `integrationgraph` | 是的   | 不   |
| `jolokia`          | 不适用 | 不   |
| `logfile`          | 不适用 | 不   |
| `loggers`          | 是的   | 不   |
| `liquibase`        | 是的   | 不   |
| `metrics`          | 是的   | 不   |
| `mappings`         | 是的   | 不   |
| `prometheus`       | 不适用 | 不   |
| `quartz`           | 是的   | 不   |
| `scheduledtasks`   | 是的   | 不   |
| `sessions`         | 是的   | 不   |
| `shutdown`         | 是的   | 不   |
| `startup`          | 是的   | 不   |
| `threaddump`       | 是的   | 不   |

要更改公开的端点，请使用以下技术特定`include`和`exclude`属性：

| 财产                                        | 默认     |
| :------------------------------------------ | :------- |
| `management.endpoints.jmx.exposure.exclude` |          |
| `management.endpoints.jmx.exposure.include` | `*`      |
| `management.endpoints.web.exposure.exclude` |          |
| `management.endpoints.web.exposure.include` | `health` |

该`include`属性列出了公开的端点的 ID。该`exclude`属性列出不应公开的端点的 ID。`exclude`财产优先于财产`include`。您可以使用端点 ID 列表来配置`include`和`exclude`属性。

例如，要停止通过 JMX 公开所有端点并仅公开`health`和`info`端点，请使用以下属性：

特性

yaml

```properties
management.endpoints.jmx.exposure.include=health,info
```

`*`可用于选择所有端点。例如，要通过 HTTP 公开除`env`和`beans`端点之外的所有内容，请使用以下属性：

特性

yaml

```properties
management.endpoints.web.exposure.include=*
management.endpoints.web.exposure.exclude=env,beans
```

|      | `*`在 YAML 中具有特殊含义，因此如果要包含（或排除）所有端点，请务必添加引号。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

|      | 如果您的应用程序是公开的，我们强烈建议您也[保护您的端点](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator.endpoints.security)。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |

|      | 如果您想在端点暴露时实现自己的策略，您可以注册一个`EndpointFilter`bean。 |
| ---- | ------------------------------------------------------------ |
|      |                                                              |









## 附录

------



### 1、**语言简称表**

| 语言               | 简称  |
| ------------------ | ----- |
| 简体中文(中国)     | zh_CN |
| 繁体中文(中国台湾) | zh_TW |
| 繁体中文(中国香港) | zh_HK |
| 英语(中国香港)     | en_HK |
| 英语(美国)         | en_US |
| 英语(英国)         | en_GB |
| 英语(全球)         | en_WW |
| 英语(加拿大)       | en_CA |
| 英语(澳大利亚)     | en_AU |
| 英语(爱尔兰)       | en_IE |
| 英语(芬兰)         | en_FI |
| 芬兰语(芬兰)       | fi_FI |
| 英语(丹麦)         | en_DK |
| 丹麦语(丹麦)       | da_DK |
| 英语(以色列)       | en_IL |
| 希伯来语(以色列)   | he_IL |
| 英语(南非)         | en_ZA |
| 英语(印度)         | en_IN |
| 英语(挪威)         | en_NO |
| 英语(新加坡)       | en_SG |
| 英语(新西兰)       | en_NZ |
| 英语(印度尼西亚)   | en_ID |
| 英语(菲律宾)       | en_PH |
| 英语(泰国)         | en_TH |
| 英语(马来西亚)     | en_MY |
| 英语(阿拉伯)       | en_XA |
| 韩文(韩国)         | ko_KR |
| 日语(日本)         | ja_JP |
| 荷兰语(荷兰)       | nl_NL |
| 荷兰语(比利时)     | nl_BE |
| 葡萄牙语(葡萄牙)   | pt_PT |
| 葡萄牙语(巴西)     | pt_BR |
| 法语(法国)         | fr_FR |
| 法语(卢森堡)       | fr_LU |
| 法语(瑞士)         | fr_CH |
| 法语(比利时)       | fr_BE |
| 法语(加拿大)       | fr_CA |
| 西班牙语(拉丁美洲) | es_LA |
| 西班牙语(西班牙)   | es_ES |
| 西班牙语(阿根廷)   | es_AR |
| 西班牙语(美国)     | es_US |
| 西班牙语(墨西哥)   | es_MX |
| 西班牙语(哥伦比亚) | es_CO |
| 西班牙语(波多黎各) | es_PR |
| 德语(德国)         | de_DE |
| 德语(奥地利)       | de_AT |
| 德语(瑞士)         | de_CH |
| 俄语(俄罗斯)       | ru_RU |
| 意大利语(意大利)   | it_IT |
| 希腊语(希腊)       | el_GR |
| 挪威语(挪威)       | no_NO |
| 匈牙利语(匈牙利)   | hu_HU |
| 土耳其语(土耳其)   | tr_TR |
| 捷克语(捷克共和国) | cs_CZ |
| 斯洛文尼亚语       | sl_SL |
| 波兰语(波兰)       | pl_PL |
| 瑞典语(瑞典)       | sv_SE |
| 西班牙语(智利)     | es_CL |

### 2、spring boot启动图像

####  	1.首先进入下面网址生成一个你想要的图像

```ruby
http://patorjk.com/software/taag/#p=display&h=3&v=3&f=4Max&t=itcast%20Spring%20Boot
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191116550.png)

#### 2.在自己的工程中建立banner.txt文件

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191440620.png)

#### 3.把生成图像复制进banner.txt

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191614928.png)

#### 4.启动项目即可！

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190221191731144.png)

搞定！

备注：代码复制下面这个就行，用你的图案代替一下中间的图案！

```html
${AnsiColor.BLUE}

██╗  ██╗██╗   ██╗██╗    ██╗███████╗██╗
╚██╗██╔╝██║   ██║██║    ██║██╔════╝██║
 ╚███╔╝ ██║   ██║██║ █╗ ██║█████╗  ██║
 ██╔██╗ ██║   ██║██║███╗██║██╔══╝  ██║
██╔╝ ██╗╚██████╔╝╚███╔███╔╝███████╗██║
╚═╝  ╚═╝ ╚═════╝  ╚══╝╚══╝ ╚══════╝╚═╝

------版本号------${spring-boot.version}
```

###  3、MVC请求重定向、转发

#### 请求重定向

```java
redirect:
```

#### 请求转发

```
forward:
```

## 问题

### 1、from 发送其他请求无效

SpringMVC使用get/post以外提交方式，例如put等需要具备以下条件：

配置HiddenHttpMethodFilter
页面创建一个post表单
创建一个input项，name=”_method” Value=“put”，值就是指定的请求方式

#### 原因：

​		**springboot自动配置，帮我们省略了第一步的配置，上面代码方法就是为了实现自动配置，但是因为注解@ConditionalOnProperty限制了自动配置，默认false不开启配置，所以页面的put提交无法使用。**

#### **解决：**

```yaml
spring.mvc.hiddenmethod.filter.enabled=true
```



# JavaScript 



[javascript 教程](https://wangdoc.com/javascript/)

## 一、常用函数、方法



### 1、String转int



方案一代码：

```
Number(str) 
```

方案二代码：

```
//parseInt 方法都有两个参数, 第一个参数就是要转换的对象, 第二个参数是进制基数, 可以是 2, 8, 10, 16, 默认以 10 进制处理
parseInt(str)
```

方案一与方案二对比：

```
var str='1250' ;
alert( Number(str) );  //得到1250
alert(parseInt(str));  //得到1250
var str1='00100';
alert( Number(str1) );  //得到100
alert(parseInt(str1));  //得到64
```

注意：parseInt方法在format'00'开头的数字时会当作2进制转10进制的方法进行转换；

所以建议string转int最好用Number方法；

### 2、map()

`map()`方法将数组的所有成员依次传入参数函数，然后把每一次的执行结果组成一个新数组返回。

```
var numbers = [1, 2, 3];

numbers.map(function (n) {
  return n + 1;
});
// [2, 3, 4]

numbers
// [1, 2, 3]
```

上面代码中，`numbers`数组的所有成员依次执行参数函数，运行结果组成一个新数组返回，原数组没有变化。

`map()`方法接受一个函数作为参数。该函数调用时，`map()`方法向它传入三个参数：当前成员、当前位置和数组本身。

```
[1, 2, 3].map(function(elem, index, arr) {
  return elem * index;
});
// [0, 2, 6]
```

上面代码中，`map()`方法的回调函数有三个参数，`elem`为当前成员的值，`index`为当前成员的位置，`arr`为原数组（`[1, 2, 3]`）。

`map()`方法还可以接受第二个参数，用来绑定回调函数内部的`this`变量（详见《this 变量》一章）。

```
var arr = ['a', 'b', 'c'];

[1, 2].map(function (e) {
  return this[e];
}, arr)
// ['b', 'c']
```

上面代码通过`map()`方法的第二个参数，将回调函数内部的`this`对象，指向`arr`数组。

如果数组有空位，`map()`方法的回调函数在这个位置不会执行，会跳过数组的空位。

```
var f = function (n) { return 'a' };

[1, undefined, 2].map(f) // ["a", "a", "a"]
[1, null, 2].map(f) // ["a", "a", "a"]
[1, , 2].map(f) // ["a", , "a"]
```

上面代码中，`map()`方法不会跳过`undefined`和`null`，但是会跳过空位。

### 3、forEach()

`forEach()`方法与`map()`方法很相似，也是对数组的所有成员依次执行参数函数。但是，`forEach()`方法不返回值，只用来操作数据。这就是说，如果数组遍历的目的是为了得到返回值，那么使用`map()`方法，否则使用`forEach()`方法。

`forEach()`的用法与`map()`方法一致，参数是一个函数，该函数同样接受三个参数：当前值、当前位置、整个数组。

```
function log(element, index, array) {
  console.log('[' + index + '] = ' + element);
}

[2, 5, 9].forEach(log);
// [0] = 2
// [1] = 5
// [2] = 9
```

上面代码中，`forEach()`遍历数组不是为了得到返回值，而是为了在屏幕输出内容，所以不必使用`map()`方法。

`forEach()`方法也可以接受第二个参数，绑定参数函数的`this`变量。

```
var out = [];

[1, 2, 3].forEach(function(elem) {
  this.push(elem * elem);
}, out);

out // [1, 4, 9]
```

上面代码中，空数组`out`是`forEach()`方法的第二个参数，结果，回调函数内部的`this`关键字就指向`out`。

注意，`forEach()`方法无法中断执行，总是会将所有成员遍历完。如果希望符合某种条件时，就中断遍历，要使用`for`循环。

```
var arr = [1, 2, 3];

for (var i = 0; i < arr.length; i++) {
  if (arr[i] === 2) break;
  console.log(arr[i]);
}
// 1
```

上面代码中，执行到数组的第二个成员时，就会中断执行。`forEach()`方法做不到这一点。

`forEach()`方法也会跳过数组的空位。

```
var log = function (n) {
  console.log(n + 1);
};

[1, undefined, 2].forEach(log)
// 2
// NaN
// 3

[1, null, 2].forEach(log)
// 2
// 1
// 3

[1, , 2].forEach(log)
// 2
// 3
```

上面代码中，`forEach()`方法不会跳过`undefined`和`null`，但会跳过空位。



## 二、常用教程

### 1、动态key获取对象属性

```js
var obj = {name:"张三",value:10}
//key动态拼接
key ="";
var value = obj[key];
```



# MyBatis 

## 常用语句

### mapper里的转义字符

| 特殊字符 | 特殊字符转义一   | 特殊字符转义二  |
| -------- | ---------------- | --------------- |
| >=       | &gt;=            | <![CDATA[>= ]]> |
| <=       | &lt;=            | <![CDATA[<= ]]> |
| !=       | <![CDATA[ <> ]]> | <![CDATA[!= ]]> |

![image-20220516113951563](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/image-20220516113951563.png)

#### **JdbcType和Oracle以及MySQL，相互之间的映射关系**

|          | JdbcType      | Oracle         | MySql              |
| :------- | :------------ | :------------- | :----------------- |
| JdbcType | ARRAY         |                |                    |
| JdbcType | BIGINT        |                | BIGINT             |
| JdbcType | BINARY        |                |                    |
| JdbcType | BIT           |                | BIT                |
| JdbcType | BLOB          | BLOB           | BLOB               |
| JdbcType | BOOLEAN       |                |                    |
| JdbcType | CHAR          | CHAR           | CHAR               |
| JdbcType | CLOB          | CLOB           | 修改为TEXT         |
| JdbcType | CURSOR        |                |                    |
| JdbcType | DATE          | DATE           | DATE               |
| JdbcType | DECIMAL       | DECIMAL        | DECIMAL            |
| JdbcType | DOUBLE        | NUMBER         | DOUBLE             |
| JdbcType | FLOAT         | FLOAT          | FLOAT              |
| JdbcType | INTEGER       | INTEGER        | INTEGER            |
| JdbcType | LONGVARBINARY |                |                    |
| JdbcType | LONGVARCHAR   | LONG VARCHAR   |                    |
| JdbcType | NCHAR         | NCHAR          |                    |
| JdbcType | NCLOB         | NCLOB          |                    |
| JdbcType | NULL          |                |                    |
| JdbcType | NUMERIC       | NUMERIC/NUMBER | NUMERIC/           |
| JdbcType | NVARCHAR      |                |                    |
| JdbcType | OTHER         |                |                    |
| JdbcType | REAL          | REAL           | REAL               |
| JdbcType | SMALLINT      | SMALLINT       | SMALLINT           |
| JdbcType | STRUCT        |                |                    |
| JdbcType | TIME          |                | TIME               |
| JdbcType | TIMESTAMP     | TIMESTAMP      | TIMESTAMP/DATETIME |
| JdbcType | TINYINT       |                | TINYINT            |
| JdbcType | UNDEFINED     |                |                    |
| JdbcType | VARBINARY     |                |                    |
| JdbcType | VARCHAR       | VARCHAR        | VARCHAR            |



### if test 判断字符串

```xml
<if test="sex=='Y'.toString()">
<if test = 'sex== "Y"'>
```

### 判断数组

```xml
<if test="array != null and array.length >0">
```

### 判断List

```xml
<if test="regions != null and regions.size() >0">
```



## 问题

### 传入Integer 数据 if 标签判断不通过



```xml
<if test="provincePointNo != null and provincePointNo != ''">   
```



**不能用空字符串来判断，因为空字符串会判断与0相等**



# Poi

## 模板

![image-20220818180556329](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202208181806654.png)



## 示例代码

```java
public void exportExcel(Map<String, String> queryMap, HttpServletResponse response) {
        log.info("-----------------导出值班记录数据开始" + LocalDateUtil.now(LocalDateUtil.DATE_COMPREHENSIVE) + "-----------------");
        log.info("--------------开始查询值班记录------------");
        List<CsdzWatch> csdzWatch = csdzWatchMapper.findAll(queryMap);
        log.info("--------------查询值班记录结束,共有" + csdzWatch.size() + "条数据------------");
        //如果没有查询到数据则不处理
        if (!CollectionUtils.isEmpty(csdzWatch)) {
            String now = LocalDateUtil.now(LocalDateUtil.DATE_DETAIL_CHINA);
            String fileName = "值班记录表" + now + ".xlsx";
            ClassPathResource classPathResource = new ClassPathResource("template/exportDutyRecord.xlsx");
            try (InputStream in = classPathResource.getInputStream();
                 ServletOutputStream outputStream = response.getOutputStream();
            ) {
                //读取excel模板
                XSSFWorkbook wb = new XSSFWorkbook(in);
                //读取了模板内所有sheet内容
                XSSFSheet sheet = wb.getSheetAt(0);
                //如果这行没有了，整个公式都不会有自动计算的效果的
                sheet.setForceFormulaRecalculation(true);
                //定义开始填充数据的行
                AtomicInteger startRow = new AtomicInteger(3);
                //定义初始数据序号
                AtomicInteger serialNumber = new AtomicInteger(1);
                csdzWatch.forEach(watch -> {
                    XSSFRow row = sheet.getRow(startRow.get());
                    //定义开始填充数据的列
                    int startCell = 0;
                    //数据序号
                    row.getCell(startCell++).setCellValue(serialNumber.toString());
                    //值班时间
                    row.getCell(startCell++).setCellValue(watch.getDutyTime());
                    //值班地点
                    row.getCell(startCell++).setCellValue(watch.getDutyAddr());
                    //值班人员
                    row.getCell(startCell++).setCellValue(watch.getDutyUserId());
                    //气象信息简介
                    row.getCell(startCell++).setCellValue(watch.getWettherIntroduct());
                    //气象信息时段1
                    row.getCell(startCell++).setCellValue(watch.getWettherTime1());
                    //气象信息时段2
                    row.getCell(startCell++).setCellValue(watch.getWettherTime2());
                    //气象信息时段3
                    row.getCell(startCell++).setCellValue(watch.getWettherTime3());
                    //气象信息时段4
                    row.getCell(startCell++).setCellValue(watch.getWettherTime4());
                    //获取预警内容
                    List<CsdzWaringInfo> waring = watch.getWaring();
                    if (!CollectionUtils.isEmpty(waring)) {
                        //记录预警数据初始列
                        AtomicInteger finalWaringCell = new AtomicInteger(startCell);
                        //定义预警列编号
                        AtomicInteger waringCell = new AtomicInteger(startCell);
                        //定义预警行编号
                        AtomicInteger waringStartRow = new AtomicInteger(startRow.get());
                        //遍历写入预警内容
                        waring.forEach(waringItem -> {
                            XSSFRow waringRow = sheet.getRow(waringStartRow.get());
                            //预警等级
                            waringRow.getCell(waringCell.getAndIncrement()).setCellValue(waringItem.getGerde());
                            //预警内容
                            waringRow.getCell(waringCell.getAndIncrement()).setCellValue(waringItem.getContent());
                            //初始化序号
                            waringCell.set(finalWaringCell.get());
                            //预警行编号增加
                            waringStartRow.getAndIncrement();
                        });
                        //增加预警列
                        startCell += 2;
                    }
                    //获取险情内容
                    List<CsdzDanger> danger = watch.getDanger();
                    if (!CollectionUtils.isEmpty(danger)) {
                        //定义险情数据编号
                        AtomicInteger dangerNumber = new AtomicInteger(0);
                        //记录险情数据初始列
                        AtomicInteger finalDangerCell = new AtomicInteger(startCell);
                        //定义险情数据编号
                        AtomicInteger dangerCell = new AtomicInteger(startCell);
                        //定义预警行编号
                        AtomicInteger dangerStartRow = new AtomicInteger(startRow.get());
                        //遍历写入险情内容
                        danger.forEach(dangerItem -> {
                            XSSFRow dangerRow = sheet.getRow(dangerStartRow.get());
                            //序号
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerNumber.get() + 1);
                            //发灾时间
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerTime());
                            //发灾内容
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerContent());
                            //发灾雨量
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerRainfall());
                            //灾险情简述
                            dangerRow.getCell(dangerCell.getAndIncrement()).setCellValue(dangerItem.getDangerAddress());
                            //初始化序号
                            dangerCell.set(finalDangerCell.get());
                            //序号增加
                            dangerNumber.getAndIncrement();
                            //行编号增加
                            dangerStartRow.getAndIncrement();
                        });
                    }
                    //获取当前数据最底层列
                    int maxRow = Math.max(waring.size(), danger.size());
                    //记录行号
                    int begRow = startRow.get();
                    //设置下一条数据的列索引
                    startRow.addAndGet(Math.max(maxRow, 1));
                    //序号增加
                    serialNumber.getAndIncrement();
                    //如果数据列需要合并
                    if (maxRow > 1) {
                        ExcelUtil.addMergeAddress(sheet, begRow, startRow.get() - 1, 0, 9);
                    }
                });
                //写入响应头信息
                response.setHeader("content-Type", "application/octet-stream;charset=utf-8");
                response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
                response.setHeader("filename", URLEncoder.encode(fileName, "UTF-8"));
                response.setHeader("Access-Control-Allow-Origin", "*");
                response.setCharacterEncoding("UTF-8");
                log.info("--------------开始写入文档------------");
                wb.write(outputStream);
                log.info("--------------写入文档结束------------");
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
                response.setStatus(204);
            }
        }
        log.info("-----------------导出值班记录数据结束" + LocalDateUtil.now(LocalDateUtil.DATE_COMPREHENSIVE) + "-----------------");
    }
```



## 合并单元格

````java

1. 添加依赖
 
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-ooxml</artifactId>
    <version>4.1.2</version>
</dependency>
2.
``` 
  @PostMapping(value = "/mergeCell",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public HashMap<String,String>  mergeCell(@RequestParam("file") MultipartFile file){
        String fileName = file.getOriginalFilename();
        System.out.println(fileName);
        String filePath = "D:\\WorkPlace";
        File dir = new File(filePath);
        if(!dir.exists()){
            dir.mkdir();
        }
        String originFilePath = filePath+File.separator+fileName;
        String newFilePath = filePath+File.separator+"合并后"+fileName;
        System.out.println(filePath);
        try {
            MergeCellUtil.mergeCellAndSave(originFilePath,newFilePath);
            return new HashMap<String, String>(){{put("单元格合并", "合并成功");}};
        } catch (Exception e) {
            return new HashMap<String,String>(){{put("单元格合并","合并失败");}};
        }
    }
```
 
3. 
 
 
/**
 * 检测 多次给付重疾险智核问卷0810.xlsx
 */
@Slf4j
public class MergeCellUtil {
    /**
     * 列方向合并相同数据的单元格. 空白单元格不合并. 将文件保存到硬盘
     *
     * @param originFilePath xlsx 原文件
     * @param newFilePath    合并后生成的 xlsx 文件保存路径
     */
    public static void mergeCellAndSave(String originFilePath, String newFilePath) {
 
        FileInputStream in = null;
        FileOutputStream outputStream = null;
        try {
            //Workbook newBook = MergeCellUtil.getMemoryWorkbook(originFilePath);
            //将文件读入
            in = new FileInputStream(new File(originFilePath));
            //追加写入----避免相同文件合并后生成相同文件名无法再写入
            outputStream = new FileOutputStream(newFilePath, true);
            //创建工作薄
            Workbook newBook = new XSSFWorkbook(in);
            MergeCellUtil.mergeCell(newBook);
            newBook.write(outputStream);
        } catch (Exception e) {
            throw new RuntimeException();
        } finally {
            try {
                outputStream.flush();
                outputStream.close();
                in.close();
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
    }
 
    /**
     * 将硬盘中的excel sheet0 的数据复制到纯内存excel中.
     * 原因: 从硬盘读入的excel操作之后, 没有写出成功
     *
     * @param originFilePath xlsx 文件路径
     * @return 内存中生成的workbook
     */
    private static Workbook getMemoryWorkbook(String originFilePath) throws Exception {
//        String originFilePath = "D:\\operate-data\\多次给付重疾险智核问卷0809.xlsx";
 
        Workbook originBook = new XSSFWorkbook(originFilePath);
        originBook.close();
        Sheet originSheet = originBook.getSheetAt(0);
 
        int lastRowIndex = originSheet.getLastRowNum();
        short columnSize = originSheet.getRow(0).getLastCellNum();
 
        Workbook newBook = new XSSFWorkbook();
        Sheet newSheet = newBook.createSheet();
 
        for (int i = 0; i <= lastRowIndex; i++) {
            Row originRowI = originSheet.getRow(i);
            Row newRowI = newSheet.createRow(i);
            for (int j = 0; j < columnSize; j++) {
                Cell newCell = newRowI.createCell(j);
 
                Cell originCell = originRowI.getCell(j);
                if (originCell != null) {
                    CellType originCellType = originCell.getCellType();
                    if (originCellType == CellType.STRING) {
                        newCell.setCellValue(originCell.getStringCellValue());
                    } else if (originCellType == CellType.NUMERIC) {
                        newCell.setCellValue(originCell.getNumericCellValue());
                    }
                }
 
            }
        }
 
        return newBook;
    }
 
    /**
     * {@code sheet} 添加合并单元格的区域. 合并方式是按列方向合并
     *
     * @param sheet    需要添加合并区域的 sheet 页
     * @param rowStart 合并区域起始行 index
     * @param rowEnd   合并区域终止行 index
     * @param column   合并区域列
     */
    public static void addMergeAddress(Sheet sheet, int rowStart, int rowEnd, int column) {
        if (rowStart < rowEnd) {
            CellRangeAddress mergeAddress = new CellRangeAddress(rowStart, rowEnd, column, column);
            sheet.addMergedRegion(mergeAddress);
        }
    }
    
    /**
     * 添加合并单元格的区域. 合并方式是按列方向合并
     *
     * @param sheet    需要添加合并区域的 sheet 页
     * @param rowStart 合并区域起始行 index
     * @param rowEnd   合并区域终止行 index
     * @param begCol   合并区域起始列
     * @param enfCol   合并区域结束列
     */
    public static void addMergeAddress(Sheet sheet, int rowStart, int rowEnd, int begCol, int enfCol) {
        for (int column = begCol; column < enfCol; column++) {
            if (rowStart < rowEnd) {
                CellRangeAddress mergeAddress = new CellRangeAddress(rowStart, rowEnd, column, column);
                sheet.addMergedRegion(mergeAddress);
            }
        }
    }
 
    /**
     * 获取sheet {@code rowIndex} 行, {@code columnIndex} 列的 String 值
     *
     * @param newSheet    sheet 页
     * @param rowIndex    行索引
     * @param columnIndex 列索引
     * @return 指定单元格的值, 如果单元格为 blank, return null
     */
    public static String getCellStringValue(Sheet newSheet, int rowIndex, int columnIndex) {
        Cell cell = newSheet.getRow(rowIndex).getCell(columnIndex);
        CellType cellType = cell.getCellType();
        if (cellType == CellType.BLANK) {
            return null;
        }
        return cell.getStringCellValue();
    }
 
    /**
     * 合并excel 单元格.
     * 合并方式: 列方向合并相同数据的单元格, 空白单元格不合并.
     *
     * @param newBook 要合并的excel
     */
    private static void mergeCell(Workbook newBook) {
        Sheet newSheet = newBook.getSheetAt(0);
 
        //合并区域最大行索引
        int mergeMaxRowIndex = newSheet.getLastRowNum();
        //合并区域最大列索引
        int mergeMaxColumnIndex = newSheet.getRow(0).getLastCellNum() - 1;
 
        //记录单元格的值
        String value = null;
        //合并区域起始行索引
        int rowStart;
 
        //开始合并
        for (int j = 0; j <= mergeMaxColumnIndex; j++) {
            //找出某列, 第一个不空的单元格的行索引, 并将单元格的值赋给 value
            int i0;
            for (i0 = 1; i0 < mergeMaxRowIndex; i0++) {
                value = MergeCellUtil.getCellStringValue(newSheet, i0, j);
                if (StringUtils.isNotBlank(value)) {
                    break;
                }
            }
 
            if (i0 == mergeMaxRowIndex + 1) {
                //整列为空, 不需要合并操作
 
                continue;
            }
 
            rowStart = i0;
            for (int i = i0 + 1; i <= mergeMaxRowIndex; i++) {
 
                //循环到当前的单元格的值
                String cellValue = MergeCellUtil.getCellStringValue(newSheet, i, j);
 
                if (StringUtils.isBlank(cellValue)) {
                    if (StringUtils.isNotBlank(value)) {
                        //当前单元格为一组连续空白区域的第一个空白格
 
                        MergeCellUtil.addMergeAddress(newSheet, rowStart, i - 1, j);
 
                        value = null;
                        rowStart = i;
                    }
                } else {
                    if (StringUtils.isBlank(value)) {
                        //当前单元格为空白区域下第一个不为空的单元格
 
                        value = cellValue;
                        rowStart = i;
                    } else if (!value.equals(cellValue)) {
                        //当前单元格和记录的值不同
 
                        MergeCellUtil.addMergeAddress(newSheet, rowStart, i - 1, j);
 
                        rowStart = i;
                        value = cellValue;
                    }
                }
 
                if (i == mergeMaxRowIndex) {
                    //最后一行
                    MergeCellUtil.addMergeAddress(newSheet, rowStart, i, j);
                }
            }
        }
 
    }
 
}

````







# EasyPoi 

[API文档](https://apidoc.gitee.com/lemur/easypoi/)

## 概况

今天做Excel导出时，发现了一款非常好用的POI框架EasyPoi，其 使用起来简洁明了。现在我们就来介绍下EasyPoi,首先感谢EasyPoi 的开发者  Lemur开源

### easypoi 简介

easypoi 是为了让开发者快速的实现excel，word,pdf的导入导出，基于Apache poi基础上的一个工具包。

## 特性

> 基于注解的导入导出，修改注解就可以修改Excel
> 支持常用的样式自定义
> 基于map可以灵活定义的表头字段
> 支持一对多的导出，导入
> 支持模板的导出，一些常见的标签，自定义标签
> 支持HTML/Excel转换
> 支持word的导出，支持图片，Excel

## 常用注解

### @Excel注解

@Excel 注解是作用到Filed 上面，是对Excel一列的一个描述，这个注解是必须要的注解，其部分属性如下：

![EasyPoi_01](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129214815689.png)


其使用如下，其中orderNum是指定该字段在Excel中的位置，name与Excel中对应的表头单元格的名称

    @Excel(name = "主讲老", orderNum = "1")
    private String name;
### @ExcelCollection 注解

@ExcelCollection 注解表示一个集合，主要针对一对多的导出
比如一个老师对应多个科目，科目就可以用集合表示，作用在一个类型是List的属性上面，属性如下：

![EasyPoi_02](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215026238.png)

其使用如下所示。

```java
@ExcelCollection(name = "学生", orderNum = "4")
    private List<StudentEntity> students;

```



### @ExcelEntity注解

@ExcelEntity注解表示一个继续深入导出的实体，是作用一个类型为实体的属性上面，其属性如下：

![EasyPoi_03](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215051213.png)

其使用如下所示。

```java
   @ExcelEntity(id = "major")
    private TeacherEntity chineseTeacher;

```

​	

### @ExcelIgnore 注解

@ExcelIgnore 注解修饰的字段，表示在导出的时候补导出，被忽略。

### @ExcelTarget 注解

@ExcelTarget注解作用于最外层的对象，描述这个对象的id,以便支持一个对象，可以针对不同导出做出不同处理，其作用在实体类的上，属性如下：

![EasyPoi_04](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129215115779.png)

其使用如下：

```java
@ExcelTarget("scoreIssueReqPOJO")
public class ScoreIssueReqPOJO implements java.io.Serializable{}
```



## EasyPOI的使用

### 引入依赖

SSM 项目，引入依赖
如果spring的版本是4.x的话引入的easypoi的版本是3.0.1，如果spring是5.x的话引入easypoi的版本是4.0.0

```xml
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-base</artifactId>
    <version>4.0.0</version>
</dependency>
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-web</artifactId>
    <version>4.0.0</version>
</dependency>
<dependency>
    <groupId>cn.afterturn</groupId>
    <artifactId>easypoi-annotation</artifactId>
    <version>4.0.0</version>
</dependency>
```



Spring Boot 项目（2.x以上的版本，我demo的版本是2.1.3.RELEASE），引入依赖

```xml
	<dependency>
        <groupId>cn.afterturn</groupId>
        <artifactId>easypoi-spring-boot-starter</artifactId>
        <version>4.0.0</version>
	</dependency>
```



需要注意的是由于easypoi的依赖内部依赖原生的poi，所以，引入了easypoi的依赖之后，需要把原生的poi的依赖删掉

### 注解方式导出Excel

#### 导出测试的demo

​    

```java
@Test
public void testExportExcel() throws Exception {
    List<CourseEntity> courseEntityList = new ArrayList<>();
    CourseEntity courseEntity = new CourseEntity();
    courseEntity.setId("1");
    courseEntity.setName("测试课程");
    TeacherEntity teacherEntity = new TeacherEntity();
    teacherEntity.setName("张老师");
    teacherEntity.setSex(1);
    courseEntity.setMathTeacher(teacherEntity);

    List<StudentEntity> studentEntities = new ArrayList<>();
    for (int i = 1; i <= 2; i++) {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setName("学生" + i);
        studentEntity.setSex(i);
        studentEntity.setBirthday(new Date());
        studentEntities.add(studentEntity);
    }
    courseEntity.setStudents(studentEntities);
    courseEntityList.add(courseEntity);
    Date start = new Date();
    Workbook workbook = ExcelExportUtil.exportExcel( new ExportParams("导出测试", null, "测试"),
            CourseEntity.class, courseEntityList);
    System.out.println(new Date().getTime() - start.getTime());
    File savefile = new File("D:/excel/");
    if (!savefile.exists()) {
        savefile.mkdirs();
    }
    FileOutputStream fos = new FileOutputStream("D:/excel/教师课程学生导出测试.xls");
    workbook.write(fos);
    fos.close();
}
```
#### 导出对应的Bean

##### CourseEntity 类

```java
@ExcelTarget("courseEntity")
public class CourseEntity implements java.io.Serializable {
    /** 主键 */
    private String        id;
    /** 课程名称 */
    @Excel(name = "课程名称", orderNum = "1", width = 25,needMerge = true)
    private String        name;
    /** 老师主键 */
    //@ExcelEntity(id = "major")
    private TeacherEntity chineseTeacher;
    /** 老师主键 */
    @ExcelEntity(id = "absent")
    private TeacherEntity mathTeacher;
    @ExcelCollection(name = "学生", orderNum = "4")
    private List<StudentEntity> students;
```

##### TeacherEntity 类

```java
@Data
public class TeacherEntity {
    /**

   * 学生姓名
     /
         @Excel(name = "教师姓名", height = 20, width = 30, isImportField = "true_st")
         private String name;
         /**
        * 学生性别
          /
              @Excel(name = "教师性别", replace = {"男_1", "女_2"}, suffix = "生", isImportField = "true_st")
              private int sex;
}
```



##### StudentEntity 类



    public class StudentEntity implements java.io.Serializable {
        /**
         * id
         */
        private String        id;
        /**
         * 学生姓名
         */
        @Excel(name = "学生姓名", height = 20, width = 30, isImportField = "true_st")
        private String        name;
        /**
         * 学生性别
         */
        @Excel(name = "学生性别", replace = { "男_1", "女_2" }, suffix = "生", isImportField = "true_st")
        private int           sex;
        
            @Excel(name = "出生日期", exportFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd", isImportField = "true_st", width = 20)
            private Date          birthday;
    
        @Excel(name = "进校日期", exportFormat = "yyyyMMddHHmmss", format = "yyyy-MM-dd")
        private Date registrationDate;
#### 导出结果

![EasyPoi_05](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319134714898.png)

##### 关于日期格式化的说明

如果是导出的实体类(就是说这个实体类是对应导出的Excel的)，那么用 @Excel注解的exportFormat属性来格式化日期。如下所示：

```java
@Excel(name = "出生日期", exportFormat = "yyyy-MM-dd HH:mm:ss", width = 20)
```

如果是导入的实体类（就是说这个实体类是对应导入的Excel的），那么用@Excel注解的importFormat属性来格式化日期。如下所示：

```java
   @Excel(name = "添加时间",importFormat =  "yyyy-MM-dd HH:mm:ss",orderNum = "14")
    private Date createTime;
```

@Excel注解的databaseFormat 属性是用于数据库的格式不是日期类型，如datetime时用。



### 注解方式导入Excel

基于注解的导入导出,配置配置上是一样的，只是方式反过来而已。首先让我们来看看 ImportParams类，这个类主要是设置导入参数，例如表格行数，表头行数等等。

#### ImportParams参数介绍

![EasyPoi_06](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20190129221846393.png)

需要说明的是
  1、titleRows表示的是表格标题行数，如果没有就是0，如果有一个标题就是1，如果是两个标题就2

2. headRows表示的是表头行数，默认是1，如果有两个表头则需要设置2。

#### 导入情形一：有标题有表头

我们有如下格式的Excel需要导入：

![EasyPoi_07](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319134942331.png)

这个Excel有一个标题行，有一个表头行，所以我们有了如下设置：

```java
  ImportParams params = new ImportParams();
		//设置标题的行数，有标题时一定要有
        params.setTitleRows(1);
		//设置表头的行数
        params.setHeadRows(1);
```

##### 导入的demo

```java
@Test
    public void haveTitleTest() {
        ImportParams params = new ImportParams();
		//设置标题的行数，有标题时一定要有
        params.setTitleRows(1);
		//设置表头的行数
        params.setHeadRows(1);
        String file = Thread.currentThread().getContextClassLoader().getResource("haveTitle.xlsx").getFile();
        List<ScoreIssueReqPOJO> list = ExcelImportUtil.importExcel(
                new File(file),
                ScoreIssueReqPOJO.class, params);
        System.out.println("解析到的数据长度是：" + list.size());
        for (ScoreIssueReqPOJO scoreIssueReqPOJO : list) {
                       System.out.println("***********有标题有表头导入的数据是=" + scoreIssueReqPOJO.toString());
        }
    }
```

导入测试的结果是：

![EasyPoi_08](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319135225371.png)



导入情形二：有表头没有标题

只有一个表头没有标题的话，我们ImportParams可以用默认的值。

导入的demo

```java
@Test
    public void notTitleTest() {
        ImportParams params = new ImportParams();
        String file = Thread.currentThread().getContextClassLoader().getResource("notTitle.xlsx").getFile();
        List<ScoreIssueReqPOJO> list = ExcelImportUtil.importExcel(
                new File(file),
                ScoreIssueReqPOJO.class, params);
        System.out.println("解析到的数据长度是：" + list.size());
        for (ScoreIssueReqPOJO scoreIssueReqPOJO : list) {
           System.out.println("***********有表头没有标题导入的数据是=" + scoreIssueReqPOJO.toString());
        }
    }
```


导入结果如下：

![EasyPoi_10](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200319135341203.png)

导入实体Bean配置
```java
public class ScoreIssueReqPOJO implements java.io.Serializable{
    /**
     * 用户手机号
     */
    @Excel(name = "个人用户手机号*",width = 14)
    private String mobile;
    /**
     * 企业用户税号
     */
    @Excel(name = "企业用户税号*",width = 20)
    private String taxNum;
    /**
     * 公司名称或者用户名
     */
    @Excel(name = "名称",width = 20)
    @Length(max =50 ,message = "名称过长")
    private String realname;
    /**
     * 积分数量
     * isStatistics 自动统计数据
     */
    @Excel(name = "积分数量*")
    @NotNull(message = "积分数量不能为空")
    private String scoreNum;
    /**
     * 平台类型
     */
    @Excel(name = "业务代码")
    @Length(max =2 ,message = "业务代码过长，最长2个字符（必须由诺诺网分配，请勿乱填）")
    private String platform;
    /**
     * 备注
     */
    @Excel(name = "备注")
    @Length(max =120 ,message = "备注过长,最长120个字符")
    private String typeContent;
}
```



### Excel导入校验

EasyPoi的校验使用也很简单,在导入对象上加上通用的校验规则或者这定义的这个看你用的哪个实现
然后params.setNeedVerfiy(true);配置下需要校验就可以了

看下具体的代码

```java
/**
     * Email校验
     */
    @Excel(name = "Email", width = 25)
    private String email;
    /**
     * 最大
     */
    @Excel(name = "Max")
    @Max(value = 15,message = "max 最大值不能超过15" ,groups = {ViliGroupOne.class})
    private int    max;
    /**
     * 最小
     */
    @Excel(name = "Min")
    @Min(value = 3, groups = {ViliGroupTwo.class})
    private int    min;
    /**
     * 非空校验
     */
    @Excel(name = "NotNull")
    @NotNull
    private String notNull;
    /**
     * 正则校验
     */
    @Excel(name = "Regex")
    @Pattern(regexp = "[\u4E00-\u9FA5]*", message = "不是中文")
    private String regex;
```



使用方式就是在导入时设置needVerfiy属性为true。导入的demo如下所示：

```java
@Test
    public void basetest() {
        try {
            ImportParams params = new ImportParams();
            params.setNeedVerfiy(true);
            params.setVerfiyGroup(new Class[]{ViliGroupOne.class});
            ExcelImportResult<ExcelVerifyEntity> result = ExcelImportUtil.importExcelMore(
                new File(PoiPublicUtil.getWebRootPath("import/verfiy.xlsx")),
                ExcelVerifyEntity.class, params);
            FileOutputStream fos = new FileOutputStream("D:/excel/ExcelVerifyTest.basetest.xlsx");
            result.getWorkbook().write(fos);
            fos.close();
            for (int i = 0; i < result.getList().size(); i++) {
                System.out.println(ReflectionToStringBuilder.toString(result.getList().get(i)));
            }
            Assert.assertTrue(result.getList().size() == 1);
            Assert.assertTrue(result.isVerfiyFail());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
```



#### 导入结果ExcelImportResult

导入之后返回一个ExcelImportResult 对象,比我们平时返回的list多了一些元素



    /**
         * 结果集
         */
    private List<T>  list;
    /**
     * 是否存在校验失败
     */
    private boolean  verfiyFail;
    
    /**
     * 数据源
     */
    private Workbook workbook;

一个是集合,是一个是是否有校验失败的数据,一个原本的文档,但是在文档后面追加了错误信息

注意,这里的list,有两种返回

> 一种是只返回正确的数据
> 一种是返回全部的数据,但是要求这个对象必须实现IExcelModel接口,如下
>
> ```java
> IExcelModel
> 
> public class ExcelVerifyEntityOfMode extends ExcelVerifyEntity implements IExcelModel {
>     private String errorMsg;
> 
>     @Override
>     public String getErrorMsg() {
>         return errorMsg;
>     }
> 
>     @Override
>     public void setErrorMsg(String errorMsg) {
>         this.errorMsg = errorMsg;
>     }
> }
> ```
>
> 




##### IExcelDataModel

###### 获取错误数据的行号



    public interface IExcelDataModel {
        /**
        * 获取行号
        * @return
        */
        public int getRowNum();
    
        /**
        *  设置行号
        * @param rowNum
        */
        public void setRowNum(int rowNum);
    }



###### 需要对象实现这个接口

每行的错误数据也会填到这个错误信息中,方便用户后面自定义处理
看下代码

```java
@Test
    public void baseModetest() {
        try {
            ImportParams params = new ImportParams();
            params.setNeedVerfiy(true);
            ExcelImportResult<ExcelVerifyEntityOfMode> result = ExcelImportUtil.importExcelMore(
                    new FileInputStream(new File(PoiPublicUtil.getWebRootPath("import/verfiy.xlsx"))),
                ExcelVerifyEntityOfMode.class, params);
            FileOutputStream fos = new FileOutputStream("D:/excel/baseModetest.xlsx");
            result.getWorkbook().write(fos);
            fos.close();
            for (int i = 0; i < result.getList().size(); i++) {
                System.out.println(ReflectionToStringBuilder.toString(result.getList().get(i)));
            }
            Assert.assertTrue(result.getList().size() == 4);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
        }
    }
```



### 定制化修改

有时候，我们需要定制化一些信息，比如，导出Excel时，我们需要统一Excel的字体的大小，字型等。我们可以通过实现**IExcelExportStyler**接口或者继承**ExcelExportStylerDefaultImpl**类来实现。如下所示：我们定义了一个ExcelStyleUtil工具类继承了ExcelExportStylerDefaultImpl（样式的默认实现类）,并且将列头，标题，单元格的字体都设置为了宋体。



    public class ExcelStyleUtil extends ExcelExportStylerDefaultImpl {
        public ExcelStyleUtil(Workbook workbook) {
            super(workbook);
        }
        /**
         * 标题样式
         */	
        @Override
        public CellStyle getTitleStyle(short color) {
            CellStyle cellStyle = super.getTitleStyle(color);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 间隔行的样式
         */
        @Override
        public CellStyle stringSeptailStyle(Workbook workbook, boolean isWarp) {
            CellStyle cellStyle = super.stringSeptailStyle(workbook, isWarp);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 列表头样式
         */
        @Override
        public CellStyle getHeaderStyle(short color) {
            CellStyle cellStyle =  super.getHeaderStyle(color);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
         /**
         * 单元格的样式
         */
        @Override
        public CellStyle stringNoneStyle(Workbook workbook, boolean isWarp) {
            CellStyle cellStyle = super.stringNoneStyle(workbook, isWarp);
            cellStyle.setFont(getFont(workbook, 11, false));
            return cellStyle;
        }
    
        /**
         * 字体样式
         *
         * @param size   字体大小
         * @param isBold 是否加粗
         * @return
         */
        private Font getFont(Workbook workbook, int size, boolean isBold) {
            Font font = workbook.createFont();
            //字体样式
            font.setFontName("宋体");
            //是否加粗
            font.setBold(isBold);
            //字体大小
            font.setFontHeightInPoints((short) size);
            return font;
        }
    }


然后就是对ExcelExportUtil类进行包装，如下所示：




    /**
     *
     */
    private static final Integer EXPORT_EXCEL_MAX_NUM = 20000;
    
    /**
     * 获取导出的Workbook对象
     *
     * @param sheetName 页签名
     * @param clazz     类对象
     * @param list      导出的数据集合
     * @return
     * @author xiagwei
     * @date 2020/2/10 4:45 PM
     */
    public static Workbook getWorkbook(String sheetName, Class clazz, List<?> list) {
        //判断数据是否为空
        if (CollectionUtils.isEmpty(list)) {
            log.info("***********导出数据行数为空!");
            list = new ArrayList<>();
        }
        if (list.size() > EXPORT_EXCEL_MAX_NUM) {
            log.info("***********导出数据行数超过:" + EXPORT_EXCEL_MAX_NUM + "条,无法导出、请添加导出条件!");
            list = new ArrayList<>();
        }
        log.info("***********"+sheetName+"的导出数据行数为"+list.size()+"");
        //获取导出参数
        ExportParams exportParams = new ExportParams();
        //设置导出样式
        exportParams.setStyle(ExcelStyleUtil.class);
        //设置sheetName
        exportParams.setSheetName(sheetName);
        //输出workbook流
        return ExcelExportUtil.exportExcel(exportParams, clazz, list);
    }


使用的话，我们只需要获取需要导出的数据，然后调用OfficeExportUtil的getWorkbook方法。

总结

**本文主要介绍了EasyPOI的使用和相关属性，EasyPOI使用起来还是蛮简单的。但是有个缺点是导入导出大批量数据时性能没那么好。**



# Activiti 

**Activiti 工作流总共包含 23 张数据表（现在是25张，新增了 ACT_EVT_LOG 和 ACT_PROCDEF_INFO ）**

## 一、结构设计

### 1、逻辑结构设计

Activiti使用到的表都是ACT_开头的。

| 表开头        | 说明                                                         |
| :------------ | :----------------------------------------------------------- |
| **ACT_RE_\*** | ’RE’表示repository(存储)，RepositoryService接口所操作的表。带此前缀的表包含的是静态信息，如，流程定义，流程的资源（图片，规则等） |
| **ACT_RU_\*** | ‘RU’表示runtime，运行时表-RuntimeService。这是运行时的表 存储着流程变量，用户任务，变量，职责（job）等运行时的数据。Activiti只存储实例执行期间的运行时数据，当流程实例结束时，将删除这些记录。 这就保证了这些运行时的表小且快 |
| **ACT_ID_\*** | ’ID’表示identity (组织机构)，IdentityService接口所操作的表。用户记录，流程中使用到的用户和组。这些表包含标识的信息，如用户，用户组，等等 |
| **ACT_HI_\*** | ’HI’表示history，历史数据表，HistoryService。就是这些表包含着流程执行的历史相关数据，如结束的流程实例，变量，任务，等等 |
| **ACT_GE_\*** | 全局通用数据及设置(general)，各种情况都使用的数据            |

### 2、所有表的含义

#### 一般数据 (ACT_GE_)

| 表名             | 解释                                                         |
| :--------------- | :----------------------------------------------------------- |
| ACT_GE_BYTEARRAY | 二进制数据表，存储通用的流程定义和流程资源。                 |
| ACT_GE_PROPERTY  | 系统相关属性，属性数据表存储整个流程引擎级别的数据，初始化表结构时，会默认插入三条记录。 |

#### 流程历史记录 (ACT_HI_)

| 表名                | 解释                           |
| :------------------ | :----------------------------- |
| ACT_HI_ACTINST      | 历史节点表                     |
| ACT_HI_ATTACHMENT   | 历史附件表                     |
| ACT_HI_COMMENT      | 历史意见表                     |
| ACT_HI_DETAIL       | 历史详情表，提供历史变量的查询 |
| ACT_HI_IDENTITYLINK | 历史流程人员表                 |
| ACT_HI_PROCINST     | 历史流程实例表                 |
| ACT_HI_TASKINST     | 历史任务实例表                 |
| ACT_HI_VARINST      | 历史变量表                     |

#### 用户用户组表 (ACT_ID_)

| 表名              | 解释                   |
| :---------------- | :--------------------- |
| ACT_ID_GROUP      | 用户组信息表           |
| ACT_ID_INFO       | 用户扩展信息表         |
| ACT_ID_MEMBERSHIP | 用户与用户组对应信息表 |
| ACT_ID_USER       | 用户信息表             |

#### 流程定义表 (ACT_RE_)

| 表名              | 解释               |
| :---------------- | :----------------- |
| ACT_RE_DEPLOYMENT | 部署信息表         |
| ACT_RE_MODEL      | 流程设计模型部署表 |
| ACT_RE_PROCDEF    | 流程定义数据表     |

#### 运行实例表 (ACT_RU_)

| 表名                | 解释                                                 |
| :------------------ | :--------------------------------------------------- |
| ACT_RU_EVENT_SUBSCR | 运行时事件 throwEvent、catchEvent 时间监听信息表     |
| ACT_RU_EXECUTION    | 运行时流程执行实例                                   |
| ACT_RU_IDENTITYLINK | 运行时流程人员表，主要存储任务节点与参与者的相关信息 |
| ACT_RU_JOB          | 运行时定时任务数据表                                 |
| ACT_RU_TASK         | 运行时任务节点表                                     |
| ACT_RU_VARIABLE     | 运行时流程变量数据表                                 |

#### 其它

| 表名             | 解释                   |
| :--------------- | :--------------------- |
| ACT_EVT_LOG      | 事件日志               |
| ACT_PROCDEF_INFO | 流程定义的动态变更信息 |



## 二、表以及索引信息

### 1、act_ge_bytearray

> 二进制数据表，存储通用的流程定义和流程资源。（act_ge_bytearray）

　　保存流程定义图片和xml、Serializable(序列化)的变量,即保存所有二进制数据，特别注意类路径部署时候，不要把svn等隐藏文件或者其他与流程无关的文件也一起部署到该表中，会造成一些错误（可能导致流程定义无法删除）。

#### **表结构说明**

| 字段名称       | 字段描述       | 数据类型       | 主键 | 为空 | 取值说明                                            |
| :------------- | :------------- | :------------- | :--- | :--- | :-------------------------------------------------- |
| ID_            | ID_            | nvarchar(64)   | Y    |      | 主键ID                                              |
| REV_           | 乐观锁         | int            |      | Y    | Version(版本)                                       |
| NAME_          | 名称           | nvarchar(255)  |      | Y    | 部署的文件名称，如：leave.bpmn.png,leave.bpmn20.xml |
| DEPLOYMENT_ID_ | 部署ID         | nvarchar(64)   |      | Y    | 部署表ID                                            |
| BYTES_         | 字节           | varbinary(max) |      | Y    | 部署文件                                            |
| GENERATED_     | 是否是引擎生成 | tinyint        |      | Y    | 0为用户生成，1为activiti生成                        |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145314651.png)

###  2、act_ge_property 

> 属性数据表(act_ge_property)

属性数据表。存储整个流程引擎级别的数据。

#### **表结构说明**

| 字段名称 | 字段描述 | 数据类型      | 主键 | 为空 | 取值说明                              |
| :------- | :------- | :------------ | :--- | :--- | :------------------------------------ |
| NAME_    | 名称     | nvarchar(64)  | √    |      | schema.versionschema.historynext.dbid |
| VALUE_   | 值       | nvarchar(300) |      | √    | 5.*create(5.*)                        |
| REV_     | 乐观锁   | int           |      | √    | version                               |

#### **索引说明**



![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145424425.png)

### 3、act_hi_actinst

> 历史节点表（act_hi_actinst）

历史活动信息。这里记录流程流转过的所有节点，与HI_TASKINST不同的是，taskinst只记录usertask内容

#### **表结构说明**

| 字段名称           | 字段描述             | 数据类型      | 主键 | 为空 | 取值说明                                |
| :----------------- | :------------------- | :------------ | :--- | :--- | :-------------------------------------- |
| ID_                | ID_                  | nvarchar(64)  | √    |      |                                         |
| PROC_DEF_ID_       | 流程定义ID           | nvarchar(64)  |      |      |                                         |
| PROC_INST_ID_      | 流程实例ID           | nvarchar(64)  |      |      |                                         |
| EXECUTION_ID_      | 执行实例ID           | nvarchar(64)  |      |      |                                         |
| ACT_ID_            | 节点ID               | nvarchar(225) |      |      | 节点定义ID                              |
| TASK_ID_           | 任务实例ID           | nvarchar(64)  |      | √    | 任务实例ID 其他节点类型实例ID在这里为空 |
| CALL_PROC_INST_ID_ | 调用外部的流程实例ID | nvarchar(64)  |      | √    | 调用外部流程的流程实例ID’               |
| ACT_NAME_          | 节点名称             | nvarchar(225) |      | √    | 节点定义名称                            |
| ACT_TYPE_          | 节点类型             | nvarchar(225) |      |      | 如startEvent、userTask                  |
| ASSIGNEE_          | 签收人               | nvarchar(64)  |      | √    | 节点签收人                              |
| START_TIME_        | 开始时间             | datetime      |      |      | 2013-09-15 11:30:00                     |
| END_TIME_          | 结束时间             | datetime      |      | √    | 2013-09-15 11:30:00                     |
| DURATION_          | 耗时                 | numeric(19,0) |      | √    | 毫秒值                                  |

#### **索引说明**

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145707616.png)

### 4、act_hi_attachment

> 历史附件表( act_hi_attachment )

#### **表结构说明** 

| 字段名称      | 字段描述   | 数据类型       | 主键 | 为空 | 取值说明             |
| :------------ | :--------- | :------------- | :--- | :--- | :------------------- |
| ID_           | ID_        | nvarchar(64)   | √    |      | 主键ID               |
| REV_          | 乐观锁     | integer        |      | √    | Version              |
| USER_ID_      | 用户ID     | nvarchar(255)  |      | √    | 用户ID               |
| NAME_         | 名称       | nvarchar(255)  |      | √    | 附件名称             |
| DESCRIPTION_  | 描述       | nvarchar(4000) |      | √    | 描述                 |
| TYPE_         | 类型       | nvarchar(255)  |      | √    | 附件类型             |
| TASK_ID_      | 任务实例ID | nvarchar(64)   |      | √    | 节点实例ID           |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)   |      | √    | 流程实例ID           |
| URL_          | URL_       | nvarchar(4000) |      | √    | 附件地址             |
| CONTENT_ID_   | 字节表的ID | nvarchar(64)   |      | √    | ACT_GE_BYTEARRAY的ID |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145803774.png)

### 5、act_hi_comment

> 历史意见表( act_hi_comment )

#### **表结构说明** 

| 字段名称      | 字段描述   | 数据类型       | 主键 | 为空 | 取值说明                             |
| :------------ | :--------- | :------------- | :--- | :--- | :----------------------------------- |
| ID_           | ID_        | nvarchar(64)   | √    |      | 主键ID                               |
| TYPE_         | 类型       | nvarchar(255)  |      | √    | 类型：event（事件）comment（意见）   |
| TIME_         | 时间       | datetime       |      |      | 填写时间’                            |
| USER_ID_      | 用户ID     | nvarchar(64)   |      | √    | 填写人                               |
| TASK_ID_      | 节点任务ID | nvarchar(64)   |      | √    | 节点实例ID                           |
| PROC_INST_ID_ | 流程实例ID | nvarchar(255)  |      | √    | 流程实例ID                           |
| ACTION_       | 行为类型   | nvarchar(64)   |      | √    | 见备注1                              |
| MESSAGE_      | 基本内容   | nvarchar(4000) |      | √    | 用于存放流程产生的信息，比如审批意见 |
| FULL_MSG_     | 全部内容   | varbinary(max) |      | √    | 附件地址                             |

#### **索引说明**

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145900380.png)

### 6、act_hi_detail

> 历史详情表( act_hi_detail )

流程中产生的变量详细，包括控制流程流转的变量，业务表单中填写的流程需要用到的变量等。

#### **表结构说明** 

| 字段名称      | 字段描述   | 数据类型         | 主键 | 为空 | 取值说明                                            |
| :------------ | :--------- | :--------------- | :--- | :--- | :-------------------------------------------------- |
| ID_           | ID_        | nvarchar(64)     | √    |      | 主键                                                |
| TYPE_         | 类型       | nvarchar(255)    |      |      | 见备注2                                             |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)     |      | √    | 流程实例ID                                          |
| EXECUTION_ID_ | 执行实例ID | nvarchar(64)     |      | √    | 执行实例ID                                          |
| TASK_ID_      | 任务实例ID | nvarchar(64)     |      | √    | 任务实例ID                                          |
| ACT_INST_ID_  | 节点实例ID | nvarchar(64)     |      | √    | ACT_HI_ACTINST表的ID                                |
| NAME_         | 名称       | nvarchar(255)    |      |      | 名称                                                |
| VAR_TYPE_     | 参数类型   | nvarchar(255)    |      | √    | 见备注3                                             |
| REV_          | 乐观锁     | int              |      | √    | Version                                             |
| TIME_         | 时间戳     | datetime         |      |      | 创建时间                                            |
| BYTEARRAY_ID_ | 字节表ID   | nvarchar         |      | √    | ACT_GE_BYTEARRAY表的ID                              |
| DOUBLE_       | DOUBLE_    | double precision |      | √    | 存储变量类型为Double                                |
| LONG_         | LONG_      | numeric          |      | √    | 存储变量类型为long                                  |
| TEXT_         | TEXT_      | nvarchar         |      | √    | 存储变量值类型为String                              |
| TEXT2_        | TEXT2_     | nvarchar         |      | √    | 此处存储的是JPA持久化对象时，才会有值。此值为对象ID |

#### **索引说明**

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205145958575.png)

### 7、act_ru_identitylink

> 历史流程人员表( act_ru_identitylink )

任务参与者数据表。主要存储历史节点参与者的信息

#### 表结构说明 

| 字段名称      | 字段描述   | 数据类型      | 主键 | 为空 | 取值说明   |
| :------------ | :--------- | :------------ | :--- | :--- | :--------- |
| ID_           | ID_        | nvarchar(64)  | √    |      | ID_        |
| GROUP_ID_     | 组ID       | nvarchar(255) |      | √    | 组ID       |
| TYPE_         | 类型       | nvarchar(255) |      | √    | 备注4      |
| USER_ID_      | 用户ID     | nvarchar(255) |      | √    | 用户ID     |
| TASK_ID_      | 节点实例ID | nvarchar(64)  |      | √    | 节点实例ID |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)  |      | √    | 流程实例ID |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150052877.png)

### 8、act_hi_procinst

> 历史流程实例表（act_hi_procinst）

#### 表结构说明 

| 字段名称                   | 字段描述     | 数据类型       | 主键 | 为空 | 取值说明               |
| :------------------------- | :----------- | :------------- | :--- | :--- | :--------------------- |
| ID_                        | ID_          | nvarchar(64)   | √    |      | 主键ID                 |
| PROC_INST_ID_              | 流程实例ID   | nvarchar(64)   |      |      | 流程实例ID             |
| BUSINESS_KEY_              | 业务主键     | nvarchar(255)  |      | √    | 业务主键，业务表单的ID |
| PROC_DEF_ID_               | 流程定义ID   | nvarchar(64)   |      |      | 流程定义ID             |
| START_TIME_                | 开始时间     | datetime       |      |      | 开始时间               |
| END_TIME_                  | 结束时间     | datetime       |      | √    | 结束时间               |
| DURATION_                  | 耗时         | Numeric(19)    |      | √    | 耗时                   |
| START_USER_ID_             | 起草人       | nvarchar(255)  |      | √    | 起草人                 |
| START_ACT_ID_              | 开始节点ID   | nvarchar(255)  |      | √    | 起草环节ID             |
| END_ACT_ID_                | 结束节点ID   | nvarchar(255)  |      | √    | 结束环节ID             |
| SUPER_PROCESS_INSTANCE_ID_ | 父流程实例ID | nvarchar(64)   |      | √    | 父流程实例ID           |
| DELETE_REASON_             | 删除原因     | nvarchar(4000) |      | √    | 删除原因               |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150200290.png)

### 9、act_hi_taskinst

> 历史任务实例表( act_hi_taskinst )

#### 表结构说明 

| 字段名称        | 字段描述                | 数据类型       | 主键 | 为空 | 取值说明                               |
| :-------------- | :---------------------- | :------------- | :--- | :--- | :------------------------------------- |
| ID_             | ID_                     | nvarchar(64)   | √    |      | 主键ID                                 |
| PROC_DEF_ID_    | 流程定义ID              | nvarchar(64)   |      | √    | 流程定义ID                             |
| TASK_DEF_KEY_   | 节点定义ID              | nvarchar(255)  |      | √    | 节点定义ID                             |
| PROC_INST_ID_   | 流程实例ID              | nvarchar(64)   |      | √    | 流程实例ID                             |
| EXECUTION_ID_   | 执行实例ID              | nvarchar(64)   |      | √    | 执行实例ID                             |
| NAME_           | 名称                    | varchar(255)   |      | √    | 名称                                   |
| PARENT_TASK_ID_ | 父节点实例ID            | nvarchar(64)   |      | √    | 父节点实例ID                           |
| DESCRIPTION_    | 描述                    | nvarchar(400)  |      | √    | 描述                                   |
| OWNER_          | 实际签收人 任务的拥有者 | nvarchar(255)  |      | √    | 签收人（默认为空，只有在委托时才有值） |
| ASSIGNEE_       | 签收人或被委托          | nvarchar(255)  |      | √    | 签收人或被委托                         |
| START_TIME_     | 开始时间                | datetime       |      |      | 开始时间                               |
| CLAIM_TIME_     | 提醒时间                | datetime       |      | √    | 提醒时间                               |
| END_TIME_       | 结束时间                | datetime       |      | √    | 结束时间                               |
| DURATION_       | 耗时                    | numeric(19)    |      | √    | 耗时                                   |
| DELETE_REASON_  | 删除原因                | nvarchar(4000) |      | √    | 删除原因(completed,deleted)            |
| PRIORITY_       | 优先级别                | int            |      | √    | 优先级别                               |
| DUE_DATE_       | 过期时间                | datetime       |      | √    | 过期时间，表明任务应在多长时间内完成   |
| FORM_KEY_       | 节点定义的formkey       | nvarchar(255)  |      | √    | desinger节点定义的form_key属性         |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150311804.png)

### 10、act_hi_varinst

> 历史变量表( act_hi_varinst )

#### 表结构说明 

| 字段名称      | 字段描述   | 数据类型      | 主键 | 为空 | 取值说明                                            |
| :------------ | :--------- | :------------ | :--- | :--- | :-------------------------------------------------- |
| ID_           | ID_        | nvarchar(64)  | √    |      | ID_                                                 |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)  |      | √    | 流程实例ID                                          |
| EXECUTION_ID_ | 执行实例ID | nvarchar(255) |      | √    | 执行实例ID                                          |
| TASK_ID_      | 任务实例ID | nvarchar(64)  |      | √    | 任务实例ID                                          |
| NAME_         | 名称       | nvarchar(64)  |      |      | 参数名称(英文)                                      |
| VAR_TYPE_     | 参数类型   | varchar(255)  |      | √    | 备注5                                               |
| REV_          | 乐观锁     | nvarchar(64)  |      | √    | 乐观锁 Version                                      |
| BYTEARRAY_ID_ | 字节表ID   | nvarchar(400) |      | √    | ACT_GE_BYTEARRAY表的主键                            |
| DOUBLE_       | DOUBLE_    | nvarchar(255) |      | √    | 存储DoubleType类型的数据                            |
| LONG_         | LONG_      | nvarchar(255) |      | √    | 存储LongType类型的数据                              |
| TEXT_         | TEXT_      | datetime      |      | √    | 备注6                                               |
| TEXT2_        | TEXT2_     | datetime      |      | √    | 此处存储的是JPA持久化对象时，才会有值。此值为对象ID |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150416912.png)

### 11、act_id_group

> 用户组信息表( act_id_group )

#### 表结构说明 

| 字段名称 | 字段描述 | 数据类型      | 主键 | 为空 | 取值说明      |
| :------- | :------- | :------------ | :--- | :--- | :------------ |
| ID_      | ID_      | nvarchar(64)  | √    |      | 主键ID        |
| REV_     | 乐观锁   | int           |      | √    | 乐观锁Version |
| NAME_    | 名称     | nvarchar(255) |      | √    | 组名称        |
| TYPE_    | 类型     | nvarchar(255) |      | √    | 类型          |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150459464.png)

### 12、act_id_info

> 用户扩展信息表( act_id_info )

#### 表结构说明 

| 字段名称   | 字段描述 | 数据类型      | 主键 | 为空 | 取值说明      |
| :--------- | :------- | :------------ | :--- | :--- | :------------ |
| ID_        | ID_      | nvarchar(64)  | √    |      | 主键ID        |
| REV_       | 乐观锁   | int           |      | √    | 乐观锁Version |
| USER_ID_   | 用户ID   | nvarchar(64)  |      | √    |               |
| TYPE_      | 类型     | nvarchar(64)  |      | √    |               |
| KEY_       |          | nvarchar(255) |      | √    |               |
| VALUE_     |          | nvarchar(255) |      | √    |               |
| PASSWORD_  |          | Image         |      | √    |               |
| PARENT_ID_ |          | nvarchar(255) |      | √    |               |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/201812051505532.png)

### 13、act_id_membership

> 用户与分组对应信息表( act_id_membership )

用来保存用户的分组信息。

#### 表结构说明 

| 字段名称 | 字段描述 | 数据类型     | 主键 | 为空 | 取值说明 |
| :------- | :------- | :----------- | :--- | :--- | :------- |
| USER_ID  | 用户ID   | nvarchar(64) | √    |      |          |
| GROUP_ID | 用户组ID | nvarchar(64) | √    |      |          |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150639681.png)

### 14、act_id_user

> 用户信息表( act_id_user )

#### 表结构说明 

| 字段名称    | 字段描述 | 数据类型      | 主键 | 为空 | 取值说明      |
| :---------- | :------- | :------------ | :--- | :--- | :------------ |
| ID_         | ID_      | nvarchar(64)  | √    |      | 主键ID        |
| REV_        | 乐观锁   | int           |      | √    | 乐观锁Version |
| FIRST_      | 姓       | nvarchar(255) |      | √    |               |
| LAST_       | 名       | nvarchar(255) |      | √    |               |
| EMAIL_      | EMAIL_   | nvarchar(255) |      | √    |               |
| PWD_        | 密码     | nvarchar(255) |      | √    |               |
| PICTURE_ID_ | 图片ID   | nvarchar(64)  |      | √    |               |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150730938.png)

### 15、act_re_deployment

> 部署信息表( act_re_deployment )

部署流程定义时需要被持久化保存下来的信息。

#### 表结构说明 

| 字段名称     | 字段描述 | 数据类型      | 主键 | 为空 | 取值说明   |
| :----------- | :------- | :------------ | :--- | :--- | :--------- |
| ID_          | ID_      | nvarchar(64)  | √    |      | 主键ID     |
| NAME_        | 部署名称 | nvarchar(255) |      | √    | 部署文件名 |
| CATEGORY_    | 分类     | nvarchar(255) |      | √    | 类别       |
| DEPLOY_TIME_ | 部署时间 | datetime      |      | √    | 部署时间   |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2018120515081466.png)

### 16、act_re_model

> 流程设计模型部署表( act_re_model )

流程设计器设计流程后，保存数据到该表。

#### 表结构说明 

| 字段名称                      | 字段描述     | 数据类型      | 主键 | 为空 | 取值说明                     |
| :---------------------------- | :----------- | :------------ | :--- | :--- | :--------------------------- |
| ID_                           | ID_          | nvarchar(64)  | √    |      | ID_                          |
| REV_                          | 乐观锁       | int           |      | √    | 乐观锁                       |
| NAME_                         | 名称         | nvarchar(255) |      | √    | 名称                         |
| KEY_                          | KEY_         | nvarchar(255) |      | √    | 分类                         |
| CATEGORY_                     | 分类         | nvarchar(255) |      | √    | 分类                         |
| CREATE_TIME_                  | 创建时间     | datetime      |      | √    | 创建时间                     |
| LAST_UPDATE_TIME_             | 最新修改时间 | datetime      |      | √    | 最新修改时间                 |
| VERSION_                      | 版本         | int           |      | √    | 版本                         |
| META_INFO_                    | META_INFO_   | nvarchar(255) |      | √    | 以json格式保存流程定义的信息 |
| DEPLOYMENT_ID_                | 部署ID       | nvarchar(255) |      | √    | 部署ID                       |
| EDITOR_SOURCE_VALUE_ID_       |              | datetime      |      | √    |                              |
| EDITOR_SOURCE_EXTRA_VALUE_ID_ |              | datetime      |      | √    |                              |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150859704.png)

### 17、act_re_procdef

> 流程定义数据表( act_re_procdef )

业务流程定义数据表。此表和 ACT_RE_DEPLOYMENT 是多对一的关系，即，一个部署的bar包里可能包含多个流程定义文件，每个流程定义文件都会有一条记录在 ACT_REPROCDEF 表内，每个流程定义的数据，都会对于 ACT_GE_BYTEARRAY 表内的一个资源文件和 PNG 图片文件。和 ACT_GE_BYTEARRAY 的关联是通过程序用ACT_GE_BYTEARRAY.NAME 与 ACT_RE_PROCDEF.NAME 完成的，在数据库表结构中没有体现。

#### 表结构说明 

| 字段名称            | 字段描述                | 数据类型       | 主键 | 为空 | 取值说明                         |
| :------------------ | :---------------------- | :------------- | :--- | :--- | :------------------------------- |
| ID_                 | ID_                     | nvarchar(64)   | √    |      | ID_                              |
| REV_                | 乐观锁                  | int            |      | √    | 乐观锁                           |
| CATEGORY_           | 分类                    | nvarchar(255)  |      | √    | 流程定义的Namespace就是类别      |
| NAME_               | 名称                    | nvarchar(255)  |      | √    | 名称                             |
| KEY_                | 定义的KEY               | nvarchar(255)  |      |      | 流程定义ID                       |
| VERSION_            | 版本                    | int            |      |      | 版本                             |
| DEPLOYMENT_ID_      | 部署表ID                | nvarchar(64)   |      | √    | 部署表ID                         |
| RESOURCE_NAME_      | bpmn文件名称            | nvarchar(4000) |      | √    | 流程bpmn文件名称                 |
| DGRM_RESOURCE_NAME_ | png图片名称             | nvarchar(4000) |      | √    | 流程图片名称                     |
| DESCRIPTION_        | 描述                    | nvarchar(4000) |      | √    | 描述                             |
| HAS_START_FORM_KEY_ | 是否存在开始节点formKey | tinyint        |      | √    | start节点是否存在formKey 0否 1是 |
| SUSPENSION_STATE_   | 是否挂起                | tinyint        |      | √    | 1 激活 2挂起                     |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205150953305.png)

### 18、act_ru_event_subscr

> 事件订阅表（act_ru_event_subscr）

 事件订阅表。此表包含所有当前存在的事件订阅。它包括预期事件的类型，名称和配置，以及有关相应流程实例和执行的信息。

#### 表结构说明

| 字段名称       | 字段描述   | 数据类型      | 主键 | 为空 | 取值说明                               |
| :------------- | :--------- | :------------ | :--- | :--- | :------------------------------------- |
| ID_            | 事件ID     | nvarchar(64)  | √    |      | 事件ID                                 |
| REV_           | 版本       | int           |      | √    | 乐观锁Version                          |
| EVENT_TYPE_    | 事件类型   | nvarchar(255) |      |      | 事件类型                               |
| EVENT_NAME_    | 事件名称   | nvarchar(255) |      | √    | 事件名称                               |
| EXECUTION_ID_  | 执行实例ID | nvarchar(64)  |      | √    | 执行实例ID                             |
| PROC_INST_ID_  | 流程实例ID | nvarchar(64)  |      | √    | 流程实例ID                             |
| ACTIVITY_ID_   | 活动实例ID | nvarchar(64)  |      | √    | 活动实例ID                             |
| CONFIGURATION_ | 配置       | nvarchar(255) |      | √    | 配置                                   |
| CREATED_       | 是否创建   | datetime      |      |      | 默认值 当前系统时间戳CURRENT_TIMESTAMP |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151054461.png)

### 19、act_ru_execution

> 运行时流程执行实例表( act_ru_execution )

#### 表结构说明 

| 字段名称          | 字段描述        | 数据类型      | 主键 | 为空 | 取值说明                       |
| :---------------- | :-------------- | :------------ | :--- | :--- | :----------------------------- |
| ID_               | ID_             | nvarchar(64)  | √    |      | ID_                            |
| REV_              | 乐观锁          | int           |      | √    | 乐观锁                         |
| PROC_INST_ID_     | 流程实例ID      | nvarchar(64)  |      |      | 流程实例ID                     |
| BUSINESS_KEY_     | 业务主键ID      | nvarchar(255) |      | √    | 业务主键ID                     |
| PARENT_ID_        | 父节点实例ID    | nvarchar(64)  |      | √    | 父节点实例ID                   |
| PROC_DEF_ID_      | 流程定义ID      | nvarchar(64)  |      | √    | 流程定义ID                     |
| SUPER_EXEC_       | SUPER_EXEC_     | nvarchar(64)  |      | √    | SUPER_EXEC_                    |
| ACT_ID_           | 节点实例ID      | nvarchar(255) |      | √    | 节点实例ID即ACT_HI_ACTINST中ID |
| IS_ACTIVE_        | 是否存活        | tinyint       |      | √    | 是否存活                       |
| IS_CONCURRENT_    | 是否并行        | tinyint       |      | √    | 是否为并行(true/false）        |
| IS_SCOPE_         | IS_SCOPE_       | tinyint       |      | √    | IS_SCOPE_                      |
| IS_EVENT_SCOPE_   | IS_EVENT_SCOPE_ | tinyint       |      | √    | IS_EVENT_SCOPE_                |
| SUSPENSION_STATE_ | 是否挂起        | tinyint       |      | √    | 挂起状态 1激活 2挂起           |
| CACHED_ENT_STATE_ |                 | int           |      | √    |                                |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151146505.png)

### 20、act_ru_identitylink

> 运行时流程人员表( act_ru_identitylink )

任务参与者数据表。主要存储当前节点参与者的信息。

#### 表结构说明 

| 字段名称      | 字段描述   | 数据类型      | 主键 | 为空 | 取值说明   |
| :------------ | :--------- | :------------ | :--- | :--- | :--------- |
| ID_           | ID_        | nvarchar(64)  | √    |      | ID_        |
| REV_          | 乐观锁     | int           |      | √    | 乐观锁     |
| GROUP_ID_     | 组ID       | nvarchar(64)  |      | √    | 组ID       |
| TYPE_         | 类型       | nvarchar(255) |      | √    | 备注7      |
| USER_ID_      | 用户ID     | nvarchar(64)  |      | √    | 用户ID     |
| TASK_ID_      | 节点实例ID | nvarchar(64)  |      | √    | 节点实例ID |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)  |      | √    | 流程实例ID |
| PROC_DEF_ID_  | 流程定义ID | nvarchar(255) |      | √    | 流程定义ID |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151233547.png)

### 21、act_ru_job

> 运行时定时任务数据表( act_ru_job )

#### 表结构说明 

| 字段名称             | 字段描述     | 数据类型       | 主键 | 为空 | 取值说明     |
| :------------------- | :----------- | :------------- | :--- | :--- | :----------- |
| ID_                  | 标识         | nvarchar(64)   | √    |      | 标识         |
| REV_                 | 版本         | int            |      | √    | 版本         |
| TYPE_                | 类型         | nvarchar(255)  |      |      | 类型         |
| LOCK_EXP_TIME_       | 锁定释放时间 | datetime       |      | √    | 锁定释放时间 |
| LOCK_OWNER_          | 挂起者       | nvarchar(255)  |      | √    | 挂起者       |
| EXCLUSIVE_           |              | bit            |      | √    |              |
| EXECUTION_ID_        | 执行实例ID   | nvarchar(64)   |      | √    | 执行实例ID   |
| PROCESS_INSTANCE_ID_ | 流程实例ID   | nvarchar(64)   |      | √    | 流程实例ID   |
| PROC_DEF_ID_         | 流程定义ID   | nvarchar(64)   |      | √    | 流程定义ID   |
| RETRIES_             |              | int            |      | √    |              |
| EXCEPTION_STACK_ID_  | 异常信息ID   | nvarchar(64)   |      | √    | 异常信息ID   |
| EXCEPTION_MSG_       | 异常信息     | nvarchar(4000) |      | √    | 异常信息     |
| DUEDATE_             | 到期时间     | datetime       |      | √    | 到期时间     |
| REPEAT_              | 重复         | nvarchar(255)  |      | √    | 重复         |
| HANDLER_TYPE_        | 处理类型     | nvarchar(255)  |      | √    | 处理类型     |
| HANDLER_CFG_         |              | nvarchar(4000) |      | √    | 标识         |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151322147.png)

### 22、act_ru_task

> 运行时任务节点表( act_ru_task )

#### 表结构说明 

| 字段名称          | 字段描述       | 数据类型       | 主键 | 为空 | 取值说明                                     |
| :---------------- | :------------- | :------------- | :--- | :--- | :------------------------------------------- |
| ID_               | ID_            | nvarchar(64)   | √    |      | ID_                                          |
| REV_              | 乐观锁         | int            |      | √    | 乐观锁                                       |
| EXECUTION_ID_     | 执行实例ID     | nvarchar(64)   |      | √    | 执行实例ID                                   |
| PROC_INST_ID_     | 流程实例ID     | nvarchar(64)   |      | √    | 流程实例ID                                   |
| PROC_DEF_ID_      | 流程定义ID     | nvarchar(64)   |      | √    | 流程定义ID                                   |
| NAME_             | 节点定义名称   | nvarchar(255)  |      | √    | 节点定义名称                                 |
| PARENT_TASK_ID_   | 父节点实例ID   | nvarchar(64)   |      | √    | 父节点实例ID                                 |
| DESCRIPTION_      | 节点定义描述   | nvarchar(4000) |      | √    | 节点定义描述                                 |
| TASK_DEF_KEY_     | 节点定义的KEY  | nvarchar(255)  |      | √    | 任务定义的ID                                 |
| OWNER_            | 实际签收人     | nvarchar(255)  |      | √    | 拥有者（一般情况下为空，只有在委托时才有值） |
| ASSIGNEE_         | 签收人或委托人 | nvarchar(255)  |      | √    | 签收人或委托人                               |
| DELEGATION_       | 委托类型       | nvarchar(64)   |      | √    | 备注8                                        |
| PRIORITY_         | 优先级别       | int            |      | √    | 优先级别，默认为：50                         |
| CREATE_TIME_      | 创建时间       | datetime       |      | √    | 创建时间                                     |
| DUE_DATE_         | 过期时间       | datetime       |      | √    | 耗时                                         |
| SUSPENSION_STATE_ | 是否挂起       | int            |      | √    | 1代表激活 2代表挂起                          |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151415778.png)

### 23、act_ru_variable

> 运行时流程变量数据表( act_ru_variable )

#### 表结构说明 

| 字段名称      | 字段描述   | 数据类型       | 主键 | 为空 | 取值说明                                                     |
| :------------ | :--------- | :------------- | :--- | :--- | :----------------------------------------------------------- |
| ID_           | ID_        | nvarchar(64)   | √    |      | 主键标识                                                     |
| REV_          | 乐观锁     | int            |      | √    | 乐观锁                                                       |
| TYPE_         | 类型       | nvarchar(255)  |      |      | 备注9                                                        |
| NAME_         | 名称       | nvarchar(255)  |      |      | 变量名称                                                     |
| EXECUTION_ID_ | 执行实例ID | nvarchar(64)   |      | √    | 执行的ID                                                     |
| PROC_INST_ID_ | 流程实例ID | nvarchar(64)   |      | √    | 流程实例ID                                                   |
| TASK_ID_      | 节点实例ID | nvarchar(64)   |      | √    | 节点实例ID(Local）                                           |
| BYTEARRAY_ID_ | 字节表ID   | nvarchar(64)   |      | √    | 字节表的ID（ACT_GE_BYTEARRAY）                               |
| DOUBLE_       | DOUBLE_    | float          |      | √    | 存储变量类型为Double                                         |
| LONG_         | LONG_      | numeric(19)    |      | √    | 存储变量类型为long                                           |
| TEXT_         | TEXT_      | nvarchar(4000) |      | √    | ‘存储变量值类型为String 如此处存储持久化对象时，值jpa对象的class |
| TEXT2_        | TEXT2_     | nvarchar(4000) |      | √    | 此处存储的是JPA持久化对象时，才会有值。此值为对象ID          |

#### 索引说明

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20181205151505312.png)







# Git 

## 常用指令



### 创建分支

> 1. 进入到你要进行分支创建的项目，鼠标右键点击，选择Git Bash Here,打开git 命令操作窗口
> 2. 使用命令**git checkout master**,切换到master分支
> 3. 使用命令**git pull**拉取master代码，保证本地master分支的代码是最新的
> 4. 使用命令**git checkout -b test**,创建test分支，并切换到test分支
> 5. 使用命令**git push origin test**,将test分支推到远程仓库
> 6. 将test本地分支和远程test分支进行关联：**git branch --set-upstream-to=origin/test**,如果不进行这个操作的话，git pull会出现以下提示：![在这里插入图片描述](https://img-blog.csdnimg.cn/1856d55874bd4f8fba6ce168462a1c4e.png)
> 7. 拉取 git pull ，拉取成功







> **复制一个已创建的仓库:**
>
> $ git clone [ssh](https://so.csdn.net/so/search?q=ssh&spm=1001.2101.3001.7020)://haorooms@domain.com/blog.git
>
> **创建一个新的本地仓库:**
>
> $ git init
>
> **本地修改**
>
> **显示工作路径下已修改的文件：**
>
> $ git status
>
> **显示与上次提交版本文件的不同：**
>
> $ git diff
>
> **把当前所有修改添加到下次提交中：**
>
> $ git add .
>
> **把对某个文件的修改添加到下次提交中：**
>
> $ git add -p <file>
>
> **提交本地的所有修改：**
>
> $ git commit -a
>
> **提交之前已标记的变化：**
>
> $ git commit
>
> **附加消息提交：**
>
> $ git commit -m 'message here'
>
> **提交，并将提交时间设置为之前的某个日期:**
>
> git commit --date="`date --date='n day ago'`" -am "Commit Message"
>
> **修改上次提交：请勿修改已发布的提交记录!**
>
> $ git commit --amend
>
> **把当前分支中未提交的修改移动到其他分支**
>
> git stash
> git checkout branch2
> git stash pop
>
> 搜索
>
> **从当前目录的所有文件中查找文本内容：**
>
> $ git grep "Hello"
>
> **在某一版本中搜索文本：**
>
> $ git grep "Hello" v2.5
>
> **提交历史**
>
> **从\**\*提交开始，显示所有的提交记录（显示hash， 作者信息，提交的标题和时间）：**
>
> $ git log
>
> **显示所有提交（仅显示提交的hash和message）**：
>
> $ git log --oneline
>
> **显示某个用户的所有提交：**
>
> $ git log --author="username"
>
> **显示某个文件的所有修改：**
>
> $ git log -p <file>
>
> **谁，在什么时间，修改了文件的什么内容：**
>
> $ git blame <file>
>
> **分支与标签**
>
> **列出所有的分支：**
>
> $ git branch
>
> **切换分支：**
>
> $ git checkout <branch>
>
> **创建并切换到新分支:**
>
> $ git checkout -b <branch>
>
> **基于当前分支创建新分支：**
>
> $ git branch <new-branch>
>
> **基于远程分支创建新的可追溯的分支：**
>
> $ git branch --track <new-branch> <remote-branch>
>
> **删除本地分支:**
>
> $ git branch -d <branch>
>
> **给当前版本打标签：**
>
> $ git tag <tag-name>
>
> **更新与发布**
>
> **列出当前配置的远程端：**
>
> $ git remote -v
>
> **显示远程端的信息：**
>
> $ git remote show <remote>
>
> **添加新的远程端：**
>
> $ git remote add <remote> <url>
>
> **下载远程端版本，但不合并到HEAD中：**
>
> $ git fetch <remote>
>
> **下载远程端版本，并自动与HEAD版本合并：**
>
> $ git remote pull <remote> <url>
>
> **将远程端版本合并到本地版本中：**
>
> $ git pull origin master
>
> **将本地版本发布到远程端**：
>
> $ git push remote <remote> <branch>
>
> **删除远程端分支：**
>
> $ git push <remote> :<branch> (since Git v1.5.0)
> 或
> git push <remote> --delete <branch> (since Git v1.7.0)
>
> **发布标签:**
>
> $ git push --tags
>
> **合并与重置**
>
> **将分支合并到当前HEAD中：**
>
> $ git merge <branch>
>
> **将当前HEAD版本重置到分支中:请勿重置已发布的提交!**
>
> $ git rebase <branch>
>
> **退出重置:**
>
> $ git rebase --abort
>
> **解决冲突后继续重置：**
>
> $ git rebase --continue
>
> **使用配置好的merge tool 解决冲突：**
>
> $ git mergetool
>
> **在编辑器中手动解决冲突后，标记文件为已解决冲突**
>
> $ git add <resolved-file>
> $ git rm <resolved-file>
>
> **撤销**
>
> **放弃工作目录下的所有修改：**
>
> $ git reset --hard HEAD
>
> **移除缓存区的所有文件（i.e. 撤销上次git add）:**
>
> $ git reset HEAD
>
> **放弃某个文件的所有本地修改：**
>
> $ git checkout HEAD <file>
>
> **重置一个提交（通过创建一个截然不同的新提交）**
>
> $ git revert <commit>
>
> **将HEAD重置到指定的版本，并抛弃该版本之后的所有修改：**
>
> $ git reset --hard <commit>
>
> **将HEAD重置到上一次提交的版本，并将之后的修改标记为未添加到缓存区的修改：**
>
> $ git reset <commit>
>
> **将HEAD重置到上一次提交的版本，并保留未提交的本地修改：**
>
> $ git reset --keep <commit>
>
> git submodule的使用
>
> **开发过程中，经常会有一些通用的部分希望抽取出来做成一个公共库来提供给别的工程来使用，这样就用到了git的git submodule命令。
> 添加**
>
> **为当前工程添加submodule，命令如下：**
>
> git submodule add 仓库地址 路径
>
> **例如：**
>
> git submodule add helloworld.git
> git commit -m "Add submodules helloworld.git"
>
> **其他人协同**
>
> git clone /path/to/repos/helloworld_parent.git
> git submodule init
> git submodule update
>
> **移除**
>
> 1.删除git cache和物理文件夹
>
> 2.删除.gitmodules的内容（或者整个文件） 因为本例只有两个子模块，直接删除文件
>
> 3.删除.git/config的submodule配置 源文件





# 开发工具

## IDEA



### 1、IDEA 快捷键



> Ctrl+Z：撤销
>
> Ctrl+Shift+Z：重做
>
> Ctrl+X：剪贴
>
> Ctrl+C：复制
>
> Ctrl+V：粘贴
>
> Ctrl+Y：删除当前行
>
> Ctrl+D:复制当前行
>
> Ctrl+Shift+J：将选中的行合并成一行
>
> Ctrl+N：查找类文件
>
> Ctrl+Shift+N：查找文件
>
> Ctrl+Shift+F: 查找字符串
>
> Ctrl+G：定位到文件某一行
>
> Alt+向左箭头：返回上次光标位置
>
> Alt+向右箭头：返回至后一次光标位置
>
> Ctrl+Shift+Backspace：返回上次编辑位置
>
> Ctrl+Shift+反斜杠：返回后一次编辑位置
>
> Ctrl+B：定位至变量定义的位置
>
> Ctrl+Alt+B：定位至选中类或者方法的具体实现
>
> Ctrl+Shift+B:直接定位至光标所在变量的类型定义
>
> Ctrl+U：直接定位至当前方法override或者implements的方法定义处
>
> Ctrl+F12：显示当前文件的文件结构
>
> Ctrl+Alt+F12：显示当前文件的路径，并可以方便的将相关父路径打开
>
> Ctrl+H：显示当前类的继承层次
>
> Ctrl+Shift+H：显示当前方法的继承层次
>
> Ctrl+Alt+H：显示当前方法的调用层次
>
> F2：定位至下一个错误处
>
> Shift+F2：定位至前一个错误处
>
> Ctrl+Alt+向上箭头：查找前一个变量共现的地方
>
> Ctrl+Alt+向下箭头：查找下一个变量共现的地方
>
> Ctrl+=：展开代码
>
> Ctrl+-：收缩代码
>
> Ctrl+Alt+=：递归展开代码
>
> Ctrl+Alt+-：递归收缩代码
>
> Ctrl+Shift+=：展开所有代码
>
> Ctrl+Shift+-：收缩所有代码
>
> Ctrl+Shitft+向下箭头：将光标所在的代码块向下整体移动
>
> Ctrl+Shift+向上箭头：将光标所在的代码块向上整体移动
>
> Ctrl+Alt+Shift+向左箭头：将元素向左移动
>
> Ctrl+Alt+Shift+向右箭头：将元素向右移动
>
> Alt+Shift+向下箭头：将行向下移动
>
> Alt+Shift+向上箭头：将行向上移动
>
> Ctrl+F：在当前文件中查找
>
> Ctrl+R：替换字符串
>
> Ctrl+Shift+F:在全局文件中查找字符串
>
> Ctrl+Shift+R：在全局中替换字符串
>
> Alt+F7：查找当前变量的使用，并列表显示
>
> Ctrl+Alt+F7：查找当前变量的使用，并直接对话框提示
>
> Ctrl+F7：在文件中查找符号的使用
>
> Ctrl+Shift+F7：在文件中高亮显示变量的使用
>
> Ctrl+O：重写基类方法
>
> Ctrl+I：实现基类或接口中的方法
>
> Alt+Insert：产生构造方法，get/set方法等
>
> Ctrl+Alt+T：将选中的代码使用if，while，try/catch等包装
>
> Ctrl+Shitf+Delete：去除相关的包装代码
>
> Alt+/：自动完成
>
> Alt+Enter：自动提示完成，抛出异常
>
> Ctrl+J：插入Live Template 快速插入一行或者多行代码
>
> Ctrl+Alt+J：使用Live Template包装
>
> Ctrl+/：使用//注释
>
> Ctrl+Shift+/：使用/**/注释
>
> Ctrl+Alt+L：格式化代码
>
> Ctrl+Alt+I：自动缩进行
>
> Ctrl+Alt+O：优化import
>
> Ctrl+]：快速跳转至诸如{}围起来的代码块的结尾处
>
> Ctrl+[：快速跳转至诸如{}围起来的代码块的开头处
>
> Ctrl+Shift+Enter：将输入的if，for，函数等等补上{}或者；使代码语句完整
>
> Shift+Enter：在当前行的下方开始新行
>
> Ctrl+Alt+Enter：在当前行的上方插入新行
>
> Ctrl+Delete：删除光标所在至单词结尾处的所有字符
>
> Ctrl+Backspace：删除光标所在至单词开头处的所有字符
>
> Ctrl+向左箭头：将光标移至前一个单词
>
> Ctrl+向右箭头：将光标移至后一个单词
>
> Ctrl+向上箭头：向上滚动一行
>
> Ctrl+向下箭头：向下滚动一行
>
> Ctrl+W：选中整个单词
>
> Ctrl+Shift+U：切换大小写
>
> Shift+F6：重命名
>
> Ctrl+F6：更改函数签名
>
> Ctrl+Shift+F6：更改类型

### 2、IDEA 插件

```
Key Promoter X   //快捷键提示
```

#### JRebel插件

##### 简介

JRebel是一套JavaEE开发工具。
Jrebel 可快速实现[热部署](https://so.csdn.net/so/search?q=热部署&spm=1001.2101.3001.7020)，节省了大量重启时间，提高了个人开发效率。
JRebel是一款JAVA[虚拟机](https://so.csdn.net/so/search?q=虚拟机&spm=1001.2101.3001.7020)插件，它使得JAVA程序员能在不进行重部署的情况下，即时看到代码的改变对一个应用程序带来的影响。JRebel使你能即时分别看到代码、类和资源的变化，你可以一个个地上传而不是一次性全部部署。当程序员在开发环境中对任何一个类或者资源作出修改的时候，这个变化会直接反应在部署好的应用程序上，从而跳过了构建和部署的过程，可以省去大量的部署用的时间。

JRebel是一款JVM插件，它使得Java代码修改后不用重启系统，立即生效。
IDEA上原生是不支持热部署的，一般更新了 Java 文件后要手动重启 Tomcat 服务器，才能生效，浪费时间浪费生命。
目前对于idea热部署最好的解决方案就是安装JRebel插件。

##### 安装

###### 第一步：安装插件

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200409112609450.png)

###### 第二步：在线GUID地址：在线生成GUID

网址：[在线GUID地址](https://www.guidgen.com/)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200409112021533.png)
如果失效刷新GUID替换就可以！

服务器地址：https://jrebel.qekang.com/`{GUID}`

###### 第三步：打开jrebel 如下所示面板，选择Connect to online licensing service

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200409112115162.png)
安装成功之后就可以通过JRebel启动项目。这样修改完Java代码后，就可以通过快捷键 **Ctrl+shift+F9** 而不再需要重启站点这样繁琐浪费时间的操作了。

##### 激活

1. [下载](https://link.zhihu.com/?target=https%3A//github.com/ilanyu/ReverseProxy/releases/tag/v1.4)反向代理软件
   根据自己的系统下载对应版本，window系统需要下载ReverseProxy_windows_amd64.exe这个版本，下载地址：[https://github.com/ilanyu/ReverseProxy/releases/tag/v1.4](https://link.zhihu.com/?target=https%3A//github.com/ilanyu/ReverseProxy/releases/tag/v1.4)
   个人分析地址：https://www.aliyundrive.com/s/pPeZxmUuZgw 提取码：ay24
   下载后运行如图所示（注意：激活成功前不要关闭反向代理程序）

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/v2-a2568c2c5dfe6b7aef1156b6d8aa3fd6_720w.jpg)



1. 激活JReable
   在idea中如下步骤点击：File ——> Setting... ——> JRebel ——> Activate now

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/v2-4f4519851c5c66da1dbfb43c31a7e96c_720w.jpg)


打开激活窗口，需要填入license的地址和邮箱，后面的邮箱可以随便写一个，license地址则需要使用我们刚才开启的工具上显示的地址：[http://127.0.0.1:8888](https://link.zhihu.com/?target=http%3A//127.0.0.1%3A8888/)，而且地址必须跟一个参数（必须要写用UUID或者GUID，而UUID这种重复的几率非常低。在线GUID地址：[https://www.guidgen.com/](https://link.zhihu.com/?target=https%3A//www.guidgen.com/)），如下图所示：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/v2-d2de2d16e20fc923345419f76c7aa3cb_720w.jpg)



然后点击Activate JRebel就可以激活了。激活成功后点击`Work offline`切换到离线状态。



![img](https://pic3.zhimg.com/80/v2-7867fcde8cab029060a26ce91d27607e_720w.jpg)

离线状态：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/v2-07bbea118de3bd0d9ef62b5d1cdf4983_720w.jpg)



##### 相关设置

###### 设置成离线工作模式

![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200420155002339.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200420155028133.png)

###### 设置自动编译



要想实现热部署，首先需要对Intellij按如下进行设置：

1. 由于JRebel是实时监控class文件的变化来实现热部署的，所以在idea环境下需要打开自动变异功能才能实现随时修改，随时生效。
   ![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2020041513244069.png)
2. 打开运行时编译

###### 设置compiler.automake.allow.when.app.running



快捷键ctrl+shift+A，搜索：registry
或者
按快捷键 Ctrl+Shift+Alt+/ ，选择 Registry
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/2020041513252896.png)
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200420155538713.png)

##### 使用

运行项目时要点击图中红框中的按钮，即可运行：
![在这里插入图片描述](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/20200420155740214.png)
第一个按钮是Run，第二个按钮是Debug。

修改代码（只测试了Java代码的修改）后，按快捷键 **Ctrl + Shift + F9**，运行后会提示有变化是否重新加载，选yes。完成加载以后，就已经实现了热更新效果。

##### 问题

###### **没有compiler.automake.allow.when.app.running**

很多文章介绍IntelliJ IDEA开启热部署功能都会写到在IntelliJ IDEA中的注册表中开启compiler.automake.allow.when.app.running选项，此选项在IntelliJ IDEA 2021.2之后的版本迁移到高级设置中。如下图所示：

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202109260837404.png)

　　如果你安装了中文语言包，那么它在这里

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/202109260837405.jpg)

**开启项目热部署**

![image-20220511155552411](https://aixz-imges.oss-cn-beijing.aliyuncs.com//typora_imges/image-20220511155552411.png)



### 3、IDEA 破解



最新版IDEA下载地址：[Download IntelliJ IDEA: The Capable & Ergonomic Java IDE by JetBrains 139](https://www.jetbrains.com/idea/download/#section=windows)

选择 `Ultimate` 版本



[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/911f543397ea2b38f85b92b89e0245e3d24faf66_2_690x277.png)image1140×458 54.8 KB](https://springboot.io/uploads/default/original/2X/9/911f543397ea2b38f85b92b89e0245e3d24faf66.png)



下载完成后，直接安装，如果有旧版本会先执行卸载。

安装完毕后，不用急着打开。

#### 下载 ja-netfilter

下载地址：[ja-netfilter-all.zip 219](https://jetbra.in/files/ja-netfilter-all-9d07348e9326795c9f3344c8f93b25f670f6f7af.zip)

> 如果 下载地址失效，那么可以点击 [https://jetbra.in/s 1.3k](https://jetbra.in/s) 获取最新的下载地址。
>
> 
>
> [![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/7cd2c1b34afaf0bc7be6f344156b5827f1ee62bc_2_690x134.png)image1563×305 25.7 KB](https://springboot.io/uploads/default/original/2X/7/7cd2c1b34afaf0bc7be6f344156b5827f1ee62bc.png)

解压到任意目录，注意，不要带中文



[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/fe3a561ffed8cf5cfe26751870a99a703bc561de.png)image760×446 20.6 KB](https://springboot.io/uploads/default/original/2X/f/fe3a561ffed8cf5cfe26751870a99a703bc561de.png)



#### 编辑 idea64.exe.vmoptions 配置文件

路径一般为

```
D:\Program Files\JetBrains\IntelliJ IDEA 2021.3\bin
```

修改`idea64.exe.vmoptions`

使用文本编辑打开，在最后添加一行配置，指向上一步解压出来的：`ja-netfilter.jar` 文件



[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/cfd186cfe84a99c5d6f8f9c45bb4c4894723a5ed.png)image874×162 3.72 KB](https://springboot.io/uploads/default/original/2X/c/cfd186cfe84a99c5d6f8f9c45bb4c4894723a5ed.png)



------

例如：

```java
-javaagent:D:\Program Files\JetBrains\ja-netfilter\ja-netfilter.jar=jetbrains
```

#### 画重点



新版本已经没有 janf_config.txt了 它 更新到 config 中去了 每个插件都是单独的 conf
![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/f48f54f7711305540f8d227ab188d3bbda547d06.png)

```
# jb 的关键 配置文件内容 

[DNS]
EQUAL,jetbrains.com

[URL]
PREFIX,https://account.jetbrains.com/lservice/rpc/validateKey.action

[MyMap]
EQUAL,licenseeName->baipiaozhenxiang
EQUAL,gracePeriodDays->100000
EQUAL,paidUpTo->2025-10-13
```

> [DNS] 和[URL] 是关键， 用来阻断jb检测我们code是否过期

> [MyMap] 底下的 licenseeName 可以自己修改喜欢的 paidUpTo 可以修改任意的时间改为2099也不是不可以

#### 新版增加的power插件来抗衡官方的反制

具体可以查看`config-jetbrains` 中的power.conf

#### 启动IDEA

填入[热心大佬 1.3k](https://jetbra.in/s)的key完事



[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/e8f29bc888a0f396b63a6b1e276d159ddad21b1b_2_690x396.png)image840×483 37.8 KB](https://springboot.io/uploads/default/original/2X/e/e8f29bc888a0f396b63a6b1e276d159ddad21b1b.png)





[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/c0e9eccb4b5357a2d894e2c43934e75c8b05a7d7_2_690x388.png)image926×521 69.8 KB](https://springboot.io/uploads/default/original/2X/c/c0e9eccb4b5357a2d894e2c43934e75c8b05a7d7.png)



大功告成

#### 可能出现的问题

如果按照上面的步骤最后无法启动，请检查一下`idea64.exe.vmoptions` ，如果除了你自己添加的配置，还有另外的类似`-javaagent:C:\Users\Public\.jetbrains\xxxx`的配置，在前面加 `#` 屏蔽掉该配置，可能是由于试用版本而生成的配置。

或者 `C:\Users\{你的电脑用户}\AppData\Roaming\JetBrains\IntelliJIdea2021.3 `底下的`idea64.exe.vmoptions `也增加一下`-javaagent:` 配置

#### 关于 一些插件的破解

这次这个ja-netfilter 可以说非常给力 ！
举个例子 MCHPro 是一款非常好用mybatis插件！只要是上架到jb市场的插件
使用这次的这个`ja-netfilter`就能轻松拿下。



[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/09a6a70fc07f5b79b0c0665887fac72e7f52d2d8_2_613x500.png)image900×733 89.6 KB](https://springboot.io/uploads/default/original/2X/0/09a6a70fc07f5b79b0c0665887fac72e7f52d2d8.png)



注意要有Marketplace 。

如法炮制

[![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/e92f4b62938002a13429f7660ad05fd54896cd35_2_690x333.png)image935×452 38.6 KB](https://springboot.io/uploads/default/original/2X/e/e92f4b62938002a13429f7660ad05fd54896cd35.png)



打开热心大佬的站点 搜索到对应的插件复制code
![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/67ebea197145ac6e98dc585f3fc827b72f413981.png)
完成
![image](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/b93b9326c5949510d0c4b1612dfde4dd3750e000.png)

#### 注意事项

如果你的插件在 Help → register… 里面没找到说明这个插件并未上架jb的市场





### 4、类和方法自动添加注释



#### 类和方法自动添加注释



> File-->Settings-->Editor-->File and Code Templates

```ruby
/**



*@author:xxxxx



*@create: ${YEAR}-${MONTH}-${DAY} ${HOUR}:${MINUTE}



*@Description: ${description}



*/
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/ac7efe628b0347168918f22486626c76.png)

此时，当你创建新的类时，就会自动生成注释了。



#### 2、为方法添加自动注释模版



> File-->Settings-->Editor-->Live Templates

点击"+"号后，选择"Templates Group…" 

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/da3bbd5fd6a34684aa947d4e57730aa0.png) 

下图的第一步就是上图所示部分，建好第一步选中新建的Method，进行第二步设置。

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/16b19d274f184252a321d223759db900.png) 

第五步：

```ruby
/**



*@Param: $params$



*@return: $return$



*@Author: xxxx



*@date: $date$



*/
```

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/1f140a6bae4e479abea17d426686f552.png)

以上设置完成就可以了。 



### 问题

​		

##### idea导入项目后没有被识别为maven项目



>1、首先点击工具栏最左边的 Help 再点击 Find Action ；或者使用快捷键 Ctrl+Shift+A
>
>2、接着在输入框中输入 maven projects ，会弹出一个 Add Maven Projects 选项，点击即可
>
>3、最后，选择本项目的 pom.xml 点击 OK 即可解决！



## PicGo



[下载地址和文档](https://molunerfinn.com/PicGo/)

### 1、Typora设置

![image-20220324152531914](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220324152531914.png)

### 2、PicGo 阿里OOS设置

![image-20220324152635400](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/image-20220324152635400.png)

> LTAI5t7EenN958zduqZN16hf
> 0MOPaVF3kFmN5bywtdXCy7WGIa1J7F
>
> aixz-imges
>
> oss-cn-beijing
>
> typora_imges/
>
> https://aixz-imges.oss-cn-beijing.aliyuncs.com

## Typora



### 1、常用快捷键

[使用快捷教程](https://www.cnblogs.com/hongdada/p/9776547.html)

![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/443934-20181012170159282-378811511.png)


![img](https://aixz-imges.oss-cn-beijing.aliyuncs.com/typora_imges/443934-20181012170211920-1988294604.png)



### 2、插入区块



> 1、在文中需要编辑的位置，输入">"符号
> 2、接着在符号“>”后面，加一个空格

## Chrome浏览器

### 问题

#### 不安全的链接

> 页面空白处键盘敲
> 							thisisunsafe



## Cmd

### 操作

#### 根据端口号查看占用进程

##### 1.列出所有端口的情况

```sh
netstat -aon
```

##### 2.查询被占用的端口号(port)

```sh
netstat -aon|findstr "port"
```

##### 3.查找某个pid对应的进程名称

```sh
tasklist|findstr "port"
```

##### 4.结束某个pid对应的进程

```sh
taskkill /pid "pid"
```

**强制杀掉进程**

```sh
taskkill /pid "pid" /F
```



### 问题

#### 运行jar文件，中文乱码

> 默认字符集为GBK 需要修改为UTF-8

```sh'
chcp 65001
```

