## 爬虫

### 问题

#### 使用execjs调用就是方法出现错误

> AttributeError: 'NoneType' object has no attribute 'replace'

#### 解决方法：

根据错误提示，找到D:\python\Lib\subprocess.py文件，点击即可打开
![在这里插入图片描述](https://img-blog.csdnimg.cn/d4f35917a66049258985480cba6a30ff.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/17340d3a50f04441932163755655fc2a.png)然后找到文件中的对应的位置，**将这块的encoding=None 改为 encoding=‘utf-8’** 即可。



## 常用用法

### print 不换行

#### 3.X

在 Python 3.x 中，我们可以在 **print()** 函数中添加 **end=""** 参数，这样就可以实现不换行效果。

在 Python3 中， print 函数的参数 **`end`** 默认值为 **"\n"**，即**end="\n"**，表示换行，给 **`end`** 赋值为空, 即**end=""**，就不会换行了，例如：

```py
print('这是字符串，', end="")
print('这里的字符串不会另起一行')
```

**end=""** 可以设置一些特色符号或字符串：

```py
print('12345', end=" ")  # 设置空格
print('6789')

print('admin', end="@")  # 设置符号
print('runoob.com')

print('Google ', end="Runoob ")  # 设置字符串
print('Taobao')
```

#### 2.X

在 Python 2.x中， 可以使用逗号 **,** 来实现不换行效果：

```python
x= 2
print "数字为：", x
```

#### Python2.x 与 Python3.x 兼容模式

如果 Python2.x 版本想使用 Python3.x 的 **print** 函数，可以导入 **__future__** 包，该包禁用 Python2.x 的 print 语句，采用 Python3.x 的 **print** 函数。

以下代码在 Python2.x 与 Python3.x 都能正确执行：

```python
from __future__ import print_function

print('12345', end=" ")  # 设置空格
print('6789')

print('admin', end="@")  # 设置符号
print('runoob.com')

print('Google ', end="Runoob ")  # 设置字符串
print('Taobao')
```

**注：**Python3.x 与 Python2.x 的许多兼容性设计的功能可以通过 **__future__** 这个包来导入。

### 遍历技巧

在字典中遍历时，关键字和对应的值可以使用 items() 方法同时解读出来：

```py
knights = {'gallahad': 'the pure', 'robin': 'the brave'}
for k,v in knights.items():
    print(k,v)

#gallahad the pure
#robin the brave
```

在序列中遍历时，索引位置和对应值可以使用 enumerate() 函数同时得到：

```py
lis =['tic', 'tac', 'toe']
for i,v in enumerate(lis):
    print(i,v)
    
#0 tic
#1 tac
#2 toe
```

同时遍历两个或更多的序列，可以使用 zip() 组合：

```py
questions = ['name', 'quest', 'favorite color']
answers = ['lancelot', 'the holy grail', 'blue']
for q, a in zip(questions, answers):
    print('What is your {0}?  It is {1}.'.format(q, a))

#What is your name?  It is lancelot.
#What is your quest?  It is the holy grail.
#What is your favorite color?  It is blue.
```

要反向遍历一个序列，首先指定这个序列，然后调用 reversed() 函数：

```python
for i in reversed(range(0,10,2)):
    print(i,end=" ")
    
# 8 6 4 2 0
```

要按顺序遍历一个序列，使用 sorted() 函数返回一个已排序的序列，并不修改原值：

```python
basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
for f in sorted(set(basket)):
    print(f, end="\t")
    
# apple	banana	orange	pear
```



